package geometry;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import geometry.math.Vector4dTest;
import geometry.rawgeometry.ObjDataTest;
import geometry.rawgeometry.ObjFileLoaderTest;
import geometry.spacepartition.BoxTest;
import geometry.spacepartition.CullerEdgeTest;
import geometry.spacepartition.OctreeChildTest;
import geometry.spacepartition.QuadtreeChildTest;
import geometry.spacepartition.SphereTest;

@RunWith(Suite.class)
@SuiteClasses({
	//math
	Vector4dTest.class,
	
	//spacepartition
	BoxTest.class,
	CullerEdgeTest.class,
	QuadtreeChildTest.class,
	OctreeChildTest.class,
	SphereTest.class,
	
	//rawgeometry
	ObjDataTest.class,
	ObjFileLoaderTest.class
})

public class AllTests {

}
