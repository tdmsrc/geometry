package geometry.rawgeometry;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData;
import geometry.rawgeometry.ObjFactory;
import geometry.rawgeometry.ObjData.VertexData;

public class ObjDataTest {

	@Test
	public void testObjDataCollectionOfObjData() {
		
		ArrayList<ObjData> boxes = new ArrayList<ObjData>();
		boxes.add( ObjFactory.box(new Vector3d(0.0f, 0.0f, 0.0f), new Vector3d(1.0f, 1.0f, 1.0f)) );
		boxes.add( ObjFactory.box(new Vector3d(0.0f, 0.0f, 0.0f), new Vector3d(1.0f, 1.0f, 1.0f)) );
		boxes.add( ObjFactory.box(new Vector3d(0.0f, 0.0f, 0.0f), new Vector3d(1.0f, 1.0f, 1.0f)) );
		
		ObjData od = new ObjData(boxes);
		Assert.assertEquals(18, od.faces.size());
		Assert.assertEquals(24, od.position.length);
	}
	
	@Test
	public void testAttributeList() {
		VertexData[] vd = ObjData.attributeList(1, 2, 3, 4, 5, 6, 7, 8, 9);
		
		Assert.assertEquals(3, vd.length);
		Assert.assertEquals(4, vd[1].posIndex);
		Assert.assertEquals(5, vd[1].texIndex);
		Assert.assertEquals(6, vd[1].normalIndex);
	}

	@Test
	public void testAttributeListPosOnly() {
		VertexData[] vd = ObjData.attributeListPosOnly(1, 4, 7);
		
		Assert.assertEquals(3, vd.length);
		Assert.assertEquals(1, vd[0].posIndex);
		Assert.assertEquals(4, vd[1].posIndex);
		Assert.assertEquals(7, vd[2].posIndex);
	}

}
