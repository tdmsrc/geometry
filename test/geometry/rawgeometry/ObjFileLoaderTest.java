package geometry.rawgeometry;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import geometry.rawgeometry.ObjData;
import geometry.rawgeometry.ObjFileLoader;

public class ObjFileLoaderTest {
	
	@Test
	public void testReadObjFileVertices() {
		//TODO: ideally, would also test OBJ files with only position, position/tex, position/normal
		
		//cube: 8 positions, 14 tex coords, 6 normals, 12 faces
		InputStream is = ObjFileLoaderTest.class.getResourceAsStream("/resource/cube.obj");
		if(is == null){
			Assert.fail("Could not find test OBJ file");
		}
		
		try{ 
			ObjData od = ObjFileLoader.readObjFileVertices(is, 1.0f);
			
			Assert.assertEquals(12, od.faces.size());
			Assert.assertEquals(8, od.position.length);
			Assert.assertEquals(14, od.tex.length);
			Assert.assertEquals(6, od.normal.length);
			Assert.assertTrue(od.hasTex);
			Assert.assertTrue(od.hasNormal);
		}catch (IOException e){ 
			Assert.fail(e.getMessage()); 
		}
	}
}
