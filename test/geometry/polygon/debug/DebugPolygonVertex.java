package geometry.polygon.debug;

import geometry.math.Vector2d;
import geometry.polygon.components.PolygonVertex;

public class DebugPolygonVertex extends PolygonVertex<DebugPolygonVertex, DebugPolygonEdge>{

	public DebugPolygonVertex(Vector2d v) {
		super(v);
	}

	@Override
	protected DebugPolygonVertex getThisVertex(){ return this; }
}
