package geometry.polygon.debug;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import geometry.common.components.Edge2d;
import geometry.common.components.Vertex2d;
import geometry.math.Vector2d;
import geometry.polygon.components.Polygon;
import geometry.polygon.components.PolygonTools;
import geometry.polygon.components.PolygonTools.ClosestObjectData;
import geometry.polygon.intersect.IntervalSubset;
import geometry.polygon.intersect.PolygonEdgeIntersection;
import geometry.polygon.intersect.IntervalSubset.IntervalTraversalMethod;
import geometry.polygon.triangulate.EdgeLoopPolygon;
import geometry.polygon.triangulate.Triangle2d;

public class PolygonDebug extends JFrame implements MouseListener, MouseMotionListener {
	
	private static final long serialVersionUID = 1L;
	
	//COLOR OPTIONS
	//----------------------------------
	protected static final Color COLOR_BACKGROUND = new Color(255,255,255);
	protected static final Color COLOR_CURSOR = new Color(0,0,0);
	protected static final Color COLOR_CLOSEST_POINT = new Color(0,0,0);
	
	protected static final Color COLOR_SEL = new Color(0,255,0);
	protected static final Color COLOR_UNSEL = new Color(0,128,0);
	
	protected static final Color COLOR_TRI_BOUNDARY = new Color(0,0,0);
	
	protected static final Color COLOR_CENTROID = new Color(128,128,255);
	//----------------------------------
	
	protected Vector2d screenDimensions;
	protected Vector2d camMin, camMax;
	protected Vector2d cursor;
	
	protected Image backbuffer;
	protected Graphics graphics;
	
	protected Polygon<DebugPolygonVertex,DebugPolygonEdge> test;
	protected List<EdgeLoopPolygon<DebugPolygonVertex>> edgeLoopPolygons;
	protected List<Triangle2d<DebugPolygonVertex>> triList;
	
	//test edge
	protected Edge2d<Vertex2d> testEdge;
	protected IntervalSubset testEdgeOverlap;
	

	public PolygonDebug(){
		super("Point in Polygon Test");
		
		//make window closable
		addWindowListener(new WindowAdapter() {
	    	public void windowClosing(WindowEvent e) { System.exit(0); }
	    });
		
		addMouseMotionListener(this);
		addMouseListener(this);
		
		screenDimensions = new Vector2d(800,600);
		camMin = new Vector2d(-1.0f, -1.0f);
		camMax = new Vector2d(7.0f, 7.0f);
	}

	public static void main(String[] args) {
		PolygonDebug debug = new PolygonDebug();
		debug.setupGUI();
		
		debug.init();
		debug.draw();
	}
	
	protected void setupGUI(){
		
		setLayout(new BorderLayout());
		
		setSize((int)screenDimensions.getX(), (int)screenDimensions.getY());
	    setVisible(true);
	}
	
	public Polygon<DebugPolygonVertex,DebugPolygonEdge> makePoly1(){

		LinkedHashSet<DebugPolygonVertex> vertices = new LinkedHashSet<DebugPolygonVertex>();
		LinkedHashSet<DebugPolygonEdge> edges = new LinkedHashSet<DebugPolygonEdge>();
		
		Vector2d[] vloop;
		ArrayList<DebugPolygonVertex> vertsByIndex;
		
		vloop = new Vector2d[]{
			new Vector2d(0.0f, 0.0f),
			new Vector2d(3.0f, 0.0f),
			new Vector2d(3.0f, 2.0f),
			new Vector2d(6.0f, 2.0f),// + Epsilon.EPSILON),
			new Vector2d(6.0f, 6.0f),
			new Vector2d(0.0f, 6.0f)
		};
		vertsByIndex = new ArrayList<DebugPolygonVertex>(vloop.length); 
		for(int i=0; i<vloop.length; i++){
			DebugPolygonVertex nv = new DebugPolygonVertex(vloop[i]);
			vertsByIndex.add(i, nv);
			vertices.add(nv);
		}
		for(int i=0; i<vloop.length; i++){
			edges.add(new DebugPolygonEdge(vertsByIndex.get(i), vertsByIndex.get((i+1)%vloop.length)));
		}
		
		vloop = new Vector2d[]{
			new Vector2d(1.0f, 2.0f),// + Epsilon.EPSILON),
			new Vector2d(1.0f, 4.0f),// + Epsilon.EPSILON),
			new Vector2d(2.0f, 4.0f),
			new Vector2d(2.0f, 2.0f)
		};
		vertsByIndex = new ArrayList<DebugPolygonVertex>(vloop.length); 
		for(int i=0; i<vloop.length; i++){
			DebugPolygonVertex nv = new DebugPolygonVertex(vloop[i]);
			vertsByIndex.add(i, nv);
			vertices.add(nv);
		}
		for(int i=0; i<vloop.length; i++){
			edges.add(new DebugPolygonEdge(vertsByIndex.get(i), vertsByIndex.get((i+1)%vloop.length)));
		}
		
		vloop = new Vector2d[]{
			new Vector2d(3.0f, 3.5f),
			new Vector2d(4.0f, 5.5f),
			new Vector2d(5.0f, 3.5f)
		};
		vertsByIndex = new ArrayList<DebugPolygonVertex>(vloop.length); 
		for(int i=0; i<vloop.length; i++){
			DebugPolygonVertex nv = new DebugPolygonVertex(vloop[i]);
			vertsByIndex.add(i, nv);
			vertices.add(nv);
		}
		for(int i=0; i<vloop.length; i++){
			edges.add(new DebugPolygonEdge(vertsByIndex.get(i), vertsByIndex.get((i+1)%vloop.length)));
		}
		
		return new Polygon<DebugPolygonVertex,DebugPolygonEdge>(vertices, edges);
	}
	
	public Polygon<DebugPolygonVertex,DebugPolygonEdge> makePoly2(){

		LinkedHashSet<DebugPolygonVertex> vertices = new LinkedHashSet<DebugPolygonVertex>();
		LinkedHashSet<DebugPolygonEdge> edges = new LinkedHashSet<DebugPolygonEdge>();
		
		Vector2d[] vloop;
		ArrayList<DebugPolygonVertex> vertsByIndex;
		
		vloop = new Vector2d[]{
			new Vector2d(0.0f, 0.0f),
			
			//spike 1
			new Vector2d(3.0f, 0.0f),
			new Vector2d(3.0f, 5.0f/3.0f),
			
			//spike 2
			new Vector2d(4.0f, 0.0f),
			new Vector2d(4.0f, 4.0f/3.0f),
			
			new Vector2d(5.0f, 0.0f),
			new Vector2d(5.0f, 4.0f),
			new Vector2d(0.0f, 4.0f)
		};
		vertsByIndex = new ArrayList<DebugPolygonVertex>(vloop.length); 
		for(int i=0; i<vloop.length; i++){
			DebugPolygonVertex nv = new DebugPolygonVertex(vloop[i]);
			vertsByIndex.add(i, nv);
			vertices.add(nv);
		}
		for(int i=0; i<vloop.length; i++){
			edges.add(new DebugPolygonEdge(vertsByIndex.get(i), vertsByIndex.get((i+1)%vloop.length)));
		}
		
		vloop = new Vector2d[]{
			new Vector2d(1.0f, 3.0f),
			new Vector2d(2.0f, 2.0f),
			new Vector2d(1.0f, 1.0f),
		};
		vertsByIndex = new ArrayList<DebugPolygonVertex>(vloop.length); 
		for(int i=0; i<vloop.length; i++){
			DebugPolygonVertex nv = new DebugPolygonVertex(vloop[i]);
			vertsByIndex.add(i, nv);
			vertices.add(nv);
		}
		for(int i=0; i<vloop.length; i++){
			edges.add(new DebugPolygonEdge(vertsByIndex.get(i), vertsByIndex.get((i+1)%vloop.length)));
		}
		
		return new Polygon<DebugPolygonVertex,DebugPolygonEdge>(vertices, edges);
	}
	
	public void init(){
		
		backbuffer = createImage((int)screenDimensions.getX(), (int)screenDimensions.getY());
		graphics = backbuffer.getGraphics();
		
		cursor = new Vector2d(0.0f, 0.0f);
		
		test = makePoly2();
		System.out.println("Created polygon with " + test.getEdgePartitionCollection().getRoots().size() + " combinatorial components");
		
		testEdge = new Edge2d<Vertex2d>(new Vertex2d(new Vector2d(0.0f, 0.0f)), new Vertex2d(new Vector2d(1.0f, 0.0f)));
		testEdgeOverlap = PolygonEdgeIntersection.intersectPolygonAndEdge(test, testEdge, 0.01f);
		
		
		int n = 1000;
		long startTime = System.currentTimeMillis();
		for(int i=0; i<n; i++){
			triList = PolygonTools.<DebugPolygonVertex,DebugPolygonEdge>triangulate(test.getVertices());
		}
		long totalTime = System.currentTimeMillis() - startTime;
		System.out.println("Avg time: " + (float)totalTime/(float)n + " ms");
	}
	
	//DRAWING ROUTINES
	//=======================================
	
	public Vector2d convertScreenToCam(Vector2d s){
		return new Vector2d(
			camMin.getX() + (camMax.getX() - camMin.getX())*s.getX()/screenDimensions.getX(),
			camMax.getY() - (camMax.getY() - camMin.getY())*s.getY()/screenDimensions.getY());
	}
	
	public Vector2d convertCamToScreen(Vector2d c){
		return new Vector2d(
			screenDimensions.getX()*(c.getX() - camMin.getX())/(camMax.getX() - camMin.getX()),
			screenDimensions.getY()*(c.getY() - camMax.getY())/(camMin.getY() - camMax.getY()));
	}
	
	public void clear(){
				
		graphics.setColor(COLOR_BACKGROUND);
		graphics.fillRect(0, 0, (int)screenDimensions.getX(), (int)screenDimensions.getY());
	}
	
	public void drawCursor(){
		
		graphics.setColor(COLOR_CURSOR);
		drawPoint(cursor, 4);
	}
	
	public void drawPoint(Vector2d p, int size){
		
		Vector2d vscr = convertCamToScreen(p);
		graphics.fillOval((int)vscr.getX()-size/2, (int)vscr.getY()-size/2, size, size);
	}
	
	public void drawEdge(Vector2d p, Vector2d q){
		Vector2d v0 = convertCamToScreen(p);
		Vector2d v1 = convertCamToScreen(q);
		graphics.drawLine((int)v0.getX(), (int)v0.getY(), (int)v1.getX(), (int)v1.getY());
	}
	
	public void drawPolygon(Polygon<DebugPolygonVertex,DebugPolygonEdge> polygon){
		
		boolean sel = polygon.containsPoint(cursor);
		
		ClosestObjectData<Vector2d,DebugPolygonVertex,DebugPolygonEdge> clobj;
		clobj = PolygonTools.getClosestObject(polygon.getVertices(), polygon.getEdges(), cursor);
		Vector2d clpt = cursor.copy(); clpt.add(clobj.v);
		graphics.setColor(COLOR_CLOSEST_POINT);
		drawPoint(clpt, 6);
		
		//epsilon interior test
		//----------------------------------
		for(int ix=0; ix<100; ix++){ float tx = (float)ix/100.0f;
		for(int iy=0; iy<100; iy++){ float ty = (float)iy/100.0f;
			Vector2d test = new Vector2d(
				camMin.getX() + tx*(camMax.getX() - camMin.getX()),
				camMin.getY() + ty*(camMax.getY() - camMin.getY()));
			boolean testIn = polygon.containsPoint(test, 0.2f);
			if(testIn){
				drawPoint(test, 2);
			}
		}}
		//----------------------------------
		
		Color cp = COLOR_UNSEL;
		if(sel){ cp = COLOR_SEL; }
		
		//draw each edge
		graphics.setColor(cp);
		
		for(DebugPolygonEdge edge : polygon.getEdges()){
			drawEdge(edge.getInitialVertex().getPosition(), edge.getFinalVertex().getPosition());
		}
		
		//draw centroid
		graphics.setColor(COLOR_CENTROID);
		Vector2d centroid = polygon.computeAreaTimesCentroid();
		centroid.scale(1.0f/polygon.computeSignedArea());
		Vector2d centroidScreen = convertCamToScreen(centroid);
		graphics.fillOval((int)centroidScreen.getX()-2, (int)centroidScreen.getY()-2, 4, 4);
	}
	
	/*public void drawEdgeLoopPolygon(EdgeLoopPolygon<DebugPolygonVertex> edgeLoopPolygon){
		
		graphics.setColor(new Color(0,255,0));
		drawEdgeLoop(edgeLoopPolygon.getPositiveHead());
		
		graphics.setColor(new Color(255,0,0));
		for(EdgeLoopNode<DebugPolygonVertex> negativeHead : edgeLoopPolygon.getNegativeHeads()){
			drawEdgeLoop(negativeHead);
		}
	}
	
	public void drawEdgeLoop(EdgeLoopNode<DebugPolygonVertex> head){
		
		Vector2d p0 = null, p = null, q = null;
		for(EdgeLoopNode<DebugPolygonVertex> it = head;;){
			//shift forward, and store initial vertex position if necessary
			p = q; q = it.getVertex().getPosition();
			if(p0 == null){
				p0 = q;
				it = it.getNextNode();
				continue;
			}
			//draw edge p->q
			drawEdge(p,q);
			//next
			it = it.getNextNode();
			if(it == head){ break; }
		}
		
		//draw edge q->p0
		p = q; q = p0;
		drawEdge(p,q);
	}*/
	
	public void drawTriangle(Triangle2d<DebugPolygonVertex> tri){
		
		graphics.setColor(new Color(192,192,192));
		Vector2d pscr = convertCamToScreen(tri.getVertex0().getPosition());
		Vector2d qscr = convertCamToScreen(tri.getVertex1().getPosition());
		Vector2d rscr = convertCamToScreen(tri.getVertex2().getPosition());
		graphics.fillPolygon(
			new int[]{(int)pscr.getX(), (int)qscr.getX(), (int)rscr.getX()}, 
			new int[]{(int)pscr.getY(), (int)qscr.getY(), (int)rscr.getY()}, 3);
		
		graphics.setColor(new Color(0,0,0));
		drawEdge(tri.getVertex0().getPosition(), tri.getVertex1().getPosition());
		drawEdge(tri.getVertex1().getPosition(), tri.getVertex2().getPosition());
		drawEdge(tri.getVertex2().getPosition(), tri.getVertex0().getPosition());
		
		Vector2d centroid = tri.computeCentroid();
		
		graphics.setColor(new Color(0,0,0));
		drawPoint(tri.computeCentroid(),6);
		
		graphics.setColor(new Color(255,0,0));
		drawEdge(tri.getVertex0().getPosition(), centroid);
		graphics.setColor(new Color(0,255,0));
		drawEdge(tri.getVertex1().getPosition(), centroid);
		graphics.setColor(new Color(0,0,255));
		drawEdge(tri.getVertex2().getPosition(), centroid);
	}

	
	public void drawTestEdge(){
		
		IntervalSubset testEdgeNonOverlap = testEdgeOverlap.getComplement();
		
		graphics.setColor(new Color(0,192,0));
		testEdgeOverlap.traverseSubIntervals(new IntervalTraversalMethod(){
			@Override
			public void processFullInterval(){
				drawEdge(testEdge.lerp(0), testEdge.lerp(1));
			}
			@Override
			public void processInteriorSubInterval(float a, float b){
				drawEdge(testEdge.lerp(a), testEdge.lerp(b));
			}
			@Override
			public void processLeftAlignedSubInterval(float b){
				drawEdge(testEdge.lerp(0), testEdge.lerp(b));
			}
			@Override
			public void processRightAlignedSubInterval(float a){
				drawEdge(testEdge.lerp(a), testEdge.lerp(1));
			}
		});
		
		
		graphics.setColor(new Color(192,0,0));
		testEdgeNonOverlap.traverseSubIntervals(new IntervalTraversalMethod(){
			@Override
			public void processFullInterval(){
				drawEdge(testEdge.lerp(0), testEdge.lerp(1));
			}
			@Override
			public void processInteriorSubInterval(float a, float b){
				drawEdge(testEdge.lerp(a), testEdge.lerp(b));
			}
			@Override
			public void processLeftAlignedSubInterval(float b){
				drawEdge(testEdge.lerp(0), testEdge.lerp(b));
			}
			@Override
			public void processRightAlignedSubInterval(float a){
				drawEdge(testEdge.lerp(a), testEdge.lerp(1));
			}
		});
	}
	
	
	public void draw(){

		clear();
		
		for(Triangle2d<DebugPolygonVertex> tri : triList){
			drawTriangle(tri);
		}
		drawPolygon(test);
		
		drawTestEdge();
		
		drawCursor();
		
		repaint();
	}
	
	//MOUSE HANDLER
	//=======================================
	
	@Override
	public void update(Graphics g){ g.drawImage(backbuffer, 0, 0, this); }
	
	@Override
	public void paint(Graphics g){ update(g); }

	@Override
	public void mouseClicked(MouseEvent e) {
		
		System.out.println("Signed Area: " + test.computeSignedArea());
		
		Vector2d clickPos = convertScreenToCam(new Vector2d((float)e.getX(), (float)e.getY()));
		if(SwingUtilities.isRightMouseButton(e)){
			testEdge.setFinalVertex(new Vertex2d(clickPos));
			testEdgeOverlap = PolygonEdgeIntersection.intersectPolygonAndEdge(test, testEdge, 0.01f);
			draw();
		}else{
			testEdge.setInitialVertex(new Vertex2d(clickPos));
			testEdgeOverlap = PolygonEdgeIntersection.intersectPolygonAndEdge(test, testEdge, 0.01f);
			draw();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {

		cursor = convertScreenToCam(new Vector2d((float)e.getX(), (float)e.getY()));
		draw();
	}
}
