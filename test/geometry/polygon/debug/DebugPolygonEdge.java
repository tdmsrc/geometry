package geometry.polygon.debug;

import geometry.polygon.components.PolygonEdge;

public class DebugPolygonEdge extends PolygonEdge<DebugPolygonVertex, DebugPolygonEdge> {

	public DebugPolygonEdge(DebugPolygonVertex p, DebugPolygonVertex q) {
		super(p, q);
	}

	@Override
	protected DebugPolygonEdge getThisEdge(){ return this; }
}
