package geometry.spacepartition;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

import geometry.math.Vector3d;
import geometry.spacepartition.OctreeChild;

public class OctreeChildTest {

	@Test
	public void testSort() {
		
		Vector3d dims = new Vector3d(1.0f, 1.0f, 1.0f);
		OctreeChild[] childOrder = new OctreeChild[8];
		
		//ensure correct ordering for some ray
		OctreeChild.sort(new Vector3d(1.0f, 1.0f, 1.0f), dims, childOrder);
		Assert.assertTrue(childOrder[0] == OctreeChild.CHILD_XMIN_YMIN_ZMIN);
		Assert.assertTrue(childOrder[7] == OctreeChild.CHILD_XMAX_YMAX_ZMAX);
		
		//ensure correct ordering for another ray
		OctreeChild.sort(new Vector3d(-1.0f, -1.0f, 1.0f), dims, childOrder);
		Assert.assertTrue(childOrder[0] == OctreeChild.CHILD_XMAX_YMAX_ZMIN);
		Assert.assertTrue(childOrder[7] == OctreeChild.CHILD_XMIN_YMIN_ZMAX);
		
		//ensure no duplicates or null children
		HashSet<OctreeChild> childSet = new HashSet<OctreeChild>();
		for(int i=0; i<8; i++){
			if(childOrder[i] == null){ 
				Assert.fail("Null child");
			}
			childSet.add(childOrder[i]);
		}
		Assert.assertEquals(8, childSet.size());
	}

}
