package geometry.spacepartition;

import org.junit.Assert;
import org.junit.Test;

import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;
import geometry.spacepartition.Culler.BoxContainment;

public class CullerEdgeTest {

	@Test
	public void testGetBoxContainment() {
		
		//edge along x-axis, [-1,1]
		Vector3d p = new Vector3d(-1.0f, 0.0f, 0.0f);
		Vector3d q = new Vector3d( 1.0f, 0.0f, 0.0f);
		CullerEdge<Vector3d> ce = new CullerEdge<Vector3d>(p,q); 
		
		//box overlapping edge
		Box<Vector3d> boxPartial = new Box<Vector3d>(
			new Vector3d(-0.2f, -0.2f, -0.2f), 
			new Vector3d(0.2f, 0.2f, 0.2f)
		);
		
		BoxContainment resultPartial = ce.getBoxContainment(boxPartial);
		boolean successPartial = (resultPartial == BoxContainment.CONTAINMENT_PARTIAL) || (resultPartial == BoxContainment.CONTAINMENT_COMPLETE);
		Assert.assertTrue(successPartial);
	}

}
