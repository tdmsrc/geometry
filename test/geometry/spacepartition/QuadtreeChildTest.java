package geometry.spacepartition;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

import geometry.math.Vector2d;
import geometry.spacepartition.QuadtreeChild;

public class QuadtreeChildTest {

	@Test
	public void testSort() {
		
		Vector2d dims = new Vector2d(1.0f, 1.0f);
		QuadtreeChild[] childOrder = new QuadtreeChild[4];
		
		//ensure correct ordering for some ray
		QuadtreeChild.sort(new Vector2d(1.0f, 1.0f), dims, childOrder);
		Assert.assertTrue(childOrder[0] == QuadtreeChild.CHILD_XMIN_YMIN);
		Assert.assertTrue(childOrder[3] == QuadtreeChild.CHILD_XMAX_YMAX);
		
		//ensure correct ordering for another ray
		QuadtreeChild.sort(new Vector2d(-1.0f, 1.0f), dims, childOrder);
		Assert.assertTrue(childOrder[0] == QuadtreeChild.CHILD_XMAX_YMIN);
		Assert.assertTrue(childOrder[3] == QuadtreeChild.CHILD_XMIN_YMAX);
		
		//ensure no duplicates or null children
		HashSet<QuadtreeChild> childSet = new HashSet<QuadtreeChild>();
		for(int i=0; i<4; i++){
			if(childOrder[i] == null){ 
				Assert.fail("Null child");
			}
			childSet.add(childOrder[i]);
		}
		Assert.assertEquals(4, childSet.size());
	}

}
