package geometry.spacepartition;

import org.junit.Assert;
import org.junit.Test;

import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.Sphere;
import geometry.spacepartition.Culler.BoxContainment;

public class SphereTest {

	@Test
	public void testGetBoxContainment() {
		
		//unit sphere at origin
		Vector3d pos = new Vector3d(0.0f, 0.0f, 0.0f);
		pos.setX(0.0f); pos.setY(0.0f); pos.setZ(0.0f);
		Sphere<Vector3d> sphere = new Sphere<Vector3d>(pos, 1.0f);
		
		//box with partial overlap
		Box<Vector3d> boxPartial = new Box<Vector3d>(
			new Vector3d(0.0f, 0.0f, 0.0f), 
			new Vector3d(1.0f, 1.0f, 1.0f)
		);
		
		//box fully contained inside sphere
		Box<Vector3d> boxComplete = new Box<Vector3d>(
			new Vector3d(-0.2f, -0.2f, -0.2f), 
			new Vector3d(0.2f, 0.2f, 0.2f)
		);
		
		BoxContainment resultPartial = sphere.getBoxContainment(boxPartial);
		BoxContainment resultComplete = sphere.getBoxContainment(boxComplete);
		
		boolean successPartial = (resultPartial == BoxContainment.CONTAINMENT_PARTIAL) || (resultPartial == BoxContainment.CONTAINMENT_COMPLETE);
		boolean successComplete = (resultComplete == BoxContainment.CONTAINMENT_COMPLETE);
		
		Assert.assertTrue(successPartial);
		Assert.assertTrue(successComplete);
	}

}
