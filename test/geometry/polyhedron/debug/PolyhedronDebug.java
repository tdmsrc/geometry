package geometry.polyhedron.debug;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.Random;

import javax.swing.JFrame;

import geometry.common.PartitionCollection;
import geometry.common.PartitionNode;
import geometry.common.components.Plane;
import geometry.math.Camera;
import geometry.math.Matrix2d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.polygon.triangulate.Triangle2d;
import geometry.polyhedron.components.EmbeddedEdge;
import geometry.polyhedron.components.EmbeddedVertex;
import geometry.polyhedron.components.Polyhedron;
import geometry.polyhedron.components.PolyhedronFactory;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.polyhedron.components.PolyhedronFace.FaceRecurseMethod;
import geometry.polyhedron.intersect.NewEdge;
import geometry.polyhedron.intersect.NewEdgeSplit;
import geometry.polyhedron.intersect.PolyhedronCSG;
import geometry.polyhedron.intersect.PolyhedronCSGScheme;
import geometry.polyhedron.intersect.PolyhedronCSG.CSGData;
import geometry.rawgeometry.ObjFactory;

public class PolyhedronDebug extends JFrame implements MouseListener, MouseMotionListener {
	
	private static final long serialVersionUID = 1L;
	
	//general display options
	protected static final int SCREEN_W = 1024;
	protected static final int SCREEN_H = 768;
	
	protected static final Color COLOR_BACKGROUND = new Color(255,255,255);
	
	protected static final float EDGE_HEAD_PIXELS = 12;
	protected static final float EDGE_HEAD_ANGLE = (float)Math.PI/6.0f;
	
	//polyhedron display options
	protected static final Color COLOR_FACE_CENTROID = new Color(128,128,128);
	protected static final int SIZE_FACE_CENTROID = 4;
	
	protected static final Color COLOR_POLY_CENTROID = new Color(64,64,64);
	protected static final int SIZE_POLY_CENTROID = 4;
	
	protected static final Color COLOR_FACE_NORMAL = new Color(192,192,192);
	protected static final float LENGTH_FACE_NORMAL = 0.5f;
	
	protected static final Color COLOR_EDGE_NORMAL_REL_POS = new Color(192,255,192);
	protected static final Color COLOR_EDGE_NORMAL_REL_NEG = new Color(255,192,192);
	protected static final float LENGTH_EDGE_NORMAL = 0.2f;
	protected static final float EDGE_NORMAL_LERP_POS = 0.5f;
	
	protected static final Color COLOR_EDGE = new Color(0,0,0);
	protected static final Color COLOR_VERTEX = new Color(0,0,255);
	protected static final int SIZE_POLY_VERTEX = 6;
	
	protected static final Color COLOR_INTERSECTION_POINT = new Color(255,0,0);
	protected static final int SIZE_INTERSECTION_POINT = 8;
	
	protected static final float LENGTH_FLOWER_TEST_VECTOR = 0.3f;
	protected static final int SIZE_FLOWER_TEST_POINT = 2;
	protected static final Color COLOR_FLOWER_TEST_INSIDE = new Color(0,255,0);
	protected static final Color COLOR_FLOWER_TEST_OUTSIDE = new Color(255,0,0);
	
	protected static final int SIZE_PIP_TEST_POINT = 3;
	protected static final Color COLOR_PIP_TEST_INSIDE = new Color(0,255,0);
	
	protected static final Color COLOR_NEW_FACE = new Color(192,192,192);
	protected static final float LENGTH_NEW_FACE_EXTRUDE = 0.05f;
	
	
	protected Camera camera;
	protected Vector2d camMin, camMax;
	
	protected int oldMouseX, oldMouseY;
	protected boolean initFinished = false;
	
	protected Image backbuffer;
	protected Graphics graphics;
	
	protected PolyhedronFactory<DebugVertex, DebugEdge, DebugFace, DebugPolyhedron> polyhedronFactory;
	protected PolyhedronCSG<DebugVertex, DebugEdge, DebugFace> polyCSG;
	protected CSGData<DebugVertex, DebugEdge, DebugFace> debugData;
	
	protected DebugPolyhedron polyA, polyB;
	protected DebugPolyhedron oldPolyA, oldPolyB;
	
	protected float cubeSize;
	
	
	public PolyhedronDebug(){
		super("Polyhedron Test");
		
		//make window closable
		addWindowListener(new WindowAdapter() {
	    	public void windowClosing(WindowEvent e) { System.exit(0); }
	    });
		
		addMouseMotionListener(this);
		addMouseListener(this);
		
		camera = new Camera(60,(float)SCREEN_W/(float)SCREEN_H,0.1f,100.0f);
		camera.setPosition(new Vector3d(0, 0, 5));
		
		camMin = new Vector2d(-1.0f, -1.0f);
		camMax = new Vector2d(7.0f, 7.0f);
	}

	public static void main(String[] args) {
		PolyhedronDebug debug = new PolyhedronDebug();
		debug.setupGUI();
		
		debug.init();
		debug.draw(true);
	}
	
	protected void setupGUI(){
		
		setLayout(new BorderLayout());
		
		setSize(SCREEN_W, SCREEN_H);
	    setVisible(true);
	}
	
	public void init(){
		
		backbuffer = createImage(SCREEN_W, SCREEN_H);
		graphics = backbuffer.getGraphics();
		//Graphics2D g2d = (Graphics2D)graphics;
		//RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//g2d.setRenderingHints(rh);
		
		oldMouseX = SCREEN_W/2;
		oldMouseY = SCREEN_H/2;
		
		polyhedronFactory = 
			new PolyhedronFactory<DebugVertex, DebugEdge, DebugFace, DebugPolyhedron>(
				DebugVertex.getConstructor(),
				DebugEdge.getConstructor(),
				DebugFace.getConstructor(),
				DebugPolyhedron.getConstructor()
			);
		
		polyCSG =
			new PolyhedronCSG<DebugVertex, DebugEdge, DebugFace>(
				DebugVertex.getConstructor(),
				DebugEdge.getConstructor(),
				DebugFace.getConstructor()
			);
		
		//polyA = polyhedronFactory.makeBlock(new Vector3d(-1,-1,-1), new Vector3d(1,1,1));
		//polyB = polyhedronFactory.makeBlock(new Vector3d(-1,-1,-1), new Vector3d(1,1,1));
		
		
		//polyA = polyhedronFactory.makeBlock(new Vector3d(-1,-1,-1), new Vector3d(1,1,1));
		//polyA.flip();
		//polyB = polyhedronFactory.makeBlock(new Vector3d(-1,0,-1), new Vector3d(2,2,0));
		//polyB = polyhedronFactory.makeBlock(new Vector3d(-1,1,-1), new Vector3d(2,2,0));
		//polyB = polyhedronFactory.makeCylinder(0, 1, new Vector2d(1,1), 2, 6);
		//polyB.flip();
		
		//polyB = polyhedronFactory.makeBlock(new Vector3d(-1,-1,-1), new Vector3d(1,1,1));
		//polyA = polyhedronFactory.makeBlock(new Vector3d(-0.5f,-1,-0.5f), new Vector3d(.5f,1,.5f));
		//polyA = polyhedronFactory.makeBlock(new Vector3d(-0.5f,-2,-0.5f), new Vector3d(.5f,2,.5f));
		//polyA.flip();
		cubeSize = 2.0f;
		
		polyA = randomCube();
		//polyA.flip();
		outputPolyStats();
		
		polyB = randomCube();
		debugData = polyCSG.computeCSGOperationData(polyA, polyB, PolyhedronCSGScheme.CSGType.A_UNION_B);
		
		oldPolyA = null;
		oldPolyB = null;
		
		/*int n = 300;
		Random rand = new Random();
		
		polyA = randomCube(rand);
		long startTime = System.currentTimeMillis();
		for(int i=1; i<n; i++){
			polyB = randomCube(rand);
			//polyCSG.subtract(polyB, polyA);
			debugData = polyCSG.union(polyA, polyB);
			
			//polyA.verifyIncidenceReferences();
			//polyA.verifySingularManifold();
		}
		long totalTime = System.currentTimeMillis() - startTime;
		System.out.println("Total time: " + (float)totalTime/(float)n + "ms avg");*/
		
		initFinished = true;
	}
	
	public void outputPolyStats(){
		
		float sgnvol =  polyA.computeSignedVolume();
		Vector3d centroid = polyA.computeVolumeTimesCentroid();
		centroid.scale(1/sgnvol);
		
		System.out.println("\nPoly A stats:\n-------------------------");
		System.out.println("V:" + polyA.getVertices().size() + " E:" + polyA.getEdges().size() + " F:" + polyA.getFaces().size());
		String temp = "";
		for(DebugFace face : polyA.getFaces()){
			temp += "[FV: " + face.getVertices().size() + "]";
		}
		System.out.println(temp);
		System.out.println("Signed volume: " + sgnvol);
		System.out.println("Centroid: " + centroid);
		System.out.println("Components: " + polyA.getFacePartitionCollection().getRoots().size());
		String temp2 = "";
		for(PartitionNode<DebugFace> root : polyA.getFacePartitionCollection().getRoots()){
			temp2 += "[PS: " + root.getPartitionSize() + "]";
		}
		System.out.println(temp2);
		System.out.println("Inertia Tensor: " + polyA.computeInertiaTensor());
	}
	
	public DebugPolyhedron randomCube(){
		return randomCube(new Random());
	}
	
	public DebugPolyhedron randomCube(Random rand){
		Vector3d center = new Vector3d(rand.nextFloat(), rand.nextFloat(), rand.nextFloat()); 
		center.scale(10.0f);
		float s = (rand.nextFloat()+0.2f)*8.0f;
		Vector3d size = new Vector3d(s,s,s);
		//Vector3d min2 = new Vector3d(-s,-s,-s); min2.add(temp2);
		//Vector3d max2 = new Vector3d(s,s,s); max2.add(temp2);
		//return polyhedronFactory.makeBlock(min2,max2);
		return polyhedronFactory.makeFromObjData(ObjFactory.box(center, size));
	}
	
	//DRAWING ROUTINES
	//=======================================
	
	public void clear(){
				
		graphics.setColor(COLOR_BACKGROUND);
		graphics.fillRect(0, 0, SCREEN_W, SCREEN_H);
	}
	
	public void drawPolyVertex(DebugVertex vert, int size){
		
		drawPoint(vert.getPosition(), size);
	}
	
	public void drawPoint(Vector3d p, int size){
		
		Vector2d vscr = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, p);
		
		graphics.fillOval((int)vscr.getX()-size/2, (int)vscr.getY()-size/2, size, size);
	}
	
	public void drawPolyEdge(DebugEdge edge, boolean head){
		
		drawEdge(edge.getInitialVertex().getPosition(), edge.getFinalVertex().getPosition(), head);
	}
	
	public void drawEdge(Vector3d p, Vector3d q, boolean head){
		
		Vector2d pscr = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, p);
		Vector2d qscr = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, q);
		
		graphics.drawLine((int)pscr.getX(), (int)pscr.getY(), (int)qscr.getX(), (int)qscr.getY());
		
		//arrow head
		if(!head){ return; }
		float phi = (2*(float)Math.PI - EDGE_HEAD_ANGLE)*0.5f;
		
		Vector2d v1 = new Vector2d(qscr); v1.subtract(pscr);
		v1.normalize(); v1.scale(EDGE_HEAD_PIXELS);
		
		Vector2d v2 = new Vector2d(v1);
		
		Matrix2d rotPhiP = new Matrix2d(); rotPhiP.setAsGlRotate(phi);
		Matrix2d rotPhiM = new Matrix2d(); rotPhiM.setAsGlRotate(-phi);
		v1 = rotPhiP.apply(v1); v1.add(qscr); 
		v2 = rotPhiM.apply(v2); v2.add(qscr);
		
		int[] xPoints = new int[] {(int)qscr.getX(), (int)v1.getX(), (int)v2.getX()};
		int[] yPoints = new int[] {(int)qscr.getY(), (int)v1.getY(), (int)v2.getY()};
		graphics.fillPolygon(xPoints, yPoints, 3);
	}
	
	public void drawFlowerTest(PolyhedronVertex<DebugVertex, DebugEdge, DebugFace> vertex){
		
		//pick some vectors
		int n=40,m=20, nv=n*m+2;
		Vector3d[] vlist = new Vector3d[nv];
		
		int k=0;
		for(int i=0; i<n; i++){
		for(int j=0; j<m; j++){
			float u = (((float)i)*2*(float)Math.PI)/(float)n;
			float v = (((float)(j+1))*(float)Math.PI)/((float)(m+2));
			float cu = (float)Math.cos(u), su = (float)Math.sin(u);
			float cv = (float)Math.cos(v), sv = (float)Math.sin(v);
			vlist[k] = new Vector3d(cu*sv,su*sv,cv); k++;
		}}
		vlist[k] = new Vector3d(0,0,1); k++;
		vlist[k] = new Vector3d(0,0,-1); k++;
		
		//test them and draw them
		Vector3d p = vertex.getPosition();
		
		for(int i=0; i<nv; i++){
			
			Vector3d q = new Vector3d(vlist[i]); q.scale(LENGTH_FLOWER_TEST_VECTOR);
			q.flip(); q.add(p);
			
			if(vertex.checkInfinitesimalInterior(vlist[i])){
				graphics.setColor(COLOR_FLOWER_TEST_INSIDE);
				drawPoint(q, SIZE_FLOWER_TEST_POINT);
			}else{
				graphics.setColor(COLOR_FLOWER_TEST_OUTSIDE);
				//drawPoint(q, SIZE_FLOWER_TEST_POINT);
			}
		}
	}
	
	public void drawAllFlowerTests(DebugPolyhedron poly){
		
		for(DebugVertex vertex : poly.getVertices()){
			drawFlowerTest(vertex);
		}
	}
	
	public void drawPIPTests(DebugPolyhedron poly){
		
		graphics.setColor(COLOR_PIP_TEST_INSIDE);
		
		int n = 20, nmax = n*n*n;
		Vector3d[] vlist = new Vector3d[nmax];
		
		Vector3d min = new Vector3d(-3,-3,-3);
		Vector3d max = new Vector3d(3,3,3);
		
		Vector3d diff = max.copy(); diff.subtract(min);
		
		int k=0;
		for(int ix=0; ix<n; ix++){
		for(int iy=0; iy<n; iy++){
		for(int iz=0; iz<n; iz++){
			Vector3d v = new Vector3d((float)ix,(float)iy,(float)iz);
			v.scale(1/(float)n);
			v.multiplyComponentWise(diff);
			v.add(min);
			
			vlist[k] = new Vector3d(v); k++;
		}}}
		
		for(int i=0; i<nmax; i++){
			if(poly.containsPoint(vlist[i])){
				drawPoint(vlist[i], SIZE_PIP_TEST_POINT);
			}
		}
	}
	
	public void drawPolyhedron(Polyhedron<DebugVertex,DebugEdge,DebugFace> poly, Color edgeColor, boolean showExtraFaceData){
		
		//draw face decorations
		if(showExtraFaceData){
		for(DebugFace face : poly.getFaces()){
			drawFace(face, COLOR_EDGE);
		}}
		
		//draw edges
		graphics.setColor(edgeColor);
		
		for(DebugEdge edge : poly.getEdges()){
			drawPolyEdge(edge, false);
		}
		
		//draw vertices
		graphics.setColor(COLOR_VERTEX);

		//for(DebugVertex vert : poly.getVertices()){
		//	drawPolyVertex(vert, SIZE_POLY_VERTEX);
			
			/*for(DebugEdge edge : vert.getIncidentEdges()){

				DebugVertex w0 = vert;
				DebugVertex w1 = edge.getInitialVertex();
				if(w0 == w1){ w1 = edge.getFinalVertex(); }
				
				Vector3d w0pos = w0.getPosition().copy();
				Vector3d w1pos = w1.getPosition().copy();
				
				Vector3d ev = new Vector3d(w1pos); ev.subtract(w0pos);
				ev.normalize();
				ev.add(w0pos);
				drawEdge(w0pos, ev, true);
			}*/
		//}
		
		//centroid
		graphics.setColor(COLOR_POLY_CENTROID);
		
		Vector3d centroid = poly.computeVolumeTimesCentroid();
		centroid.scale(1 / poly.computeSignedVolume());
		
		drawPoint(centroid, SIZE_POLY_CENTROID);
	}
	
	public void drawEdgeNormals(Plane plane, Collection<EmbeddedEdge<DebugVertex, DebugEdge>> edges){
		
		for(EmbeddedEdge<DebugVertex, DebugEdge> edge : edges){
			
			//midpoint
			Vector2d p = edge.lerp(EDGE_NORMAL_LERP_POS);
			
			//inward-pointing normal vector
			Vector2d q = edge.getInwardPointingNormal();
			q.normalize(); q.scale(LENGTH_EDGE_NORMAL);
			q.add(p);
			
			//draw edge
			switch(edge.getGlobalEdgeRelativeOrientation()){
			case POSITIVE: graphics.setColor(COLOR_EDGE_NORMAL_REL_POS); break;
			case NEGATIVE: graphics.setColor(COLOR_EDGE_NORMAL_REL_NEG); break;
			}
			
			drawEdge(plane.getEmbedding(p), plane.getEmbedding(q), true);
		}
	}
	
	public void drawFacePartitionCollection(PartitionCollection<DebugFace> partitions, Color cEdge){

		for(PartitionNode<DebugFace> root : partitions.getRoots()){
			drawFaceComponent(root.getObject(), cEdge);
		}
	}
	
	public void drawFaceComponent(DebugFace lead, final Color cEdge){
		
		lead.recurse(new FaceRecurseMethod<DebugFace>(){
			@Override
			public boolean action(DebugFace previousFace, DebugFace thisFace) {
				drawFace(thisFace, cEdge);
				return true;
			}
		});
	}
	
	public void drawFaceCollection(Collection<DebugFace> faces, Color cEdge){
		for(DebugFace face : faces){
			drawFace(face, cEdge);
		}
	}
	
	public void drawFaceCollection(Collection<DebugFace> faces){
		drawFaceCollection(faces, COLOR_EDGE);
	}
	
	public void drawFace(DebugFace face, Color cEdge){
		
		graphics.setColor(cEdge);
		for(DebugEdge edge : face.getGlobalEdges()){
			
			Vector3d p = new Vector3d(edge.getInitialVertex().getPosition());
			Vector3d q = new Vector3d(edge.getFinalVertex().getPosition());
			drawEdge(p,q,false);
		}
		
		/*for(Triangle2d<EmbeddedVertex<DebugVertex,DebugEdge>> tri : face.getTriangles()){
			drawTriangle(face.getPlane(), tri);
		}*/
		
		//face centroid
		Vector2d centroid = face.computeAreaTimesCentroid();
		centroid.scale(1/face.computeSignedArea());
		Vector3d centroidAmbient = face.getPlane().getEmbedding(centroid);
		
		graphics.setColor(COLOR_FACE_CENTROID);
		drawPoint(centroidAmbient, SIZE_FACE_CENTROID);
		
		//face normal
		graphics.setColor(COLOR_FACE_NORMAL);
		
		Vector3d q = new Vector3d(centroidAmbient);
		
		Vector3d n = new Vector3d(face.getPlane().getNormal());
		n.normalize(); n.scale(LENGTH_FACE_NORMAL);
		q.add(n);
		drawEdge(centroidAmbient, q, true);
	
		//face edge normals
		drawEdgeNormals(face.getPlane(), face.getEdges());
	}
	
	public void drawTriangle(Plane plane, Triangle2d<EmbeddedVertex<DebugVertex,DebugEdge>> tri){
		
		Vector3d p3d = tri.getVertex0().getGlobalVertex().getPosition().copy(); //p3d.add(n);
		Vector3d q3d = tri.getVertex1().getGlobalVertex().getPosition().copy(); //q3d.add(n);
		Vector3d r3d = tri.getVertex2().getGlobalVertex().getPosition().copy(); //r3d.add(n);
		
		Vector2d p = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, p3d);
		Vector2d q = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, q3d);
		Vector2d r = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, r3d);
		
		Vector3d w = new Vector3d(1,1.2f,1.4f); w.normalize();
		float l = plane.getNormal().dot(w);
		if(l < 0){ l = -l; }
		Color c = new Color(l,l,l);
		
		Vector2d u = q.copy(); u.subtract(p);
		Vector2d v = r.copy(); v.subtract(p);
		if(u.cross(v) > 0){ return; } //c = new Color(0,0,255); } 
		
		
		graphics.setColor(c);
		graphics.fillPolygon(
			new int[]{(int)p.getX(), (int)q.getX(), (int)r.getX()}, 
			new int[]{(int)p.getY(), (int)q.getY(), (int)r.getY()}, 3);
		
		Vector3d centroid = plane.getEmbedding(tri.computeCentroid());
		graphics.setColor(new Color(255,0,0));
		drawEdge(p3d, centroid, false);
		graphics.setColor(new Color(0,255,0));
		drawEdge(q3d, centroid, false);
		graphics.setColor(new Color(0,0,255));
		drawEdge(r3d, centroid, false);
	}
	
	public void drawDebugData(CSGData<DebugVertex, DebugEdge, DebugFace> debugData, boolean debugtext){
		
		if(debugData == null){
			System.err.println("DebugData is null");
			return; 
		}
		
		//intersections
		int numNVs = 0;
		int numSEs = 0;
		int numNEs = 0;
		
		graphics.setColor(COLOR_INTERSECTION_POINT);
		//for(NewVertex<DebugVertex,DebugEdge,DebugFace> nv : debugData.idata.getNewVertices()){
		//	drawPoint(nv.getVertex().getPosition(), SIZE_INTERSECTION_POINT);
		//	numNVs++;
		//}
		
		for(NewEdge<DebugVertex,DebugEdge,DebugFace> ne : debugData.newEdgeData.getNewEdgeList()){	
			Vector3d p = ne.getInitialVertex().getPosition();
			Vector3d q = ne.getFinalVertex().getPosition();

			switch(ne.getNewEdgeType()){
			case FACE_FACE_INTERSECTION: 
				graphics.setColor(new Color(255,0,255)); break;
			case SPLIT_EDGE:
				NewEdgeSplit<DebugVertex,DebugEdge,DebugFace> se = ne.getAsSplitEdge();
				switch(se.getSplitEdgeType()){
				case NON_BOUNDARY:
					switch(se.getAsSplitEdgeNonBoundary().getKeepType()){
					case UNDETERMINED:	graphics.setColor(new Color(0,255,255));	break;
					case KEEP:			graphics.setColor(new Color(0,255,0));		break;
					case DISCARD:		graphics.setColor(new Color(0,0,255));		break;
					}
					break;
				case BOUNDARY_EDGE:	graphics.setColor(new Color(255,0,0));		break;
				case BOUNDARY_FACE:	graphics.setColor(new Color(255,192,64));	break;
				}
			}
			
			drawEdge(p,q,true);
		}
		
		if(debugtext){
			System.out.println("DrawDebugData: [nvs: " + numNVs + "] [ses: " + numSEs + "] [nes: " + numNEs + "]");
		}
	}

	public void draw(boolean debugtext){
		
		//if(debugData == null){ return; }

		clear();
		
		drawPolyhedron(polyA, new Color(0,0,0), true);
		drawPolyhedron(polyB, new Color(255,64,0), true);
		
		//drawDebugData(debugData, debugtext);
		//drawPolyhedron(polyB, new Color(192,192,255), false);
		
		//drawAllFlowerTests(polyB);
		//drawPIPTests(polyB);
		
		//drawFaceCollection(debugData.nfdata.getNewFaces());
		//drawPolyhedron(debugData.nfdata.getNewFacePolyhedron(), new Color(0,0,0), true);
		
		//drawPolyhedron(debugData.nfdata.getKeepFaceBPolyhedron(), new Color(0,192,0), true);
		
		//drawFacePartitionCollection(debugData.nfdata.getKeepComponentsA(), new Color(128,0,0));
		//drawFacePartitionCollection(debugData.nfdata.getDiscardComponentsA(), new Color(255,0,0));
		
		//drawFacePartitionCollection(debugData.nfdata.getKeepComponentsB(), new Color(0,0,128));
		//drawFaceComponentCollection(debugData.nfdata.getDiscardComponentsB(), new Color(192,192,255));
		
		
		//drawFaceCollection(debugData.nfdata.getKeepFacesA(), new Color(128,64,0));
		
		//drawFaceCollection(debugData.nfdata.getDiscardFacesA(), new Color(255,0,0));
		
		//drawFaceCollection(debugData.idata.getIntersectedFaceListPolyA(), new Color(0,255,0));
		
		//graphics.setColor(new Color(0,0,255));
		//for(Edge3d<? extends Vertex3d> edge : debugData.nedata.getBoundaryDiscardEdgesA()){
		//	drawEdge(edge.getInitialVertex().getPosition(), edge.getFinalVertex().getPosition(), false);
		//}
		
		/*drawFaceCollection(debugData.newFaceData.getKeepFacesB(), new Color(0,128,0));
		drawFaceCollection(debugData.newFaceData.getDiscardFacesB(), new Color(192,255,192));
		
		graphics.setColor(new Color(255,0,255));
		for(Edge3d<? extends Vertex3d> edge : debugData.newFaceData.getBoundaryEdgesA()){
			drawEdge(edge.getInitialVertex().getPosition(), edge.getFinalVertex().getPosition(), false);
		}
		graphics.setColor(new Color(255,0,0));
		for(Edge3d<? extends Vertex3d> edge : debugData.newFaceData.getBoundaryEdgesB()){
			drawEdge(edge.getInitialVertex().getPosition(), edge.getFinalVertex().getPosition(), false);
		}*/
		
		repaint();
	}
	
	//MOUSE HANDLER
	//=======================================
	
	@Override
	public void update(Graphics g){ g.drawImage(backbuffer, 0, 0, this); }
	
	@Override
	public void paint(Graphics g){ update(g); }

	@Override
	public void mouseClicked(MouseEvent e) {
		
		//boolean leftButton = (e.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK;
		boolean rightButton = (e.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK;
		
		if(rightButton){
			
			/*polyCSG.subtract(polyA, polyB);
			polyA.verifyIncidenceReferences();
			polyA.verifySingularManifold();
			
			polyB = randomCube();
			*/
			polyCSG.intersect(polyB, polyA);
			
			debugData = polyCSG.computeCSGOperationData(polyA, polyB, PolyhedronCSGScheme.CSGType.A_UNION_B);
			
		}else{
			
			polyCSG.union(polyA, polyB);
			//polyCSG.subtract(polyA, polyB);
			polyA.verifyIncidenceReferences();
			polyA.verifySingularManifold();
			
			polyB = randomCube();
			debugData = polyCSG.computeCSGOperationData(polyA, polyB, PolyhedronCSGScheme.CSGType.A_UNION_B);
		}

				
		//OutputTreeText debugOut = new OutputTreeText();
		//test.populateOutputTree(debugOut);
		//System.out.println(debugOut);
		outputPolyStats();
		
		draw(false);
	}
	
	
	@Override
	public void mouseEntered(MouseEvent e) { }

	@Override
	public void mouseExited(MouseEvent e) { }
	
	@Override
	public void mouseMoved(MouseEvent e) { 
		if(!initFinished){ return; }
		
		float xm = 10.0f*(-0.5f+(float)e.getX()/SCREEN_W);
		float ym = -10.0f*(-0.5f+(float)e.getY()/SCREEN_H);
		
		Vector3d cx = new Vector3d(xm,ym,0);
		Vector3d diag = new Vector3d(cubeSize, cubeSize, cubeSize);
		Vector3d min = cx.copy(); min.subtract(diag);
		Vector3d max = cx.copy(); max.add(diag);
		//polyB = polyhedronFactory.makeBlock(min,max);
		//debugData = polyCSG.computeCSGOperationData(polyA, polyB, PolyhedronCSGScheme.CSGType.A_UNION_B);
		
		
		//polyA.flip();
		//polyB = polyhedronFactory.makeCylinder(-1, 1, new Vector2d(1,1), 2, 8);
		//polyB = polyhedronFactory.makeBlock(new Vector3d(-1,1,-0.8f), new Vector3d(2,2,0.8f));
		
		//polyB = polyhedronFactory.makeCylinder(0, 1, new Vector2d(1+xm,1+ym), 2, 5);
		/*ArrayList<DebugPolyhedron> plist = new ArrayList<DebugPolyhedron>();
		
		plist.add(polyhedronFactory.makeBlock(
			new Vector3d(-1.4f,-1.4f,-1.4f), new Vector3d(1.4f,1.4f,1.4f)) );
		
		DebugPolyhedron temp = polyhedronFactory.makeBlock(
				new Vector3d(-0.8f,-0.8f,-0.8f), new Vector3d(0.8f,0.8f,0.8f));
		temp.flip();
		plist.add(temp);
		
		plist.add(polyhedronFactory.makeBlock(
			new Vector3d(1.8f+1,-1,-1), new Vector3d(1.8f+2,1,1)) );
		
		HashSet<DebugVertex> vertices = new HashSet<DebugVertex>();
		HashSet<DebugEdge> edges = new HashSet<DebugEdge>();
		HashSet<DebugFace> faces = new HashSet<DebugFace>();
		
		for(DebugPolyhedron poly : plist){
			vertices.addAll(poly.getVertices());
			edges.addAll(poly.getEdges());
			faces.addAll(poly.getFaces());
		}

		polyA = new DebugPolyhedron(vertices, edges, faces);
		//MessageOutput.printDebug("Created a polyhedron with " + polyB.getFacePartitionCollection().getAllRoots().size() + " combinatorial components");
		
		//debugData = polyCSG.union(polyA,polyB);
		
		//polyA = polyhedronFactory.makeBlock(new Vector3d(-2,-2,-2), new Vector3d(2,2,2));
		//polyB = polyhedronFactory.makeBlock(new Vector3d(-1,-1,-1), new Vector3d(1,1,1));
		debugData = polyCSG.union(polyA,polyB);
		
		//TODO get rid of this
		/*int k = 0;
		for(DebugVertex debugvert : polyB.getVertexList()){
			k += debugvert.testThing();
		}
		System.out.println("Sum (incident edge count) over all verts of polyB: " + k);*/
		
		draw(false);
	}

	@Override
	public void mouseReleased(MouseEvent e) { }
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		oldMouseX = -e.getX(); oldMouseY = -e.getY();
	}

	
	private static final float RADIANS_PER_PIXEL = 0.007f;
	
	@Override
	public void mouseDragged(MouseEvent e) {
		
		int mx = -e.getX(), my = -e.getY();
		int dx = oldMouseX - mx, dy = oldMouseY - my;
		
		//figure out configuration of left/right buttons
		boolean leftButton = (e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) == InputEvent.BUTTON1_DOWN_MASK;
		boolean rightButton = (e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) == InputEvent.BUTTON3_DOWN_MASK;
		
		float movespeed = 0.1f;
		if(leftButton){ 
			if(rightButton){
				camera.addToPosition(camera.getRotation().moveSideways((float)dx*movespeed));
				camera.addToPosition(camera.getRotation().moveUp(-(float)dy*movespeed));
			}else{
				camera.addToPosition(camera.getRotation().moveForwardUPerp(-(float)dy*movespeed));
				camera.addToPosition(camera.getRotation().moveSideways((float)dx*movespeed));
			}
		}else{
			if(rightButton){
				camera.getRotation().rotate(-(float)dx*RADIANS_PER_PIXEL, -(float)dy*RADIANS_PER_PIXEL);
			}
		}
		
		oldMouseX = mx; oldMouseY = my;
		draw(false);
	}
}
