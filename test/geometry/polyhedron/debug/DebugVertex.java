package geometry.polyhedron.debug;

import geometry.common.components.Vertex3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronVertex;

public class DebugVertex extends PolyhedronVertex<DebugVertex, DebugEdge, DebugFace> {

	//constructor for PolyhedronFactory
	public static class Constructor implements Vertex3d.Vertex3dConstructor<DebugVertex>{
		@Override
		public DebugVertex construct(Vector3d v) {
			return new DebugVertex(v);
		}
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; } 
	
	
	public DebugVertex(Vector3d v){
		super(v);
	}

	@Override
	protected DebugVertex getThisVertex() { return this; }
	
	@Override
	public DebugVertex copy(){
		return new DebugVertex(getPosition().copy());
	}
}
