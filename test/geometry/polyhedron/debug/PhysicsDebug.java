package geometry.polyhedron.debug;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import geometry.math.Camera;
import geometry.math.ObjectTransformation3d;
import geometry.math.PhysicalState3d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.Polyhedron;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFactory;
import geometry.polyhedron.physics.CollisionData;
import geometry.polyhedron.physics.PhysicsObject;
import geometry.polyhedron.physics.PhysicsObject.CollisionPoint;
import geometry.rawgeometry.ObjFactory;

public class PhysicsDebug extends JFrame implements MouseListener, MouseMotionListener{
	private static final long serialVersionUID = 1L;
	
	//general display options
	protected static final int SCREEN_W = 1024;
	protected static final int SCREEN_H = 768;
	protected static final Color COLOR_BACKGROUND = new Color(255,255,255);
	
	protected static final Vector3d ZERO = new Vector3d(0,0,0);
	
	protected static final boolean DECORATIONS = true;
	
	protected static final float TARGET_DRAW_FPS = 60.0f;
	protected static final float TARGET_PHYS_FPS = 250.0f;
	
	protected Camera camera;
	protected Vector2d camMin, camMax;
	
	protected int oldMouseX, oldMouseY;
	
	protected Image backbuffer;
	protected Graphics graphics;
	
	//physics objects
	protected static final float C_RESTITUTION = 0.7f;
	protected static final float C_FRICTION = 0.4f;
	
	protected boolean paused = true;
	PolyhedronFactory<DebugVertex, DebugEdge, DebugFace, DebugPolyhedron> factory;
	
	protected PhysicsObject bounds;
	protected LinkedList<PhysicsObject> objs = new LinkedList<PhysicsObject>();
	
	
	protected LinkedList<CollisionPoint> collisionsDraw = new LinkedList<CollisionPoint>();
	protected LinkedList<CollisionPoint> collisionsDrawAvg = new LinkedList<CollisionPoint>();
	
	
	public PhysicsDebug(){
		super("Physics Test (3D)");
		
		//make window closable
		addWindowListener(new WindowAdapter() {
	    	public void windowClosing(WindowEvent e) { System.exit(0); }
	    });
		
		addMouseMotionListener(this);
		addMouseListener(this);
		
		camera = new Camera(60,(float)SCREEN_W/(float)SCREEN_H,0.1f,100.0f);
		camera.setPosition(new Vector3d(0, 10, 60));
		
		camMin = new Vector2d(-1.0f, -1.0f);
		camMax = new Vector2d(7.0f, 7.0f);
	}
	
	public static void main(String[] args) {
		PhysicsDebug debug = new PhysicsDebug();
		debug.setupGUI();
		
		debug.init();
	}
	
	protected void setupGUI(){
		
		setLayout(new BorderLayout());
		
		setSize(SCREEN_W, SCREEN_H);
	    setVisible(true);
	}
	
	
	//=======================================
	// INIT AND OBJECTS
	//=======================================
	
	private static final float DENSITY = 700.0f; //water = 1000
	
	public void init(){
		
		backbuffer = createImage(SCREEN_W, SCREEN_H);
		graphics = backbuffer.getGraphics();
		
		oldMouseX = SCREEN_W/2;
		oldMouseY = SCREEN_H/2;
		
		factory = 
			new PolyhedronFactory<DebugVertex, DebugEdge, DebugFace, DebugPolyhedron>(
				DebugVertex.getConstructor(),
				DebugEdge.getConstructor(),
				DebugFace.getConstructor(),
				DebugPolyhedron.getConstructor()
			);
		
		//BOUNDING OBJECT
		//-----------------------------------------
		DebugPolyhedron boundsBody0 = factory.makeFromObjData(ObjFactory.box(ZERO, new Vector3d(40,60,40)));
		//DebugPolyhedron boundsBody0 = factory.makeFromObjData(ObjFactory.ellipsoid(ZERO, new Vector3d(40,40,40), 6, 6, 1, 1));
		boundsBody0.flip();
		
		ObjectTransformation3d boundsTrans = new ObjectTransformation3d();
		//boundsTrans.getRotation().setEqualTo(new Rotation3d(new Vector3d(0,0,1), (float)Math.PI*0.2f));
		Polyhedron<?,?,?> boundsBody = boundsBody0.copy(boundsTrans, 
			DebugVertex.getConstructor(),
			DebugEdge.getConstructor(),
			DebugFace.getConstructor()); 
		
		PhysicalState3d boundsState = new PhysicalState3d(boundsBody.computeSignedVolume(), DENSITY, boundsBody.computeVolumeTimesCentroid(), boundsBody.computeInertiaTensor());
		boundsState.getTranslation().setEqualTo(new Vector3d(0,30,0));
		//boundsState.setW(new Vector3d(0,0,0.1f));
		bounds = new PhysicsObject("Wall", boundsState, boundsBody);
		bounds.immovable = true;
		objs.add(bounds);
		
		//OTHER OBJECTS
		//-----------------------------------------
		//initRandomObjects(6);
		initStack(6);
		
		//MAIN LOOP
		//-----------------------------------------
		initMainLoop();
	}
	
	protected void initStack(int n){
		float s0 = 4.0f;
		float dw = -0.6f;
		float offset = 0.1f;
		
		for(int i=0; i<n; i++){
			float sx = s0-i*dw;
			float sy = 1.0f; 
			float sz = s0-i*dw;
			DebugPolyhedron body = factory.makeFromObjData(ObjFactory.box(ZERO, new Vector3d(sx,sy,sz)));
			//DebugPolyhedron body = factory.makeFromObjData(ObjFactory.ellipsoid(ZERO, new Vector3d(sx*0.5f,sy*0.5f,sz*0.5f), 4, 2, 1, 1));
			
			float px = i*offset;//((i%2)-0.5f)*offset;
			float py = 0.05f + (sy+0.3f)*(i+0.5f);
			float pz = i*offset;//((i%2)-0.5f)*offset;
			
			PhysicalState3d state = new PhysicalState3d(body.computeSignedVolume(), DENSITY, body.computeVolumeTimesCentroid(), body.computeInertiaTensor());
			state.getTranslation().setEqualTo(new Vector3d(px,py,pz));
			//state.setW(new Vector3d(0.1f,0,0));
			//state.setV(new Vector3d(0.1f,0,0));
			
			PhysicsObject objc = new PhysicsObject("Stack " + i, state, body);
			objs.add(objc);
		}
	}
	
	protected void initRandomObjects(int n){
		Random rand = new Random();
		
		for(int i=0; i<n; i++){
			float sx = 4.0f + rand.nextFloat()*3.0f; sx*=0.5f;
			float sy = 4.0f + rand.nextFloat()*3.0f; sy*=0.5f;
			float sz = 4.0f + rand.nextFloat()*3.0f; sz*=0.5f;
			
			DebugPolyhedron body;
			int m = i%3;
			switch(m){
			case 0: body = factory.makeFromObjData(ObjFactory.box(ZERO, new Vector3d(sx,sy,sz))); break;
			//case 1: body = factory.makeFromObjData(ObjFactory.ellipsoid(ZERO, new Vector3d(sx,sy,sz), 20, 18, 1, 1)); break;
			case 1: body = factory.makeFromObjData(ObjFactory.ellipsoid(ZERO, new Vector3d(sx,sy,sz), 6, 3, 1, 1)); break;
			//default: body = factory.makeFromObjData(ObjFactory.torus(ZERO, sx*0.8f, 22, sx*0.2f, 12)); 
			default: body = factory.makeFromObjData(ObjFactory.torus(ZERO, sx*0.8f, 10, sx*0.2f, 5));
			}
			
			PhysicalState3d state = new PhysicalState3d(body.computeSignedVolume(), DENSITY, body.computeVolumeTimesCentroid(), body.computeInertiaTensor());
			
			float px = (rand.nextFloat()-0.5f)*30.0f;
			float pz = (rand.nextFloat()-0.5f)*30.0f;
			float py = 12.0f + rand.nextFloat()*20.0f;
			Vector3d p = new Vector3d(px,py,pz);
			state.getTranslation().setEqualTo(p);
			
			float wx = rand.nextFloat() - 0.5f;
			float wy = rand.nextFloat() - 0.5f;
			float wz = rand.nextFloat() - 0.5f;
			Vector3d w = new Vector3d(wx, wy, wz); w.scale(6.0f);
			state.setW(w);
			
			PhysicsObject objc = new PhysicsObject("Rand " + i, state, body);
			objs.add(objc);
		}
	}
	
	
	//=======================================
	// MAIN LOOP
	//=======================================
	
	//TODO
	boolean wasPaused = false;
	
	protected void initMainLoop(){
		
		TimerTask doPhysics = new TimerTask(){
			
			private static final int SLOW_FACTOR = 1;
			
			private long curTime = System.currentTimeMillis()/SLOW_FACTOR;
			private long ltFPS = curTime;
			private long ltPhys = curTime; int nPhys = 0;
			private long ltDraw = curTime; int nDraw = 0;
			
			@Override
			public void run(){
				
				long curTime = System.currentTimeMillis()/SLOW_FACTOR;
				float dtFPS = (curTime - ltFPS)/1000.0f;
				float dtDraw = (curTime - ltDraw)/1000.0f;
				float dtPhys = (curTime - ltPhys)/1000.0f;
				
				if(dtFPS > 1.0f/(2.0f*(float)SLOW_FACTOR)){
					System.out.println("Per second: [Draw: " + (nDraw*2) + "] [Phys: " + (nPhys*2) + "]");
					nPhys = 0; nDraw = 0;
					ltFPS = curTime;
				}
				
				if(dtDraw > 1.0f/(TARGET_DRAW_FPS*(float)SLOW_FACTOR)){
					ltDraw = curTime; nDraw++;
					draw();
				}
				
				//------------------------------------------
				if(dtPhys > 1.0f/TARGET_PHYS_FPS){
					ltPhys = curTime; nPhys++;
					
					//decorations for collisions, debug only
					collisionsDraw.clear();
					collisionsDrawAvg.clear();
					
					//clear objects' integration constraints
					for(PhysicsObject oi : objs){ oi.clearConstraints(); }
					
					//get list of collisions and populate objects' integration constraints
					float depth = 0; //TODO
					LinkedList<CollisionData> objCollisions = new LinkedList<CollisionData>();
					for(int i=0; i<objs.size(); i++){
					for(int j=i+1; j<objs.size(); j++){
						depth = PhysicsObject.getObjCollision(objCollisions, objs.get(i), objs.get(j), collisionsDraw, collisionsDrawAvg);
					}}
					
					if(!objCollisions.isEmpty() && (depth > 0.2f)){
						if(!wasPaused){
							//paused = true;
							wasPaused = true;
						}
					}
					
					if(!paused){
						wasPaused = false;
						
						//collision resolution
						CollisionData.resolveCollisions(objs, objCollisions, C_RESTITUTION, C_FRICTION);
						
						//integrate
						for(PhysicsObject obj : objs){ obj.integrate(dtPhys); }
					}
				}
				//------------------------------------------
			}
		};
		
		Timer t = new Timer();
		t.schedule(doPhysics, 0, 1);
	}
	
	
	//=======================================
	// DRAWING
	//=======================================

	public void clear(){
		
		graphics.setColor(COLOR_BACKGROUND);
		graphics.fillRect(0, 0, SCREEN_W, SCREEN_H);
	}
	
	public void drawEdge(Vector3d p, Vector3d q){
		
		Vector2d pscr = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, p);
		Vector2d qscr = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, q);
		
		graphics.drawLine((int)pscr.getX(), (int)pscr.getY(), (int)qscr.getX(), (int)qscr.getY());
	}
	
	public void drawPoint(Vector3d p, int size){
		
		Vector2d vscr = camera.getScreenCoordsFromObjectCoords(SCREEN_W, SCREEN_H, true, p);
		
		graphics.fillOval((int)vscr.getX()-size/2, (int)vscr.getY()-size/2, size, size);
	}
	
	public void drawPolyhedron(PhysicsObject obj, boolean showExtraFaceData){
		
		//draw edges
		Color edgeNoSleep = new Color(64,64,64);
		Color edgeSleep = DECORATIONS ? new Color(128,128,128) : edgeNoSleep;
		
		boolean lastSleeping = false;
		graphics.setColor(edgeNoSleep);
		
		for(PolyhedronEdge<?,?,?> edge : obj.body.getEdges()){
			Vector3d p = obj.state.applyToPoint(edge.getInitialVertex().getPosition());
			Vector3d q = obj.state.applyToPoint(edge.getFinalVertex().getPosition());
			
			if(obj.sleeping != lastSleeping){
				graphics.setColor(obj.sleeping ? edgeSleep : edgeNoSleep);
				lastSleeping = obj.sleeping;
			}
			drawEdge(p,q);
		}
		
		if(DECORATIONS){
			//draw linear velocity
			graphics.setColor(new Color(0,128,0));
			Vector3d vp = obj.state.getTranslation();
			Vector3d vq = vp.copy(); vq.add(obj.state.getV());
			drawEdge(vp,vq);
			
			//draw angular velocity
			graphics.setColor(new Color(192,0,128));
			Vector3d wp = obj.state.getTranslation();
			Vector3d wq = wp.copy(); wq.add(obj.state.getW());
			drawEdge(wp,wq);
		}
	}
	
	public void draw(){
		
		clear();
		
		//draw polyhedron
		for(PhysicsObject obj : objs){
			drawPolyhedron(obj, true);
		}
		
		if(DECORATIONS){
			graphics.setColor(new Color(255,0,0));
			for(CollisionPoint cx : collisionsDraw){
				Vector3d cv = cx.x;
				Vector3d cnTip = cx.xn.copy(); cnTip.add(cv);
				
				drawPoint(cv, 4);
				drawEdge(cv, cnTip);			
			}
			
			graphics.setColor(new Color(0,64,192));
			for(CollisionPoint cx : collisionsDrawAvg){
				Vector3d cv = cx.x;
				Vector3d cnTip = cx.xn.copy(); cnTip.add(cv);
				
				drawPoint(cv, 4);
				drawEdge(cv, cnTip);			
			}
		}
		
		//finish
		repaint();
	}
	
	
	//=======================================
	// EVENT HANDLERS
	//=======================================
	
	@Override
	public void update(Graphics g){ g.drawImage(backbuffer, 0, 0, this); }
	
	@Override
	public void paint(Graphics g){ update(g); }
	
	@Override
	public void mouseEntered(MouseEvent e) { }

	@Override
	public void mouseExited(MouseEvent e) { }
	
	@Override
	public void mouseMoved(MouseEvent e) { }

	@Override
	public void mouseReleased(MouseEvent e) { }
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		oldMouseX = -e.getX(); oldMouseY = -e.getY();
	}
	
	@Override
	public void mouseClicked(MouseEvent e){
		
		boolean shift = ((e.getModifiersEx() & MouseEvent.SHIFT_DOWN_MASK) == MouseEvent.SHIFT_DOWN_MASK);
		if(shift){
			//jump
			for(PhysicsObject obj : objs){
				if(!obj.immovable){
					obj.state.getV().add(new Vector3d(0,5,0));
				}
			}
		}else{
			paused = !paused;
		}		
	}
	
	
	private static final float RADIANS_PER_PIXEL = 0.007f;
	
	@Override
	public void mouseDragged(MouseEvent e) {
		
		int mx = -e.getX(), my = -e.getY();
		int dx = oldMouseX - mx, dy = oldMouseY - my;
		
		//figure out configuration of left/right buttons
		boolean leftButton = (e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) == InputEvent.BUTTON1_DOWN_MASK;
		boolean rightButton = (e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) == InputEvent.BUTTON3_DOWN_MASK;
		
		float movespeed = 0.1f;
		if(leftButton){ 
			if(rightButton){
				camera.addToPosition(camera.getRotation().moveSideways((float)dx*movespeed));
				camera.addToPosition(camera.getRotation().moveUp(-(float)dy*movespeed));
			}else{
				camera.addToPosition(camera.getRotation().moveForwardUPerp(-(float)dy*movespeed));
				camera.addToPosition(camera.getRotation().moveSideways((float)dx*movespeed));
			}
		}else{
			if(rightButton){
				camera.getRotation().rotate(-(float)dx*RADIANS_PER_PIXEL, -(float)dy*RADIANS_PER_PIXEL);
			}
		}
		
		oldMouseX = mx; oldMouseY = my;
		//draw();
	}
}