package geometry.polyhedron.debug;

import java.util.Random;

import geometry.math.Matrix3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronFactory;
import geometry.rawgeometry.ObjFactory;

public class MassPropertiesDebug {

	public static void main(String[] args){
		
		PolyhedronFactory<DebugVertex, DebugEdge, DebugFace, DebugPolyhedron> factory;
		factory = 
			new PolyhedronFactory<DebugVertex, DebugEdge, DebugFace, DebugPolyhedron>(
				DebugVertex.getConstructor(),
				DebugEdge.getConstructor(),
				DebugFace.getConstructor(),
				DebugPolyhedron.getConstructor()
			);
		
		//COMPARISONS
		Random rand = new Random();
		DebugPolyhedron polyTest;
		float m;
		Matrix3d cmp, act;
		
		//BOX
		float sx = 1.0f + rand.nextFloat()*10.0f;
		float sy = 1.0f + rand.nextFloat()*10.0f;
		float sz = 1.0f + rand.nextFloat()*10.0f;
		polyTest = factory.makeFromObjData(ObjFactory.box(new Vector3d(0,0,0), new Vector3d(sx,sy,sz)));
		
		//computed value
		cmp = polyTest.computeInertiaTensor();
		
		act = new Matrix3d();
		m = sx*sy*sz;
		act.set(0, 0, m*(sy*sy + sz*sz)/12.0f);
		act.set(1, 1, m*(sx*sx + sz*sz)/12.0f);
		act.set(2, 2, m*(sx*sx + sy*sy)/12.0f);
		
		
		System.out.println(cmp);
		System.out.println(act);
		
		//ELLIPSOID
		for(int i=0; i<100; i++){
			float rx = 1.0f + rand.nextFloat()*10.0f;
			float ry = 1.0f + rand.nextFloat()*10.0f;
			float rz = 1.0f + rand.nextFloat()*10.0f;
			polyTest = factory.makeFromObjData(ObjFactory.ellipsoid(new Vector3d(0,0,0), new Vector3d(rx,ry,rz), 80, 78, 1, 1));
			
			//computed value
			cmp = polyTest.computeInertiaTensor();
			
			act = new Matrix3d();
			m = 4.0f*(float)Math.PI*rx*ry*rz/3.0f;
			act.set(0, 0, m*(ry*ry+rz*rz)/5.0f);
			act.set(1, 1, m*(rx*rx+rz*rz)/5.0f);
			act.set(2, 2, m*(rx*rx+ry*ry)/5.0f);
			
			//System.out.println(cmp);
			System.out.println("Radii: (" + rx + "," + ry + "," + rz + ")");
			System.out.println("COMP: " + cmp);
			System.out.println("REAL: " + act);
			
		}
	}
}
