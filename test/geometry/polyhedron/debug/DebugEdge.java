package geometry.polyhedron.debug;

import geometry.common.components.Edge3d;
import geometry.polyhedron.components.PolyhedronEdge;

public class DebugEdge extends PolyhedronEdge<DebugVertex, DebugEdge, DebugFace>{

	//constructor for PolyhedronFactory
	public static class Constructor implements Edge3d.Edge3dConstructor<DebugVertex, DebugEdge>{
		@Override
		public DebugEdge construct(DebugVertex p, DebugVertex q) {
			return new DebugEdge(p, q);
		}
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; } 
	
	
	public DebugEdge(DebugVertex p, DebugVertex q){
		super(p, q);
	}

	@Override
	protected DebugEdge getThisEdge() { return this; }
}
