package geometry.polyhedron.debug;

import java.util.LinkedHashSet;

import geometry.polyhedron.components.Polyhedron;

public class DebugPolyhedron extends Polyhedron<DebugVertex, DebugEdge, DebugFace>{

	public DebugPolyhedron(LinkedHashSet<DebugVertex> vertices, LinkedHashSet<DebugEdge> edges, LinkedHashSet<DebugFace> faces){
		super(vertices, edges, faces);
	}
	
	//constructor for PolyhedronFactory
	public static class Constructor implements Polyhedron.PolyhedronConstructor<DebugVertex, DebugEdge, DebugFace, DebugPolyhedron>{
		@Override
		public DebugPolyhedron construct(LinkedHashSet<DebugVertex> vertices, LinkedHashSet<DebugEdge> edges, LinkedHashSet<DebugFace> faces){
			return new DebugPolyhedron(vertices, edges, faces);
		}
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; } 
}
