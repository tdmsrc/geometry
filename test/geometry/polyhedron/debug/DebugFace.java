package geometry.polyhedron.debug;

import java.util.LinkedHashSet;

import geometry.common.components.Plane;
import geometry.polyhedron.components.EmbeddedEdge;
import geometry.polyhedron.components.EmbeddedPolygon;
import geometry.polyhedron.components.EmbeddedVertex;
import geometry.polyhedron.components.PolyhedronFace;

public class DebugFace extends PolyhedronFace<DebugVertex, DebugEdge, DebugFace> {

	//constructor for PolyhedronFactory
	public static class Constructor implements EmbeddedPolygon.Face3dConstructor<DebugVertex, DebugEdge, DebugFace>{
		@Override
		public DebugFace construct(Plane plane, LinkedHashSet<EmbeddedVertex<DebugVertex, DebugEdge>> vertices, LinkedHashSet<EmbeddedEdge<DebugVertex, DebugEdge>> edges){
			return new DebugFace(plane, vertices, edges);
		}

		@Override
		public DebugFace construct(DebugFace oldFace, Plane plane, LinkedHashSet<EmbeddedVertex<DebugVertex, DebugEdge>> vertices, LinkedHashSet<EmbeddedEdge<DebugVertex, DebugEdge>> edges){
			return construct(plane, vertices, edges);
		}
		
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; }  
	
	public DebugFace(Plane plane, LinkedHashSet<EmbeddedVertex<DebugVertex, DebugEdge>> vertices, LinkedHashSet<EmbeddedEdge<DebugVertex, DebugEdge>> edges){
		super(plane, vertices, edges);
	}

	@Override
	protected DebugFace getThisFace() { return this; }
}
