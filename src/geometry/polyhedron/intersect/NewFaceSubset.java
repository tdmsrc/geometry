package geometry.polyhedron.intersect;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import geometry.common.Orientation;
import geometry.common.components.Plane;
import geometry.polygon.components.PolygonTools;
import geometry.polygon.components.PolygonTools.PolygonComponentData;
import geometry.polyhedron.components.EmbeddedEdge;
import geometry.polyhedron.components.EmbeddedVertex;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.polyhedron.components.EmbeddedPolygon.Face3dConstructor;


class NewFaceSubset
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>>
{
	
	protected Plane basePlane; //should be either baseFace.getPlane() or flipped version
	protected Face baseFace; //used to copy properties, e.g. textures etc
	protected boolean baseFaceGetsFlipped;

	final protected Collection<Face> goodFaces;
	final protected Collection<Face> badFaces;
	
	protected PolyhedronCSGScheme scheme;
	Face3dConstructor<Vertex, Edge, Face> faceConstructor;
	
	protected HashMap<NewEdge<Vertex,Edge,Face>, Orientation> newFaceEdges;
	
	
	public NewFaceSubset(Face baseFace, Orientation basePlaneRelativeOrientation, 
			Collection<Face> goodFaces, Collection<Face> badFaces,
			PolyhedronCSGScheme scheme,
			Face3dConstructor<Vertex, Edge, Face> faceConstructor){
		
		this.baseFace = baseFace;
		baseFaceGetsFlipped = 
			(basePlaneRelativeOrientation == Orientation.NEGATIVE) ^ scheme.checkSimulateFlipFinal();
		
		basePlane = baseFace.getPlane().copy();
		if(scheme.checkSimulateFlip(baseFace.getOwner()) ^ scheme.checkSimulateFlipFinal()){
			basePlane.flip();
		}
		
		this.goodFaces = goodFaces;
		this.badFaces = badFaces;
		
		this.scheme = scheme;
		this.faceConstructor = faceConstructor;
		
		newFaceEdges = new HashMap<NewEdge<Vertex,Edge,Face>, Orientation>();
	}
	
	//add NewEdge to this NewFace subset if it meets the conditions
	//defining this subset--returns true if it is added, false otherwise.
	//orientation is to indicate whether left and right should be swapped from usual
	public boolean addNewEdge(
		NewEdge<Vertex,Edge,Face> ne, NewEdge.FaceAdjacencyData<Face> faceAdjacencyData){
		
		//check condition with usual orientation
		Orientation or = Orientation.POSITIVE;
		if(checkCondition(or, faceAdjacencyData)){
			newFaceEdges.put(ne, or);
			return true;
		}
		//check condition with reverse orientation
		or = or.flip();
		if(checkCondition(or, faceAdjacencyData)){
			newFaceEdges.put(ne, or);
			return true;
		}
		
		return false;
	}
	
	//make sure left contains at least one good face, left contains no bad face,
	//and that if right contains a good face, it also contains a bad face.
	//(i.e., L's must be {G,!B}; R's must be {G,B} or {!G,!G}) 
	private boolean checkCondition(Orientation orientation, NewEdge.FaceAdjacencyData<Face> faceAdjacencyData){
		
		boolean flipLR = 
			(orientation == Orientation.NEGATIVE) ^ baseFaceGetsFlipped;
		
		Face leftFace1 = faceAdjacencyData.getLeftFace1(flipLR);
		Face leftFace2 = faceAdjacencyData.getLeftFace2(flipLR);
		
		boolean leftContainsGood = 
			((leftFace1 != null) && goodFaces.contains(leftFace1)) ||
			((leftFace2 != null) && goodFaces.contains(leftFace2));
		if(!leftContainsGood){ return false; }
		
		boolean leftContainsBad = 
			((leftFace1 != null) && badFaces.contains(leftFace1)) ||
			((leftFace2 != null) && badFaces.contains(leftFace2));
		if(leftContainsBad){ return false; }
		
		Face rightFace1 = faceAdjacencyData.getRightFace1(flipLR);
		Face rightFace2 = faceAdjacencyData.getRightFace2(flipLR);
		
		boolean rightContainsGood = 
			((rightFace1 != null) && goodFaces.contains(rightFace1)) ||
			((rightFace2 != null) && goodFaces.contains(rightFace2));
		if(!rightContainsGood){ return true; }
		
		boolean rightContainsBad =  
			((rightFace1 != null) && badFaces.contains(rightFace1)) ||
			((rightFace2 != null) && badFaces.contains(rightFace2));
		return rightContainsBad;
	}
	
	//after NewEdges have been added, construct the new faces
	//(make sure the faces are connected, otherwise
	public List<Face> constructFaces(){
		if(newFaceEdges.size() < 3){ return new LinkedList<Face>(); }
		
		HashMap<Vertex, EmbeddedVertex<Vertex,Edge>> embVertexLookup = new HashMap<Vertex, EmbeddedVertex<Vertex,Edge>>();
		
		HashSet<EmbeddedVertex<Vertex,Edge>> newFaceEmbVertices = new HashSet<EmbeddedVertex<Vertex,Edge>>();
		HashSet<EmbeddedEdge<Vertex,Edge>> newFaceEmbEdges = new HashSet<EmbeddedEdge<Vertex,Edge>>();
		
		for(NewEdge<Vertex,Edge,Face> ne : newFaceEdges.keySet()){
			Orientation o = newFaceEdges.get(ne);
			
			//mark used in new face, and get edge
			ne.setUsedInNewFace();
			Edge edge = ne.getEdge();
			
			//make embedded vertices
			Vertex gp = edge.getInitialVertex(o);
			Vertex gq = edge.getFinalVertex(o);
			
			EmbeddedVertex<Vertex,Edge> ep = embVertexLookup.get(gp);
			if(ep == null){ 
				ep = new EmbeddedVertex<Vertex,Edge>(basePlane.getProjection(gp.getPosition()) , gp);
				embVertexLookup.put(gp, ep);
				newFaceEmbVertices.add(ep);
			}
			
			EmbeddedVertex<Vertex,Edge> eq = embVertexLookup.get(gq);
			if(eq == null){ 
				eq = new EmbeddedVertex<Vertex,Edge>(basePlane.getProjection(gq.getPosition()) , gq);
				embVertexLookup.put(gq, eq);
				newFaceEmbVertices.add(eq);
			}
			
			//make embedded edge
			EmbeddedEdge<Vertex,Edge> ee = new EmbeddedEdge<Vertex,Edge>(ep, eq, edge, o);
			newFaceEmbEdges.add(ee);
		}
		
		List<Face> faceList = new LinkedList<Face>();
		
		//split newFaceEmbVertices and newFaceEmbEdges into subsets to create connected faces
		List<PolygonComponentData<EmbeddedVertex<Vertex,Edge>, EmbeddedEdge<Vertex,Edge>>> componentDataList = 
			PolygonTools.<EmbeddedVertex<Vertex,Edge>, EmbeddedEdge<Vertex,Edge>>computeConnectedComponents(newFaceEmbVertices, newFaceEmbEdges);
		
		//construct the faces
		for(PolygonComponentData<EmbeddedVertex<Vertex,Edge>, EmbeddedEdge<Vertex,Edge>> componentData : componentDataList){
			faceList.add(faceConstructor.construct(baseFace, basePlane, componentData.vertices, componentData.edges));
		}
		
		return faceList;
	}
}
