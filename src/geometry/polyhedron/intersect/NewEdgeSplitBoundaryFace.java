package geometry.polyhedron.intersect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import geometry.common.components.Edge3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

public class NewEdgeSplitBoundaryFace
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 

	extends NewEdgeSplit<Vertex, Edge, Face>
{
	
	protected Face oppositeFace;

	protected NewEdgeSplitBoundaryFace(
		NewVertex<Vertex,Edge,Face> p, NewVertex<Vertex,Edge,Face> q,
		Intersection<Vertex,Edge,Face> pInt, Intersection<Vertex,Edge,Face> qInt,
		Edge originalEdge, Face oppositeFace,
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
		
		super(p, q, pInt, qInt, originalEdge, edgeConstructor);
		
		this.oppositeFace = oppositeFace;
	}
	
	@Override
	public SplitEdgeType getSplitEdgeType(){ return SplitEdgeType.BOUNDARY_FACE; }
	
	@Override
	public NewEdgeSplitNonBoundary<Vertex,Edge,Face> getAsSplitEdgeNonBoundary(){ return null; }
	
	@Override
	public NewEdgeSplitBoundaryFace<Vertex,Edge,Face> getAsSplitEdgeBoundaryFace(){ return this; }
	
	@Override
	public NewEdgeSplitBoundaryEdge<Vertex,Edge,Face> getAsSplitEdgeBoundaryEdge(){ return null; }
	
	@Override
	public boolean checkIncidence(Face face){
		return ((oppositeFace == face) || originalEdge.checkIncidence(face));
	}
	
	@Override
	public List<Face> getIncidentFaces(){
		
		Set<Face> originalEdgeFaces = originalEdge.getIncidentFaces();
		List<Face> flist = new ArrayList<Face>(originalEdgeFaces.size() + 1);
		
		flist.addAll(originalEdgeFaces);
		flist.add(oppositeFace);
		
		return flist;
	}
	
	public Face getOppositeFace(){
		return oppositeFace;
	}
	
	/**
	 * See new face construction in NewFaceData, and NewFaceSubset
	 */
	@Override
	protected FaceAdjacencyData<Face> computeInfinitesimalFaceAdjacency(Vector3d refNormal, PolyhedronCSGScheme scheme, Collection<Face> mergedFace){

		//grab left and right inf vectors
		Vector3d leftVector = refNormal.cross(getEdgeVector()); leftVector.flip();
		Vector3d rightVector = leftVector.copy(); rightVector.flip();
		
		//adjacency data (put inc faces in 1st slots, oppF in 2nd slots) 
		Face leftFace1 = null, leftFace2 = null, rightFace1 = null, rightFace2 = null;
		
		//face such that new edge is contained in its interior
		Face oppF = getOppositeFace();
		boolean oppFcontained = mergedFace.contains(oppF);
		
		//check adjacency to faces incident to edge
		for(Face incF : getOriginalEdge().getIncidentFaces()){
			if(!mergedFace.contains(incF)){ continue; }
			
			//get inward-pointing edge normal
			Vector3d en = incF.getPlane().getVectorEmbedding(getOriginalEdge().getEmbeddedEdge(incF).getInwardPointingNormal());
			
			//check left vector contained in incF, and on boundary or non-interior of opp mesh
			if(	(en.dot(leftVector) < 0) && 
				(oppFcontained || !scheme.simulateCheckInfinitesimalInterior(oppF, leftVector)) ){
				leftFace1 = incF;
			}
			//check right vector contained in incF, and on boundary or non-interior of opp mesh
			if(	(en.dot(rightVector) < 0) && 
				(oppFcontained || !scheme.simulateCheckInfinitesimalInterior(oppF, rightVector)) ){
				rightFace1 = incF;
			}
		}
		
		//check adjacency to oppF
		if(oppFcontained){
			
			//if left not on boundary of opposite mesh, use inf check to see if in interior
			boolean leftOnBoundary = (leftFace1 != null);
			if(leftOnBoundary || !scheme.simulateCheckInfinitesimalInterior(getOriginalEdge(), leftVector)){
				leftFace2 = oppF;
			}
			//if right not on boundary of opposite mesh, use inf check to see if in interior
			boolean rightOnBoundary = (rightFace1 != null);
			if(rightOnBoundary || !scheme.simulateCheckInfinitesimalInterior(getOriginalEdge(), rightVector)){
				rightFace2 = oppF;
			}
		}
		
		return new FaceAdjacencyData<Face>(leftFace1, leftFace2, rightFace1, rightFace2);
	}
}