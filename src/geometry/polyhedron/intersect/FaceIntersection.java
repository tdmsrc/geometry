package geometry.polyhedron.intersect;

import geometry.math.Vector2d;
import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

class FaceIntersection 
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>>  

	extends Intersection<Vertex,Edge,Face>
{
	
	protected Vector2d coords;
	
	public FaceIntersection(
		PolyhedronComponent<Vertex,Edge,Face> thisObject, PolyhedronComponent<Vertex,Edge,Face> intersectedObject, 
		NewVertex<Vertex,Edge,Face> newVertex, 
		Vector2d coords
	){
		super(thisObject, intersectedObject, newVertex);

		this.coords = coords;
	}

	public Vector2d getCoords(){
		return coords;
	}
}
