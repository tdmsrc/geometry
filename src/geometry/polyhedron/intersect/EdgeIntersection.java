package geometry.polyhedron.intersect;

import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

class EdgeIntersection	
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>>  
	
	extends Intersection<Vertex,Edge,Face>
	implements Comparable<EdgeIntersection<Vertex,Edge,Face>>
{

	protected float lerp;
	
	public EdgeIntersection(
		PolyhedronComponent<Vertex,Edge,Face> thisObject, PolyhedronComponent<Vertex,Edge,Face> intersectedObject,
		NewVertex<Vertex,Edge,Face> newVertex, 
		float lerp
	){
		super(thisObject, intersectedObject, newVertex);

		this.lerp = lerp;
	}
	
	public float getLerp(){
		return lerp;
	}

	@Override
	public int compareTo(EdgeIntersection<Vertex,Edge,Face> rhs){
		if(lerp < rhs.lerp){ return -1; }
		if(lerp > rhs.lerp){ return 1; }
		return 0;
	}
}
