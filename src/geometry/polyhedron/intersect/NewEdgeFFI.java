package geometry.polyhedron.intersect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import geometry.common.components.Edge3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

//NewEdge resulting from a face-face intersection

public class NewEdgeFFI 
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 

	extends NewEdge<Vertex, Edge, Face>
{

	private Face incidentFace0, incidentFace1;
	
	public NewEdgeFFI(
		NewVertex<Vertex, Edge, Face> p, NewVertex<Vertex, Edge, Face> q,
		Face incidentFace0, Face incidentFace1,
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
		
		super(p, q, edgeConstructor);
		
		this.incidentFace0 = incidentFace0;
		this.incidentFace1 = incidentFace1;
		
		//update endpoints' references to include this NewEdgeFFI
		p.getIncidentFFIEdges().add(this);
		q.getIncidentFFIEdges().add(this);
	}

	@Override
	public NewEdgeType getNewEdgeType(){ return NewEdgeType.FACE_FACE_INTERSECTION; }
	
	@Override
	public NewEdgeFFI<Vertex, Edge, Face> getAsFaceFaceIntersection(){ return this; }

	@Override
	public NewEdgeSplit<Vertex, Edge, Face> getAsSplitEdge(){ return null; }
	
	@Override
	public boolean checkIncidence(Face face){
		return ((getIncidentFace0() == face) || (getIncidentFace1() == face));
	}
	
	@Override
	public List<Face> getIncidentFaces(){
		List<Face> flist = new ArrayList<Face>(2);
		flist.add(getIncidentFace0());
		flist.add(getIncidentFace1());
		return flist;
	}
	
	/**
	 * See new face construction in NewFaceData, and NewFaceSubset
	 */
	@Override
	protected FaceAdjacencyData<Face> computeInfinitesimalFaceAdjacency(
		Vector3d refNormal, PolyhedronCSGScheme scheme, Collection<Face> mergedFace){
		
		//grab left and right inf vectors
		Vector3d leftVector = refNormal.cross(getEdgeVector()); leftVector.flip();
		Vector3d rightVector = leftVector.copy(); rightVector.flip();

		//adjacency data (put inc faces in 1st slots, oppF in 2nd slots) 
		Face leftFace1 = null, leftFace2 = null, rightFace1 = null, rightFace2 = null;
		
		//relevant faces
		Face incF0 = getIncidentFace0();
		Face incF1 = getIncidentFace1();
		
		//swap if necessary so that incF0 is contained, and incF1 is not
		if(mergedFace.contains(incF1)){
			Face temp = incF0;
			incF0 = incF1;
			incF1 = temp;
		}
		
		//check left vector on interior of opp mesh
		if(!scheme.simulateCheckInfinitesimalInterior(incF1, leftVector)){
			leftFace1 = incF0;
		}
		//check right vector on interior of opp mesh
		else{ rightFace1 = incF0; }
		
		return new FaceAdjacencyData<Face>(leftFace1, leftFace2, rightFace1, rightFace2);
	}
	
	public Face getIncidentFace0(){
		return incidentFace0;
	}
	
	public Face getIncidentFace1(){
		return incidentFace1;
	}
}
