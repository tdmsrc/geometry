package geometry.polyhedron.intersect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import geometry.common.components.Edge3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

public class NewEdgeSplitBoundaryEdge
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 

	extends NewEdgeSplit<Vertex, Edge, Face>
{
	
	protected Edge oppositeOriginalEdge;

	protected NewEdgeSplitBoundaryEdge(
		NewVertex<Vertex,Edge,Face> p, NewVertex<Vertex,Edge,Face> q,
		Intersection<Vertex,Edge,Face> pInt, Intersection<Vertex,Edge,Face> qInt,
		Edge originalEdge, Edge oppositeOriginalEdge,
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
		
		super(p, q, pInt, qInt, originalEdge, edgeConstructor);
		
		this.oppositeOriginalEdge = oppositeOriginalEdge;
	}
	
	@Override
	public SplitEdgeType getSplitEdgeType(){ return SplitEdgeType.BOUNDARY_EDGE; }
	
	@Override
	public NewEdgeSplitNonBoundary<Vertex,Edge,Face> getAsSplitEdgeNonBoundary(){ return null; }
	
	@Override
	public NewEdgeSplitBoundaryFace<Vertex,Edge,Face> getAsSplitEdgeBoundaryFace(){ return null; }
	
	@Override
	public NewEdgeSplitBoundaryEdge<Vertex,Edge,Face> getAsSplitEdgeBoundaryEdge(){ return this; }
	
	@Override
	public boolean checkIncidence(Face face){
		return (originalEdge.checkIncidence(face) || oppositeOriginalEdge.checkIncidence(face));
	}
	
	@Override
	public List<Face> getIncidentFaces(){
		
		Set<Face> originalEdgeFaces = originalEdge.getIncidentFaces();
		Set<Face> oppositeEdgeFaces = oppositeOriginalEdge.getIncidentFaces();
		List<Face> flist = new ArrayList<Face>(originalEdgeFaces.size() + oppositeEdgeFaces.size());
		
		flist.addAll(originalEdgeFaces);
		flist.addAll(oppositeEdgeFaces);
		
		return flist;
	}
	
	public Edge getOppositeOriginalEdge(){
		return oppositeOriginalEdge;
	}
	
	/**
	 * See new face construction in NewFaceData, and NewFaceSubset
	 */
	@Override
	protected FaceAdjacencyData<Face> computeInfinitesimalFaceAdjacency(Vector3d refNormal, PolyhedronCSGScheme scheme, Collection<Face> mergedFace){
		
		//grab left and right inf vectors
		Vector3d leftVector = refNormal.cross(getEdgeVector()); leftVector.flip();
		Vector3d rightVector = leftVector.copy(); rightVector.flip();

		//adjacency data (put inc faces in 1st slots, opp faces in 2nd slots) 
		Face leftFace1 = null, leftFace2 = null, rightFace1 = null, rightFace2 = null;
		
		//check adjacency to incF faces
		for(Face incF : getOriginalEdge().getIncidentFaces()){
			if(!mergedFace.contains(incF)){ continue; }
					
			//get inward-pointing edge normal
			Vector3d en = incF.getPlane().getVectorEmbedding(getOriginalEdge().getEmbeddedEdge(incF).getInwardPointingNormal());
			
			//check if edge normal is on opp mesh boundary
			boolean onBoundary = false;
			for(Face oppF : getOppositeOriginalEdge().getIncidentFaces()){
				if(!mergedFace.contains(oppF)){ continue; }
				Vector3d enOppF = oppF.getPlane().getVectorEmbedding(getOppositeOriginalEdge().getEmbeddedEdge(oppF).getInwardPointingNormal());
				if(en.dot(enOppF) > 0){ onBoundary = true; break; }
			}
			
			//check left vector contained in incF, and on boundary or non-interior of opp mesh
			if(	(en.dot(leftVector) < 0) && 
				(onBoundary || !scheme.simulateCheckInfinitesimalInterior(getOppositeOriginalEdge(), leftVector)) ){
				leftFace1 = incF;
			}
			//check right vector contained in incF, and on boundary or non-interior of opp mesh
			if(	(en.dot(rightVector) < 0) && 
				(onBoundary || !scheme.simulateCheckInfinitesimalInterior(getOppositeOriginalEdge(), rightVector)) ){
				rightFace1 = incF;
			}
		}
		
		//check adjacency to incF faces
		for(Face oppF : getOppositeOriginalEdge().getIncidentFaces()){
			if(!mergedFace.contains(oppF)){ continue; }
					
			//get inward-pointing edge normal
			Vector3d en = oppF.getPlane().getVectorEmbedding(getOppositeOriginalEdge().getEmbeddedEdge(oppF).getInwardPointingNormal());
			
			//check if edge normal is on opp mesh boundary
			boolean onBoundary = false;
			for(Face incF : getOriginalEdge().getIncidentFaces()){
				if(!mergedFace.contains(incF)){ continue; }
				Vector3d enIncF = incF.getPlane().getVectorEmbedding(getOriginalEdge().getEmbeddedEdge(incF).getInwardPointingNormal());
				if(en.dot(enIncF) > 0){ onBoundary = true; break; }
			}
			
			//check left vector contained in incF, and on boundary or non-interior of opp mesh
			if(	(en.dot(leftVector) < 0) && 
				(onBoundary || !scheme.simulateCheckInfinitesimalInterior(getOriginalEdge(), leftVector)) ){
				leftFace2 = oppF;
			}
			//check right vector contained in incF, and on boundary or non-interior of opp mesh
			if(	(en.dot(rightVector) < 0) && 
				(onBoundary || !scheme.simulateCheckInfinitesimalInterior(getOriginalEdge(), rightVector)) ){
				rightFace2 = oppF;
			}
		}
		
		return new FaceAdjacencyData<Face>(leftFace1, leftFace2, rightFace1, rightFace2);
	}
}