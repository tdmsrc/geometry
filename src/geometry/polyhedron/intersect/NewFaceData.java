package geometry.polyhedron.intersect;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import geometry.common.OrderedPair;
import geometry.common.Orientation;
import geometry.common.PartitionCollection;
import geometry.common.PartitionNode;
import geometry.common.components.Edge3d;
import geometry.common.components.Edge3d.Edge3dConstructor;
import geometry.math.Vector3d;
import geometry.polyhedron.components.Polyhedron;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronTools;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.polyhedron.components.EmbeddedPolygon.Face3dConstructor;
import geometry.polyhedron.components.PolyhedronFace.FaceRecurseMethod;
import geometry.polyhedron.components.PolyhedronFace.RecurseToken;
import geometry.polyhedron.components.PolyhedronTools.CopyFacesData;
import geometry.polyhedron.intersect.PolyhedronCSGScheme.KeepType;

//This class handles and organizes all the NewFaces that
//are created when performing a CSG operation on polyhedra.
//It also handles unmodified faces and unmodified
//combinatorial components of each polyhedron.

public class NewFaceData 
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 
{

	//CSG scheme
	private PolyhedronCSGScheme scheme;
	//constructors
	private Face3dConstructor<Vertex, Edge, Face> faceConstructor;
	
	//new faces created from NewEdges
	private LinkedHashSet<Face> newFaces;
	
	//unmodified combinatorial components sorted by KeepType and polyhedron
	private PartitionCollection<Face> keepComponentsA;
	private PartitionCollection<Face> discardComponentsA;
	private PartitionCollection<Face> keepComponentsB;

	//mod faces + unmod discard faces from mod components
	private LinkedList<Face> discardFacesA;
	//unmod keep component faces + unmod keep faces from mod components
	private LinkedList<Face> keepFacesB;
	//if flipA xor flipFinal, unmod keep component faces + unmod keep faces from mod components; else, empty 
	private LinkedList<Face> flipFacesA;
	
	//polyhedra and glue for PolyA
	private Polyhedron<Vertex,Edge,Face> newFacePolyhedron;
	private LinkedList<OrderedPair<Edge>> newFacePolyhedronGlue;
	
	
	public NewFaceData(
		Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs, 
		IntersectionData<Vertex,Edge,Face> idata, 
		NewEdgeData<Vertex,Edge,Face> nedata,
		PolyhedronCSGScheme scheme,
		Edge3dConstructor<Vertex, Edge> edgeConstructor,
		Face3dConstructor<Vertex, Edge, Face> faceConstructor){
		
		//store scheme and edge constructor
		this.scheme = scheme;
		this.faceConstructor = faceConstructor;
		
		//initialize data structures
		newFaces = new LinkedHashSet<Face>();
		
		keepComponentsA = new PartitionCollection<Face>();
		discardComponentsA = new PartitionCollection<Face>();
		keepComponentsB = new PartitionCollection<Face>();

		//populate data structures:
		
		//construct new faces
		constructNewFaces(nedata);
		
		//organize unmodified components
		organizeUnmodifiedComponents(lhs, rhs, idata);
		
		//get unmodified keep faces from PolyB, and add faces from unmodified keep components
		keepFacesB = organizeUnmodifiedFaces(KeepType.KEEP, 
			idata.getIntersectedFaceListPolyB(), nedata.getBoundaryKeepEdgesB());
		
		for(PartitionNode<Face> root : keepComponentsB.getRoots()){
			 root.addPartitionElementsTo(keepFacesB);
		}

		//get unmodified discard faces from PolyA, and add all modified faces from PolyA
		discardFacesA = organizeUnmodifiedFaces(KeepType.DISCARD, 
			idata.getIntersectedFaceListPolyA(), nedata.getBoundaryDiscardEdgesA());
		
		discardFacesA.addAll(idata.getIntersectedFaceListPolyA());
		
		//if necessary, get unmodified keep faces from PolyA, and add faces from unmodified keep components
		flipFacesA = new LinkedList<Face>();
		
		if(scheme.checkSimulateFlipPolyhedronA() ^ scheme.checkSimulateFlipFinal()){
			flipFacesA = organizeUnmodifiedFaces(KeepType.KEEP, 
				idata.getIntersectedFaceListPolyA(), nedata.getBoundaryKeepEdgesA());
			
			for(PartitionNode<Face> root : keepComponentsA.getRoots()){
				 root.addPartitionElementsTo(flipFacesA);
			}
		}
		
		//compute newFacePolyhedron and its glue
		newFacePolyhedron = constructNewFacePolyhedron(nedata, edgeConstructor);
		newFacePolyhedronGlue = constructNewFacePolyhedronGlue(nedata);
	}

	public PartitionCollection<Face> getDiscardComponentsA(){
		return discardComponentsA;
	}
	
	public PartitionCollection<Face> getKeepComponentsB(){
		return keepComponentsB;
	}
	
	public List<Face> getDiscardFacesA(){
		return discardFacesA;
	}
	
	public List<Face> getKeepFacesB(){
		return keepFacesB;
	}
	
	public List<Face> getFlipFacesA(){
		return flipFacesA;
	}
	
	public Polyhedron<Vertex,Edge,Face> getNewFacePolyhedron(){
		return newFacePolyhedron;
	}
	
	public List<OrderedPair<Edge>> getNewFacePolyhedronGlue(){
		return newFacePolyhedronGlue;
	}
	
	
	//===============================================
	// NEW FACES
	//===============================================
	
	/**
	 * Construct new Faces from NewEdgeData.
	 */
	private void constructNewFaces(NewEdgeData<Vertex,Edge,Face> nedata){
		
		//for each partition in merged faces, associate a collection of newFaceEdges
		HashMap<PartitionNode<Face>,List<NewEdge<Vertex,Edge,Face>>> newFaceEdgesByRoot = new HashMap<PartitionNode<Face>,List<NewEdge<Vertex,Edge,Face>>>(); 

		for(NewEdge<Vertex,Edge,Face> ne : nedata.getNewEdgeList()){
			
			//ignore if NonBoundary with KeepType UNDETERMINED or DISCARD
			if(ne.checkUnusableInNewFace()){ continue; }
			
			//for each incident face, get root, and add to corresponding list of edges
			for(Face face : ne.getIncidentFaces()){
				PartitionNode<Face> node = nedata.getPartitionNode(face);
				if(node == null){ continue; }
				PartitionNode<Face> root = node.getRoot();
				
				//get list, or create if it doesn't exist
				List<NewEdge<Vertex,Edge,Face>> nfelist = newFaceEdgesByRoot.get(root);
				if(nfelist == null){
					nfelist = new LinkedList<NewEdge<Vertex,Edge,Face>>();
					newFaceEdgesByRoot.put(root, nfelist);
				}
				
				//add edge
				nfelist.add(ne);
			}
		}

		//assemble NewFace data for each face equivalence class of overlapping faces
		for(PartitionNode<Face> root : nedata.getMergedFaces().getRoots()){
			
			List<NewEdge<Vertex,Edge,Face>> nfelist = newFaceEdgesByRoot.get(root);
			if(nfelist == null){ continue; }
			
			LinkedList<Face> mergedFaceList = new LinkedList<Face>(); 
			root.addPartitionElementsTo(mergedFaceList);
			constructNewFaces(newFaces, nfelist, mergedFaceList);
		}
	}
	
	private void constructNewFaces(HashSet<Face> newFaces, List<NewEdge<Vertex,Edge,Face>> newFaceEdges, List<Face> mergedFace){
		
		Vector3d refNormal = scheme.simulateGetNormal(mergedFace.get(0));

		//determine NewFaceSubset conditions
		List<NewFaceSubset<Vertex,Edge,Face>> newFaceSubsets = 
			populateSubsetConditionsMerge(refNormal, mergedFace);
		
		//determine face adjacencies and populate NewFaceSubsets
		for(NewEdge<Vertex,Edge,Face> ne : newFaceEdges){
			NewEdge.FaceAdjacencyData<Face> faceAdjacencyData = 
				ne.computeInfinitesimalFaceAdjacency(refNormal, scheme, mergedFace);
			
			//add to appropriate NewFaceSubsets
			for(NewFaceSubset<Vertex,Edge,Face> nfs : newFaceSubsets){ 
				nfs.addNewEdge(ne, faceAdjacencyData);
			}
		}
		
		//get new faces
		for(NewFaceSubset<Vertex,Edge,Face> nfs : newFaceSubsets){
			newFaces.addAll(nfs.constructFaces());
		}
	}
	
	/**
	 * Create subset conditions which merge all faces in mergedFace which have
	 * the same orientation.  If the faces have decorations like textures, this
	 * will cause the textures to bleed when the faces are merged.
	 */
	private List<NewFaceSubset<Vertex,Edge,Face>> populateSubsetConditionsMerge(Vector3d refNormal, List<Face> mergedFace){
		
		LinkedList<NewFaceSubset<Vertex,Edge,Face>> nfsList = new LinkedList<NewFaceSubset<Vertex,Edge,Face>>();
		
		Face basePos = null, baseNeg = null;
		HashSet<Face> posFaces = new HashSet<Face>();
		HashSet<Face> negFaces = new HashSet<Face>();
		
		for(Face face : mergedFace){
			if(scheme.simulateGetNormal(face).dot(refNormal) > 0){
				if(basePos == null){ basePos = face; }
				posFaces.add(face);
			}else{
				if(baseNeg == null){ baseNeg = face; }
				negFaces.add(face);
			}
		}
		
		if(basePos != null){
			nfsList.add(new NewFaceSubset<Vertex,Edge,Face>(basePos, Orientation.POSITIVE, posFaces, negFaces, scheme, faceConstructor));
		}
		if(baseNeg != null){
			nfsList.add(new NewFaceSubset<Vertex,Edge,Face>(baseNeg, Orientation.NEGATIVE, negFaces, posFaces, scheme, faceConstructor));
		}
		
		return nfsList;
	}
	
	//[TODO] create non-merged subset conditions
	//creates subset conditions which guarantee that no faces get merged;
	//this ensure that e.g. textures never bleed out of their original faces
	/*private void populateSubsetConditionsNoMerges(){

		HashSet<Face> posFaces = new HashSet<Face>();
		HashSet<Face> negFaces = new HashSet<Face>();
		
		//get set of positive and negative faces
		for(Face face : mergedFace){
			if(scheme.getNormalSimulateFlip(face).dot(refNormal) > 0){
				posFaces.add(face);
			}
			else{ negFaces.add(face); }
		}
	}*/
	
	
	//===============================================
	// UNMODIFIED COMBINATORIAL COMPONENTS
	//===============================================

	/**
	 * Determine unmodified face partitions for each 
	 * polyhedron, and organize them by KeepTypes as needed.
	 * 
	 * @param lhs The primary polyhedron.
	 * @param rhs The secondary polyhedron.
	 * @param idata The IntersectionData.
	 */
	private void organizeUnmodifiedComponents(
		Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs,
		IntersectionData<Vertex,Edge,Face> idata){
		
		//retrieve set of all components of poly A 
		//[the modified components will be removed in the next step]
		PartitionCollection<Face> unmodifiedComponentsA = new PartitionCollection<Face>(lhs.getFacePartitionCollection());
		
		//get list of modified components, and remove from unmodified set
		PartitionCollection<Face> modifiedComponentsA = new PartitionCollection<Face>();
		for(Face face : idata.getIntersectedFaceListPolyA()){
			PartitionNode<Face> faceNode = face.getFacePartitionNode();
			unmodifiedComponentsA.remove(faceNode);
			modifiedComponentsA.add(faceNode);
		}
		
		//for each unmodified component, use Polyhedron.containsPoint to determine KeepType
		for(PartitionNode<Face> root : unmodifiedComponentsA.getRoots()){
			//find any point on the component
			Face leadFace = root.getObject();
			Vertex faceVertex = leadFace.getVertices().iterator().next().getGlobalVertex();
			Vector3d cPoint = faceVertex.getPosition();
			
			//use Polyhedron.containsPoint to determine KeepType
			boolean inFlippedOpp = rhs.containsPoint(cPoint) ^ scheme.checkSimulateFlip(rhs);  
			if(inFlippedOpp){ discardComponentsA.add(root); }
			else{ keepComponentsA.add(root); }
		}
		
		
		//retrieve set of all components of poly B 
		//[the modified components will be removed in the next step]
		PartitionCollection<Face> unmodifiedComponentsB = new PartitionCollection<Face>(rhs.getFacePartitionCollection());
		
		//get list of modified components, and remove from unmodified set
		PartitionCollection<Face> modifiedComponentsB = new PartitionCollection<Face>();
		for(Face face : idata.getIntersectedFaceListPolyB()){
			PartitionNode<Face> faceNode = face.getFacePartitionNode();
			unmodifiedComponentsB.remove(faceNode);
			modifiedComponentsB.add(faceNode);
		}
		
		//for each unmodified component, use Polyhedron.containsPoint to determine KeepType
		for(PartitionNode<Face> root : unmodifiedComponentsB.getRoots()){
			//find any point on the component
			Face leadFace = root.getObject();
			Vertex faceVertex = leadFace.getVertices().iterator().next().getGlobalVertex();
			Vector3d cPoint = faceVertex.getPosition();
			
			//use Polyhedron.containsPoint to determine KeepType
			boolean inFlippedOpp = lhs.containsPoint(cPoint) ^ scheme.checkSimulateFlip(lhs);  
			if(!inFlippedOpp){ keepComponentsB.add(root); }
		}
	}

	
	//===============================================
	// UNMODIFIED FACES
	//===============================================

	/**
	 * Get the unmodified faces of the specified KeepType by recursing over
	 * each face incident to each unmodified edge of the specified KeepType.
	 * 
	 * This method bypasses checking faces of the KeepType opposite the one specified.
	 * 
	 * This assumes that Polyhedron faces are connected; if not, any face with a single
	 * discarded component will get discarded in its entirety, even if there is a
	 * component that should be kept.
	 * 
	 * @param keepType The KeepType the unmodified faces are to have.
	 * @param modifiedFaces The list of modified faces--these form a boundary between
	 * faces of different KeepTypes.
	 * @param unmodifiedEdges The list of edges on modified faces which have no 
	 * intersection on the interior or endpoints.  Every face incident to such edges 
	 * with specified KeepType are either modified faces, or unmodified faces with 
	 * the specified KeepType which are incident to a modified face.
	 * @return The collection of unmodified faces of the specified KeepType.
	 */
	private LinkedList<Face> organizeUnmodifiedFaces(
		KeepType keepType,
		final Set<Face> modifiedFaces, 
		Collection<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> unmodifiedEdges){
		
		final LinkedList<Face> unmodifiedFaces = new LinkedList<Face>();

		//one token for all "recurse" calls, so they don't ever overlap
		RecurseToken token = PolyhedronFace.generateRecurseToken();
		
		//method to call for each face
		FaceRecurseMethod<Face> recurseMethod = new FaceRecurseMethod<Face>(){
			@Override
			public boolean action(Face previousFace, Face thisFace) {
				if(modifiedFaces.contains(thisFace)){ return false; }
				unmodifiedFaces.add(thisFace);
				return true;
			}
		};
		
		for(NewEdgeSplitNonBoundary<Vertex,Edge,Face> ne : unmodifiedEdges){
			//ignore if not the specified KeepType
			if(ne.getKeepType() != keepType){ continue; }
			
			//recurse on every incident face
			for(Face face : ne.getOriginalEdge().getIncidentFaces()){
				face.recurse(null, recurseMethod, token);
			}
		}
		
		return unmodifiedFaces;
	}
	
	//===============================================
	// POLYHEDRA AND GLUE
	//===============================================

	private Polyhedron<Vertex,Edge,Face> constructNewFacePolyhedron(
		NewEdgeData<Vertex,Edge,Face> nedata,
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
		
		//construct polyhedron from new faces
		newFacePolyhedron = new Polyhedron<Vertex,Edge,Face>(newFaces);
		
		//glue keepFacesB (which includes faces from keepComponentsB) onto newFacePolyhedron:
		
		//copy all the faces in keepFacesB into a new polyhedron
		CopyFacesData<Vertex,Edge,Face> copyFacesData = 
			PolyhedronTools.<Vertex,Edge,Face>copyFaces(keepFacesB, edgeConstructor, faceConstructor);
		
		//flip these if necessary (only in A_MINUS_B scheme)
		//[keepFacesB should have unmodified keep faces, and faces from keep components, of PolyB]
		if(scheme.checkSimulateFlipPolyhedronB() ^ scheme.checkSimulateFlipFinal()){
			copyFacesData.polyhedron.flip();
		}
		
		//make glue [A object from newFacePolyhedron, B object from keepFacesB polyhedron]
		LinkedList<OrderedPair<Edge>> glueB = new LinkedList<OrderedPair<Edge>>(); 
		for(NewEdgeSplitNonBoundary<Vertex,Edge,Face> ne : nedata.getBoundaryKeepEdgesB()){
			glueB.add(new OrderedPair<Edge>(
				ne.getEdge(),
				copyFacesData.newEdgeLookup.get(ne.getOriginalEdge())
			));
		}
		
		//glue
		newFacePolyhedron.glue(copyFacesData.polyhedron, glueB);
		
		//return glued polyhedron
		return newFacePolyhedron;
	}
	
	private LinkedList<OrderedPair<Edge>> constructNewFacePolyhedronGlue(
		NewEdgeData<Vertex,Edge,Face> nedata){
		
		//make newFacePolyhedron glue
		//[A object from PolyA, B object from newFacePolyhedron]
		LinkedList<OrderedPair<Edge>> glueA = new LinkedList<OrderedPair<Edge>>();
		
		for(NewEdgeSplitNonBoundary<Vertex,Edge,Face> ne : nedata.getBoundaryKeepEdgesA()){
			glueA.add(new OrderedPair<Edge>(ne.getOriginalEdge(), ne.getEdge()));
		}
		
		return glueA;
	}
}
