package geometry.polyhedron.intersect;

import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

//Intersection of some object with another object.
//
//This should be stored in a context where the first object is known,
//and the second object is stored here as "intersected".

class Intersection
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 
{

	protected NewVertex<Vertex,Edge,Face> newVertex;
	protected PolyhedronComponent<Vertex,Edge,Face> thisObject, intersectedObject;
	
	public Intersection(
		PolyhedronComponent<Vertex,Edge,Face> thisObject, PolyhedronComponent<Vertex,Edge,Face> intersectedObject,
		NewVertex<Vertex,Edge,Face> newVertex
	){
		this.newVertex = newVertex;
		
		this.thisObject = thisObject;
		this.intersectedObject = intersectedObject;
	}
	
	/**
	 * Get the new vertex created by this intersection.
	 * 
	 * @return The new vertex created by this intersection.
	 */
	public NewVertex<Vertex,Edge,Face> getNewVertex(){
		return newVertex;
	}
	
	/**
	 * Get the primary object that this intersection is on.
	 * 
	 * @return The primary object that was intersected.
	 */
	public PolyhedronComponent<Vertex,Edge,Face> getThisObject(){
		return thisObject;
	}
	
	/**
	 * Get the object that the primary object intersected.
	 * 
	 * @return The object that the primary object intersected.
	 */
	public PolyhedronComponent<Vertex,Edge,Face> getIntersectedObject(){
		return intersectedObject;
	}
}
