package geometry.polyhedron.intersect;

import geometry.math.Vector3d;
import geometry.polyhedron.components.Polyhedron;
import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronFace;


/**
 * The CSG operation is by default a union.  Other CSG operations can be performed
 * by flipping the orientation of one or both of the Polyhedra before and/or after
 * performing the union operation.  This can be done efficiently by <i>simulating</i>
 * it during the operation, instead of actually performing the flip.  
 * The PolyhedronCSGScheme keeps track of, and provides methods for simulating, the desired flips.
 */
public class PolyhedronCSGScheme{

	public static enum KeepType{ 
		UNDETERMINED, KEEP, DISCARD 
	}
	
	public static enum CSGType{
		A_UNION_B, A_INTERSECT_B, A_MINUS_B, B_MINUS_A	
	}
	
	protected Polyhedron<?,?,?> polyA, polyB;
	protected CSGType csgType;
	
		
	public PolyhedronCSGScheme(
		Polyhedron<?,?,?> polyA, Polyhedron<?,?,?> polyB, 
		CSGType csgType){
		
		this.polyA = polyA; this.polyB = polyB;
		this.csgType = csgType;
	}
	
	public boolean checkSimulateFlip(Polyhedron<?,?,?> thisPolyhedron){
		switch(csgType){
		case A_UNION_B:		return false;
		case A_INTERSECT_B:	return true;
		case A_MINUS_B:		return (thisPolyhedron == polyA);
		case B_MINUS_A:		return (thisPolyhedron == polyB);
		}
		return false;
	}
	
	public boolean checkSimulateFlipFinal(){
		switch(csgType){
		case A_UNION_B:		return false;
		case A_INTERSECT_B:	return true;
		case A_MINUS_B:		return true;
		case B_MINUS_A:		return true;
		}
		return false;
	}
	
	public boolean checkOwnedByPolyhedronA(PolyhedronComponent<?,?,?> polyhedronComponent){
		return (polyhedronComponent.getOwner() == polyA);
	}
	
	public boolean checkOwnedByPolyhedronB(PolyhedronComponent<?,?,?> polyhedronComponent){
		return (polyhedronComponent.getOwner() == polyB);
	}
	
	public boolean checkSimulateFlipPolyhedronA(){
		return checkSimulateFlip(polyA);
	}
	
	public boolean checkSimulateFlipPolyhedronB(){
		return checkSimulateFlip(polyB);
	}
	
	//==============================
	// SIMULATION
	//==============================

	public boolean simulatePolyAContainsPoint(Vector3d p){
		return polyA.containsPoint(p) ^ checkSimulateFlip(polyA);
	}
	
	public boolean simulatePolyBContainsPoint(Vector3d p){
		return polyB.containsPoint(p) ^ checkSimulateFlip(polyB);
	}
	
	public Vector3d simulateGetNormal(PolyhedronFace<?,?,?> face){
		
		Vector3d n = face.getPlane().getNormal().copy();
		if(checkSimulateFlip(face.getOwner())){ n.flip(); }
		return n;
	}
	
	public boolean simulateCheckInfinitesimalInterior(PolyhedronComponent<?,?,?> polyhedronComponent, Vector3d v){
		
		return	polyhedronComponent.checkInfinitesimalInterior(v) ^ 
				checkSimulateFlip(polyhedronComponent.getOwner());
	}
}
