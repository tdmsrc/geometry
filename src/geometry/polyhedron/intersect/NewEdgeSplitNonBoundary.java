package geometry.polyhedron.intersect;

import java.util.Collection;
import java.util.Set;

import geometry.common.components.Edge3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.polyhedron.intersect.PolyhedronCSGScheme.KeepType;

public class NewEdgeSplitNonBoundary 
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 

	extends NewEdgeSplit<Vertex, Edge, Face>
{
	
	protected KeepType keepType;
	

	protected NewEdgeSplitNonBoundary(
		NewVertex<Vertex,Edge,Face> p, NewVertex<Vertex,Edge,Face> q,
		Intersection<Vertex,Edge,Face> pInt, Intersection<Vertex,Edge,Face> qInt,
		Edge originalEdge, PolyhedronCSGScheme scheme, 
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
		
		super(p, q, pInt, qInt, originalEdge, edgeConstructor);
		
		this.keepType = KeepType.UNDETERMINED;
		computeSplitEdgeNonBoundaryType(scheme);
	}
	
	@Override
	public SplitEdgeType getSplitEdgeType(){ return SplitEdgeType.NON_BOUNDARY; }
	
	@Override
	public NewEdgeSplitNonBoundary<Vertex,Edge,Face> getAsSplitEdgeNonBoundary(){ return this; }
	
	@Override
	public NewEdgeSplitBoundaryFace<Vertex,Edge,Face> getAsSplitEdgeBoundaryFace(){ return null; }
	
	@Override
	public NewEdgeSplitBoundaryEdge<Vertex,Edge,Face> getAsSplitEdgeBoundaryEdge(){ return null; }
	
	@Override
	public boolean checkIncidence(Face face){
		return originalEdge.checkIncidence(face);
	}
	
	@Override
	public Set<Face> getIncidentFaces(){
		return originalEdge.getIncidentFaces();
	}
	
	public void setKeepType(KeepType keepType){
		this.keepType = keepType;
	}
	
	public KeepType getKeepType(){
		return keepType;
	}
	
	/**
	 * See new face construction in NewFaceData, and NewFaceSubset
	 */
	@Override
	protected FaceAdjacencyData<Face> computeInfinitesimalFaceAdjacency(Vector3d refNormal, PolyhedronCSGScheme scheme, Collection<Face> mergedFace){

		//grab left and right inf vectors
		Vector3d leftVector = refNormal.cross(getEdgeVector()); leftVector.flip();
		Vector3d rightVector = leftVector.copy(); rightVector.flip();
				
		//adjacency data (put inc faces in 1st slots, oppF in 2nd slots) 
		Face leftFace1 = null, leftFace2 = null, rightFace1 = null, rightFace2 = null;
				
		//check adjacency to each incident face
		for(Face incF : getOriginalEdge().getIncidentFaces()){
			if(!mergedFace.contains(incF)){ continue; }

			//get inward-pointing edge normal
			Vector3d en = incF.getPlane().getVectorEmbedding(getOriginalEdge().getEmbeddedEdge(incF).getInwardPointingNormal());
			
			//check left vector contained in incF
			if(en.dot(leftVector) < 0){ leftFace1 = incF; }
			//check right vector contained in incF
			else{ rightFace1 = incF; }
		}
		
		return new FaceAdjacencyData<Face>(leftFace1, leftFace2, rightFace1, rightFace2);
	}
	
	//determine KeepType using infinitesimal check
	//return true if type was determined, false otherwise
	private boolean computeSplitEdgeNonBoundaryType(PolyhedronCSGScheme scheme){
			
		//get vector starting at q and ending at p
		Vector3d v = new Vector3d(getInitialVertex().getPosition());
		v.subtract(getFinalVertex().getPosition());
			
		//determine which endpoint to do inf check on
		PolyhedronComponent<Vertex,Edge,Face> pc = null;
		
		if(pInt == null){
			if(qInt == null){ return false; }
			else{
				v.flip();
				pc =  qInt.getIntersectedObject();
			}
		}else{
			pc = pInt.getIntersectedObject();
			if(qInt != null){
				//if neither null, do the easiest, with order of preference: face, edge, vertex
				int easep = 0;
				switch(pInt.getIntersectedObject().getPolyhedronComponentType()){
				case FACE: easep = 3; break;
				case EDGE: easep = 2; break;
				case VERTEX: easep = 1; break;
				}
	
				int easeq = 0;
				switch(qInt.getIntersectedObject().getPolyhedronComponentType()){
				case FACE: easeq = 3; break;
				case EDGE: easeq = 2; break;
				case VERTEX: easeq = 1; break;
				}
				
				if(easeq > easep){ 
					v.flip();
					pc = qInt.getIntersectedObject();
				}
			}
		}

		keepType = 	scheme.simulateCheckInfinitesimalInterior(pc, v) ? 
					KeepType.DISCARD : KeepType.KEEP;
		
		return true;
	}
	
	/**
	 * If this KeepType is known, and either endpoint is a non-intersection,
	 * then every split edge with undetermined type incident to such an 
	 * endpoint is set to have the same KeepType as this.  This is
	 * then propagated recursively to each such updated split edge.
	 */
	public void propagateType(){

		if(keepType == KeepType.UNDETERMINED){ return; }
		
		if(pInt == null){ propagateTypeRecurse(getFinalVertex()); }
		else if(qInt == null){ propagateTypeRecurse(getInitialVertex()); }	
	}
	
	private void propagateTypeRecurse(NewVertex<Vertex,Edge,Face> callingVertex){
		
		//if the endpoint that is not callingVertex has no intersection, call this 
		//method on any of its incident NonBoundary SplitEdges with undetermined type
		NewVertex<Vertex,Edge,Face> opVertex = getInitialVertex();
		Intersection<Vertex,Edge,Face> opInt = pInt;
		if(callingVertex == getInitialVertex()){
			opVertex = getFinalVertex();
			opInt = qInt;
		}
		
		if(opInt != null){ return; }
		
		for(NewEdgeSplit<Vertex,Edge,Face> splitEdge : opVertex.getIncidentSplitEdges()){
			//ignore if not NON_BOUNDARY type (although all of them will be, since 
			//they are connected to a non-intersection vertex--BOUNDARY impossible).
			if(splitEdge.getSplitEdgeType() != SplitEdgeType.NON_BOUNDARY){ continue; }
			//get as NON_BOUNDARY SplitEdge
			NewEdgeSplitNonBoundary<Vertex,Edge,Face> splitEdgeNB = splitEdge.getAsSplitEdgeNonBoundary();
			
			if(splitEdgeNB.keepType == KeepType.UNDETERMINED){
				splitEdgeNB.keepType = keepType;
				splitEdgeNB.propagateTypeRecurse(opVertex);
			}
		}
	}
}
