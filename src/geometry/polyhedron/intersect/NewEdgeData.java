package geometry.polyhedron.intersect;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import geometry.common.Epsilon;
import geometry.common.MessageOutput;
import geometry.common.OrderedPair;
import geometry.common.PartitionCollection;
import geometry.common.PartitionNode;
import geometry.common.components.Edge3d;
import geometry.math.Vector3d;
import geometry.polygon.components.PolygonTools;
import geometry.polygon.components.PolygonTools.ClosestObjectData;
import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronTools;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.polyhedron.intersect.NewEdgeSplit.SplitEdgeType;
import geometry.polyhedron.intersect.PolyhedronCSGScheme.KeepType;

//This class handles and organizes all the NewEdges that
//are created when performing a CSG operation on polyhedra.

public class NewEdgeData
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 
{
	
	//CSG scheme
	private PolyhedronCSGScheme scheme;
	//Edge constructor
	private Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor;
	
	//number of warnings issued
	private int warningCount = 0, errorCount = 0;
	
	//intersection data
	private IntersectionData<Vertex,Edge,Face> idata;
		
	//list of all NewEdges
	private LinkedList<NewEdge<Vertex,Edge,Face>> newEdges;
	//all NewEdges, separated by type
	private LinkedList<NewEdgeSplit<Vertex,Edge,Face>> splitEdges;
	private LinkedList<NewEdgeFFI<Vertex,Edge,Face>> ffiEdges;
	
	//new edges created which are simply copies of old edges
	private LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> unmodifiedNewEdgesA;
	private LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> unmodifiedNewEdgesB;
	
	//unmodified new edges which are KEEP and incident to at least one unmodified face
	private LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> boundaryKeepEdgesA;
	private LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> boundaryKeepEdgesB;
	private LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> boundaryDiscardEdgesA;
	private LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> boundaryDiscardEdgesB;
	
	//overlapping face info
	private HashMap<Face, PartitionNode<Face>> partitionNodeLookup;
	private PartitionCollection<Face> mergedFaces;
	
	
	//====================================================
	// CONSTRUCTORS
	//====================================================
	
	private NewEdgeData(
		IntersectionData<Vertex,Edge,Face> idata, 
		PolyhedronCSGScheme scheme, 
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
		
		//store scheme, edge constructor, and previous data
		this.scheme = scheme;
		this.edgeConstructor = edgeConstructor;
		
		this.idata = idata;
		
		//initialize data structures
		newEdges = new LinkedList<NewEdge<Vertex,Edge,Face>>();
		splitEdges = new LinkedList<NewEdgeSplit<Vertex,Edge,Face>>();
		ffiEdges = new LinkedList<NewEdgeFFI<Vertex,Edge,Face>>();
		
		unmodifiedNewEdgesA = new LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>>();
		unmodifiedNewEdgesB = new LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>>();
		
		boundaryKeepEdgesA = new LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>>();
		boundaryKeepEdgesB = new LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>>();
		boundaryDiscardEdgesA = new LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>>();
		boundaryDiscardEdgesB = new LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>>();
		
		partitionNodeLookup = new HashMap<Face, PartitionNode<Face>>();
		mergedFaces = new PartitionCollection<Face>();
	}
	
	public static 		
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>> 
	
	List<NewEdge<Vertex,Edge,Face>> constructIntersectionSkeleton(
		IntersectionData<Vertex,Edge,Face> idata, 
		PolyhedronCSGScheme scheme, 
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
	
		NewEdgeData<Vertex,Edge,Face> ned = new NewEdgeData<Vertex,Edge,Face>(idata, scheme, edgeConstructor);
		
		//see comments in constructNewEdgeData below for what these do
		ned.computeSplitEdges();
		ned.computeMergedFaces();
		ned.computeFFIEdges();
		
		//assemble list of NewEdgeFFI, NewEdgeSplitBoundaryEdge, and NewEdgeSplitBoundaryFace
		List<NewEdge<Vertex,Edge,Face>> skeletonEdges = new LinkedList<NewEdge<Vertex,Edge,Face>>();
		skeletonEdges.addAll(ned.ffiEdges);
		for(NewEdgeSplit<Vertex,Edge,Face> nes : ned.splitEdges){
			if((nes.getSplitEdgeType() == SplitEdgeType.BOUNDARY_EDGE) || (nes.getSplitEdgeType() == SplitEdgeType.BOUNDARY_FACE)){
				skeletonEdges.add(nes);
			}
		}
		
		return skeletonEdges;
	}
	
	public static 		
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>> 
	
	NewEdgeData<Vertex,Edge,Face> constructNewEdgeData(
		IntersectionData<Vertex,Edge,Face> idata, 
		PolyhedronCSGScheme scheme, 
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
	
		NewEdgeData<Vertex,Edge,Face> ned = new NewEdgeData<Vertex,Edge,Face>(idata, scheme, edgeConstructor);
		
		//populate data structures:
		
		//split up original edges at NewVertices, and determine SplitEdgeType when possible
		//do this for all edges incident to an intersected face (i.e., on interior or any sub-component)
		ned.computeSplitEdges();
		
		//determine list of face pairs which have overlapping interiors 
		//[uses splitEdges, but not types]
		ned.computeMergedFaces();
		
		//check for edges formed by transverse intersection of face interiors 
		//[uses merged faces; uses splitEdges, but not types]
		ned.computeFFIEdges();
		
		//compute remaining split edge non-boundary types
		//[for this to finish finding all types, polyhedron faces must be connected]
		ned.propagateNonBoundarySplitEdgeTypes();
		ned.computeSplitEdgeTypesByInfCheck();
		
		//compute boundary edges [all split-edge types are necessary]
		ned.getBoundaryEdges(ned.boundaryKeepEdgesA, ned.boundaryDiscardEdgesA, idata.getIntersectedFaceListPolyA(), ned.unmodifiedNewEdgesA);
		ned.getBoundaryEdges(ned.boundaryKeepEdgesB, ned.boundaryDiscardEdgesB, idata.getIntersectedFaceListPolyB(), ned.unmodifiedNewEdgesB);
		
		return ned;
	}
	
	public int getWarningCount(){ return warningCount; }
	public int getErrorCount(){ return errorCount; }
	
	//====================================================
	
	
	/**
	 * Get list of all NewEdges.
	 * 
	 * @return The list of all NewEdges.
	 */
	public LinkedList<NewEdge<Vertex,Edge,Face>> getNewEdgeList(){
		return newEdges;
	}
	
	/**
	 * Get list of all NewEdges which are created from old edges or from
	 * splitting old edges between intersection points.
	 * 
	 * @return List of NewEdgeSplits.
	 */
	public LinkedList<NewEdgeSplit<Vertex,Edge,Face>> getSplitNewEdges(){
		return splitEdges;
	}
	
	/**
	 * Get list of all NewEdges which are created from the transverse
	 * intersection of face interiors.
	 * 
	 * @return List of NewEdgeFFIs.
	 */
	public LinkedList<NewEdgeFFI<Vertex,Edge,Face>> getFFINewEdges(){
		return ffiEdges;
	}
	
	/**
	 */
	public LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> getBoundaryKeepEdgesA(){
		return boundaryKeepEdgesA;
	}
	
	/**
	 */
	public LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> getBoundaryKeepEdgesB(){
		return boundaryKeepEdgesB;
	}
	
	/**
	 */
	public LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> getBoundaryDiscardEdgesA(){
		return boundaryDiscardEdgesA;
	}
	
	/**
	 */
	public LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> getBoundaryDiscardEdgesB(){
		return boundaryDiscardEdgesB;
	}
	
	/**
	 * Get the partition root node for a given face.
	 * 
	 * @return The partition root node for the given face.
	 */
	public PartitionNode<Face> getPartitionNode(Face face){
		
		return partitionNodeLookup.get(face);
	}
	
	/**
	 * Get a structure which indicates which modified faces have overlapping interiors.
	 * 
	 * @return A structure which indicates which modified faces have overlapping interiors.
	 */
	public PartitionCollection<Face> getMergedFaces(){
		return mergedFaces;
	}

	
	//===============================================
	// SPLIT NEW EDGES
	//===============================================

	/**
	 * Create NewEdges resulting from splitting old edges at NewVertices.
	 * This is done for every edge incident to faces which have an intersection on 
	 * the interior or any sub-components (i.e., intersected/modified faces).
	 * 
	 * @param intersectionData The IntersectionData object.
	 */
	private void computeSplitEdges(){

		//for all vertices which are contained in the faces in intersectedFaces, there must be
		//an associated NewVertex; if there is an intersection at a vertex, then this can be 
		//found, but if there isn't, a new NewVertex needs to be made [will have only 1 intersection component, rather than 2]
		//this lookup will keep track of the NewVertices created in this way, so as not to duplicate.
		HashMap<Vertex, NewVertex<Vertex,Edge,Face>> newVertexLookup = new HashMap<Vertex, NewVertex<Vertex,Edge,Face>>();
		
		//make a list of all edges which are incident to the faces in intersectedFaces.
		HashSet<Edge> edgeList = new HashSet<Edge>();
		for(Face face : idata.getIntersectedFaceListPolyA()){
			edgeList.addAll(face.getGlobalEdges());
		}
		for(Face face : idata.getIntersectedFaceListPolyB()){
			edgeList.addAll(face.getGlobalEdges());
		}
		
		//Make a NewEdge for each edge in edgeList, 
		//splitting up the edge at intersection points if there are any.
		for(Edge e : edgeList){
			
			//get data for endpoints:
			NewVertex<Vertex,Edge,Face> ep, eq;
			Intersection<Vertex,Edge,Face> epInt, eqInt;
			
			//check if there is a no-intersection newVertex in the lookup at initial point
			Vertex v = e.getInitialVertex();
			ep = newVertexLookup.get(v); epInt = null;
			if(ep == null){
				//check if there is an intersection at the initial point
				IntersectionList<Vertex,Edge,Face,Intersection<Vertex,Edge,Face>> vInts = idata.getVertexIntersections(v);
				if(vInts == null){
					//no intersection, so make a new NewVertex with one component null
					ep = new NewVertex<Vertex,Edge,Face>(v.copy(), v, null);
					newVertexLookup.put(v, ep);
				}else{
					epInt = vInts.getFirst();
					ep = epInt.getNewVertex();
					//[TODO] if > 1 intersections, pinch point
					if(vInts.size() > 1){ 
						MessageOutput.printWarning("Intersection at vertex pinch point");
						warningCount++;
					}
				}
			}
			
			//check if there is a no-intersection newVertex in the lookup at final point
			Vertex w = e.getFinalVertex();
			eq = newVertexLookup.get(w); eqInt = null;
			if(eq == null){
				//check if there is an intersection at the initial point
				IntersectionList<Vertex,Edge,Face,Intersection<Vertex,Edge,Face>> wInts = idata.getVertexIntersections(w);
				if(wInts == null){
					//no intersection, so make a new NewVertex with one component null
					eq = new NewVertex<Vertex,Edge,Face>(w.copy(), w, null);
					newVertexLookup.put(w, eq);
				}else{
					eqInt = wInts.getFirst();
					eq = eqInt.getNewVertex();
					//[TODO] if > 1 intersections, pinch point
					if(wInts.size() > 1){ 
						MessageOutput.printWarning("Intersection at vertex pinch point");
						warningCount++;
					}
				}
			}
			
			//split this edge
			computeSplitEdges(ep, eq, epInt, eqInt, e, idata.getEdgeIntersections(e));
		}
	}
	
	/**
	 * Compute NewEdges resulting from splitting originalEdge at NewVertices.
	 * 
	 * The parameters ep, eq, epInt, and eqInt represent the data at the endpoints
	 * of the original edge.
	 */
	private void computeSplitEdges(
		NewVertex<Vertex,Edge,Face> ep, NewVertex<Vertex,Edge,Face> eq, 
		Intersection<Vertex,Edge,Face> epInt, Intersection<Vertex,Edge,Face> eqInt,
		Edge originalEdge,
		IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>> edgeIntersections){
		
		//if there aren't any edge intersections, create a NewEdgeSplit with the same 
		//endpoints as e; if morever both intersections are null, it is unmodified
		if(edgeIntersections == null){ 
			addSplitNewEdge(ep, epInt, eq, eqInt, originalEdge, (epInt == null) && (eqInt == null)); 
			return;
		}

		//sort edge intersections by lerp value
		sortEdgeIntersections(edgeIntersections);
		
		//split the edge e
		NewVertex<Vertex,Edge,Face> p, q;
		Intersection<Vertex,Edge,Face> pInt, qInt;
		
		//set p = initial point of edge
		p = ep; pInt = epInt;
		for(EdgeIntersection<Vertex,Edge,Face> ei : edgeIntersections){
			//set q = the new vertex at this intersection
			q = ei.getNewVertex(); qInt = ei;
			
			//create NewEdgeSplit from p to q
			addSplitNewEdge(p, pInt, q, qInt, originalEdge, false);
			
			//set initial point of next split edge to the endpoint of the one just created
			p = q; pInt = qInt;
		}
		//set q = endpoint of edge
		q = eq; qInt = eqInt;
		
		//create NewEdgeSplit from p to q
		addSplitNewEdge(p, pInt, q, qInt, originalEdge, false);
	}
	
	/**
	 * Sort the intersections which occur on an edge by the edge lerp value.
	 */
	private void sortEdgeIntersections(
		IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>> edgeIntersections){

		Collections.sort(edgeIntersections);
	}
	
	/**
	 * Update data structures to reflect a new NewEdgeSplit (if non-null).
	 */
	private void addSplitNewEdge(
		NewVertex<Vertex,Edge,Face> p, Intersection<Vertex,Edge,Face> pInt,
		NewVertex<Vertex,Edge,Face> q, Intersection<Vertex,Edge,Face> qInt,
		Edge originalEdge, boolean unmodified){
		
		//[TODO] warn about pinch point
		if(p.getPosition().distance(q.getPosition()) < Epsilon.get()){
			MessageOutput.printWarning("Pinch point while creating split edge");
			warningCount++;
		}
		
		//create and add edge
		NewEdgeSplit<Vertex,Edge,Face> newSplitEdge = constructNewEdgeSplit(p, q, pInt, qInt, originalEdge);
		if(newSplitEdge != null){
			splitEdges.add(newSplitEdge);
			newEdges.add(newSplitEdge);
			if(unmodified){ addUnmodifiedEdge(newSplitEdge.getAsSplitEdgeNonBoundary()); }
		}
	}
	
	/**
	 * Update data structures to reflect an unmodified edge.
	 */
	private void addUnmodifiedEdge(NewEdgeSplitNonBoundary<Vertex,Edge,Face> ne){

		(scheme.checkOwnedByPolyhedronA(ne.getOriginalEdge()) ? 
			unmodifiedNewEdgesA : unmodifiedNewEdgesB).add(ne);
	}
	
	/**
	 * Returns a new NewEdgeSplit from the specified endpoints.
	 * 
	 * If an endpoint NewVertex object was created from an intersection,
	 * the corresponding Intersection object should be the one with 
	 * "thisObject" equal to originalEdge or an endpoint of originalEdge.
	 * 
	 * If an endpoint NewVertex was created from an old vertex, the
	 * corresponding Intersection object should be null.
	 * 
	 * This may return null, in the case a NewEdgeSplitBoundaryEdge
	 * was already created with the specified endpoints.
	 * 
	 * @return The new NewEdgeSplit, or null if it would be a duplicate.
	 * @see computeSplitEdges
	 */
	private NewEdgeSplit<Vertex,Edge,Face> constructNewEdgeSplit(
		NewVertex<Vertex,Edge,Face> p, NewVertex<Vertex,Edge,Face> q,
		Intersection<Vertex,Edge,Face> pInt, Intersection<Vertex,Edge,Face> qInt,
		Edge originalEdge){
		
		//only of BOUNDARY type if both endpoints have intersections
		if((pInt != null) && (qInt != null)){
			
			//get the objects that originalEdge (or its endpoints) intersected
			//to create p and q.
			PolyhedronComponent<Vertex,Edge,Face> pObj = pInt.getIntersectedObject();
			PolyhedronComponent<Vertex,Edge,Face> qObj = qInt.getIntersectedObject();
		
			//if pObj and qObj are contained in a common edge, BOUNDARY_EDGE
			Edge commonEdge = PolyhedronTools.<Vertex,Edge,Face>getCommonEdgeContainment(pObj, qObj);
			if(commonEdge != null){
				//make sure this isn't a duplicate (BOUNDARY_EDGEs will always be duplicated)
				//if it is, do not want to create a NewEdge at all, so return null.
				if(PolyhedronCSG.<Vertex,Edge,Face>findCommonSplitEdge(p, q) != null){ return null; }
				return new NewEdgeSplitBoundaryEdge<Vertex,Edge,Face>(p, q, pInt, qInt, originalEdge, commonEdge, edgeConstructor); 
			}
			
			//if pObj and qObj are contained in a common face, *may be* BOUNDARY_FACE
			Face commonFace = PolyhedronTools.<Vertex,Edge,Face>getCommonFaceContainment(pObj, qObj);
			if(commonFace != null){
				//since faces are not convex, need to do 2d inf check to make sure the
				//split edge is actually on the interior of the face (otherwise NonBoundary)
				if(PolyhedronCSG.<Vertex,Edge,Face>checkEdgeOnFaceInterior(p, q, commonFace)){
					return new NewEdgeSplitBoundaryFace<Vertex,Edge,Face>(p, q, pInt, qInt, originalEdge, commonFace, edgeConstructor); 
				}
			}
		}
		
		//if a NewEdgeSplitBoundaryX has not been returned at this point, this is
		//definitely a NewEdgeSplitNonBoundary.
		return new NewEdgeSplitNonBoundary<Vertex,Edge,Face>(p, q, pInt, qInt, originalEdge, scheme, edgeConstructor); 
	}

	
	//===============================================
	// FACE MERGES
	//===============================================

	/**
	 * Create a structure which indicates which modified faces have overlapping interiors.
	 * 
	 * @param intersectionData The IntersectionData object.
	 */
	private void computeMergedFaces(){

		//initialize equivalence class collection
		for(Face face : idata.getIntersectedFaceListPolyA()){
			partitionNodeLookup.put(face, mergedFaces.add(face));
		}
		for(Face face : idata.getIntersectedFaceListPolyB()){
			partitionNodeLookup.put(face, mergedFaces.add(face));
		}
		
		//look through every face pair that has NewVertices on both (interior or boundary)
		for(OrderedPair<Face> facePair : idata.getIntersectedFacePairList()){
			
			LinkedList<NewVertex<Vertex,Edge,Face>> nvList = idata.getNewVertices(facePair.getA(), facePair.getB());
			
			//only possibly overlapping if nvList.size() > 2
			if(nvList.size() <= 2){ continue; }
			
			//there is a cycle of split edges iff the faces overlap
			boolean containsCycle = PolyhedronCSG.checkContainsSplitEdgeCycle(nvList);
			if(containsCycle){
				mergedFaces.mergePartitions(getPartitionNode(facePair.getA()), getPartitionNode(facePair.getB())); 
			}
		}
	}

	
	//===============================================
	// FACE-FACE INTERSECTION NEW EDGES
	//===============================================
	
	/**
	 * Create NewEdges resulting from transverse intersection of face interiors.
	 * 
	 * @param intersectionData The IntersectionData object.
	 * @return A list of NewEdgeFFIs created from transverse intersections of face interiors.
	 */
	private void computeFFIEdges(){

		//look through every face pair that has NewVertices on both (interior or boundary)
		for(OrderedPair<Face> facePair : idata.getIntersectedFacePairList()){
			
			computeFFIEdges(facePair, idata.getNewVertices(facePair.getA(), facePair.getB()));
		}
	}
	
	private void computeFFIEdges(OrderedPair<Face> facePair, LinkedList<NewVertex<Vertex,Edge,Face>> nvList){
		
		//nothing to do if < 2 verts
		if(nvList.size() < 2){ return; }
		
		//faces overlap, and verts need to be sorted, only if there are > 2 verts
		if(nvList.size() > 2){
			//if the faces overlap, do not add FFI edges
			PartitionNode<Face> nodeA = getPartitionNode(facePair.getA());
			PartitionNode<Face> nodeB = getPartitionNode(facePair.getB());
			if(nodeA.checkPartitionEquivalence(nodeB)){ return; }

			//sort vertices along intersection line (see comments on this method)
			sortFacePairNewVertices(facePair, nvList);
		}
		
		//iterate through (if necessary, sorted) list of vertices, and create FFI edges
		boolean haveFirst = false;
		NewVertex<Vertex,Edge,Face> p = null, q = null;
		
		for(NewVertex<Vertex,Edge,Face> nv : nvList){
			
			//shift endpoints forward; skip the first item
			p = q; q = nv;
			if(!haveFirst){ haveFirst = true; continue; }
			
			//reject if there is already a split edge joining this pair
			if(PolyhedronCSG.<Vertex,Edge,Face>findCommonSplitEdge(p, q) != null){ continue; }
			
			//ensure that the segment is on the interior of both faces
			if( !PolyhedronCSG.<Vertex,Edge,Face>checkEdgeOnFaceInterior(p, q, facePair.getA()) ||
				!PolyhedronCSG.<Vertex,Edge,Face>checkEdgeOnFaceInterior(p, q, facePair.getB()) ){
				continue;
			}
			
			//create NewEdgeFFI from p to q
			addFFINewEdge(p, q, facePair.getA(), facePair.getB());
		}
	}
	
	/**
	 * Sort new vertices along the line of intersection of the faces in facePair
	 * for the purpose of creating NewEdgeFFIs.
	 * 
	 * If the faces are parallel, the sorting will not work, but no FFI edges
	 * will get created anyway (either the faces overlap, or none of the edges
	 * will be contained in the interior of both faces).
	 * 
	 * @param facePair The face pair.
	 * @param vertexCollection The collection of NewVertices on both faces.
	 */
	private void sortFacePairNewVertices(OrderedPair<Face> facePair, List<NewVertex<Vertex,Edge,Face>> vertexCollection){
		
		Vector3d normalA = facePair.getA().getPlane().getNormal();
		Vector3d normalB = facePair.getB().getPlane().getNormal();
		final Vector3d lineIntersection = normalA.cross(normalB);
		
		Collections.sort(vertexCollection, new Comparator<NewVertex<Vertex,Edge,Face>>(){
			@Override
			public int compare(NewVertex<Vertex, Edge, Face> nv1, NewVertex<Vertex, Edge, Face> nv2) {
				float value1 = lineIntersection.dot(nv1.getPosition());
				float value2 = lineIntersection.dot(nv2.getPosition());
				
				if(value1 < value2){ return -1; }
				if(value2 < value1){ return 1; }
				return 0;
			}
		});
	}
	
	/**
	 * Update data structures to reflect a new NewEdgeFFI.
	 */
	private void addFFINewEdge(
		NewVertex<Vertex, Edge, Face> p, NewVertex<Vertex, Edge, Face> q,
		Face incidentFace0, Face incidentFace1){
		
		//[TODO] warn about pinch point
		if(p.getPosition().distance(q.getPosition()) < Epsilon.get()){
			MessageOutput.printWarning("Pinch point while creating face-face intersection");
			warningCount++;
		}
		
		//create and add edge
		NewEdgeFFI<Vertex,Edge,Face> ffiEdge = new NewEdgeFFI<Vertex,Edge,Face>(p, q, incidentFace0, incidentFace1, edgeConstructor);
		ffiEdges.add(ffiEdge);
		newEdges.add(ffiEdge);
	}

	
	//===============================================
	// NON-BOUNDARY SPLIT NEW EDGE TYPE DETERMINATION
	//===============================================
	
	/**
	 * Recursively get KeepType for those NewEdgeSplits with undetermined type which 
	 * are connected via non-intersection vertices to NewEdgeSplits with determined type.
	 */
	private void propagateNonBoundarySplitEdgeTypes(){

		for(NewEdgeSplit<Vertex,Edge,Face> splitEdge : splitEdges){
			switch(splitEdge.getSplitEdgeType()){
			case NON_BOUNDARY:
				splitEdge.getAsSplitEdgeNonBoundary().propagateType();
			default: }
		}
	}

	/**
	 * Determines KeepType for those NewEdgeSplits such that both the constructor
	 * and propagateNonBoundarySplitEdgeTypes failed.  
	 * 
	 * This is done by seeing if there is an incident face with NewVertices only on its
	 * interior, in which case a 2d inf check is done in a way similar to Polygon.containsPoint;
	 * if this fails, do a Polyhedron.containsPoint on the midpoint of the edge.
	 */
	private void computeSplitEdgeTypesByInfCheck(){
		
		for(NewEdgeSplit<Vertex,Edge,Face> splitEdge : splitEdges){

			//grab edge as a NON_BOUNDARY SplitEdge; if it isn't one, ignore it
			if(splitEdge.getSplitEdgeType() != SplitEdgeType.NON_BOUNDARY){ continue; }			
			NewEdgeSplitNonBoundary<Vertex,Edge,Face> splitEdgeNB = splitEdge.getAsSplitEdgeNonBoundary();
			
			//ignore if the SplitEdgeNonBoundaryType is already known
			if(splitEdgeNB.getKeepType() != KeepType.UNDETERMINED){ continue; }
			
			//choose an incident face which has NewVertices on interior
			Face face = null;
			IntersectionList<Vertex,Edge,Face,FaceIntersection<Vertex,Edge,Face>> ilist = null;
			for(Face incFace : splitEdge.getOriginalEdge().getIncidentFaces()){
				face = incFace;
				ilist = idata.getFaceIntersections(incFace);
				if(ilist != null){ break; }
			}
			
			if(ilist != null){
				//use 2d inf check
				computeSplitEdgeTypeByInfCheck2d(face, splitEdgeNB);
			}else{
				//use 3d inf check
				MessageOutput.printDebug("Resorting to 3d inf check to determine a split-edge type");
				computeSplitEdgeTypeByInfCheck3d(splitEdgeNB);
			}
			
			//if type was successfully determined, propagate; otherwise, error
			if(splitEdgeNB.getKeepType() != KeepType.UNDETERMINED){ 
				splitEdgeNB.propagateType();
			}else{
				MessageOutput.printError("Could not determine a split-edge type (no intersections on an incident face)");
				errorCount++;
				continue;
			}
		}
	}
	
	/**
	 * Use 2d inf check to determine KeepType for the specified NewEdgeSplitNonBoundary.
	 * 
	 * The inf check is performed in the same way as Polygon.containsPoint, but with
	 * the the vertices and faces the NewVertices and NewEdges on the specified face.
	 */
	private void computeSplitEdgeTypeByInfCheck2d(Face face, NewEdgeSplitNonBoundary<Vertex,Edge,Face> splitEdgeNB){
		
		IntersectionList<Vertex,Edge,Face,FaceIntersection<Vertex,Edge,Face>> ilist = 
			idata.getFaceIntersections(face);
		
		//get vertices and faces on "face" which are candidates for a 2d inf check
		HashSet<NewVertex<Vertex,Edge,Face>> faceNewVertices = new HashSet<NewVertex<Vertex,Edge,Face>>(); 
		HashSet<NewEdge<Vertex,Edge,Face>> faceNewEdges = new HashSet<NewEdge<Vertex,Edge,Face>>();
		
		for(FaceIntersection<Vertex,Edge,Face> fi : ilist){
			//add NewVertex at this intersection point to the list
			faceNewVertices.add(fi.getNewVertex());
			
			//loop through each NewEdge incident to the NewVertex at this intersection point
			for(NewEdge<Vertex,Edge,Face> ne : fi.getNewVertex().getIncidentNewEdges()){
				//add it to the list if it is, in fact, incident to face
				if(ne.checkIncidence(face)){ faceNewEdges.add(ne); }
			}
		}
		
		//do the same thing as Polygon.containsPoint; the point to check will be 
		//any point along split edge--just use midpoint
		ClosestObjectData<Vector3d, NewVertex<Vertex,Edge,Face>, NewEdge<Vertex,Edge,Face>> clobj =
			PolygonTools.<Vector3d, NewVertex<Vertex,Edge,Face>, NewEdge<Vertex,Edge,Face>>
			getClosestObject(faceNewVertices, faceNewEdges, splitEdgeNB.lerp(0.5f));
		
		//get the polyhedron component on which to perform infinitesimal interior check
		PolyhedronComponent<Vertex,Edge,Face> infCheck = null;
		
		if(clobj.closestVertex != null){
			//do inf check on the object that "face" intersected to create newvertex
			infCheck = clobj.closestVertex.getComponent0();
			if(infCheck == face){
				infCheck = clobj.closestVertex.getComponent1();
			}
		}else if(clobj.closestEdge != null){
			switch(clobj.closestEdge.getNewEdgeType()){
			case SPLIT_EDGE:
				//do inf check on the edge this split edge was split from
				NewEdgeSplit<Vertex,Edge,Face> nesplit = clobj.closestEdge.getAsSplitEdge();
				infCheck = nesplit.getOriginalEdge();
				break;
			case FACE_FACE_INTERSECTION:
				//do inf check on the face that intersected "face" to create ffi edge
				NewEdgeFFI<Vertex,Edge,Face> neffi = clobj.closestEdge.getAsFaceFaceIntersection();
				infCheck = neffi.getIncidentFace0();
				if(infCheck == face){
					infCheck = neffi.getIncidentFace1();
				}
				break;
			}
		}
		
		//do the check and set the split edge type
		splitEdgeNB.setKeepType(
			scheme.simulateCheckInfinitesimalInterior(infCheck, clobj.v) ? 
			KeepType.DISCARD : KeepType.KEEP
		);
	}
	
	/**
	 * Use 3d inf check to determine KeepType for the specified NewEdgeSplitNonBoundary.
	 * 
	 * This is done as a last resort, ideally should never need to do this.
	 */
	private void computeSplitEdgeTypeByInfCheck3d(NewEdgeSplitNonBoundary<Vertex,Edge,Face> splitEdgeNB){
		
		Vector3d p = splitEdgeNB.lerp(0.5f);
		
		boolean inFlippedOpp = scheme.checkOwnedByPolyhedronA(splitEdgeNB.getOriginalEdge()) ?
			scheme.simulatePolyAContainsPoint(p) : scheme.simulatePolyBContainsPoint(p); 
		
		splitEdgeNB.setKeepType(inFlippedOpp ? KeepType.DISCARD : KeepType.KEEP);
	}
	
	
	//===============================================
	// BOUNDARY EDGE DETERMINATION
	//===============================================
	
	/**
	 * Get list of boundary edges.
	 */
	private void getBoundaryEdges(
		LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> boundaryKeep,
		LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> boundaryDiscard,
		Set<Face> modifiedFaces,
		List<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> unmodifiedEdges){
		
		LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>> boundaryEdges = new LinkedList<NewEdgeSplitNonBoundary<Vertex,Edge,Face>>();
		
		//an unmodified edge is KEEP boundary if it is KEEP and is incident
		//to at least one unmodified face; similarly for DISCARD
		for(NewEdgeSplitNonBoundary<Vertex,Edge,Face> ne : unmodifiedEdges){
			
			boolean isBoundary = false;
			for(Face incFace : ne.getOriginalEdge().getIncidentFaces()){
				if(!modifiedFaces.contains(incFace)){ 
					isBoundary = true;
					break;
				}
			}
			
			if(isBoundary){ boundaryEdges.add(ne); 
				switch(ne.getKeepType()){
				case KEEP: boundaryKeep.add(ne); break;
				case DISCARD: boundaryDiscard.add(ne); break;
				default: }
			}
		}
	}
}
