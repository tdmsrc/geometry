package geometry.polyhedron.intersect;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import geometry.common.Epsilon;
import geometry.common.OrderedPair;
import geometry.common.components.FixedDimensionalEdge.EdgeEdgeIntersection;
import geometry.common.components.Vertex3d.Vertex3dConstructor;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.Polyhedron;
import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

//This class handles and organizes all the intersections
//that occur when performing a CSG operation on polyhedra.

public class IntersectionData 
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>>
{
	private static boolean SHOW_DEBUG_TIMINGS = false;
	
	//CSG scheme
	private Polyhedron<Vertex,Edge,Face> lhs; //, rhs; //aka polyA and polyB
	//Vertex constructor
	private Vertex3dConstructor<Vertex> vertexConstructor;
	
	//lookups for the intersections on each component
	private HashMap<Vertex, IntersectionList<Vertex, Edge, Face, Intersection<Vertex, Edge, Face>>> lookupVertexInts;
	private HashMap<Edge, IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>>> lookupEdgeInts;
	private HashMap<Face, IntersectionList<Vertex, Edge, Face, FaceIntersection<Vertex, Edge, Face>>> lookupFaceInts;
	
	//list of faces which have an intersection on interior or boundary
	private HashSet<Face> intersectedFacesA;
	private HashSet<Face> intersectedFacesB;
	
	//list of new vertices organized by common pair of faces
	//[the face pair always has the poly A face in the first slot, poly B face in second]
	private HashMap<OrderedPair<Face>, LinkedList<NewVertex<Vertex,Edge,Face>>> newVerticesByFacePair;
	
	
	public IntersectionData(
		Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs,
		Vertex3dConstructor<Vertex> vertexConstructor){
		
		//store scheme and vertex constructor
		this.lhs = lhs; //this.rhs = rhs;
		this.vertexConstructor = vertexConstructor;
		
		//initialize data structures
		lookupVertexInts = new HashMap<Vertex, IntersectionList<Vertex, Edge, Face, Intersection<Vertex, Edge, Face>>>();
		lookupEdgeInts = new HashMap<Edge, IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>>>();
		lookupFaceInts = new HashMap<Face, IntersectionList<Vertex, Edge, Face, FaceIntersection<Vertex, Edge, Face>>>();
		
		intersectedFacesA = new HashSet<Face>();
		intersectedFacesB = new HashSet<Face>();
		
		newVerticesByFacePair = new HashMap<OrderedPair<Face>, LinkedList<NewVertex<Vertex,Edge,Face>>>();
		
		//populate data structures:
		
		//The order of these matters, in order to catch certain degenerate configurations,
		//and therefore be able to make general position assumptions in certain places. 
		
		if(SHOW_DEBUG_TIMINGS){
			long startTime = 0;
			
			startTime = System.currentTimeMillis();
			System.out.println("VV START");
			computeVertexVertexIntersections(lhs, rhs);
			System.out.println("VV DONE (" + (System.currentTimeMillis() - startTime) + "ms)");
			
			startTime = System.currentTimeMillis();
			System.out.println("VE START");
			computeVertexEdgeIntersections(lhs, rhs);
			computeVertexEdgeIntersections(rhs, lhs);
			System.out.println("VE DONE (" + (System.currentTimeMillis() - startTime) + "ms)");
			
			startTime = System.currentTimeMillis();
			System.out.println("VF START");
			computeVertexFaceIntersections(lhs, rhs);
			computeVertexFaceIntersections(rhs, lhs);
			System.out.println("VF DONE (" + (System.currentTimeMillis() - startTime) + "ms)");
			
			startTime = System.currentTimeMillis();
			System.out.println("EE START");
			computeEdgeEdgeIntersections(lhs, rhs);
			System.out.println("EE DONE (" + (System.currentTimeMillis() - startTime) + "ms)");
			
			startTime = System.currentTimeMillis();
			System.out.println("EF START");
			computeEdgeFaceIntersections(lhs, rhs);
			computeEdgeFaceIntersections(rhs, lhs);
			System.out.println("EF DONE (" + (System.currentTimeMillis() - startTime) + "ms)");
		}else{
			
			computeVertexVertexIntersections(lhs, rhs);
			
			computeVertexEdgeIntersections(lhs, rhs);
			computeVertexEdgeIntersections(rhs, lhs);
			
			computeVertexFaceIntersections(lhs, rhs);
			computeVertexFaceIntersections(rhs, lhs);
			
			computeEdgeEdgeIntersections(lhs, rhs);
			
			computeEdgeFaceIntersections(lhs, rhs);
			computeEdgeFaceIntersections(rhs, lhs);
		}
	}
	
	/**
	 * Get list of faces from the primary polyhedron such that its interior or 
	 * boundary contains an intersection.
	 * 
	 * @return The list of faces containing an intersection.
	 */
	public Set<Face> getIntersectedFaceListPolyA(){ 
		return intersectedFacesA; 
	}
	
	/**
	 * Get list of faces from the secondary polyhedron such that its interior or
	 * boundary contains an intersection.
	 * 
	 * @return The list of faces containing an intersection.
	 */
	public Set<Face> getIntersectedFaceListPolyB(){
		return intersectedFacesB;
	}
	
	/**
	 * Get list of face pairs such that there exist intersections on both faces
	 * (interior or boundary) of the pair.
	 * 
	 * @return The list of face pairs containing an intersection.
	 */
	public Set<OrderedPair<Face>> getIntersectedFacePairList(){
		return newVerticesByFacePair.keySet();
	}
	
	/**
	 * Get list of NewVertices which appear on both faces (interior or boundary) 
	 * of the specified pair.
	 * 
	 * If there are no intersections sharing both faces, returns null.
	 * 
	 * @return The list of NewVertices on both specified faces (interior or boundary) 
	 * or null if there are none.
	 */
	public LinkedList<NewVertex<Vertex,Edge,Face>>getNewVertices(Face face1, Face face2){
		
		//ensure the face from polyA is first
		Face facePolyA = face1, facePolyB = face2;
		if(face2.getOwner() == lhs){ //if(scheme.checkOwnedByPolyhedronA(face2)){
			facePolyA = face2; facePolyB = face1;
		}
		
		return newVerticesByFacePair.get(new OrderedPair<Face>(facePolyA, facePolyB));
	}
	
	private LinkedList<NewVertex<Vertex,Edge,Face>>getNewVerticesOrCreate(Face face1, Face face2){
		
		//ensure the face from polyA is first
		Face facePolyA = face1, facePolyB = face2;
		if(face2.getOwner() == lhs){ //if(scheme.checkOwnedByPolyhedronA(face2)){
			facePolyA = face2; facePolyB = face1;
		}
		
		OrderedPair<Face> facePair = new OrderedPair<Face>(facePolyA, facePolyB);
		LinkedList<NewVertex<Vertex,Edge,Face>> list = newVerticesByFacePair.get(facePair);
		if(list == null){
			list = new LinkedList<NewVertex<Vertex,Edge,Face>>();
			newVerticesByFacePair.put(facePair, list);
		}
		return list;
	}

	/**
	 * Get list of intersections for a vertex "v".
	 * If there are no intersections, returns null.
	 * 
	 * @param v The vertex whose intersections are to be returned.
	 * @return The IntersectionList of intersections.
	 */
	public IntersectionList<Vertex, Edge, Face, Intersection<Vertex, Edge, Face>> 
		getVertexIntersections(Vertex v){
		
		return lookupVertexInts.get(v);
	}
	
	private IntersectionList<Vertex, Edge, Face, Intersection<Vertex, Edge, Face>> 
		getVertexIntersectionsOrCreate(Vertex v){
		
		IntersectionList<Vertex, Edge, Face, Intersection<Vertex, Edge, Face>> list = lookupVertexInts.get(v);
		if(list == null){
			list = new IntersectionList<Vertex, Edge, Face, Intersection<Vertex, Edge, Face>>();
			lookupVertexInts.put(v, list);
		}
		return list;
	}
	
	/**
	 * Get list of intersections for an edge "e".
	 * If there are no intersections, returns null.
	 * 
	 * @param e The edge whose intersections are to be returned.
	 * @return The IntersectionList of intersections.
	 */
	public IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>> 
		getEdgeIntersections(Edge e){
		
		return lookupEdgeInts.get(e);
	}
	
	private IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>> 
		getEdgeIntersectionsOrCreate(Edge e){
		
		IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>> list = lookupEdgeInts.get(e);
		if(list == null){
			list = new IntersectionList<Vertex, Edge, Face, EdgeIntersection<Vertex, Edge, Face>>();
			lookupEdgeInts.put(e, list);
		}
		return list;
	}
	
	/**
	 * Get list of intersections for a face "f".
	 * If there are no intersections, returns null.
	 * 
	 * @param f The face whose intersections are to be returned.
	 * @return The IntersectionList of intersections.
	 */
	public IntersectionList<Vertex, Edge, Face, FaceIntersection<Vertex, Edge, Face>> 
		getFaceIntersections(Face f){
		
		return lookupFaceInts.get(f);
	}

	private IntersectionList<Vertex, Edge, Face, FaceIntersection<Vertex, Edge, Face>> 
		getFaceIntersectionsOrCreate(Face f){
		
		IntersectionList<Vertex, Edge, Face, FaceIntersection<Vertex, Edge, Face>> list = lookupFaceInts.get(f);
		if(list == null){
			list = new IntersectionList<Vertex, Edge, Face, FaceIntersection<Vertex, Edge, Face>>();
			lookupFaceInts.put(f, list);
		}
		return list;
	}

	/**
	 * Adds all faces incident to the specified polyhedron component to the
	 * appropriate intersectedFaces list.
	 * 
	 * @param pc The polyhedron component.
	 */
	private void updateIntersectedFaces(PolyhedronComponent<Vertex,Edge,Face> pc){
		
		HashSet<Face> intersectedFaces =
			((pc.getOwner() == lhs) ? intersectedFacesA : intersectedFacesB);
			//(scheme.checkOwnedByPolyhedronA(pc) ? intersectedFacesA : intersectedFacesB);
		 
		switch(pc.getPolyhedronComponentType()){
		case VERTEX: 
			intersectedFaces.addAll(pc.getAsVertex().getIncidentFaces()); 
			return;
		case EDGE:
			intersectedFaces.addAll(pc.getAsEdge().getIncidentFaces());
			return;
		case FACE:
			intersectedFaces.add(pc.getAsFace());
			return;
		}
	}
	
	//===============================================
	// VERTEX-VERTEX
	//===============================================

	private void computeVertexVertexIntersections(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		//[TODO] use some space partitioning to make this more efficient
		for(Vertex vertA : lhs.getVertices()){
		for(Vertex vertB : rhs.getVertices()){
			computeVertexVertexIntersection(vertA, vertB);
		}}
	}
	
	private void computeVertexVertexIntersection(Vertex vertA, Vertex vertB){
		
		//see if vertices intersect
		if(!(vertA.getPosition().distance(vertB.getPosition()) < Epsilon.get())){ return; }
		
		//create new vertex
		Vertex newVertex = vertA.copy(); 
		
		//add references to the intersection
		addVertexVertexIntersection(vertA, vertB, newVertex);
	}

	private void addVertexVertexIntersection(Vertex v1, Vertex v2, Vertex newVertex){
		
		//create NewVertex
		NewVertex<Vertex,Edge,Face> nv = new NewVertex<Vertex,Edge,Face>(newVertex, v1, v2);

		//create intersection objects
		getVertexIntersectionsOrCreate(v1).add(new Intersection<Vertex,Edge,Face>(v1, v2, nv));
		getVertexIntersectionsOrCreate(v2).add(new Intersection<Vertex,Edge,Face>(v2, v1, nv));
		
		//update list of intersected faces
		updateIntersectedFaces(v1);
		updateIntersectedFaces(v2);

		for(Face face1 : v1.getIncidentFaces()){
		for(Face face2 : v2.getIncidentFaces()){
			getNewVerticesOrCreate(face1, face2).add(nv);
		}}
	}
	
	//===============================================
	// VERTEX-EDGE
	//===============================================
	
	private void computeVertexEdgeIntersections(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){

		//[TODO] use some space partitioning to make this more efficient
		for(Vertex vertex : lhs.getVertices()){
		for(Edge edge : rhs.getEdges()){
			computeVertexEdgeIntersection(vertex, edge);
		}}
	}
	
	private void computeVertexEdgeIntersection(Vertex vertex, Edge edge){
		
		if(checkVertexEdgeAlreadyHandled(vertex, edge)){ return; }
		
		//check for intersection
		float t = edge.computePointIntersection(vertex.getPosition());
		if(!((t > 0) && (t < 1))){ return; }
		
		Vector3d p = edge.lerp(t);
		if(!(p.distance(vertex.getPosition()) < Epsilon.get())){ return; }
		
		//create new vertex
		Vertex newVertex = vertex.copy();
		
		//add references to intersection
		addVertexEdgeIntersection(vertex, edge, t, newVertex);
	}
	
	/**
	 * Returns true if this intersection should not be checked.
	 * 
	 * In particular, returns true if "v" intersected either endpoint of "e".
	 */
	private boolean checkVertexEdgeAlreadyHandled(Vertex v, Edge e){
		
		IntersectionList<Vertex,Edge,Face,?> vertexInts = getVertexIntersections(v);
		if((vertexInts != null) && vertexInts.containsEdgeBoundary(e)){ return true; }
		
		return false;
	}
	
	/**
	 * Update the various data structures to reflect the specified intersection.
	 */
	private void addVertexEdgeIntersection(Vertex v, Edge e, float lerp, Vertex newVertex){
		
		//create NewVertex
		NewVertex<Vertex,Edge,Face> nv = new NewVertex<Vertex,Edge,Face>(newVertex, v, e);

		//create intersection objects
		getVertexIntersectionsOrCreate(v).add(new Intersection<Vertex,Edge,Face>(v, e, nv));
		getEdgeIntersectionsOrCreate(e).add(new EdgeIntersection<Vertex,Edge,Face>(e, v, nv, lerp));
		
		//update list of intersected faces
		updateIntersectedFaces(v);
		updateIntersectedFaces(e);

		for(Face face1 : v.getIncidentFaces()){
		for(Face face2 : e.getIncidentFaces()){
			getNewVerticesOrCreate(face1, face2).add(nv);
		}}
	}
	
	//===============================================
	// VERTEX-FACE
	//===============================================

	private void computeVertexFaceIntersections(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		//[TODO] use some space partitioning to make this more efficient
		for(Vertex vertex : lhs.getVertices()){
		for(Face face : rhs.getFaces()){
			computeVertexFaceIntersection(vertex, face);
		}}
	}
	
	private void computeVertexFaceIntersection(Vertex vertex, Face face){
		
		if(checkVertexFaceAlreadyHandled(vertex, face)){ return; }
		
		//check for intersection
		if(!(face.getPlane().getDistance(vertex.getPosition()) < Epsilon.get())){ return; }
		
		Vector2d p2d = face.getPlane().getProjection(vertex.getPosition());
		if(!face.containsPoint(p2d)){ return; }
		
		//create new vertex
		Vertex newVertex = vertex.copy();
		
		//add references to intersection
		addVertexFaceIntersection(vertex, face, p2d, newVertex);
	}
	
	/**
	 * Returns true if this intersection should not be checked.
	 * 
	 * In particular, returns true if "v" intersected any vertex
	 * or edge belonging to the boundary of "f".
	 */
	private boolean checkVertexFaceAlreadyHandled(Vertex v, Face f){
		
		IntersectionList<Vertex,Edge,Face,?> vertexInts = getVertexIntersections(v);
		if((vertexInts != null) && vertexInts.containsFaceBoundary(f)){ return true; }
		
		return false;
	}
	
	/**
	 * Update the various data structures to reflect the specified intersection.
	 */
	private void addVertexFaceIntersection(Vertex v, Face f, Vector2d coords, Vertex newVertex){
		
		//create NewVertex
		NewVertex<Vertex,Edge,Face> nv = new NewVertex<Vertex,Edge,Face>(newVertex, v, f); 

		//create intersection objects
		getVertexIntersectionsOrCreate(v).add(new Intersection<Vertex,Edge,Face>(v, f, nv));
		getFaceIntersectionsOrCreate(f).add(new FaceIntersection<Vertex,Edge,Face>(f, v, nv, coords));
		
		//update list of intersected faces
		updateIntersectedFaces(v);
		updateIntersectedFaces(f);

		for(Face face1 : v.getIncidentFaces()){
			getNewVerticesOrCreate(face1, f).add(nv);
		}
	}
	
	//===============================================
	// EDGE-EDGE
	//===============================================
	
	private void computeEdgeEdgeIntersections(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		//[TODO] use some space partitioning to make this more efficient
		for(Edge edgeA : lhs.getEdges()){
		for(Edge edgeB : rhs.getEdges()){
			computeEdgeEdgeIntersection(edgeA, edgeB);
		}}
	}
	
	private void computeEdgeEdgeIntersection(Edge edgeA, Edge edgeB){
		
		if(checkEdgeEdgeAlreadyHandled(edgeA, edgeB)){ return; }
		
		//check for intersection
		EdgeEdgeIntersection eei = edgeA.computeEdgeIntersection(edgeB);
		if(eei == null){ return; }
		if((eei.lerpCallingEdge < 0) || (eei.lerpCallingEdge > 1)){ return; }
		if((eei.lerpArgumentEdge < 0) || (eei.lerpArgumentEdge > 1)){ return; }
		
		Vector3d p = edgeA.lerp(eei.lerpCallingEdge);
		Vector3d q = edgeB.lerp(eei.lerpArgumentEdge);
		if(!(p.distance(q) < Epsilon.get())){ return; }
		
		//create new vertex
		Vertex newVertex = vertexConstructor.construct(p);
		
		//add references to intersection
		addEdgeEdgeIntersection(edgeA, eei.lerpCallingEdge, edgeB, eei.lerpArgumentEdge, newVertex);
	}
	
	/**
	 * Returns true if this intersection should not be checked.
	 * 
	 * In particular, returns true if "e1" intersected either endpoint
	 * of "e2" or vice-versa.
	 */
	private boolean checkEdgeEdgeAlreadyHandled(Edge e1, Edge e2){
		
		//interior of e1 intersects endpoint of e2 or vice-versa
		IntersectionList<Vertex,Edge,Face,?> e1Ints = getEdgeIntersections(e1);
		if((e1Ints != null) && e1Ints.containsEdgeBoundary(e2)){ return true; }
		IntersectionList<Vertex,Edge,Face,?> e2Ints = getEdgeIntersections(e2);
		if((e2Ints != null) && e2Ints.containsEdgeBoundary(e1)){ return true; }
		
		//endpoint of e1 intersects endpoint of e2
		IntersectionList<Vertex,Edge,Face,?> e1v0Ints = getVertexIntersections(e1.getInitialVertex());
		if((e1v0Ints != null) && e1v0Ints.containsEdgeBoundary(e2)){ return true; }
		IntersectionList<Vertex,Edge,Face,?> e1v1Ints = getVertexIntersections(e1.getFinalVertex());
		if((e1v1Ints != null) && e1v1Ints.containsEdgeBoundary(e2)){ return true; }
		
		return false;
	}
	
	/**
	 * Update the various data structures to reflect the specified intersection.
	 */
	private void addEdgeEdgeIntersection(Edge e1, float lerp1, Edge e2, float lerp2, Vertex newVertex){
		
		//create NewVertex
		NewVertex<Vertex,Edge,Face> nv = new NewVertex<Vertex,Edge,Face>(newVertex, e1, e2); 

		//create intersection objects
		getEdgeIntersectionsOrCreate(e1).add(new EdgeIntersection<Vertex,Edge,Face>(e1, e2, nv, lerp1));
		getEdgeIntersectionsOrCreate(e2).add(new EdgeIntersection<Vertex,Edge,Face>(e2, e1, nv, lerp2));
		
		//update list of intersected faces
		updateIntersectedFaces(e1);
		updateIntersectedFaces(e2);

		for(Face face1 : e1.getIncidentFaces()){
		for(Face face2 : e2.getIncidentFaces()){
			getNewVerticesOrCreate(face1, face2).add(nv);
		}} 
	}
	
	//===============================================
	// EDGE-FACE
	//===============================================

	private void computeEdgeFaceIntersections(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){

		//[TODO] use some space partitioning to make this more efficient
		for(Edge edge : lhs.getEdges()){
		for(Face face : rhs.getFaces()){
			computeEdgeFaceIntersection(edge, face);
		}}
	}
	
	private void computeEdgeFaceIntersection(Edge edge, Face face){
		
		//check for intersection:
		//don't bother if both endpoints are more than epsilon away on the same side
		float d0 = face.getPlane().getOrientedDistance(edge.getInitialVertex().getPosition());
		float d1 = face.getPlane().getOrientedDistance(edge.getFinalVertex().getPosition());
		if((d0 > Epsilon.get()) && (d1 > Epsilon.get())){ return; }
		if((d0 < -Epsilon.get()) && (d1 < -Epsilon.get())){ return; }
		
		if(checkEdgeFaceAlreadyHandled(edge, face)){ return; }
		
		Float t = face.getPlane().computeEdgeIntersection(edge);
		if(t == null){ return; }
		if((t < 0) || (t > 1)){ return; }
		
		Vector3d p = edge.lerp(t);
		Vector2d p2d = face.getPlane().getProjection(p);
		
		if(!face.containsPoint(p2d)){ return; }
		
		//create new vertex
		Vertex newVertex = vertexConstructor.construct(p);
		
		//add references to intersection
		addEdgeFaceIntersection(edge, t, face, p2d, newVertex);
	}
	
	/**
	 * Returns true if this intersection should not be checked.
	 * 
	 * In particular, returns true if interior of "e" intersected
	 * any vertex or edge belonging to the boundary of "f", or
	 * either endpoint of "e" intersected the interior of "f" or
	 * any vertex or edge belonging to the boundary of "f".
	 */
	private boolean checkEdgeFaceAlreadyHandled(Edge e, Face f){
		
		//edge interior intersected any vertex or edge of face
		IntersectionList<Vertex,Edge,Face,?> edgeInts = getEdgeIntersections(e);
		if((edgeInts != null) && edgeInts.containsFaceBoundary(f)){ return true; }
		
		//either endpoint of edge intersected any vertex or edge of face
		IntersectionList<Vertex,Edge,Face,?> edgev0Ints = getVertexIntersections(e.getInitialVertex());
		if((edgev0Ints != null) && edgev0Ints.containsFaceBoundary(f)){ return true; }
		IntersectionList<Vertex,Edge,Face,?> edgev1Ints = getVertexIntersections(e.getFinalVertex());
		if((edgev1Ints != null) && edgev1Ints.containsFaceBoundary(f)){ return true; }
		
		//either endpoint of edge intersected the interior of face
		IntersectionList<Vertex,Edge,Face,?> faceInts = getFaceIntersections(f);
		if((faceInts != null) && faceInts.containsEdgeBoundary(e)){ return true; }
		
		return false;
	}
	
	/**
	 * Update the various data structures to reflect the specified intersection.
	 */
	private void addEdgeFaceIntersection(Edge e, float lerp, Face f, Vector2d coords, Vertex newVertex){
		
		//create NewVertex
		NewVertex<Vertex,Edge,Face> nv = new NewVertex<Vertex,Edge,Face>(newVertex, e, f);
		
		//create intersection objects
		getEdgeIntersectionsOrCreate(e).add(new EdgeIntersection<Vertex,Edge,Face>(e, f, nv, lerp));
		getFaceIntersectionsOrCreate(f).add(new FaceIntersection<Vertex,Edge,Face>(f, e, nv, coords));
		
		//update list of intersected faces
		updateIntersectedFaces(e);
		updateIntersectedFaces(f);

		for(Face face : e.getIncidentFaces()){
			getNewVerticesOrCreate(f, face).add(nv);
		} 
	}
}
