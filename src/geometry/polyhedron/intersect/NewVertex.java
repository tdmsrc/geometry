package geometry.polyhedron.intersect;

import java.util.HashSet;

import geometry.common.components.Vertex3d;
import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

//A vertex to appear on a new face.
//This could refer to an old vertex, or a vertex that arises from an intersection.

public class NewVertex
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 

	extends Vertex3d
{

	//the newly created vertex; if it coincides with an old vertex, use Vertex.copy()
	protected Vertex vertex;
	
	//this will be set to "true" when this vertex is used in a new face
	protected boolean usedInNewFace; 
	
	//the components that intersected to create this vertex
	//if this is simply an old vertex, then one of the components will be null
	protected PolyhedronComponent<Vertex,Edge,Face> component0, component1;

	//set of incident NewEdges
	protected HashSet<NewEdge<Vertex,Edge,Face>> incidentNewEdges;
	//subsets of incidentNewEdges consisting of the SplitEdges or FFIEdges
	protected HashSet<NewEdgeSplit<Vertex,Edge,Face>> incidentSplitEdges;
	protected HashSet<NewEdgeFFI<Vertex,Edge,Face>> incidentFFIEdges;
	
	
	public NewVertex(Vertex vertex, PolyhedronComponent<Vertex,Edge,Face> component0, PolyhedronComponent<Vertex,Edge,Face> component1){
		super(vertex.getPosition());
		
		this.vertex = vertex;
		
		usedInNewFace = false;
		
		this.component0 = component0;
		this.component1 = component1;
		
		this.incidentNewEdges = new HashSet<NewEdge<Vertex,Edge,Face>>();
		this.incidentSplitEdges = new HashSet<NewEdgeSplit<Vertex,Edge,Face>>();
		this.incidentFFIEdges = new HashSet<NewEdgeFFI<Vertex,Edge,Face>>();
	}

	public Vertex getVertex(){ return vertex; }
	
	public boolean checkUsedInNewFace(){ return usedInNewFace; }
	
	/**
	 * Marks this NewVertex as having been used in a new face.
	 * It is not necessary to call this directly--NewEdge will call it
	 * on its endpoints when it is marked as having been used in a new face.
	 */
	public void setUsedInNewFace(){ usedInNewFace = true; }
	
	/**
	 * Get one of the polyhedron components that intersected to create this NewVertex.  
	 * May be null, in the case where NewVertex is an original vertex not created as
	 * an intersection (e.g. as the endpoint of a NewEdge created by splitting an old edge).
	 * 
	 * @return One of the polyhedron components that intersected to create this NewVertex.
	 */
	public PolyhedronComponent<Vertex,Edge,Face> getComponent0(){ return component0; }
	
	/**
	 * Get one of the polyhedron components that intersected to create this NewVertex.  
	 * May be null, in the case where NewVertex is an original vertex not created as
	 * an intersection (e.g. as the endpoint of a NewEdge created by splitting an old edge).
	 * 
	 * @return One of the polyhedron components that intersected to create this NewVertex.
	 */
	public PolyhedronComponent<Vertex,Edge,Face> getComponent1(){ return component1; }
	
	public HashSet<NewEdge<Vertex,Edge,Face>> getIncidentNewEdges(){
		return incidentNewEdges;
	}
	
	public HashSet<NewEdgeSplit<Vertex,Edge,Face>> getIncidentSplitEdges(){
		return incidentSplitEdges;
	}
	
	public HashSet<NewEdgeFFI<Vertex,Edge,Face>> getIncidentFFIEdges(){
		return incidentFFIEdges;
	}
}
