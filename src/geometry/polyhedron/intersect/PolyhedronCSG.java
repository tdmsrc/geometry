package geometry.polyhedron.intersect;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import geometry.common.MessageOutput;
import geometry.common.PartitionNode;
import geometry.common.components.Edge3d.Edge3dConstructor;
import geometry.common.components.Vertex3d.Vertex3dConstructor;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.EmbeddedEdge;
import geometry.polyhedron.components.EmbeddedVertex;
import geometry.polyhedron.components.Polyhedron;
import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.polyhedron.components.EmbeddedPolygon.Face3dConstructor;
import geometry.polyhedron.components.PolyhedronComponent.PolyhedronComponentType;
import geometry.polyhedron.intersect.PolyhedronCSGScheme.CSGType;


public class PolyhedronCSG
	//type for face, edge, and vertex of polyhedron
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 
{
		
	//constructors for the polyhedron components
	protected Vertex3dConstructor<Vertex> vertexConstructor;
	protected Edge3dConstructor<Vertex, Edge> edgeConstructor;
	protected Face3dConstructor<Vertex, Edge, Face> faceConstructor;
	
	
	public PolyhedronCSG(
		Vertex3dConstructor<Vertex> vertexConstructor,
		Edge3dConstructor<Vertex, Edge> edgeConstructor,
		Face3dConstructor<Vertex, Edge, Face> faceConstructor){
		
		this.vertexConstructor = vertexConstructor;
		this.edgeConstructor = edgeConstructor;
		this.faceConstructor = faceConstructor;
	}
	
	//===============================================
	// CONSTRUCTOR GETTERS/SETTERS
	//===============================================
	
	public Vertex3dConstructor<Vertex> getVertexConstructor(){ return vertexConstructor; }
	public Edge3dConstructor<Vertex, Edge> getEdgeConstructor(){ return edgeConstructor; }
	public Face3dConstructor<Vertex, Edge, Face> getFaceConstructor(){ return faceConstructor; }
	
	public void setVertexConstructor(Vertex3dConstructor<Vertex> vertexConstructor){ 
		this.vertexConstructor = vertexConstructor; 
	}
	
	public void setEdgeConstructor(Edge3dConstructor<Vertex, Edge> edgeConstructor){ 
		this.edgeConstructor = edgeConstructor; 
	}
	
	public void setFaceConstructor(Face3dConstructor<Vertex, Edge, Face> faceConstructor){ 
		this.faceConstructor = faceConstructor; 
	}
	
	//===============================================
	// CSG OPERATION PUBLIC METHODS
	//===============================================
	/**
	 * Modify the polyhedron "lhs" so that it is the union of the polyhedra
	 * "lhs" and "rhs".  This leaves "rhs" unchanged.
	 */
	public void union(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		computeCSGOperation(lhs, rhs, PolyhedronCSGScheme.CSGType.A_UNION_B);
	}
	
	/**
	 * Modify the polyhedron "lhs" so that it is the intersection of the polyhedra
	 * "lhs" and "rhs".  This leaves "rhs" unchanged.
	 */
	public void intersect(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		computeCSGOperation(lhs, rhs, PolyhedronCSGScheme.CSGType.A_INTERSECT_B);
	}
	
	/**
	 * Modify the polyhedron "lhs" so that it is the polyhedron "lhs" minus the
	 * polyhedron "rhs".  This leaves "rhs" unchanged.
	 */
	public void subtract(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		computeCSGOperation(lhs, rhs, PolyhedronCSGScheme.CSGType.A_MINUS_B);
	}
	
	/**
	 * Modify the polyhedron "lhs" so that it is the polyhedron "rhs" minus the
	 * polyhedron "lhs".  This leaves "rhs" unchanged.
	 */
	public void subtractFrom(Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		computeCSGOperation(lhs, rhs, PolyhedronCSGScheme.CSGType.B_MINUS_A);
	}
	
	//===============================================
	// GENERAL CSG OPERATION
	//===============================================
	
	//data of the intersection skeleton
	public static class SkeletonData
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
		 Face extends PolyhedronFace<Vertex, Edge, Face>>
	{
		public Collection<Vertex> vertices;
		public Collection<Edge> edges;
	}
	
	public SkeletonData<Vertex,Edge,Face> computeBoundaryIntersection(
		Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs){
		
		//set up intersection scheme
		PolyhedronCSGScheme scheme = new PolyhedronCSGScheme(lhs, rhs, CSGType.A_UNION_B); //CSGType doesn't matter
		
		//compute and organize component intersections
		IntersectionData<Vertex,Edge,Face> intersectionData = new IntersectionData<Vertex,Edge,Face>(lhs, rhs, vertexConstructor);
		
		//compute and organize NewEdges and determine overlapping faces
		List<NewEdge<Vertex,Edge,Face>> newEdges = NewEdgeData.<Vertex,Edge,Face>constructIntersectionSkeleton(intersectionData, scheme, edgeConstructor);
		
		//prepare SkeletonData
		SkeletonData<Vertex,Edge,Face> skeletonData = new SkeletonData<Vertex,Edge,Face>(); 
		
		skeletonData.vertices = new LinkedList<Vertex>(); 
		skeletonData.edges = new LinkedList<Edge>();
		
		//create lookup of Vertex objects corresponding to endpoints of FFI edges
		HashMap<NewVertex<Vertex,Edge,Face>,Vertex> skeletonVertexLookup = new HashMap<NewVertex<Vertex,Edge,Face>,Vertex>();
		
		for(NewEdge<Vertex,Edge,Face> edge : newEdges){
			NewVertex<Vertex,Edge,Face> p = edge.getInitialVertex();
			if(skeletonVertexLookup.get(p) == null){
				Vertex nv = vertexConstructor.construct(p.getPosition());
				skeletonData.vertices.add(nv);
				skeletonVertexLookup.put(p, nv);
			}
			
			NewVertex<Vertex,Edge,Face> q = edge.getFinalVertex();
			if(skeletonVertexLookup.get(q) == null){
				Vertex nv = vertexConstructor.construct(q.getPosition());
				skeletonData.vertices.add(nv);
				skeletonVertexLookup.put(q, nv);
			}
		}
		
		//create Edge objects corresponding to relevant NewEdges
		for(NewEdge<Vertex,Edge,Face> edge : newEdges){
			Vertex p = skeletonVertexLookup.get(edge.getInitialVertex());
			Vertex q = skeletonVertexLookup.get(edge.getFinalVertex());
			skeletonData.edges.add(edgeConstructor.construct(p,q));
		}
		
		return skeletonData;
	}
	
	//data collected in the process of a CSG operation
	public static class CSGData
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
		 Face extends PolyhedronFace<Vertex, Edge, Face>>
	{
		public int warningCount, errorCount;
		public IntersectionData<Vertex,Edge,Face> intersectionData;
		public NewEdgeData<Vertex,Edge,Face> newEdgeData;
		public NewFaceData<Vertex,Edge,Face> newFaceData;
	}
	
	/**
	 * Return the CSGData for the requested CSG operation for polyhedra "lhs" and "rhs".
	 * 
	 * This does not modify either polyhedron.
	 * 
	 * @param lhs The primary polyhedron.
	 * @param rhs The secondary polyhedron.
	 */
	public CSGData<Vertex,Edge,Face> computeCSGOperationData(
			Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs,
			PolyhedronCSGScheme.CSGType csgType){
		
		//set up intersection scheme
		PolyhedronCSGScheme scheme = new PolyhedronCSGScheme(lhs, rhs, csgType);
		
		//create CSG data object to populate and return
		CSGData<Vertex,Edge,Face> csgData = new CSGData<Vertex,Edge,Face>();
		csgData.warningCount = 0;
		csgData.errorCount = 0;
		
		//compute and organize component intersections
		csgData.intersectionData = new IntersectionData<Vertex,Edge,Face>(lhs, rhs, vertexConstructor);
		
		//compute and organize NewEdges and determine overlapping faces
		csgData.newEdgeData = NewEdgeData.<Vertex,Edge,Face>constructNewEdgeData(csgData.intersectionData, scheme, edgeConstructor);
		//csgData.newEdgeData = new NewEdgeData<Vertex,Edge,Face>(csgData.intersectionData, scheme, edgeConstructor);
		csgData.warningCount += csgData.newEdgeData.getWarningCount();
		csgData.errorCount += csgData.newEdgeData.getErrorCount();
		
		//compute and organize NewFaces and unmodified faces and components
		csgData.newFaceData = new NewFaceData<Vertex,Edge,Face>(lhs, rhs, csgData.intersectionData, csgData.newEdgeData, scheme, edgeConstructor, faceConstructor);

		//return
		return csgData;
	}
	
	/**
	 * Compute the requested CSG operation for polyhedra "lhs" and "rhs".
	 * This method modifies "lhs", and leaves "rhs" unchanged.
	 * 
	 * @param lhs The primary polyhedron.
	 * @param rhs The secondary polyhedron.
	 */
	public void computeCSGOperation(
			Polyhedron<Vertex,Edge,Face> lhs, Polyhedron<Vertex,Edge,Face> rhs,
			PolyhedronCSGScheme.CSGType csgType){
		
		//get CSG data; abort if there are warnings or errors
		//-------------------------------------------------------
		CSGData<Vertex,Edge,Face> csgData = computeCSGOperationData(lhs, rhs, csgType);
		
		if((csgData.warningCount > 0) || (csgData.errorCount > 0)){
			MessageOutput.printError("Aborted CSG operation due to error(s) or warning(s)");
			return;
		}
		
		//perform cutting and gluing
		//-------------------------------------------------------
		NewFaceData<Vertex,Edge,Face> newFaceData = csgData.newFaceData;
		
		//remove face components from PolyA that are to be discarded
		for(PartitionNode<Face> root : newFaceData.getDiscardComponentsA().getRoots()){
			lhs.removeComponent(root.getObject());
		}
		//flip faces from PolyA when necessary 
		for(Face face : newFaceData.getFlipFacesA()){ face.flip(); }
		//glue newFacesPolyhedron onto PolyA
		lhs.glue(newFaceData.getNewFacePolyhedron(), newFaceData.getNewFacePolyhedronGlue());
		//remove faces from PolyA that are to be discarded
		lhs.removeFaces(newFaceData.getDiscardFacesA());
	}
	
	
	//===============================================
	// GENERAL STATIC METHODS
	//===============================================
	
	//-----------------------------------------------
	// COMMON SPLIT-EDGE
	//-----------------------------------------------
	
	/**
	 * Determines whether a NewEdgeSplit has been created with endpoints "a" and "b". 
	 * If so, returns that NewEdgeSplit.  If not, returns null.
	 * 
	 * @param a A NewVertex.
	 * @param b A NewVertex.
	 * @return The NewEdgeSplit with endpoints "a" and "b". 
	 */
	public static 
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
		 Face extends PolyhedronFace<Vertex, Edge, Face>> 
	
	NewEdgeSplit<Vertex,Edge,Face> findCommonSplitEdge(
		NewVertex<Vertex,Edge,Face> a, NewVertex<Vertex,Edge,Face> b){
		
		for(NewEdgeSplit<Vertex,Edge,Face> splitEdge : a.getIncidentSplitEdges()){
			if((splitEdge.getInitialVertex() == b) || (splitEdge.getFinalVertex() == b)){ return splitEdge; }
		}
		return null;
	}
	
	//-----------------------------------------------
	// SPLIT-EDGE CYCLE DETECTION
	//-----------------------------------------------
	
	//data to gather for each edge-connected component of NewVertex collection
	private static class EdgeConnectedComponentData{
		public int vertexCount, edgeCount;
		public Set<NewVertex<?,?,?>> visitedVertices;
		
		public EdgeConnectedComponentData(){
			vertexCount = 0; edgeCount = 0;
			visitedVertices = new HashSet<NewVertex<?,?,?>>();
		}
	}
	
	//recursively gather data for the edge-connected component of NewVertex collection
	private static void populateEdgeConnectedComponentData(
		NewVertex<?,?,?> currentVertex, 
		Collection<NewVertex<?,?,?>> vertexList, 
		EdgeConnectedComponentData currentComponentData){
		
		//mark this vertex as having been visited, and add one to vertex count
		currentComponentData.vertexCount++;
		currentComponentData.visitedVertices.add(currentVertex);

		//for each neighboring vertex,
		for(NewEdgeSplit<?,?,?> ne : currentVertex.getIncidentSplitEdges()){
			
			//get neighboring vertex
			NewVertex<?,?,?> opp = ne.getInitialVertex();
			if(opp == currentVertex){ opp = ne.getFinalVertex(); }
			
			//ignore if it is not in the list of vertices under consideration
			if(!vertexList.contains(opp)){ continue; }

			//add to edge count
			currentComponentData.edgeCount++;
			
			//visit the vertex, if it hasn't already been visited
			if(!currentComponentData.visitedVertices.contains(opp)){
				PolyhedronCSG.populateEdgeConnectedComponentData(opp, vertexList, currentComponentData);
			}
		}
	}
	
	/**
	 * Determines whether the graph, with vertices the specified collection
	 * of NewVertices and edges the NewEdgeSplits with endpoints in the specified
	 * collection, contains a cycle.
	 * 
	 * @param vertexCollection A collection of NewVertices.
	 * @return True if there is a cycle of split edges joining vertices in nvList,
	 * false otherwise.
	 */
	public static boolean checkContainsSplitEdgeCycle(
		Collection<? extends NewVertex<?,?,?>> vertexCollection){

		//duplicate collection of NewVertices
		HashSet<NewVertex<?,?,?>> vertexSetCopy = new HashSet<NewVertex<?,?,?>>(vertexCollection);
		
		while(!vertexSetCopy.isEmpty()){
			//grab the first guy in the list, and recurse to get connected component data
			EdgeConnectedComponentData data = new EdgeConnectedComponentData();
			PolyhedronCSG.populateEdgeConnectedComponentData(vertexSetCopy.iterator().next(), vertexSetCopy, data);
			
			//check if this edge-connected component has edgeCount >= vertCount 
			//[note edges are double counted by the populateEdgeConnectedComponentData recursion] 
			if(data.edgeCount/2 >= data.vertexCount){ return true; }
			else{
				//drop everything in "visited" from nvListCopy
				vertexSetCopy.removeAll(data.visitedVertices);
			}
		}
		
		return false;
	}
	
	//-----------------------------------------------
	// NEWEDGE ON FACE INTERIOR
	//-----------------------------------------------
	
	private static 	
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
		 Face extends PolyhedronFace<Vertex, Edge, Face>> 
	
	boolean componentIsOnFaceOrInterior(
		Face face, PolyhedronComponent<Vertex,Edge,Face> component){
		
		if(component == null){ return false; }
		
		switch(component.getPolyhedronComponentType()){
		case VERTEX: return component.getAsVertex().checkIncidence(face);
		case EDGE: return component.getAsEdge().checkIncidence(face);
		case FACE: return (component.getAsFace() == face);
		}
		return false;
	}
	
	/**
	 * Determine whether the NewVertex is incident to the given face.
	 * If it is, return the PolyhedronComponent on which this NewVertex
	 * is contained, and which is incident to the specified face.
	 * If not, return null.
	 * 
	 * @return The component containing NewVertex and incident to "face".
	 */
	public static 		
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
		 Face extends PolyhedronFace<Vertex, Edge, Face>> 
	
	PolyhedronComponent<Vertex,Edge,Face> getComponentOnIncidentFace(
		NewVertex<Vertex,Edge,Face> p, Face face){
		
		if(componentIsOnFaceOrInterior(face, p.getComponent0())){ return p.getComponent0(); }
		else if(componentIsOnFaceOrInterior(face, p.getComponent1())){ return p.getComponent1(); }
		return null;
	}
	
	/**
	 * Determines whether the edge connecting the specified pair of NewVertices 
	 * is contained on the interior of the specified face.
	 * 
	 * Returns false if it is not true that both vertices are incident to the specified face.
	 * 
	 * @param p A NewVertex
	 * @param q A NewVertex
	 * @param face The face for which containment of the edge is to be checked.
	 * @return True if the edge is on the face interior, false otherwise.
	 */
	public static 
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
		 Face extends PolyhedronFace<Vertex, Edge, Face>>
	
	boolean checkEdgeOnFaceInterior(
		NewVertex<Vertex,Edge,Face> p, NewVertex<Vertex,Edge,Face> q, Face face){

		PolyhedronComponent<Vertex,Edge,Face> pObj = getComponentOnIncidentFace(p, face);
		PolyhedronComponent<Vertex,Edge,Face> qObj = getComponentOnIncidentFace(q, face);
		
		//make sure both are incident to face
		if((pObj == null) || (qObj == null)){ return false; }
		
		//get vector starting at q and ending at p
		Vector3d v = new Vector3d(p.getPosition());
		v.subtract(q.getPosition());
		Vector2d vproj = face.getPlane().getVectorProjection(v);
		
		if(	(pObj.getPolyhedronComponentType() == PolyhedronComponentType.FACE) ||
			(qObj.getPolyhedronComponentType() == PolyhedronComponentType.FACE) ){
				
			return true;
				
		}else if(pObj.getPolyhedronComponentType() == PolyhedronComponentType.EDGE){
			
			Edge pe = pObj.getAsEdge();
			//get embedded edge on commonFace linked to pe
			EmbeddedEdge<Vertex,Edge> embEdge = pe.getEmbeddedEdge(face);
			//do the inf-check
			return embEdge.checkInfinitesimalInterior(vproj);
				
		}else if(qObj.getPolyhedronComponentType() == PolyhedronComponentType.EDGE){
				
			Edge qe = qObj.getAsEdge();
			//get embedded edge on commonFace linked to qe
			EmbeddedEdge<Vertex,Edge> embEdge = qe.getEmbeddedEdge(face);
			//flip since we are inf-checking at q instead of p, then do the inf-check
			vproj.flip();
			return embEdge.checkInfinitesimalInterior(vproj);
				
		}else{
				
			Vertex pv = pObj.getAsVertex();
			//get embedded vertex on commonFace linked to pv
			EmbeddedVertex<Vertex,Edge> embVertex = pv.getEmbeddedVertex(face);
			//do the inf-check
			return embVertex.checkInfinitesimalInterior(vproj);
		}
	}

}
