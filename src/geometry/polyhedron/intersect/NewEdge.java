package geometry.polyhedron.intersect;

import java.util.Collection;

import geometry.common.components.Edge3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

//An edge to appear on a new face, after intersection.
//This will either be of type NewEdgeSplit or NewEdgeFFI
//The former is an old edge split by NewVertices; the latter is a face-face intersection.

public abstract class NewEdge 
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 

	extends Edge3d<NewVertex<Vertex,Edge,Face>>
{
	
	//the edge that will be used when creating a new face
	//not created until "markUsedInNewFace" is called, so that there is no reference
	//to this edge in its endpoints in the event the edge is *not* used in a new face.
	protected Edge3dConstructor<Vertex, Edge> edgeConstructor;
	protected Edge edge;
	
	//this will be set to "true" when this edge is used in a new face
	protected boolean usedInNewFace;
	
	
	public static enum NewEdgeType{
		SPLIT_EDGE, FACE_FACE_INTERSECTION
	}
	
	public NewEdge(
		NewVertex<Vertex,Edge,Face> p, NewVertex<Vertex,Edge,Face> q,
		Edge3dConstructor<Vertex, Edge> edgeConstructor){
		
		super(p,q);
		
		//add references to this new edge to the endpoints
		p.getIncidentNewEdges().add(this);
		q.getIncidentNewEdges().add(this);
		
		//store constructor, but wait to make edge till this newedge is marked as used in new face
		this.edgeConstructor = edgeConstructor;
		edge = null; 
	}
	
	public abstract NewEdgeType getNewEdgeType();
	
	public abstract NewEdgeSplit<Vertex,Edge,Face> getAsSplitEdge();
	public abstract NewEdgeFFI<Vertex,Edge,Face> getAsFaceFaceIntersection();
	
	public abstract boolean checkIncidence(Face face);
	public abstract Collection<Face> getIncidentFaces();
	
	
	//See new face construction in NewFaceData, and NewFaceSubset
	public static class FaceAdjacencyData<Face>{
		private final Face leftFace1, leftFace2, rightFace1, rightFace2;
		
		public FaceAdjacencyData(Face leftFace1, Face leftFace2, Face rightFace1, Face rightFace2){
			this.leftFace1 = leftFace1; this.rightFace1 = rightFace1;
			this.leftFace2 = leftFace2; this.rightFace2 = rightFace2;
		}
		
		public Face getLeftFace1(boolean flipLR){ return (flipLR ? rightFace1 : leftFace1); }
		public Face getLeftFace2(boolean flipLR){ return (flipLR ? rightFace2 : leftFace2); } 
		public Face getRightFace1(boolean flipLR){ return (flipLR ? leftFace1 : rightFace1); } 
		public Face getRightFace2(boolean flipLR){ return (flipLR ? leftFace2 : rightFace2); } 
	}

	protected abstract FaceAdjacencyData<Face> computeInfinitesimalFaceAdjacency(
		Vector3d refNormal, PolyhedronCSGScheme scheme, Collection<Face> mergedFace);
	
	
	/**
	 * Returns an Edge for use in constructing a new face.
	 * The Edge is created only when "setUsedInNewFace" is called.
	 * If "setUsedInNewFace" has not been called, this will return null.
	 * 
	 * @return An edge for use in constructing a new face, or null.
	 */
	public Edge getEdge(){
		return edge;
	}
	
	public boolean checkUsedInNewFace(){
		return usedInNewFace;
	}
	
	/**
	 * Mark this NewEdge as having been used in a new face.
	 * 
	 * This will automatically mark its endpoints as having been used
	 * in a new face, and will create the Edge that is to be used in
	 * the new face (returned by getEdge).
	 */
	public void setUsedInNewFace(){
		usedInNewFace = true;
		getInitialVertex().setUsedInNewFace();
		getFinalVertex().setUsedInNewFace();
		createEdge();
	}
	
	private void createEdge(){
		if(edge != null){ return; }
		edge = edgeConstructor.construct(getInitialVertex().getVertex(),  getFinalVertex().getVertex());
	}
	
	/**
	 * Returns true if this is a NewEdgeSplitNonBoundary with
	 * KeepType either UNDETERMINED or DISCARD.
	 * 
	 * @see NewFaceData.constructNewFaces
	 */
	public boolean checkUnusableInNewFace(){
		
		switch(getNewEdgeType()){
		case SPLIT_EDGE:
			NewEdgeSplit<Vertex,Edge,Face> neSplit = getAsSplitEdge();
			switch(neSplit.getSplitEdgeType()){
			case NON_BOUNDARY:
				switch(neSplit.getAsSplitEdgeNonBoundary().getKeepType()){
				case UNDETERMINED:
				case DISCARD: return true;
				default: }
			default: }
		default: }
		return false;
	}
}
