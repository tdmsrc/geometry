package geometry.polyhedron.intersect;

import geometry.common.components.Edge3d;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

//NewEdgeSplit resulting from an old edge that is split by NewVertices
//
//Three types of NewEdgeSplit (plus "undetermined"), each with different "extra data":
//i)	NON_BOUNDARY; extra data: KEEP or DISCARD
//ii)	BOUNDARY_FACE; extra data: the Face on which it is contained
//		[this face and originalEdge should be on opposite polyhedra]
//iii)	BOUNDARY_EDGE; extra data: the Edge on which it is contained
//		[this edge and originalEdge should be on opposite polyhedra]
//
//The type can always be determined on creation.
//The only thing that may not be possible to determine initially is 
//KEEP or DISCARD for a NON_BOUNDARY NewEdgeSplit.
//[This might be able to be propagated; if even this doesn't work, must use
// computeLeftoverSplitEdgeTypes after NewEdgeFFI's are created.]

public abstract class NewEdgeSplit
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>, 
	Edge extends PolyhedronEdge<Vertex, Edge, Face>, 
	Face extends PolyhedronFace<Vertex, Edge, Face>> 

	extends NewEdge<Vertex, Edge, Face>
{ 
	//the type of split edge
	public static enum SplitEdgeType{
		NON_BOUNDARY, BOUNDARY_FACE, BOUNDARY_EDGE,
	}

	//the original edge this belonged to, and the polyhedron label for it
	protected Edge originalEdge;
	
	//the intersection data for each endpoint
	//i.e., thisObject of both should be either originalEdge or an endpoint of originalEdge.
	//these could be null, if no intersection occurred at one of the endpoints
	Intersection<Vertex,Edge,Face> pInt, qInt;
	
	
	public NewEdgeSplit(
		NewVertex<Vertex,Edge,Face> p, NewVertex<Vertex,Edge,Face> q,
		Intersection<Vertex,Edge,Face> pInt, Intersection<Vertex,Edge,Face> qInt,
		Edge originalEdge,
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor){
			
		super(p, q, edgeConstructor);
		
		this.pInt = pInt;  
		this.qInt = qInt;  
		
		this.originalEdge = originalEdge;
		
		//update endpoints' references to include this NewEdgeSplit
		getInitialVertex().getIncidentSplitEdges().add(this);
		getFinalVertex().getIncidentSplitEdges().add(this);
	}
	
	public abstract SplitEdgeType getSplitEdgeType();
	
	public abstract NewEdgeSplitNonBoundary<Vertex,Edge,Face> getAsSplitEdgeNonBoundary();
	public abstract NewEdgeSplitBoundaryFace<Vertex,Edge,Face> getAsSplitEdgeBoundaryFace();
	public abstract NewEdgeSplitBoundaryEdge<Vertex,Edge,Face> getAsSplitEdgeBoundaryEdge();

	@Override
	public NewEdgeType getNewEdgeType(){ return NewEdgeType.SPLIT_EDGE; }
	
	@Override
	public NewEdgeFFI<Vertex, Edge, Face> getAsFaceFaceIntersection(){ return null; }

	@Override
	public NewEdgeSplit<Vertex, Edge, Face> getAsSplitEdge(){ return this; }

	
	public Edge getOriginalEdge(){
		return originalEdge;
	}
}
