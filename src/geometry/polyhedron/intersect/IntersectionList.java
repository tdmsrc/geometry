package geometry.polyhedron.intersect;

import java.util.LinkedList;

import geometry.polyhedron.components.PolyhedronComponent;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;

class IntersectionList
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>,
	 IntersectionType extends Intersection<Vertex,Edge,Face>> 

	extends LinkedList<IntersectionType> 
{
	private static final long serialVersionUID = 1L;

	/**
	 * Check if an intersection with "test" is contained in this list.
	 * (It is assumed that "thisObject" of each intersection is already known;
	 * only "intersectedObject" is checked)
	 * 
	 * @param test The object whose containment is to be checked.
	 * This could be a vertex, edge (interior), or face (interior).
	 * @return True if an intersection with "test" is in this list, false otherwise.
	 */
	/*public boolean containsIntersectedObject(PolyhedronComponent<Vertex, Edge, Face> test){
		
		for(Intersection<Vertex,Edge,Face> i : this){
			if(i.getIntersectedObject() == test){ return true; }
		}
		return false;
	}*/

	/**
	 * Check if an intersection with either endpoint of "e" is in this list.
	 * (It is assumed that "thisObject" of each intersection is already known;
	 * only "intersectedObject" is checked)
	 * 
	 * @param e The edge whose endpoints are to be checked.
	 * @return True if an intersection with an endpoint of "e" is in this list, false otherwise.
	 */
	public boolean containsEdgeBoundary(Edge e){
		
		Vertex p = e.getInitialVertex(); 
		Vertex q = e.getFinalVertex();
		
		for(Intersection<Vertex,Edge,Face> i : this){
			if((i.getIntersectedObject() == p) || (i.getIntersectedObject() == q)){ return true; }
		}
		return false;
	}
	
	/**
	 * Check if an intersection with a vertex or edge of the face "f" is in this list.
	 * (It is assumed that "thisObject" of each intersection is already known;
	 * only "intersectedObject" is checked)
	 * 
	 * @param f The face whose boundary vertices and edges are to be checked.
	 * @return True if an intersection with the boundary of "f" is in this list, false otherwise.
	 */
	public boolean containsFaceBoundary(Face f){
		
		for(Intersection<Vertex,Edge,Face> i : this){
			PolyhedronComponent<Vertex, Edge, Face> io = i.getIntersectedObject();
			switch(io.getPolyhedronComponentType()){
			case VERTEX: 
				if(io.getAsVertex().checkIncidence(f)){ return true; } 
				break;
			case EDGE: 
				if(io.getAsEdge().checkIncidence(f)){ return true; } 
				break;
			default: }
		}
		return false;
	}
}
