package geometry.polyhedron.physics;

import java.util.Collection;
import java.util.LinkedList;

import geometry.common.components.Edge3d;
import geometry.common.components.Plane;
import geometry.common.components.Vertex3d;
import geometry.math.PhysicalState3d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.math.PhysicalState3d.IntegrationConstraint;
import geometry.polyhedron.components.EmbeddedEdge;
import geometry.polyhedron.components.EmbeddedPolygon;
import geometry.polyhedron.components.Polyhedron;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFace;
import geometry.polyhedron.components.PolyhedronVertex;


public class PhysicsObject{
	
	protected static final Vector3d ACC_GRAVITY = new Vector3d(0,-9.806f,0);
	
	private static final float DRAG_FACTOR = 0.025f;
	private static final float SLEEP_V_SQ = .00002f;
	private static final float SLEEP_W_SQ = .00002f;
	
	protected String name;
	
	public PhysicalState3d state;
	public boolean immovable = false;
	public boolean allowSleepStart = false;
	public boolean sleeping = false;
	
	//propagates during collision resolution phase, then resets
	//TODO indicate a direction in which the object should not be pushed during resolution
	public boolean contactingImmovable = false;
	
	public Collection<IntegrationConstraint> constraints;
	
	public Polyhedron<?,?,?> body;
	public float radius,radiusSq; //radius of centroid-centered boundary sphere
	
	
	public PhysicsObject(String name, PhysicalState3d state, Polyhedron<?,?,?> body){
		this.name = name;
		
		this.state = state;
		this.body = body;
		
		constraints = new LinkedList<IntegrationConstraint>();
		
		//compute radius
		radiusSq = 0;
		for(PolyhedronVertex<?,?,?> v : body.getVertices()){
			float d = v.getPosition().distanceSquared(state.getOffset());
			if(d > radiusSq){ radiusSq = d; }
		}
		radius = (float)Math.sqrt(radiusSq);
	}
	
	public String getName(){
		return name;
	}
	
	
	//CONSTRAINTS
	//------------------------------------------------
	public void clearConstraints(){
		constraints.clear();
	}
	
	public void addConstraint(IntegrationConstraint c){
		constraints.add(c);
	}
	
	public Collection<IntegrationConstraint> getConstraints(){
		return constraints;
	}
	//------------------------------------------------
	
	
	public void integrate(float dt){
		
		//sleep
		//float KE = state.getKineticEnergy();
		//boolean sleepy = KE < SLEEP_KE;
		
		float wl2 = state.getW().lengthSquared();
		float vl2 = state.getV().lengthSquared();
		//float outw = Math.round(wl2*100000.0f)/100000.0f;
		//float outv = Math.round(vl2*100000.0f)/100000.0f;
		//System.out.println("Wlen: " + outw + ", Vlen: " + outv);
		boolean sleepy = (vl2 < SLEEP_V_SQ) && (wl2 < SLEEP_W_SQ);
		
		if(!sleepy){ sleeping = false; }
		else if(allowSleepStart){ sleeping = true; }
		
		if(sleeping){ return; }
		
		//integrate, ignore gravity and drag for immovable objects
		if(immovable){ state.integrate(dt); }
		else{ state.integrateWithConstraints(dt, ACC_GRAVITY, constraints); }
		
		if(!immovable){
			state.drag(dt, DRAG_FACTOR);
		}
	}
	
	
	//=======================================
	// COLLISION DETECTION
	//=======================================
	
	private static final float MAX_PENETRATION_DEPTH = 10.0f;
	
	
	public static class CollisionPoint{
		public Vector3d x;
		public Vector3d xn;
		public PolyhedronEdge<?,?,?> e;
		public PolyhedronFace<?,?,?> f;
		
		public CollisionPoint(Vector3d x, Vector3d xn, PolyhedronEdge<?,?,?> e, PolyhedronFace<?,?,?> f){
			this.x = x;
			this.xn = xn;
			this.e = e;
			this.f = f;
		}
	}
	
	//TODO return overall depth
	public static float getObjCollision(LinkedList<CollisionData> addTo, PhysicsObject obj1, PhysicsObject obj2,
		Collection<CollisionPoint> DEBUGcollisionsDraw, 
		Collection<CollisionPoint> DEBUGcollisionsDrawAvg){ //TODO
		
		//nothing to do if both immovable
		if(obj1.immovable && obj2.immovable){ return 0; }
		
		//quick rejection via bounding spheres
		float d = obj1.state.getTranslation().distance(obj2.state.getTranslation());
		if(d > (obj1.radius + obj2.radius)){ return 0; }
		
		//find edge-face collisions (normals point toward obj1)
		LinkedList<CollisionPoint> collisions1 = new LinkedList<CollisionPoint>(); //obj1 edges 
		LinkedList<CollisionPoint> collisions2 = new LinkedList<CollisionPoint>(); //obj2 edges
		getEdgeFaceIntersections(collisions1, false, obj1, obj2);  
		getEdgeFaceIntersections(collisions2, true, obj2, obj1);
		
		LinkedList<CollisionPoint> collisions = new LinkedList<CollisionPoint>();
		collisions.addAll(collisions1);
		collisions.addAll(collisions2);
		
		//do nothing if no collisions
		int k = collisions.size();
		if(k == 0){ return 0; }
		
		//collision point
		final Vector3d cx = new Vector3d();
		for(CollisionPoint c : collisions){ cx.add(c.x); }
		cx.scale(1.0f / (float)k);
		
		//collision normal
		Vector3d cnAvg = new Vector3d();
		for(CollisionPoint c : collisions){ cnAvg.add(c.xn); }
		//TODO pretty lame method, but it works reasonably well
		//------------------------------
		final Vector3d cn = new Vector3d();
		Vector3d dx0 = collisions.get(0).x.copy(); dx0.subtract(cx);
		for(int i=1; i<collisions.size(); i++){
			Vector3d dxi = collisions.get(i).x.copy(); dxi.subtract(cx);
			Vector3d cni = dxi.cross(dx0);
			if(cni.dot(cnAvg) < 0){ cni.flip(); }
			cn.add(cni);
		}
		if(cn.length() < 0.001f){ return 0; }
		cn.normalize();
		//------------------------------
		
		//TODO depth
		//------------------------------
		float depth = 0; boolean haveDepth = false;
		
		Vector3d en = cn.copy();
		for(CollisionPoint c : collisions1){
			float de = computeEdgePenetrationDepth(c.x, en, c.e, obj1.state, c.f, obj2.state);
			if(!haveDepth || (de > depth)){ depth = de; haveDepth = true; }
		}
		
		en.flip();
		for(CollisionPoint c : collisions2){
			float de = computeEdgePenetrationDepth(c.x, en, c.e, obj2.state, c.f, obj1.state);
			if(!haveDepth || (de > depth)){ depth = de; haveDepth = true; }
		}
		
		if(depth > MAX_PENETRATION_DEPTH){
			depth = MAX_PENETRATION_DEPTH;
		}
		
		//DEBUG: assuming ground is plane y=0, find depth
		/*PhysicsObject grnd = obj1, obj = obj2;
		if(obj2.immovable){ grnd=obj2; obj=obj1; }
		
		float depth = 0;
		for(PolyhedronVertex<?,?,?> v : obj.body.getVertices()){
			Vector3d p = obj.state.applyToPoint(v.getPosition());
			if(-p.getY() > depth){ depth = -p.getY(); }
		}*/
		//------------------------------
		
		
		//TODO add to objects' integration constraints
		//------------------------------
		final Vector3d cn1c = cn.copy();
		final Vector3d cn2c = cn.copy(); cn2c.flip();
		final Vector3d cxc = cx.copy();
		
		obj1.addConstraint(new IntegrationConstraint(){
			@Override public Vector3d getConstraintP() { return cxc; }
			@Override public Vector3d getConstraintN() { return cn1c; }
		});
		obj2.addConstraint(new IntegrationConstraint(){
			@Override public Vector3d getConstraintP() { return cxc; }
			@Override public Vector3d getConstraintN() { return cn2c; }
		});
		//------------------------------
		
		
		addTo.add(new CollisionData(obj1, obj2, cx, cn, depth));
		if(DEBUGcollisionsDraw != null){ DEBUGcollisionsDraw.addAll(collisions); }
		if(DEBUGcollisionsDrawAvg != null){ DEBUGcollisionsDrawAvg.add(new CollisionPoint(cx, cn, null, null)); } 
		return depth;
	}
	
	/**
	 * Assume that e intersects f at x.  This method computes how far the edge e must be
	 * moved along n in order for the edge to no longer intersect f.
	 */
	private static float computeEdgePenetrationDepth(Vector3d xw, Vector3d n,
		Edge3d<?> e, PhysicalState3d eState, EmbeddedPolygon<?,?> f, PhysicalState3d fState){
		
		float EPSILON = 0.00000001f; //allow slight movement in opposite direction
		float SWEEP_EDGE_EPSILON = 0.000000001f; //max dist from moved "e" to an endpoint of "f" edge
		
		//work entirely in f coordinates
		Vector3d p = fState.unApplyToPoint(eState.applyToPoint(e.getInitialVertex().getPosition()));
		Vector3d q = fState.unApplyToPoint(eState.applyToPoint(e.getFinalVertex().getPosition()));
		Vector3d x = fState.unApplyToPoint(xw);
		
		//assume both of these are normalized
		Vector3d eDir = fState.unApplyToVector(n);
		Vector3d nf = f.getPlane().getNormal();
		
		//get distance necessary for edge to move on one side of the plane
		float depth = 0;
		float nfDoteDir = nf.dot(eDir);
		
		float dp = f.getPlane().getOrientedDistance(p);
		float dpn = -dp / nfDoteDir; 
		if(dpn > -EPSILON){ depth = dpn; }
		
		float dq = f.getPlane().getOrientedDistance(q);
		float dqn = -dq / nfDoteDir;
		if(dqn > -EPSILON){ depth = dqn; }
		
		//get distance necessary for edge to hit the boundary of the face
		Edge3d<?> pq = new Edge3d<Vertex3d>(new Vertex3d(p), new Vertex3d(q));
		Vector3d pqVec = pq.getEdgeVector();
		
		//get the plane that the edge will sweep out as it moves along eDir
		Plane edgeSweep = new Plane(x, pqVec.cross(eDir));
		
		for(EmbeddedEdge<?,?> feEmb : f.getEdges()){
			Edge3d<?> fe = feEmb.getGlobalEdge();
			
			//intersect the face's edge with the sweep plane
			Float tfe = edgeSweep.computeEdgeIntersection(fe);
			float fe_lerp_eps_min = -SWEEP_EDGE_EPSILON / fe.getLength();
			float fe_lerp_eps_max = 1.0f - fe_lerp_eps_min;
			if(tfe == null){ continue; } //edge and face are parallel
			if((tfe < fe_lerp_eps_min) || (tfe > fe_lerp_eps_max)){ continue; } //no intersection
			Vector3d xfe = fe.lerp(tfe);
			
			//see how far the edge would have to move along eDir to hit xfe
			//x + a*eDir + b*pqVec = xfe <- a,b unknown; solve for a 
			Vector3d dx = xfe.copy(); dx.subtract(x);
			
			float pq2 = pqVec.dot(pqVec), pqedir = pqVec.dot(eDir);
			float a = eDir.dot(dx)*pq2 - pqedir*pqVec.dot(dx);
			float det = eDir.dot(eDir)*pq2 - pqedir*pqedir;
			a = a / det;
			
			//use as depth if between -EPS and current value
			if(a < -EPSILON){ continue; }
			if(a < depth){ depth = a; }
		}
		 
		return depth;
	}
	
	private static void getEdgeFaceIntersections(Collection<CollisionPoint> addTo, 
		boolean flip, PhysicsObject objEdge, PhysicsObject objFace){
		
		float EPSILON = 0.0000001f;
		
		//position of objFace in objEdge body coordinates
		Vector3d fcT = objEdge.state.unApplyToPoint(objFace.state.getTranslation());
		
		
		for(PolyhedronEdge<?,?,?> e : objEdge.body.getEdges()){
			
			//edge original coordinates
			Vector3d p = e.getInitialVertex().getPosition();
			Vector3d q = e.getFinalVertex().getPosition();
			float LERP_EPSILON_MIN = -EPSILON / q.distance(p);
			float LERP_EPSILON_MAX = 1.0f - LERP_EPSILON_MIN;
			
			//quick rejection: edge outside of bounding sphere
			float tc = e.computePointIntersection(fcT);
			Vector3d xc = (tc < 0) ? p : ( (tc > 1) ? q : e.lerp(tc) );
			if(xc.distanceSquared(fcT) > objFace.radiusSq){ continue; }
			
			//edge into world coordinates
			Vector3d pw = objEdge.state.applyToPoint(p);
			Vector3d qw = objEdge.state.applyToPoint(q);
			
			//edge into objFace body coordinates
			Vertex3d pT = new Vertex3d(objFace.state.unApplyToPoint(pw));
			Vertex3d qT = new Vertex3d(objFace.state.unApplyToPoint(qw));
			
			//edge object
			Edge3d<Vertex3d> eT = new Edge3d<Vertex3d>(pT, qT);
			
			for(PolyhedronFace<?,?,?> f : objFace.body.getFaces()){
				Plane plane = f.getPlane();
				
				//ensure p,q on different sides of plane
				float dp = plane.getOrientedDistance(pT.getPosition());
				float dq = plane.getOrientedDistance(qT.getPosition());
				if((dp > EPSILON) && (dq > EPSILON)){ continue; }
				if((dp < -EPSILON) && (dq < -EPSILON)){ continue; }
				
				//make sure not parallel
				boolean dpZero = ((dp < EPSILON) && (dp > -EPSILON));
				boolean dqZero = ((dq < EPSILON) && (dq > -EPSILON));
				if(dpZero && dqZero){ continue; }
				
				//intersect edge with plane
				Float t = plane.computeEdgeIntersection(eT);
				if(t == null){ continue; } //edge and face are parallel
				if((t < LERP_EPSILON_MIN) || (t > LERP_EPSILON_MAX)){ continue; } //no intersection
				
				Vector3d x = eT.lerp(t); //intersection point
				Vector2d xp = plane.getProjection(x); //project onto plane
				if(!f.containsPoint(xp, EPSILON)){ continue; }
				
				//add vertex and face normal
				Vector3d xw = objFace.state.applyToPoint(x);
				Vector3d xnw = objFace.state.applyToVector(plane.getNormal());
				if(flip){ xnw.flip(); }
				addTo.add(new CollisionPoint(xw, xnw, e, f));
			}
		}
	}
}