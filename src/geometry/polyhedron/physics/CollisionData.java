package geometry.polyhedron.physics;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import geometry.math.PhysicalState3d;
import geometry.math.Vector3d;


public class CollisionData{
	
	public PhysicsObject obj1, obj2;
	public Vector3d p, n; //normal points toward obj1
	public float depth;
	
	public CollisionData(PhysicsObject obj1, PhysicsObject obj2, Vector3d p, Vector3d n, float depth){
		this.obj1 = obj1;
		this.obj2 = obj2;
		this.p = p;
		this.n = n;
		this.depth = depth;
	}
	
	
	//=======================================
	// COLLISION RESOLUTION
	//=======================================
	
	private static final int MAX_COLLISION_RESOLUTION_PHASES = 100000;
	
	/**
	 * Resolve all collisions in phases, propagating contactingImmovable property
	 */
	public static void resolveCollisions(Collection<PhysicsObject> objs, LinkedList<CollisionData> collisions, float e, float mu){
		
		//reset contactingImmovable and allowSleepStart properties
		for(PhysicsObject obj : objs){ 
			obj.contactingImmovable = obj.immovable;
			obj.allowSleepStart = false; 
		}
		
		//resolve any collisions between (indirectly) immovable objects
		for(int k=0; k<MAX_COLLISION_RESOLUTION_PHASES; k++){ //no reason for a limit here, but just in case...
			int cdRemoved = 0; //number of collisions handled this iteration
			
			//resolve and remove collisions involving immovable||contactingImmovable objects
			Iterator<CollisionData> it = collisions.iterator();
			while(it.hasNext()){
				CollisionData cd = it.next();
				boolean obj1stuck = cd.obj1.immovable || cd.obj1.contactingImmovable;
				boolean obj2stuck = cd.obj2.immovable || cd.obj2.contactingImmovable;
				
				//if either object is immovable||contactingImmovable, resolve and remove
				if(obj1stuck || obj2stuck){
					resolveCollision(cd, e, mu);
					it.remove(); cdRemoved++;
				}
			}
			
			//quit when no collisions involving immovable||contactingImmovable objects
			if(cdRemoved == 0){ break; }
		}
		
		//resolve the rest of the collisions
		for(CollisionData cd : collisions){
			resolveCollision(cd, e, mu);
		}
	}
	
	
	private static final float DEPTH_EPSILON = 0.0001f; //add this to depth being resolved
	
	/**
	 * Resolve a single collision
	 */
	public static void resolveCollision(CollisionData cd, float e, float mu){
		
		PhysicsObject obj1 = cd.obj1, obj2 = cd.obj2;
		boolean obj1stuck = obj1.immovable || obj1.contactingImmovable;
		boolean obj2stuck = obj2.immovable || obj2.contactingImmovable;
		
		//move away from normal
		//TODO still move contactingImmovable objects, just disallow movement in immovable direction
		Vector3d move = cd.n.copy(); move.normalize(); move.scale(cd.depth + DEPTH_EPSILON);
		
		if(!obj1stuck){
			if(obj2stuck){ obj1.state.getTranslation().add(move); }
			else{
				move.scale(0.5f);
				obj1.state.getTranslation().add(move);
				obj2.state.getTranslation().subtract(move);
			}
		}else if(!obj2stuck){
			obj2.state.getTranslation().subtract(move);
		}
				
		//perform collision
		if(obj1.immovable){
			if(!obj2.immovable){
				//velocity of p on immovable object
				Vector3d r = cd.p.copy(); r.subtract(obj1.state.getTranslation());
				Vector3d vpi = obj1.state.getVelocity(r);
				//collide
				Vector3d cdn = cd.n.copy(); cdn.flip(); //normal points toward obj1, so flip it
				obj2.state.collide(cd.p, cdn, vpi, e, mu);
				obj2.allowSleepStart = true;
			}
		}else{
			if(obj2.immovable){
				//velocity of p on immovable object
				Vector3d r = cd.p.copy(); r.subtract(obj2.state.getTranslation());
				Vector3d vpi = obj2.state.getVelocity(r);
				//collide
				obj1.state.collide(cd.p, cd.n, vpi, e, mu);
				obj1.allowSleepStart = true;
			}else{
				PhysicalState3d.collide(obj1.state, obj2.state, cd.p, cd.n, e, mu);
				if(obj1.sleeping){ obj2.allowSleepStart = true; }
				if(obj2.sleeping){ obj1.allowSleepStart = true; }
			}
		}
		
		//propagate contactingImmovable to disallow pushing into immovable object
		boolean stickobj1 = !obj1stuck && obj2stuck;
		boolean stickobj2 = !obj2stuck && obj1stuck;
		
		//TODO set immovable direction (careful with initially immovable objects...)
		if(stickobj1){ obj1.contactingImmovable = true; }
		if(stickobj2){ obj2.contactingImmovable = true; }
	}
}