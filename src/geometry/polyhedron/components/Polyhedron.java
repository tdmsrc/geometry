package geometry.polyhedron.components;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import geometry.common.GeometricComponent;
import geometry.common.MessageOutput;
import geometry.common.OrderedPair;
import geometry.common.OutputTree;
import geometry.common.PartitionCollection;
import geometry.common.PartitionNode;
import geometry.common.components.Edge3d.Edge3dConstructor;
import geometry.common.components.Vertex3d.Vertex3dConstructor;
import geometry.math.Matrix3d;
import geometry.math.ObjectTransformation3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.EmbeddedPolygon.Face3dConstructor;
import geometry.polyhedron.components.PolyhedronFace.FaceRecurseMethod;

/**
 * A Polyhedron holds the following data:
 * <ul>
 * <li><i>Sub-components</i>: 
 * <ul>
 * <li>A collection of Vertex objects (extending {@link PolyhedronVertex})
 * <li>A collection of Edge objects (extending {@link PolyhedronEdge})
 * <li>A collection of Face objects (extending {@link PolyhedronFace})
 * </ul>
 * <li><i>Extra data</i>:
 * <ul>
 * <li>A {@link PartitionCollection} of Face objects.
 * Two faces are in the same partition iff they can be joined by a sequence of incident edges.
 * </ul>
 * </ul> 
 * 
 * In general:
 * <ul>
 * <li>Polyhedron components naturally contain references to sub-components 
 * (e.g. edges refer to vertex endpoints, faces refer to vertices and edges comprising it)
 * and hold lists of super-components which are updated automatically by those 
 * super-components' constructors.
 * <li>Polyhedron components refer to Polyhedron that owns them, and this reference is updated
 * either by Polyhedron directly or by a super-component's owner being explicitly set
 * (except for disown, which should not modify sub-components).
 * </ul>
 */
public class Polyhedron
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>>

	extends GeometricComponent
{

	//components of the polyhedron
	private LinkedHashSet<Vertex> vertices;
	private LinkedHashSet<Edge> edges;
	private LinkedHashSet<Face> faces;
	
	//faces partitioned by equiv rel: f1 ~ f2 iff joined by a sequence of incident faces
	private PartitionCollection<Face> facePartitionCollection;
	
	/**
	 * Constructs a Polyhedron with the specified collection of vertices, edges, and faces.
	 * <br><br>
	 * The new Polyhedron takes ownership of all its components
	 * via the {@link PolyhedronComponent#setOwner(Polyhedron)} method.
	 * <br><br>
	 * The Face partition data is initialized using
	 * {@link PolyhedronTools#computeFacePartitions(Collection)}.
	 * 
	 * @param vertices The collection of vertices of this Polyhedron.
	 * @param edges The collection of edges of this Polyhedron.
	 * @param faces The collection of faces of this Polyhedron.
	 */
	public Polyhedron(LinkedHashSet<Vertex> vertices, LinkedHashSet<Edge> edges, LinkedHashSet<Face> faces){
		
		//store components
		this.vertices = vertices;
		this.edges = edges;
		this.faces = faces;
		
		//trickle down owner from faces
		for(Face face : faces){
			face.setOwner(this);
		}
		
		//set combinatorial components for faces
		facePartitionCollection = PolyhedronTools.<Vertex,Edge,Face>computeFacePartitions(faces);
	}
	
	/**
	 * Constructs a Polyhedron with the specified collection of faces.
	 * <br>
	 * The collections of vertices and edges of the new Polyhedron are determined from the 
	 * edges occurring as "global edges" (i.e., edges in 3D) of the faces in the specified
	 * collection, and the endpoints of those edges.
	 * <br><br>
	 * The new Polyhedron takes ownership of all its components
	 * via the {@link PolyhedronComponent#setOwner(Polyhedron)} method.
	 * <br><br>
	 * The Face partition data is initialized using
	 * {@link PolyhedronTools#computeFacePartitions(Collection)}.
	 * 
	 * @param faces The collection of faces of this Polyhedron.
	 */
	public Polyhedron(LinkedHashSet<Face> faces){
		
		//store components
		this.faces = faces; //new HashSet<Face>(faces);
		
		this.edges = new LinkedHashSet<Edge>();
		for(Face face : faces){
			for(EmbeddedEdge<Vertex,Edge> embEdge : face.getEdges()){
				edges.add(embEdge.getGlobalEdge());
			}
		}
		
		this.vertices = new LinkedHashSet<Vertex>();
		for(Edge edge : edges){
			vertices.add(edge.getInitialVertex());
			vertices.add(edge.getFinalVertex());
		}
		
		//trickle down owner from faces
		for(Face face : faces){
			face.setOwner(this);
		}
		
		//set combinatorial components for faces
		facePartitionCollection = PolyhedronTools.<Vertex,Edge,Face>computeFacePartitions(faces);
	}
	
	//generic implementation of constructor for subclasses
	public static interface PolyhedronConstructor
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>,
		 Poly extends Polyhedron<Vertex, Edge, Face>>{
		
		public Poly construct(LinkedHashSet<Vertex> vertices, LinkedHashSet<Edge> edges, LinkedHashSet<Face> faces);
	}
	
	/**
	 * Returns the set of vertices of this polyhedron.
	 * 
	 * @return The set of vertices.
	 */
	public LinkedHashSet<Vertex> getVertices(){ return vertices; }	
	
	/**
	 * Returns the set of edges of this polyhedron.
	 * 
	 * @return The set of edges.
	 */
	public LinkedHashSet<Edge> getEdges(){ return edges; }	
	
	/**
	 * Returns the set of faces of this polyhedron.
	 * 
	 * @return The set of faces.
	 */
	public LinkedHashSet<Face> getFaces(){ return faces; }	
	
	/**
	 * Returns the set of combinatorial connected components
	 * for this polyhedron.
	 * 
	 * Two faces are in the same combinatorial component iff they are
	 * connected by a sequence of incident faces. 
	 */
	public PartitionCollection<Face> getFacePartitionCollection(){
		return facePartitionCollection;
	}

	/**
	 *  Determines if "p" is in the interior of the polyhedron.
	 *  
	 *  @see {@link PolyhedronTools#containsPoint(Collection, Collection, Collection, Vector3d)}
	 */
	public boolean containsPoint(Vector3d p){
		return PolyhedronTools.containsPoint(vertices, edges, faces, p);
	}
	
	/**
	 *  Determines if "p" is in the interior of the polyhedron.
	 *  
	 *  @param epsilon Return true if p is this close to the boundary of the polyhedron.
	 *  @see {@link PolyhedronTools#containsPoint(Collection, Collection, Collection, Vector3d, float)}
	 */
	public boolean containsPoint(Vector3d p, float epsilon){
		return PolyhedronTools.containsPoint(vertices, edges, faces, p, epsilon);
	}
	
	/**
	 * Determines the signed volume of this polyhedron.
	 * 
	 * @return The signed volume of this polyhedron.
	 */
	public float computeSignedVolume(){
		return PolyhedronTools.computeSignedVolume(faces);
	}
	
	/**
	 * Determines the centroid of this polyhedron (i.e., the center of mass 
	 * assuming uniform density) scaled by the signed volume.
	 * 
	 * @return The centroid scaled by the volume.
	 */
	public Vector3d computeVolumeTimesCentroid(){
		return PolyhedronTools.computeVolumeTimesCentroid(faces);
	}
	
	/**
	 * Determines the inertia tensor for this polyhedron.
	 * <br>
	 * Assumes unit density; the result can be scaled by the desired density.
	 * 
	 * @return The inertia tensor as a {@link Matrix3d}.
	 */
	public Matrix3d computeInertiaTensor(){
		return PolyhedronTools.computeInertiaTensor(faces);
	}
	
	/**
	 * Flips the normal of every plane on which the faces are embedded.
	 */
	public void flip(){
		for(Face face : faces){ face.flip(); }
	}
	
	/**
	 * Clears everything, leaving an empty Polyhedron.
	 */
	public void clear(){
		vertices = new LinkedHashSet<Vertex>();
		edges = new LinkedHashSet<Edge>();
		faces = new LinkedHashSet<Face>();
		
		facePartitionCollection = new PartitionCollection<Face>();
	}
	
	
	//===============================================
	// COPY
	//===============================================
	
	/**
	 * Return a new Polyhedron with the specified transformation applied.
	 * Has no effect on this Polyhedron or any of its associated data. 
	 * All components are duplicated.
	 */
	public Polyhedron<Vertex,Edge,Face> copy(ObjectTransformation3d trans,
		Vertex3dConstructor<Vertex> vertexConstructor,
		Edge3dConstructor<Vertex, Edge> edgeConstructor,
		Face3dConstructor<Vertex, Edge, Face> faceConstructor){
		
		//create new vertices
		LinkedHashSet<Vertex> newVertices = new LinkedHashSet<Vertex>();
		HashMap<Vertex,Vertex> newVertexFromOld = new HashMap<Vertex,Vertex>(); 
		
		for(Vertex vertex : vertices){
			
			Vertex newVertex = vertexConstructor.construct(
				trans.applyToPoint(vertex.getPosition()));
			
			newVertices.add(newVertex);
			newVertexFromOld.put(vertex, newVertex);
		}
		
		//create new edges
		LinkedHashSet<Edge> newEdges = new LinkedHashSet<Edge>();
		HashMap<Edge,Edge> newEdgeFromOld = new HashMap<Edge,Edge>();
		
		for(Edge edge : edges){
			
			Edge newEdge = edgeConstructor.construct(
				newVertexFromOld.get(edge.getInitialVertex()), 
				newVertexFromOld.get(edge.getFinalVertex()));
			
			newEdges.add(newEdge);
			newEdgeFromOld.put(edge, newEdge);
		}
		
		//create new faces
		LinkedHashSet<Face> newFaces = new LinkedHashSet<Face>();
		
		for(Face face : faces){
			
			//create new embedded vertices
			LinkedHashSet<EmbeddedVertex<Vertex,Edge>> newEmbVertices = 
				new LinkedHashSet<EmbeddedVertex<Vertex,Edge>>();
			HashMap<EmbeddedVertex<Vertex,Edge>,EmbeddedVertex<Vertex,Edge>> newEmbVertexFromOld = 
				new HashMap<EmbeddedVertex<Vertex,Edge>,EmbeddedVertex<Vertex,Edge>>(); 
			
			for(EmbeddedVertex<Vertex,Edge> embVertex : face.getVertices()){
				
				EmbeddedVertex<Vertex,Edge> newEmbVertex = new EmbeddedVertex<Vertex,Edge>(
					embVertex.getPosition().copy(),
					newVertexFromOld.get(embVertex.getGlobalVertex())
				);
				
				newEmbVertices.add(newEmbVertex);
				newEmbVertexFromOld.put(embVertex, newEmbVertex);
			}
			
			//create new embedded edges
			LinkedHashSet<EmbeddedEdge<Vertex,Edge>> newEmbEdges = 
				new LinkedHashSet<EmbeddedEdge<Vertex,Edge>>();
			
			for(EmbeddedEdge<Vertex,Edge> embEdge : face.getEdges()){
				
				EmbeddedEdge<Vertex,Edge> newEmbEdge = new EmbeddedEdge<Vertex,Edge>(
					newEmbVertexFromOld.get(embEdge.getInitialVertex()),
					newEmbVertexFromOld.get(embEdge.getFinalVertex()),
					newEdgeFromOld.get(embEdge.getGlobalEdge()),
					embEdge.getGlobalEdgeRelativeOrientation()
				);
				
				newEmbEdges.add(newEmbEdge);
			}
			
			//create new face
			Face newFace = faceConstructor.construct(
				face.getPlane().copy(trans), newEmbVertices, newEmbEdges);
			
			newFaces.add(newFace);
		}
		
		//construct new polyhedron and return
		return new Polyhedron<Vertex,Edge,Face>(newVertices, newEdges, newFaces);
	}
	
	//===============================================
	// SERIALIZATION
	//===============================================
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("Polyhedron (index: " + getIndex() + ")");
		
		for(Vertex vertex : vertices){
			OutputTree child = outputTree.spawnChild("Vertex");
			vertex.populateOutputTree(child);
		}
		
		for(Edge edge : edges){
			OutputTree child = outputTree.spawnChild("Edge");
			edge.populateOutputTree(child);
		}
		
		for(Face face : faces){
			OutputTree child = outputTree.spawnChild("Face");
			face.populateOutputTree(child);
		}
	}
	
	
	//==========================================================
	// CUT AND GLUE
	//==========================================================
	
	/**
	 * Removes the face partition which contains "face".
	 * 
	 * @param face A face contained in the partition to be removed.
	 */
	public void removeComponent(Face face){
		
		//remove face partition
		facePartitionCollection.remove(face.getFacePartitionNode());
		
		//remove components
		face.recurse(new FaceRecurseMethod<Face>(){
			@Override
			public boolean action(Face previousFace, Face thisFace) {

				//remove vertices
				for(EmbeddedVertex<Vertex,Edge> embVertex : thisFace.getVertices()){
					Vertex vertex = embVertex.getGlobalVertex();
					vertex.disown();
					vertices.remove(vertex);
				}
				
				//remove edges
				for(EmbeddedEdge<Vertex,Edge> embEdge : thisFace.getEdges()){
					Edge edge = embEdge.getGlobalEdge();
					edge.disown();
					edges.remove(edge);
				}
				
				//remove face
				thisFace.disown();
				faces.remove(thisFace);
				
				return true;
			}
			
		});
	}
	
	/**
	 * Removes a collection of faces from the Polyhedron, and then fixes
	 * the face partition collection.
	 */
	public void removeFaces(Collection<Face> removeFaces){
		
		for(Face face : removeFaces){
			removeFace(face);
		}

		//[TODO] fix this
		facePartitionCollection = PolyhedronTools.<Vertex,Edge,Face>computeFacePartitions(faces);
	}
	
	/**
	 * Removes a face from this polyhedron; does not fix face
	 * partition collection.
	 * 
	 * After removing the face, removes from this polyhedron
	 * any edges which have no incident faces.
	 * 
	 * After removing such edges, removes from this polyhedron
	 * any vertices which have no incident edges.
	 * 
	 * Note that this breaks the face in that its component
	 * vertices and edges will no longer recognize it as being
	 * incident.  This will also break any isolated edges that
	 * are removed in the sense that their endpoints will no
	 * longer recognize them as being incident. 
	 */
	private void removeFace(Face removeFace){
		
		//set face's owner to null and remove from polyhedron face set
		if(!faces.remove(removeFace)){ return; }
		removeFace.disown();
		
		//remove references to "face" from its edges and vertices
		for(EmbeddedEdge<Vertex,Edge> embEdge : removeFace.getEdges()){
			embEdge.getGlobalEdge().removeIncidentFace(removeFace);
		}
		for(EmbeddedVertex<Vertex,Edge> embVertex : removeFace.getVertices()){
			embVertex.getGlobalVertex().removeIncidentFace(removeFace);
		}
		
		//check for isolated edges
		for(EmbeddedEdge<Vertex,Edge> embEdge : removeFace.getEdges()){
			Edge edge = embEdge.getGlobalEdge(); 
			if(!edge.isIsolated()){ continue; }
				
			//set edge's owner to null and remove from polyhedron edge set
			edge.disown();
			edges.remove(edge);
			//remove references to "edge" from its endpoints
			edge.getInitialVertex().removeIncidentEdge(edge);
			edge.getFinalVertex().removeIncidentEdge(edge);
		}
		
		//check for isolated vertices
		for(EmbeddedVertex<Vertex,Edge> embVertex : removeFace.getVertices()){
			Vertex vertex = embVertex.getGlobalVertex();
			if(!vertex.isIsolated()){ continue; }
			
			//set vertex's owner to null and remove from polyhedron vertex set
			vertex.disown();
			vertices.remove(vertex);
		}
	}
	
	/**
	 * Glue the Polyhedron rhs onto this Polyhedron by gluing each specified
	 * pair of edges.  Also glues corresponding initial vertices and final vertices
	 * together (always initial to initial and final to final). 
	 * Updates all necessary component cross-references.
	 * <br><br>
	 * Any edge of "rhs" which is glued, and any vertex of "rhs" which is incident to a
	 * glued edge, will be replaced in the newly-assembled Polyhedron by its correpsonding
	 * component from "lhs".  In other words, the components of "rhs" that survive
	 * the gluing process are exactly the ones that are not part of the glue.
	 * <br><br>
	 * After gluing, this method clears the Polyhedron "rhs".
	 * 
	 * @param rhs A Polyhedron; will be cleared after this operation.
	 * @param gluedEdges A collection of OrderedPair<Edge> objects which indicate
	 * which edges are to be glued.  The A object of the pair should be an 
	 * edge from this Polyhedron, and the B object should be from "rhs".
	 */
	public void glue(
		Polyhedron<Vertex,Edge,Face> rhs,
		Collection<OrderedPair<Edge>> gluedEdges){
		
		//first, merge component lists 
		//(some vertices and edges will get removed later--the ones being glued)
		vertices.addAll(rhs.vertices);
		edges.addAll(rhs.edges);
		faces.addAll(rhs.faces);
		
		//take ownership of faces in rhs; trickles down to edges and vertices
		for(Face face : rhs.faces){
			face.setOwner(this);
		}
		
		//merge partition collections (will need to merge some partitions after gluing edges)
		for(PartitionNode<Face> root : rhs.facePartitionCollection.getRoots()){
			facePartitionCollection.add(root);
		}
		
		//make list of glued RHS vertices and edges; while doing this, remove them from
		//component list.  also, make pairs of RHS and LHS vertices that will get glued.
		Collection<OrderedPair<Vertex>> gluedVertices = new LinkedList<OrderedPair<Vertex>>();

		HashSet<Vertex> gluedVerticesRhs = new HashSet<Vertex>();
		HashSet<Edge> gluedEdgesRhs = new HashSet<Edge>();
		
		for(OrderedPair<Edge> edgePair : gluedEdges){
			Edge edgeLhs = edgePair.getA();
			Edge edgeRhs = edgePair.getB();
			
			gluedEdgesRhs.add(edgeRhs);
			edges.remove(edgeRhs);
			
			//initial vertex of edgeRhs
			if(gluedVerticesRhs.add(edgeRhs.getInitialVertex())){
				//if this hasn't already been added to the set of RHS vertices,
				//remove from component list and add glued vertex pair
				gluedVertices.add(new OrderedPair<Vertex>(
					edgeLhs.getInitialVertex(),
					edgeRhs.getInitialVertex()
				));
				vertices.remove(edgeRhs.getInitialVertex());
			}
			
			//final vertex of edgeRhs
			if(gluedVerticesRhs.add(edgeRhs.getFinalVertex())){
				//if this hasn't been already added to the set of RHS vertices,
				//remove from component list and add glued vertex pair
				gluedVertices.add(new OrderedPair<Vertex>(
					edgeLhs.getFinalVertex(),
					edgeRhs.getFinalVertex()
				));
				vertices.remove(edgeRhs.getFinalVertex());
			}
		}
		
		//now glue specified vertices and edges together, 
		//updating incidence references and merging face partitions:
		
		//glue vertices
		for(OrderedPair<Vertex> vertexPair : gluedVertices){
			Vertex vertexLhs = vertexPair.getA();
			Vertex vertexRhs = vertexPair.getB();

			//for each embedded vertex pointing to vertexRhs, point it instead to vertexLhs
			//also, add faces incident to vertexRhs to vertexLhs's incident face set
			for(Face face : vertexRhs.getIncidentFaces()){
				EmbeddedVertex<Vertex,Edge> embVertex = vertexRhs.getEmbeddedVertex(face);
				embVertex.setGlobalVertex(vertexLhs);
				
				vertexLhs.addIncidentFace(face, embVertex);
			}
			
			//add edges incident to vertexRhs to vertexLhs's incident edge set
			//but only if it is not one of the glued edges from rhs
			for(Edge edge : vertexRhs.getIncidentEdges()){
				if(gluedEdgesRhs.contains(edge)){ continue; }
				
				//change endpoint of edge to vertexLhs
				if(edge.getInitialVertex() == vertexRhs){
					edge.setInitialVertex(vertexLhs);
				}else if(edge.getFinalVertex() == vertexRhs){
					edge.setFinalVertex(vertexLhs);
				}
				
				vertexLhs.addIncidentEdge(edge);
			}
		}
		
		//glue edges
		for(OrderedPair<Edge> edgePair : gluedEdges){
			Edge edgeLhs = edgePair.getA();
			Edge edgeRhs = edgePair.getB();
			
			//merge newly-incident partitions
			facePartitionCollection.mergePartitions(
				edgeLhs.getIncidentFaces().iterator().next().getFacePartitionNode(), 
				edgeRhs.getIncidentFaces().iterator().next().getFacePartitionNode()
			);
			
			//for each embedded edge pointing to edgeRhs, point it instead to edgeLhs
			//[need not change relative orientation; assumes edges are glued without flipping]
			//also, add faces incident to edgeRhs to edgeLhs's incident face set
			for(Face face : edgeRhs.getIncidentFaces()){
				EmbeddedEdge<Vertex,Edge> embEdge = edgeRhs.getEmbeddedEdge(face);
				embEdge.setGlobalEdge(edgeLhs, embEdge.getGlobalEdgeRelativeOrientation());
				
				edgeLhs.addIncidentFace(face, embEdge);
			}
		}

		//this process breaks the RHS polyhedron, so clear it
		rhs.clear();
	}

	
	//==========================================================
	// CROSS-REFERENCE AND COMBINATORIAL VERIFICATION
	//==========================================================
	
	/**
	 * Makes sure every incident or sub-component reference is cross-referenced 
	 * correctly; makes sure embedded components match with global references;
	 * makes sure every "owner" is set correctly.
	 * 
	 * @return Returns true if verification passed, false if there was a warning.
	 */
	public boolean verifyIncidenceReferences(){
		
		boolean pass = true;
		MessageOutput.printDebug("Verifying polyhedron component incidence references...");
		
		for(Vertex vertex : vertices){
			//make sure owner is set to this
			if(vertex.getOwner() != this){
				MessageOutput.printDebug("Vertex owner not set to this polyhedron.");
				pass = false;
			}
			
			//make sure incident edges have this vertex as an endpoint,
			//and belong to the polyhedron's edge set
			for(Edge edge : vertex.getIncidentEdges()){
				if(!edges.contains(edge)){
					MessageOutput.printDebug("Vertex incident edge does not belong to polyhedron edge set.");
					pass = false;
				}
				if((edge.getInitialVertex() != vertex) && (edge.getFinalVertex() != vertex)){
					MessageOutput.printDebug("Vertex incident edge does not have vertex as an endpoint.");
					pass = false;
				}
			}
			
			//make sure incident faces contain this vertex,
			//and belong to polyhedron's face set
			for(Face face : vertex.getIncidentFaces()){
				if(!faces.contains(face)){
					MessageOutput.printDebug("Vertex incident face does not belong to polyhedron face set.");
					pass = false;
				}
				if(!face.getGlobalVertices().contains(vertex)){
					MessageOutput.printDebug("Vertex incident face does not have vertex as component.");
					pass = false;
				}
			}
			
			//make sure embedded vertex references have this vertex as global vertex
			for(Face face : vertex.getIncidentFaces()){
				if(vertex.getEmbeddedVertex(face).getGlobalVertex() != vertex){
					MessageOutput.printDebug("Vertex embedding does not refer to vertex as global vertex");
					pass = false;
				}
			}
		}
		
		for(Edge edge : edges){
			//make sure owner is set to this
			if(edge.getOwner() != this){
				MessageOutput.printDebug("Edge owner not set to this polyhedron.");
				pass = false;
			}
			
			//make sure incident faces contain this edge,
			//and belong to polyhedron's face set
			for(Face face : edge.getIncidentFaces()){
				if(!faces.contains(face)){
					MessageOutput.printDebug("Edge incident face does not belong to polyhedron face set.");
					pass = false;
				}
				if(!face.getGlobalEdges().contains(edge)){
					MessageOutput.printDebug("Edge incident face does not have edge as component.");
					pass = false;
				}
			}
			
			//make sure embedded edge references have this edge as global edge
			for(Face face : edge.getIncidentFaces()){
				if(edge.getEmbeddedEdge(face).getGlobalEdge() != edge){
					MessageOutput.printDebug("Edge embedding does not refer to edge as global edge.");
					pass = false;
				}
			}
			
			//make sure endpoints have this edge as an incident edge,
			//and belong to polyhedron's vertex set
			if(!edge.getInitialVertex().getIncidentEdges().contains(edge) || !edge.getFinalVertex().getIncidentEdges().contains(edge)){
				MessageOutput.printDebug("Edge endpoint does not have edge as incident edge.");
				pass = false;
			}
			if(!vertices.contains(edge.getInitialVertex()) || !vertices.contains(edge.getFinalVertex())){
				MessageOutput.printDebug("Edge endpoint does not belong to polyhedron vertex set.");
				pass = false;
			}
		}
		
		for(Face face : faces){
			//make sure owner is set to this
			if(face.getOwner() != this){
				MessageOutput.printDebug("Face owner not set to this polyhedron.");
				pass = false;
			}
			
			//make sure vertices have this face as an incident face,
			//and belong to polyhedron's vertex set
			for(Vertex vertex : face.getGlobalVertices()){
				if(!vertex.getIncidentFaces().contains(face)){
					MessageOutput.printDebug("Face component vertex does not have face as an incident face.");
					pass = false;
				}
				if(!vertices.contains(vertex)){
					MessageOutput.printDebug("Face component vertex does not belong to polyhedron vertex set.");
					pass = false;
				}
			}
			
			//make sure edges have this face as incident face,
			//and belong to polyhedron's edge set
			for(Edge edge : face.getGlobalEdges()){
				if(!edge.getIncidentFaces().contains(face)){
					MessageOutput.printDebug("Face component edge does not have face as an incident face.");
					pass = false;
				}
				if(!edges.contains(edge)){
					MessageOutput.printDebug("Face component edge does not belong to polyhedron edge set.");
					pass = false;
				}
			}
		}
		
		MessageOutput.printDebug("Finished.");
		return pass;
	}
	
	/**
	 * Makes sure every point on edges and vertices satisfy "singular manifold"
	 * condition.
	 * 
	 * @return Returns true if verification passed, false if there was a warning.
	 */
	public boolean verifySingularManifold(){
		//[TODO]
		
		boolean pass = true;
		MessageOutput.printDebug("Verifying polyhedron singular manifold condition...");
		
		for(Edge edge : edges){
			if(edge.getIncidentFaces().size() < 2){
				MessageOutput.printDebug("Edge has " + edge.getIncidentFaces().size() + " incident faces.");
				pass = false;
			}
		}
		
		MessageOutput.printDebug("Finished.");
		return pass;
	}
}
