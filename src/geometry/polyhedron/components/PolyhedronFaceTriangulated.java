package geometry.polyhedron.components;

import java.util.LinkedHashSet;
import java.util.List;

import geometry.common.components.Plane;
import geometry.polygon.components.PolygonTools;
import geometry.polygon.triangulate.Triangle2d;

/**
 * Same as a {@link PolyhedronFace}, but with a collection of {@link Triangle2d} objects
 * representing a triangulation of the face.  The triangulation is computed in the
 * constructor of this object.
 */
public abstract class PolyhedronFaceTriangulated
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFaceTriangulated<Vertex, Edge, Face>>

	extends PolyhedronFace<Vertex, Edge, Face>
{

	private List<Triangle2d<EmbeddedVertex<Vertex,Edge>>> triangles;
	
	/**
	 * Creates a {@link PolyhedronFace} with specified data, then computes
	 * the triangulation.
	 * 
	 * @see {@link PolyhedronFace#PolyhedronFace(Plane, LinkedHashSet, LinkedHashSet)}
	 */
	public PolyhedronFaceTriangulated(Plane plane, 
		LinkedHashSet<EmbeddedVertex<Vertex,Edge>> vertices, 
		LinkedHashSet<EmbeddedEdge<Vertex,Edge>> edges){
		
		super(plane, vertices, edges);
		
		triangles = PolygonTools.triangulate(getVertices());
	}
	
	//when flipping polygon orientation, need to flip its constituent triangles as well
	@Override
	public void flip(){
		super.flip();
		
		for(Triangle2d<?> tri : triangles){ tri.flip(); }
	}
	
	/**
	 * Retrieve the list of {@link Triangle2d} objects making up this face.
	 */
	public List<Triangle2d<EmbeddedVertex<Vertex,Edge>>> getTriangles(){
		return triangles;
	}
}
