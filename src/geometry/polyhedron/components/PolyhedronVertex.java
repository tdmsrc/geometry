package geometry.polyhedron.components;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import geometry.common.components.Vertex3d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;

/**
 * A PolyhedronVertex extends {@link Vertex3d} and holds the following data:
 * <ul>
 * <li><i>Sub-components</i> (inherited from {@link Vertex3d}): 
 * <ul>
 * <li>Reference to position as {@link Vector3d} 
 * </ul>
 * <li><i>Super-components</i>:
 * <ul>
 * <li>Reference to {@link Polyhedron} that owns this component
 * <li>Collection of incident Edge objects
 * <li>Collection of incident Face objects
 * </ul> 
 * </ul>
 * 
 * Automatic cross-reference updating:
 * <ul>
 * <li>If a {@link Polyhedron} is created with this vertex specified as belonging to it, {@link #setOwner}
 * is automatically called to take ownership of this vertex.
 * <li>If a {@link PolyhedronEdge} is created with this vertex as an endpoint, {@link #addIncidentEdge}
 * is called automatically to add it to this vertex's collection of incident edges.
 * <li>If a {@link PolyhedronFace} is created with this vertex referenced as a "global vertex" of one
 * of its vertices, {@link #addIncidentFace} is called automatically to add it to this vertex's
 * collection of incident faces.
 * </ul>
 */
public abstract class PolyhedronVertex
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>>

	extends Vertex3d
	implements PolyhedronComponent<Vertex, Edge, Face>
{
	
	//polyhedron this component belongs to
	protected Polyhedron<Vertex, Edge, Face> owner;
	
	//incident edge data
	protected HashSet<Edge> incidentEdges;
	//incident face data
	protected HashSet<Face> incidentFaces;
	protected HashMap<Face, EmbeddedVertex<Vertex, Edge>> embVertices;
	
	
	/**
	 * Creates a PolyhedronVertex with the specified position,
	 * "null" as owner, and empty collections of incident components.
	 * 
	 * @see {@link Vertex3d#Vertex3d(Vector3d)}
	 */
	public PolyhedronVertex(Vector3d v) {
		super(v);
		owner = null;
		
		//initialize incidence data
		incidentEdges = new HashSet<Edge>();
		incidentFaces = new HashSet<Face>();
		embVertices = new HashMap<Face, EmbeddedVertex<Vertex, Edge>>();
	}

	protected abstract Vertex getThisVertex();
	public abstract Vertex copy();
	
	@Override public Vertex getAsVertex(){ return getThisVertex(); }
	@Override public Edge getAsEdge(){ return null; }
	@Override public Face getAsFace(){ return null; }
	
	@Override public PolyhedronComponentType getPolyhedronComponentType(){ return PolyhedronComponentType.VERTEX; }
	
	/**
	 * Return {@link Polyhedron} that owns this vertex.
	 */
	@Override
	public Polyhedron<Vertex,Edge,Face> getOwner(){ return owner; }
	
	/**
	 * Set to null the reference to the {@link Polyhedron} that owns this vertex. <br> 
	 * This has no effect on components incident to this vertex. <br>
	 * This has no effect on the component lists of the current owner.
	 */
	@Override
	public void disown(){ owner = null; }
	
	/**
	 * Set {@link Polyhedron} that owns this vertex to "owner". <br>
	 * This has no effect on components incident to this vertex. <br>
	 * This has no effect on the component list of the current or new owner.
	 */
	@Override
	public void setOwner(Polyhedron<Vertex,Edge,Face> owner){
		this.owner = owner;
	}
	
	/**
	 * Returns the set of edges incident to this vertex.
	 * 
	 * @return The set of edges incident to this vertex.
	 */
	public Set<Edge> getIncidentEdges(){
		return incidentEdges;
	}
	
	/**
	 * Returns the set of faces incident to this vertex.
	 * 
	 * @return The set of faces incident to this vertex.
	 */
	public Set<Face> getIncidentFaces(){
		return incidentFaces;
	}
	
	/**
	 * Returns true if this vertex has no incident edges, otherwise false.
	 * 
	 * @return True if this vertex has no incident edges, otherwise false.
	 */
	public boolean isIsolated(){
		return incidentEdges.isEmpty();
	}

	/**
	 * Add reference to an incident edge.
	 * 
	 * @param edge The edge to be specified as incident to this vertex.
	 */
	protected void addIncidentEdge(Edge edge){
		incidentEdges.add(edge);
	}
	
	/**
	 * Add reference to an incident face.
	 * Must also specify the embedded vertex of that face which links to this vertex.
	 * 
	 * @param face The face to be specified as incident to this vertex.
	 * @param embVertex The embedded vertex of "face" which links to this vertex.
	 */
	protected void addIncidentFace(Face face, EmbeddedVertex<Vertex, Edge> embVertex){
		incidentFaces.add(face);
		embVertices.put(face, embVertex);
	}
	
	/**
	 * Remove reference to an incident edge. 
	 *  
	 * @param edge The edge whose reference is to be removed.
	 * @see Polyhedron.removeFace
	 */
	protected void removeIncidentEdge(Edge edge){
		incidentEdges.remove(edge);
	}

	/**
	 * Remove reference to an incident face. 
	 *  
	 * @param face The face whose reference is to be removed.
	 * @see Polyhedron.removeFace
	 */
	protected void removeIncidentFace(Face face){
		incidentFaces.remove(face);
		embVertices.remove(face);
	}
	
	/**
	 * Check if "edge" has been specified as being incident to this vertex.
	 * 
	 * @param edge The edge whose incidence to this vertex is to be checked.
	 * @return True if the edge is incident, false otherwise.
	 */
	public boolean checkIncidence(Edge edge){
		return incidentEdges.contains(edge);
	}
	
	/**
	 * Check if "face" has been specified as being incident to this vertex.
	 * 
	 * @param face The face whose incidence to this vertex is to be checked.
	 * @return True if the face is incident, false otherwise.
	 */
	public boolean checkIncidence(Face face){
		return incidentFaces.contains(face);
	}
	
	/**
	 * Return the embedded vertex of "face" which links to this vertex.
	 * If this vertex is not incident to "face", returns null.
	 * 
	 * @param face The incident face whose embedded vertex is to be returned.
	 * @return The embedded vertex of "face" which links to this vertex.
	 */
	public EmbeddedVertex<Vertex, Edge> getEmbeddedVertex(Face face){
		return embVertices.get(face);
	}
	
	/**
	 * Checks whether, if the vector "v" were placed with its head on 
	 * this vertex, points infinitesimally near the head of "v" 
	 * would be inside the polyhedron of which this vertex is a component.
	 * 
	 * Assumes points infinitesimally near the head of "v" are not on any edge
	 * or face incident to this vertex.
	 * 
	 * @param v The vector whose head would be placed on this vertex.
	 * @return True if points infinitesimally near the head of "v" 
	 * are inside the polyhedron, false otherwise.
	 */
	@Override
	public boolean checkInfinitesimalInterior(Vector3d v){
		
		//The method here is similar to Polygon.containsPoint
		//[if we place an epsilon-radius sphere around this vertex, and intersect
		// the vector v, as well as the faces and edges incident to this vertex,
		// then we get a polygonal edge loop on the sphere, and a point to check]
		//Method: For each face F, find d_F = v.e_F, where e_F is the vector based at
		//this vertex, is contained in F, and which minimizes v.e among all such vectors.
		//If F is the face for which d_F is minimal among all faces, do inf check on F.
		//[need not bother normalizing v; all d values will be v.length*cos, so comparable]
		
		PolyhedronComponent<Vertex,Edge,Face> nearestObject = null;
		boolean hadFirst = false;
		float dmin = 0;
		
		//check the rest of the edges
		for(Edge edge : incidentEdges){
			
			//find and compare d
			Vector3d awayPointingEV = edge.getEdgeVector();
			if(edge.getFinalVertex() == this){ awayPointingEV.flip(); }
			awayPointingEV.normalize();
			
			float d = awayPointingEV.dot(v);
			
			//compare
			if(!hadFirst || (d < dmin)){
				nearestObject = edge;
				dmin = d;
				hadFirst = true;
			}
		}
		
		//check faces
		for(Face face : incidentFaces){
			//get vertex of face linked to this one
			EmbeddedVertex<Vertex,Edge> ev = embVertices.get(face); 
			
			//the vector with head at this vertex, contained in face's plane, closest to
			//v is exactly the projection of v onto the face's plane
			//[vproj could be ~zero, but it doesn't matter--everything will still work]
			Vector2d vproj = face.getPlane().getVectorProjection(v);
			
			//check if this is contained in the face polygon
			//[this is stable: if v projects onto an edge of face, then the result
			// will be the same, with certainty, whether or not this test passes]
			if(!ev.checkInfinitesimalInterior(vproj)){ continue; }
			
			//now if w = face.plane.embedding(vproj), then d = (-w.v)/|w|,
			//but one can check this is just -|w|, which is -|vproj|.
			float d = -vproj.length();
			if(d < dmin){
				nearestObject = face;
				dmin = d;
			}
		}
		
		//determine return value
		return nearestObject.checkInfinitesimalInterior(v);
	}
}
