package geometry.polyhedron.components;

import geometry.common.Orientation;
import geometry.common.OutputTree;
import geometry.common.components.Edge2d;
import geometry.common.components.Edge3d;
import geometry.common.components.Plane;
import geometry.common.components.Vertex3d;
import geometry.polygon.components.Polygon;
import geometry.polygon.components.PolygonEdge;
import geometry.polygon.components.PolygonVertex;

/**
 * Represents an edge of an {@link EmbeddedPolygon}, which is a {@link Polygon} 
 * contained in a {@link Plane}.
 * <br><br>
 * Extends {@link PolygonEdge} and therefore {@link Edge2d}, but also refers
 * to an {@link Edge3d}, called the "global edge", which contains {@link Polyhedron}
 * combinatorial information.
 */
public class EmbeddedEdge
	//the type of 3d vertex and edge the embedded vertices and edges link to
	<Vertex extends Vertex3d, Edge extends Edge3d<Vertex>>

	extends PolygonEdge<EmbeddedVertex<Vertex,Edge>, EmbeddedEdge<Vertex,Edge>>
{

	private Edge edge;
	private Orientation relativeOrientation;
	
	/**
	 * Creates a new {@link EmbeddedEdge}. 
	 * Sets endpoints and "global edge" reference data as specified.
	 * 
	 *  @see {@link PolygonEdge#PolygonEdge(PolygonVertex, PolygonVertex)}
	 */
	public EmbeddedEdge(
		EmbeddedVertex<Vertex,Edge> p, EmbeddedVertex<Vertex,Edge> q,
		Edge edge, Orientation relativeOrientation) {
		
		super(p,q);
		setGlobalEdge(edge, relativeOrientation);
	}
	
	@Override
	protected EmbeddedEdge<Vertex, Edge> getThisEdge(){ return this; }

	/**
	 * Set the 3d edge object that this 2d edge represents.
	 *
	 * @param edge The 3d edge object.
	 * @param relativeOrientation The relative orientation of
	 * the 2d edge to the 3d edge.
	 */
	public void setGlobalEdge(Edge edge, Orientation relativeOrientation){
		this.edge = edge;
		this.relativeOrientation = relativeOrientation;
	}
	
	/**
	 * Get the 3d edge object that this 2d edge represents.
	 * Note that the 2d and 3d edges may have the opposite orientation.
	 * 
	 * @return The 3d edge object.
	 * @see EmbeddedEdge2d.getGlobalEdgeRelativeOrientation()
	 */
	public Edge getGlobalEdge(){
		return edge;
	}
	
	/**
	 * Get the relative orientation of the 2d edge to the 3d edge.
	 * 
	 * @return Returns Orientation.POSITIVE if they are oriented the same way,
	 * and returns Orientation.NEGATIVE otherwise.
	 */
	public Orientation getGlobalEdgeRelativeOrientation(){
		return relativeOrientation;
	}
	
	/**
	 * Exchanges the initial point and final point.
	 */
	public void flip(){
		relativeOrientation = relativeOrientation.flip();
		super.flip();
	}
	
	
	//===============================================
	// SERIALIZATION
	//===============================================
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("EmbeddedEdge2d (index: " + getIndex() + ")");
		
		OutputTree childLocal = outputTree.spawnChild("Local Edge");
		super.populateOutputTree(childLocal);
		
		OutputTree childGlobal = outputTree.spawnChild("Global Edge");
		getGlobalEdge().populateOutputTree(childGlobal);
	}
}
