package geometry.polyhedron.components;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import geometry.common.Orientation;
import geometry.common.components.Plane;
import geometry.common.components.Edge3d.Edge3dConstructor;
import geometry.common.components.Vertex3d.Vertex3dConstructor;
import geometry.math.Vector3d;
import geometry.polygon.components.PolygonTools;
import geometry.polyhedron.components.EmbeddedPolygon.Face3dConstructor;
import geometry.polyhedron.components.Polyhedron.PolyhedronConstructor;
import geometry.rawgeometry.ObjData;
import geometry.rawgeometry.ObjData.VertexData;

public class PolyhedronFactory
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>,
	 Poly extends Polyhedron<Vertex, Edge, Face>> 
{
	
	//constructors for the polyhedron and its components
	protected Vertex3dConstructor<Vertex> vertexConstructor;
	protected Edge3dConstructor<Vertex, Edge> edgeConstructor;
	protected Face3dConstructor<Vertex, Edge, Face> faceConstructor;
	protected PolyhedronConstructor<Vertex, Edge, Face, Poly> polyConstructor;
	
	
	public PolyhedronFactory(
		Vertex3dConstructor<Vertex> vertexConstructor,
		Edge3dConstructor<Vertex, Edge> edgeConstructor,
		Face3dConstructor<Vertex, Edge, Face> faceConstructor,
		PolyhedronConstructor<Vertex, Edge, Face, Poly> polyConstructor)
	{
		
		this.vertexConstructor = vertexConstructor;
		this.edgeConstructor = edgeConstructor;
		this.faceConstructor = faceConstructor;
		this.polyConstructor = polyConstructor;
	}
	
	/**
	 * Create a {@link Polyhedron} from an {@link ObjData}.
	 * Only uses position data; ignores tex and normal data. 
	 * 
	 * The ObjData faces should be flat and have at least 3 vertices,
	 * but they <i>need not</i> be convex for this method.
	 * 
	 * @param objData The {@link ObjData} representing the geometry of the {@link Polyhedron}
	 * to be constructed.
	 * @return The {@link Polyhedron}.
	 */
	public Poly makeFromObjData(ObjData objData){
		
		//method:
		//- create 3d vertices from objData in both a LinkedHashSet and an ArrayList
		//  the latter so they can be retrieved by index in objData position array
		//- for each objData face, 
		//  - create *some* plane that contains all its vertices
		//  - create 2d vertices for face (in plane coords) in both LinkedHashSet and ArrayList
		//    the latter so they can be retrieved by index in face's vertex array
		//    also grab "global vertex" reference using ArrayList of 3d vertices
		//  - for each edge in the face,
		//    - create 2d edges for face by using 2d vertex ArrayList, and create 
		//      3d "global edge" by grabbing global vertices of its 2d endpoints
		//      (this establishes incident edge references in all 2d, 3d verts)
		//  - if area of face specified by 2d edges is negative, flip the plane over 
		//  - create the new face (this establishes incident face references in 3d edges
		//    and owning polygon of 2d components)
		//- create the Polyhedron (this establishes owning polyhedron of 3d components)
		
		//get vertex, edge, and face count
		int nverts = objData.position.length;
		int nfaces = objData.faces.size();
		
		//running descriptive indices
		int vindex = 0, eindex = 0, findex = 0;
		
		//create vertices
		LinkedHashSet<Vertex> vertices = new LinkedHashSet<Vertex>(nverts);
		ArrayList<Vertex> vertsByIndex = new ArrayList<Vertex>(nverts);
		
		for(int i=0; i<nverts; i++){
			Vertex newVertex = vertexConstructor.construct(objData.position[i]);
			newVertex.setIndex(vindex); vindex++;
			vertices.add(newVertex);
			vertsByIndex.add(newVertex);
		}
		
		//create faces and edges
		LinkedHashSet<Edge> edges = new LinkedHashSet<Edge>();
		LinkedHashSet<Face> faces = new LinkedHashSet<Face>(nfaces);
		
		for(VertexData[] vd : objData.faces){
			int nfaceverts = vd.length;
			
			//get plane (the normal might be pointing the wrong way; this is corrected later)
			Vector3d[] faceVerts = new Vector3d[nfaceverts];
			for(int i=0; i<nfaceverts; i++){
				faceVerts[i] = objData.position[vd[i].posIndex];
			}
			Plane plane = Plane.getFromVertices(faceVerts);
			
			//running descriptive indices
			int lvindex = 0, leindex = 0;

			//create list of embedded vertices (indexed by order of appearance in indexedFace)
			LinkedHashSet<EmbeddedVertex<Vertex, Edge>> embeddedVertices = new LinkedHashSet<EmbeddedVertex<Vertex, Edge>>(nfaceverts);
			ArrayList<EmbeddedVertex<Vertex, Edge>> embeddedVerticesByIndex = new ArrayList<EmbeddedVertex<Vertex, Edge>>(nfaceverts);
			
			for(int i=0; i<nfaceverts; i++){
				Vertex v = vertsByIndex.get(vd[i].posIndex);
				
				//create embedded vertex
				EmbeddedVertex<Vertex, Edge> newLocalVertex = new EmbeddedVertex<Vertex, Edge>(plane.getProjection(v.getPosition()), v);
				newLocalVertex.setIndex(lvindex); lvindex++;
				embeddedVertices.add(newLocalVertex);
				embeddedVerticesByIndex.add(newLocalVertex);
			}
			
			//create edges and embedded edges
			LinkedHashSet<EmbeddedEdge<Vertex, Edge>> embeddedEdges = new LinkedHashSet<EmbeddedEdge<Vertex, Edge>>(nfaceverts);
			
			for(int i=0; i<nfaceverts; i++){
				
				//get vertices making up current edge
				EmbeddedVertex<Vertex, Edge> p = embeddedVerticesByIndex.get(i);
				EmbeddedVertex<Vertex, Edge> q = embeddedVerticesByIndex.get((i+1)%nfaceverts);
				
				Vertex pVert = p.getGlobalVertex();
				Vertex qVert = q.getGlobalVertex();

				//retrieve or create new edge
				Edge globalEdge = null;
				Orientation relativeOrientation = Orientation.POSITIVE; 
				//check if there is an edge between p and q's global vertices
				for(Edge e : pVert.getIncidentEdges()){
					if(e.getInitialVertex() == qVert){
						globalEdge = e;
						relativeOrientation = Orientation.NEGATIVE;
						break;
					}
					else if(e.getFinalVertex() == qVert){
						globalEdge = e;
						break;
					}
				}
				//if no edge was retrieved, create a new one
				if(globalEdge == null){
					globalEdge = edgeConstructor.construct(pVert, qVert);
					globalEdge.setIndex(eindex); eindex++;
					edges.add(globalEdge);
				}

				//create embedded edge
				EmbeddedEdge<Vertex, Edge> newLocalEdge = new EmbeddedEdge<Vertex, Edge>(p, q, globalEdge, relativeOrientation);
				newLocalEdge.setIndex(leindex); leindex++;
				embeddedEdges.add(newLocalEdge);
			}
			
			//make sure plane is oriented correctly
			if(PolygonTools.computeSignedArea(embeddedEdges) < 0){
				plane.flip();
				for(EmbeddedVertex<Vertex, Edge> ev : embeddedVertices){
					ev.flipXValue();
				}
			}
			
			//create Face using faceFactory, and add it to the list
			Face newFace = faceConstructor.construct(plane, embeddedVertices, embeddedEdges);
			newFace.setIndex(findex); findex++;
			faces.add(newFace);
		}
		
		return polyConstructor.construct(vertices, edges, faces);
	}
}
