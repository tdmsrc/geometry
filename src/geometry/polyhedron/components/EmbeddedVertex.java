package geometry.polyhedron.components;

import geometry.common.OutputTree;
import geometry.common.components.Edge3d;
import geometry.common.components.Plane;
import geometry.common.components.Vertex2d;
import geometry.common.components.Vertex3d;
import geometry.math.Vector2d;
import geometry.polygon.components.Polygon;
import geometry.polygon.components.PolygonVertex;

/**
 * Represents a vertex of an {@link EmbeddedPolygon}, which is a {@link Polygon} 
 * contained in a {@link Plane}.
 * <br><br>
 * Extends {@link PolygonVertex} and therefore {@link Vertex2d}, but also refers
 * to a {@link Vertex3d}, called the "global vertex", which contains {@link Polyhedron}
 * combinatorial information.
 */
public class EmbeddedVertex
	//the type of 3d vertex and edge the embedded vertices and edges link to
	<Vertex extends Vertex3d, Edge extends Edge3d<Vertex>>

	extends PolygonVertex<EmbeddedVertex<Vertex,Edge>, EmbeddedEdge<Vertex,Edge>>
{

	private Vertex vertex; //"global vertex"
	
	/**
	 * Creates a new {@link EmbeddedVertex}. 
	 * Sets position and "global vertex" reference as specified.
	 * 
	 * @see {@link PolygonVertex#PolygonVertex(Vector2d)}
	 */
	public EmbeddedVertex(Vector2d v, Vertex globalVertex) {
		super(v);
		
		setGlobalVertex(globalVertex);
	}
	
	@Override
	protected EmbeddedVertex<Vertex, Edge> getThisVertex(){ return this; }

	/**
	 * Set the 3d vertex object that this 2d vertex represents.
	 * 
	 * @param globalVertex The 3d vertex object.
	 */
	public void setGlobalVertex(Vertex globalVertex){
		this.vertex = globalVertex;
	}
	
	/**
	 * Get the 3d vertex object that this 2d vertex represents.
	 * 
	 * @return The 3d vertex object.
	 */
	public Vertex getGlobalVertex(){
		return vertex;
	}
	
	
	//===============================================
	// SERIALIZATION
	//===============================================
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("EmbeddedVertex2d (index: " + getIndex() + ")");
		
		OutputTree childLocal = outputTree.spawnChild("Local Vertex");
		super.populateOutputTree(childLocal);
		
		OutputTree childGlobal = outputTree.spawnChild("Global Vertex");
		getGlobalVertex().populateOutputTree(childGlobal);
	}
}
