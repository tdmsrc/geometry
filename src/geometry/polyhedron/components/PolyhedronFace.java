package geometry.polyhedron.components;

import java.util.LinkedHashSet;

import geometry.common.PartitionNode;
import geometry.common.components.Plane;
import geometry.math.Vector3d;

/**
 * A PolyhedronFace extends {@link EmbeddedPolygon} and holds the following data:
 * <ul>
 * <li><i>Sub-components</i> (inherited from {@link EmbeddedPolygon}):
 * <ul>
 * <li>A collection of {@link EmbeddedVertex} objects (2D verts with 3D vert references)
 * <li>A collection of {@link EmbeddedEdge} objects (2D edges with 3D edge references)
 * <li>A {@link Plane} containing the embedded vertices and edges
 * </ul>
 * <li><i>Super-components</i>: 
 * <ul>
 * <li>Reference to {@link Polyhedron} that owns this component
 * </ul>
 * <li><i>Extra data</i>: 
 * <ul>
 * <li>{@link PartitionNode} of faces to which this face belongs
 * </ul>
 * </ul>
 * 
 * Automatic cross-reference updating:<br>
 * If a {@link Polyhedron} is created with this face specified as belonging to it, that Polyhedron
 * <ul>
 * <li>will take ownership of this face by calling {@link #setOwner(Polyhedron)} 
 * <li>will partition its faces and set the PartitionNode to which this face belongs by calling {@link #setFacePartitionNode(PartitionNode)}
 * </ul>
 */
public abstract class PolyhedronFace
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>>
	
	extends EmbeddedPolygon<Vertex, Edge>
	implements PolyhedronComponent<Vertex, Edge, Face>
{

	//polyhedron this component belongs to
	protected Polyhedron<Vertex, Edge, Face> owner;
	//faces partitioned by equiv rel: f1 ~ f2 iff joined by a sequence of incident faces
	protected PartitionNode<Face> partitionNode;
	
	/**
	 * Creates a PolyhedronFace with the specified embedded vertices and edges contained in the specified plane,
	 * "null" as owner, and belonging to a {@link PartitionNode} containing only this face
	 * (the PartitionNode will be updated automatically if a {@link Polyhedron} is created
	 * with this face specified as belonging to it).
	 * <br><br>
	 * Automatically adds the new PolyhedronFace to the collection of incident faces of any
	 * vertices or edges occurring as "global vertices" or "global edges" (i.e., vertices
	 * or edges in 3D) of this face.
	 * 
	 * @see {@link EmbeddedPolygon#EmbeddedPolygon(Plane, LinkedHashSet, LinkedHashSet)}
	 */
	public PolyhedronFace(Plane plane, LinkedHashSet<EmbeddedVertex<Vertex,Edge>> vertices, LinkedHashSet<EmbeddedEdge<Vertex,Edge>> edges){
		super(plane, vertices, edges);
		owner = null;
		partitionNode = new PartitionNode<Face>(getThisFace());
		
		for(EmbeddedEdge<Vertex,Edge> edge : edges){
			edge.getGlobalEdge().addIncidentFace(getThisFace(), edge);
		}
		
		for(EmbeddedVertex<Vertex, Edge> vertex : vertices){
			vertex.getGlobalVertex().addIncidentFace(getThisFace(), vertex);
		}
	}

	protected abstract Face getThisFace(); 

	@Override public Vertex getAsVertex(){ return null; }
	@Override public Edge getAsEdge(){ return null; }
	@Override public Face getAsFace(){ return getThisFace(); }
	
	@Override public PolyhedronComponentType getPolyhedronComponentType(){ return PolyhedronComponentType.FACE; }
	
	/**
	 * Return {@link Polyhedron} that owns this face.
	 */
	@Override
	public Polyhedron<Vertex,Edge,Face> getOwner(){ return owner; }
	
	/**
	 * Set to null the reference to the {@link Polyhedron} that owns this face. <br> 
	 * This has no effect on components incident to this face. <br>
	 * This has no effect on the component lists of the current owner.
	 */
	@Override
	public void disown(){ owner = null; }
	
	/**
	 * Set {@link Polyhedron} that owns this face to "owner".<br>
	 * Calls {@link PolyhedronEdge#setOwner} with argument "owner" on all edges occurring as 
	 * "global edges" (i.e., edges in 3D) of this face.<br>
	 * This has no effect on the component lists of the current or new owner.
	 */
	@Override
	public void setOwner(Polyhedron<Vertex,Edge,Face> owner){
		this.owner = owner;
		
		//trickle down to edges
		for(EmbeddedEdge<Vertex,Edge> embEdge : getEdges()){
			embEdge.getGlobalEdge().setOwner(owner);
		}
	}
	
	public PartitionNode<Face> getFacePartitionNode(){
		return partitionNode;
	}
	
	public void setFacePartitionNode(PartitionNode<Face> partitionNode){
		this.partitionNode = partitionNode;
	}
	
	/**
	 * Checks whether, if the vector "v" were placed with its head on any point
	 * on the interior of this face, points infinitesimally near the head of "v" 
	 * would be inside the polyhedron of which this face is a component.
	 * 
	 * Assumes points infinitesimally near the head of "v" are not on this face.
	 * 
	 * @param v The vector whose head would be placed on the interior of this face.
	 * @return True if points infinitesimally near the head of "v" 
	 * are inside the polyhedron, false otherwise.
	 */
	@Override
	public boolean checkInfinitesimalInterior(Vector3d v){
		
		return (getPlane().getNormal().dot(v) > 0);
	}
	
	
	//==============================================
	// RECURSION OVER EDGE-CONNECTED FACES
	//==============================================
	
	/**
	 * An action to be performed on a face.
	 * 
	 * @see {@link PolyhedronFace#recurse(FaceRecurseMethod)}
	 * @see {@link PolyhedronFace#recurse(PolyhedronFace, FaceRecurseMethod, RecurseToken)}
	 */
	public static interface FaceRecurseMethod<Face extends PolyhedronFace<?, ?, Face>>{
		/**
		 * An action to be performed on a face.
		 * 
		 * @param previousFace The face immediately preceding thisFace in the recursion. 
		 * @param thisFace The face on which the action is to be performed.
		 * @return Return true to continue the recursion.  If false is returned, recursion will stop.
		 */
		public boolean action(Face previousFace, Face thisFace);
	}
	
	//see recurse method
	public static class RecurseToken{
		private static long lastid = 0;
		private long id;
		
		protected RecurseToken(){
			id = RecurseToken.lastid++;
			RecurseToken.lastid = id;
		}
		public long getID(){ return id; }
	}
	private RecurseToken lastRecurseToken = null;
	
	
	/**
	 * This method generates a token which represents a face recursion.
	 * When a face is encountered, the recursion stops if the last set
	 * token is equal to the current token.  Otherwise, the last set
	 * token is set to the current token.
	 * 
	 * @return A recursion token.
	 */
	public static RecurseToken generateRecurseToken(){
		return new RecurseToken();
	}
	
	
	/**
	 * This method generates a new token and calls 
	 * {@link PolyhedronFace#recurse(PolyhedronFace, FaceRecurseMethod, RecurseToken)}
	 * with parameters (null, faceRecurseMethod, new token).
	 * 
	 * @param faceRecurseMethod The action to perform for each face.
	 */
	public void recurse(FaceRecurseMethod<Face> faceRecurseMethod){
		recurse(null, faceRecurseMethod, PolyhedronFace.generateRecurseToken());
	}
	
	/**
	 * This method does nothing if the last token set was "token".
	 * 
	 * Otherwise, this method calls faceRecurseMethod for this face,
	 * and sets the last token to "token".  
	 * 
	 * If faceRecurseMethod returns true, then this method is called for
	 * for every face incident to this one.
	 * 
	 * @param previousFace The face immediately preceding this face in the recursion.  May be set to null initially.
	 * @param faceRecurseMethod The action to perform for each face.
	 * @param token A token which indicates whether a face has been visited during
	 * the current recursion.
	 */
	public void recurse(Face previousFace, FaceRecurseMethod<Face> faceRecurseMethod, RecurseToken token){
		
		//if this face was already visited, do nothing
		if(lastRecurseToken == token){ return; }
		
		//perform method, and if it returns false, stop
		lastRecurseToken = token;
		if(!faceRecurseMethod.action(previousFace, getThisFace())){ return; }

		//recurse
		for(EmbeddedEdge<Vertex,Edge> embEdge : getEdges()){
			for(Face face : embEdge.getGlobalEdge().getIncidentFaces()){
				face.recurse(getThisFace(), faceRecurseMethod, token);
			}
		}
	}
}
