package geometry.polyhedron.components;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import geometry.common.components.Edge3d;
import geometry.common.components.Vertex3d;
import geometry.math.Vector3d;

/**
 * A PolyhedronEdge extends {@link Edge3d} and holds the following data:
 * <ul>
 * <li><i>Sub-components</i> (inherited from {@link Edge3d}):
 * <ul>
 * <li>Reference to Vertex endpoints 
 * </ul>
 * <li><i>Super-components</i>:
 * <ul>
 * <li>Reference to {@link Polyhedron} that owns this component
 * <li>A collection of incident Face objects 
 * </ul>
 * </ul>
 * 
 * Automatic cross-reference updating:
 * <ul>
 * <li>If a {@link Polyhedron} is created with this edge specified as belonging to it, {@link #setOwner}
 * is automatically called to take ownership of this edge.
 * <li>If a {@link PolyhedronFace} is created with this edge referenced as a "global edge" of one
 * of its edges, {@link #addIncidentFace} is called automatically to add it to this edge's
 * collection of incident faces.
 * </ul>
 */
public abstract class PolyhedronEdge
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>>

	extends Edge3d<Vertex>
	implements PolyhedronComponent<Vertex, Edge, Face>
{

	//polyhedron this component belongs to
	protected Polyhedron<Vertex, Edge, Face> owner;
		
	//incident face data
	protected HashSet<Face> incidentFaces;
	protected HashMap<Face, EmbeddedEdge<Vertex,Edge>> embEdges;
	
	/**
	 * Constructs a PolyhedronEdge with the specified endpoints,
	 * "null" as owner, and empty collection of incident faces.
	 * <br><br>
	 * Automatically adds the new PolyhedronEdge to the collection of incident edges of the
	 * specified endpoints.
	 * 
	 * @see {@link Edge3d#Edge3d(Vertex3d, Vertex3d)}
	 */
	public PolyhedronEdge(Vertex p, Vertex q){
		super(p, q);
		owner = null;
		
		//initialize incidence data
		incidentFaces = new HashSet<Face>();
		embEdges = new HashMap<Face, EmbeddedEdge<Vertex,Edge>>();
		
		//update endpoints' incidence data
		p.addIncidentEdge(getThisEdge());
		q.addIncidentEdge(getThisEdge());
	}
	
	protected abstract Edge getThisEdge(); 
	
	@Override public Vertex getAsVertex(){ return null; }
	@Override public Edge getAsEdge(){ return getThisEdge(); }
	@Override public Face getAsFace(){ return null; }

	@Override public PolyhedronComponentType getPolyhedronComponentType(){ return PolyhedronComponentType.EDGE; }
	
	/**
	 * Return {@link Polyhedron} that owns this edge.
	 */
	@Override
	public Polyhedron<Vertex,Edge,Face> getOwner(){ return owner; }
	
	/**
	 * Set to null the reference to the {@link Polyhedron} that owns this edge. <br> 
	 * This has no effect on components incident to this edge. <br>
	 * This has no effect on the component lists of the current owner.
	 */
	@Override
	public void disown(){ owner = null; }
	
	/**
	 * Set {@link Polyhedron} that owns this edge to "owner".<br>
	 * Calls {@link PolyhedronVertex#setOwner} with argument "owner" on endpoints of this edge.<br>
	 * This has no effect on the component lists of the current or new owner.
	 */
	@Override
	public void setOwner(Polyhedron<Vertex,Edge,Face> owner){
		this.owner = owner;
		
		//trickle down to endpoints
		getInitialVertex().setOwner(owner);
		getFinalVertex().setOwner(owner);
	}
	
	/**
	 * Returns the set of faces incident to this edge.
	 * 
	 * @return The set of faces incident to this edge.
	 */
	public Set<Face> getIncidentFaces(){
		return incidentFaces;
	}
	
	/**
	 * Returns true if this edge has no incident faces, otherwise false.
	 * 
	 * @return True if this edge has no incident faces, otherwise false.
	 */
	public boolean isIsolated(){
		return incidentFaces.isEmpty();
	}
	
	/**
	 * Add reference to an incident face.
	 * Must also specify the embedded edge of that face which links to this edge. 
	 *  
	 * @param incidentFace The face to be specified as incident to this edge.
	 * @param embEdge The embedded edge of "face" which links to this edge.
	 */
	protected void addIncidentFace(Face face, EmbeddedEdge<Vertex, Edge> embEdge){
		incidentFaces.add(face);
		embEdges.put(face, embEdge);
	}
	
	/**
	 * Remove reference to an incident face. 
	 *  
	 * @param face The face whose reference is to be removed.
	 * @see Polyhedron.removeFace
	 */
	protected void removeIncidentFace(Face face){
		incidentFaces.remove(face);
		embEdges.remove(face);
	}
	
	/**
	 * Checks if "face" has been specified as being incident to this edge.
	 * 
	 * @param face The face whose incidence to this edge is to be checked.
	 * @return True if the face is incident, false otherwise.
	 */
	public boolean checkIncidence(Face face){
		return incidentFaces.contains(face);
	}
	
	/**
	 * Return the embedded edge of "face" which links to this edge.
	 * If this edge is not incident to "face", returns null.
	 * 
	 * @param face The incident face whose embedded edge is to be returned.
	 * @return The embedded edge of "face" which links to this edge.
	 */
	public EmbeddedEdge<Vertex, Edge> getEmbeddedEdge(Face face){
		return embEdges.get(face);
	}
	
	
	/**
	 * Checks whether, if the vector "v" were placed with its head on any point
	 * on the interior of this edge, points infinitesimally near the head of "v" 
	 * would be inside the polyhedron of which this edge is a component.
	 * 
	 * Assumes points infinitesimally near the head of "v" are not on this edge
	 * or either face incident to it.
	 * 
	 * If "v" is very near zero, this could be true or false, but will run successfully.
	 * 
	 * @param v The vector whose head would be placed on the interior of this edge.
	 * @return True if points infinitesimally near the head of "v" 
	 * are inside the polyhedron, false otherwise.
	 */
	@Override
	public boolean checkInfinitesimalInterior(Vector3d v){
		
		//use same method as in PolyhedronFace, on the face such that v.e/(|v||e|) is smaller, where e
		//is the inward-pointing edge normal [so, the one "angled toward v" the most]
		//this is only unstable when the dihedral angle at this edge is near 0 or 2pi
		//[given the assumption that points inf near v's head are reasonably far from faces]
		
		boolean hadFirst = false;
		float dmin = 0;
		Face goodFace = null;
		
		for(Face face : incidentFaces){
			
			//find v.length*cos for inward-pointing edge normal (need not bother dividing by v.length)
			Vector3d e = face.getPlane().getVectorEmbedding(getEmbeddedEdge(face).getInwardPointingNormal());
			float d = e.dot(v)/e.length();
			
			//compare and set minimum
			if(!hadFirst || (d < dmin)){
				dmin = d;
				goodFace = face;
				hadFirst = true;
			}
		}

		return (goodFace.getPlane().getNormal().dot(v) > 0);
	}
}
