package geometry.polyhedron.components;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import geometry.common.PartitionCollection;
import geometry.common.PartitionNode;
import geometry.common.components.Edge3d;
import geometry.common.components.Vertex3d;
import geometry.math.Matrix3d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronFace.FaceRecurseMethod;
import geometry.polyhedron.components.PolyhedronFace.RecurseToken;

//This is an assortment of methods that can be used on various
//collections of Polyhedron components, but need not be used
//on an actual Polyhedron object.

public class PolyhedronTools {

	private PolyhedronTools(){ }
	
	
	//=================================================
	// VOLUME, CENTROID, INERTIA
	//=================================================
	
	/**
	 * Determines the signed volume of the polyhedron with specified faces.
	 * 
	 * @return The signed volume of the polyhedron with specified faces
	 */
	public static float computeSignedVolume(Collection<? extends EmbeddedPolygon<?,?>> faces){
		//quantity is additive; add for each cone over F from (0,0,0)
		//vol(cone) = 1/3 area(F) (n.o)
		
		float val = 0;
		for(EmbeddedPolygon<?,?> face : faces){
			Vector3d o = face.getPlane().getOrigin();
			Vector3d n = face.getPlane().getNormal();
			
			val += face.computeSignedArea() * n.dot(o);
		}
		return val/3.0f;
	}
	
	/**
	 * Determines the centroid of the polyhedron (i.e., the center of mass 
	 * assuming uniform density) scaled by the signed volume for the 
	 * polyhedron with the specified faces.
	 * 
	 * @return The centroid scaled by the volume.
	 */
	public static Vector3d computeVolumeTimesCentroid(Collection<? extends EmbeddedPolygon<?,?>> faces){
		//quantity is additive; add for each cone over F from (0,0,0)
		//vol(cone) = 1/3 area(F) (n.o); centroid is 3/4 of the way to centroid(F)
		
		float valx = 0, valy = 0, valz = 0;
		
		for(EmbeddedPolygon<?,?> face : faces){
			Vector3d o = face.getPlane().getOrigin();
			Vector3d n = face.getPlane().getNormal();
						
			float a = face.computeSignedArea();
			float v = n.dot(o) * a;
			
			Vector2d c = face.computeAreaTimesCentroid(); c.scale(1/a);
			Vector3d cEmb = face.getPlane().getEmbedding(c);
			
			valx += v*cEmb.getX();
			valy += v*cEmb.getY();
			valz += v*cEmb.getZ();
		}
		return new Vector3d(valx/4.0f, valy/4.0f, valz/4.0f);
	}
	
	/**
	 * Determines the inertia tensor for the polyhedron with the specified faces.
	 * <br>
	 * Assumes unit density; the result can be scaled by the desired density.
	 * 
	 * @return The inertia tensor as a {@link Matrix3d}.
	 */
	public static Matrix3d computeInertiaTensor(Collection<? extends EmbeddedPolygon<?,?>> faces){
		
		Matrix3d ret = new Matrix3d();
		
		//temporary matrices
		Matrix3d mI = new Matrix3d();
		Matrix3d mT = new Matrix3d();
		
		for(EmbeddedPolygon<?,?> face : faces){
			Vector3d o = face.getPlane().getOrigin();
			Vector3d n = face.getPlane().getNormal();
			Vector3d u = face.getPlane().getXAxisVector();
			Vector3d v = face.getPlane().getYAxisVector();
			
			//integrals of 1, x, y, x^2, xy, y^2
			float a = face.computeSignedArea();
			Vector2d c0 = face.computeAreaTimesCentroid();
			float intx2 = face.computeIntegralX2();
			float intxy = face.computeIntegralXY();
			float inty2 = face.computeIntegralY2();
			
			//compute the integral of |X|^2 I - X \otimes X
			//where X = xu + yv + o, i.e. ambient coords (x,y,z) in terms of plane coords (x,y)
			mI.setAsIdentity();
			mI.scaleEntries(o.dot(o)*a + 2*(o.dot(u)*c0.getX() + o.dot(v)*c0.getY()) + intx2 + inty2);
			
			mT.setAsTensorProduct(o, o);	mT.scaleEntries(a);			mI.subtract(mT);
			mT.setAsSymmetricProduct(u, o);	mT.scaleEntries(c0.getX());	mI.subtract(mT);
			mT.setAsSymmetricProduct(v, o);	mT.scaleEntries(c0.getY());	mI.subtract(mT);
			mT.setAsTensorProduct(u, u);	mT.scaleEntries(intx2);		mI.subtract(mT);
			mT.setAsSymmetricProduct(u, v);	mT.scaleEntries(intxy);		mI.subtract(mT);
			mT.setAsTensorProduct(v, v);	mT.scaleEntries(inty2);		mI.subtract(mT);
			
			//scale the result by n.o and accumulate
			mI.scaleEntries(n.dot(o));
			ret.add(mI);
		}
		
		//divide overall sum by 5 and return
		ret.scaleEntries(0.2f);
		
		//apply parallel axis theorem
		float vol = PolyhedronTools.computeSignedVolume(faces);
		Vector3d cv = PolyhedronTools.computeVolumeTimesCentroid(faces);
		cv.scale(1/vol);
		
		Matrix3d temp = new Matrix3d();
		temp.setAsIdentity();
		temp.scaleEntries(cv.lengthSquared());
		
		Matrix3d mCC = new Matrix3d();
		mCC.setAsTensorProduct(cv, cv);
		temp.subtract(mCC);
		
		temp.scaleEntries(vol);
		
		ret.subtract(temp);
		return ret;
	}
	
	
	//=================================================
	// CLOSEST OBJECT/CONTAINMENT QUERIES
	//=================================================
	
	//return value for getClosestObject
	/**
	 * Contains information about the closest Polyhedron component to a point.
	 * Exactly one of closestVertex, closestEdge, and closestFace should be non-null.
	 * The vector v is the vector from the query point to the closest point.
	 * 
	 * @see {@link PolyhedronTools#getClosestObject(Collection, Collection, Collection, Vector3d)}
	 */
	public static class ClosestPolyhedronObjectData
		<Vertex extends Vertex3d,
		 Edge extends Edge3d<? extends Vertex3d>,
		 Face extends EmbeddedPolygon<?, ?>>
	{
		
		public Vertex closestVertex;
		public Edge closestEdge;
		public Face closestFace;
		
		public Vector3d v; //vector from query point to closest point
		
		public ClosestPolyhedronObjectData(Vertex closestVertex, Edge closestEdge, Face closestFace, Vector3d v){
			this.closestVertex = closestVertex;
			this.closestEdge = closestEdge;
			this.closestFace = closestFace;
			this.v = v;
		}
	}
	
	/**
	 * Given collections of fixed-dimensional vertices and edges, a collection
	 * of faces, and a point, determines which object is the closest to the point.  
	 * Returns a {@link ClosestPolyhedronObjectData} object, which contains the closest 
	 * object from either collection, as well as the vector from the point to the closest 
	 * point on the closest object. 
	 * 
	 * @param verts Any collection of fixed-dimensional vertices.
	 * @param edges Any collection of fixed-dimensional edges.
	 * @param faces Any collection of faces.
	 * @param p A point.
	 * @return A {@link ClosestPolyhedronObjectData} object.
	 */
	public static 
		<Vertex extends Vertex3d,
		 Edge extends Edge3d<? extends Vertex3d>,
		 Face extends EmbeddedPolygon<?, ?>>
	
	ClosestPolyhedronObjectData<Vertex,Edge,Face> getClosestObject(
		Collection<? extends Vertex> verts, 
		Collection<? extends Edge> edges, 
		Collection<? extends Face> faces, 
		Vector3d p){ 
		
		boolean haveFirst = false;
		Vertex closestVertex = null;
		Edge closestEdge = null;
		Face closestFace = null;
		Vector3d closestPoint = null;
		float d2min = 0;
		
		//check vertices
		for(Vertex vertex : verts){
	
			//find and compare distance
			float d2 = p.distanceSquared(vertex.getPosition());
			if((d2 < d2min) || !haveFirst){
				closestVertex = vertex; closestEdge = null; closestFace = null;
				closestPoint = vertex.getPosition().copy();
				d2min = d2;
				haveFirst = true;
			}
		}
		
		//check edges
		for(Edge edge : edges){
			
			//find closest point on edge
			float t = edge.computePointIntersection(p);
			
			//if closest point is not on edge interior, endpoint is closer; defer to that
			if((t < 0) || (t > 1)){ continue; }
			
			//find distance from p to the edge and compare
			Vector3d x = edge.lerp(t);
			float d2 = p.distanceSquared(x);
			if(d2 < d2min){
				closestVertex = null; closestEdge = edge; closestFace = null;
				closestPoint = x;
				d2min = d2;
			}
		}
		
		//check faces
		for(Face face : faces){
			
			//find closest point on plane containing face
			Vector2d pproj = face.getPlane().getProjection(p);
			
			//if closest point is not in face interior, edge or endpoint is closer; defer to that
			if(!face.containsPoint(pproj)){ continue; }
			
			//find distance from p to the face and compare
			Vector3d x = face.getPlane().getEmbedding(pproj);
			float d2 = p.distanceSquared(x);
			if(d2 < d2min){
				closestVertex = null; closestEdge = null; closestFace = face;
				closestPoint = x;
				d2min = d2;
			}
		}
		
		//return ClosestPolyhedronObjectData
		Vector3d v = closestPoint; v.subtract(p);
		return new ClosestPolyhedronObjectData<Vertex,Edge,Face>(closestVertex, closestEdge, closestFace, v);
	}
	
	/**
	 *  Determines if "p" is in the interior of the polyhedron
	 *  with specified components (plus all edges and faces incident to
	 *  specified vertices, plus faces incident to specified edges).
	 *  <br><br>
	 *  Note that the boundary of a polyhedron is oriented, so if
	 *  e.g. the normals of the faces are pointing away from the region
	 *  that the boundary of the polyhedron bounds, that region is
	 *  considered the interior of the polyhedron.  If the normals are
	 *  pointing the opposite way, that region is the exterior.
	 *  
	 *  @param p The point whose containment is to be checked.
	 *  @param epsilon Return true if p is this close to the boundary of the polyhedron. 
	 *  @return True or false.
	 */
	public static boolean containsPoint(
		Collection<? extends PolyhedronVertex<?,?,?>> vertices, 
		Collection<? extends PolyhedronEdge<?,?,?>> edges,
		Collection<? extends PolyhedronFace<?,?,?>> faces,
		Vector3d p){
		
		ClosestPolyhedronObjectData<PolyhedronVertex<?,?,?>, PolyhedronEdge<?,?,?>, PolyhedronFace<?,?,?>> clobj = 
			PolyhedronTools.getClosestObject(vertices, edges, faces, p);
		
		if(clobj.closestVertex != null){
			return clobj.closestVertex.checkInfinitesimalInterior(clobj.v);
		}else if(clobj.closestEdge != null){
			return clobj.closestEdge.checkInfinitesimalInterior(clobj.v);
		}else if(clobj.closestFace != null){
			return clobj.closestFace.checkInfinitesimalInterior(clobj.v);
		}
		
		return false;
	}

	/**
	 *  Same as {@link #containsPoint(Collection, Collection, Collection, Vector3d)}, but
	 *  checks if p is within epsilon of the boundary.
	 *  
	 *  @param epsilon Return true if p is this close to the boundary of the polyhedron. 
	 */
	public static boolean containsPoint(
		Collection<? extends PolyhedronVertex<?,?,?>> vertices, 
		Collection<? extends PolyhedronEdge<?,?,?>> edges,
		Collection<? extends PolyhedronFace<?,?,?>> faces,
		Vector3d p, float epsilon){
		
		ClosestPolyhedronObjectData<PolyhedronVertex<?,?,?>, PolyhedronEdge<?,?,?>, PolyhedronFace<?,?,?>> clobj = 
			PolyhedronTools.getClosestObject(vertices, edges, faces, p);
		if(clobj.v.length() < epsilon){ return true; }
		
		if(clobj.closestVertex != null){
			return clobj.closestVertex.checkInfinitesimalInterior(clobj.v);
		}else if(clobj.closestEdge != null){
			return clobj.closestEdge.checkInfinitesimalInterior(clobj.v);
		}else if(clobj.closestFace != null){
			return clobj.closestFace.checkInfinitesimalInterior(clobj.v);
		}
		
		return false;
	}
	
	
	//=================================================
	// COMBINATORIAL QUERIES/COMPUTATIONS
	//=================================================
	
	/**
	 * Determines if there is an edge in which both "a" and "b" are contained.
	 * That is, both components are either equal to the edge, or equal to one
	 * of the endpoints of that edge.
	 * If so, that edge is returned; otherwise, returns null.
	 * 
	 * @param a Any polyhedron component.
	 * @param b Any other polyhedron component.
	 * @return The edge that contains "a" and "b", or null if there isn't one.
	 */
	public static 
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>>
	
		Edge getCommonEdgeContainment(PolyhedronComponent<Vertex,Edge,Face> a, PolyhedronComponent<Vertex,Edge,Face> b){
	
		switch(a.getPolyhedronComponentType()){
		case VERTEX:
			Vertex av = a.getAsVertex();
			switch(b.getPolyhedronComponentType()){
			case VERTEX: 
				Vertex bv = b.getAsVertex();
				for(Edge edge : av.getIncidentEdges()){
					if((edge.getInitialVertex() == bv) || (edge.getFinalVertex() == bv)){ return edge; } 
				}
				return null;
			case EDGE: 
				Edge be = b.getAsEdge();
				if((be.getInitialVertex() == av) || (be.getFinalVertex() == av)){ return be; }
				return null;
				
			default: return null;
			}
		case EDGE:
			Edge ae = a.getAsEdge();
			switch(b.getPolyhedronComponentType()){
			case VERTEX: 
				Vertex bv = b.getAsVertex();
				if((ae.getInitialVertex() == bv) || (ae.getFinalVertex() == bv)){ return ae; }
				return null;
			case EDGE: 
				if(ae == b.getAsEdge()){ return ae; }
				return null;
				
			default: return null;
			}
			
		default: return null;
		}
	}
	
	/**
	 * Determines if there is a face in which both "a" and "b" are contained.
	 * That is, both components are either equal to the face, or equal to an
	 * edge or vertex on the boundary of that face.
	 * If so, that face is returned; otherwise, returns null.
	 * 
	 * @param a Any polyhedron component.
	 * @param b Any other polyhedron component.
	 * @return The face that contains "a" and "b", or null if there isn't one.
	 */
	public static 
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>> 
	
		Face getCommonFaceContainment(PolyhedronComponent<Vertex,Edge,Face> a, PolyhedronComponent<Vertex,Edge,Face> b){
		
		switch(a.getPolyhedronComponentType()){
		case VERTEX:
			Vertex av = a.getAsVertex();
			switch(b.getPolyhedronComponentType()){
			case VERTEX:
				Vertex bv = b.getAsVertex();
				for(Face face : av.getIncidentFaces()){
					if(bv.checkIncidence(face)){ return face; }
				}
				return null;
			case EDGE:
				Edge be = b.getAsEdge();
				for(Face face : be.getIncidentFaces()){
					if(av.checkIncidence(face)){ return face; }
				}
				return null;
			case FACE:
				Face bf = b.getAsFace();
				if(av.checkIncidence(bf)){ return bf; }
				return null;
			}
		case EDGE:
			Edge ae = a.getAsEdge();
			switch(b.getPolyhedronComponentType()){
			case VERTEX:
				Vertex bv = b.getAsVertex();
				for(Face face : ae.getIncidentFaces()){
					if(bv.checkIncidence(face)){ return face; }
				}
				return null;
			case EDGE:
				Edge be = b.getAsEdge();
				for(Face face : be.getIncidentFaces()){
					if(ae.checkIncidence(face)){ return face; }
				}
				return null;
			case FACE:
				Face bf = b.getAsFace();
				if(ae.checkIncidence(bf)){ return bf; }
				return null;
			}
		case FACE:
			Face af = a.getAsFace();
			switch(b.getPolyhedronComponentType()){
			case VERTEX:
				Vertex bv = b.getAsVertex();
				if(bv.checkIncidence(af)){ return af; }
				return null;
			case EDGE:
				Edge be = b.getAsEdge();
				if(be.checkIncidence(af)){ return af; }
				return null;
			case FACE:
				if(af == b.getAsFace()){ return af; }
				return null;
			}
		}
		
		return null;
	}

	/**
	 * Create a PartitionCollection for the specified collection of faces.
	 * 
	 * This assigns to each face a new PartitionNode, and completely ignores
	 * any existing PartitionNodes.
	 */
	protected static 
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>>
	
		PartitionCollection<Face> computeFacePartitions(Collection<Face> faces){
	
		//new face partition collection
		final PartitionCollection<Face> partitionCollection = new PartitionCollection<Face>();
		
		//recursion method:
		FaceRecurseMethod<Face> faceRecurseMethod = new FaceRecurseMethod<Face>(){
			@Override
			public boolean action(Face previousFace, Face thisFace){
				
				//set partition node to a new node, which is added to the collection
				PartitionNode<Face> newNode = partitionCollection.add(thisFace);
				thisFace.setFacePartitionNode(newNode);
				
				//merge to face that called this face via the recursion
				if(previousFace != null){
					partitionCollection.mergePartitions(thisFace.getFacePartitionNode(), previousFace.getFacePartitionNode());
				}
				
				return true;
			}
		};
		
		//recurse with single token over all faces
		RecurseToken token = PolyhedronFace.generateRecurseToken();
		for(Face face : faces){
			face.recurse(null, faceRecurseMethod, token);
		}
		
		return partitionCollection;
	}
	
	
	//=================================================
	// CUT AND GLUE METHODS
	//=================================================
	
	//copyFaces return data
	public static class CopyFacesData
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>>
	{
		public Polyhedron<Vertex,Edge,Face> polyhedron;
		public HashMap<Vertex, Vertex> newVertexLookup;
		public HashMap<Edge, Edge> newEdgeLookup;
		public HashMap<Face, Face> newFaceLookup;
		
		public CopyFacesData(
			Polyhedron<Vertex,Edge,Face> polyhedron,
			HashMap<Vertex, Vertex> newVertexLookup,
			HashMap<Edge, Edge> newEdgeLookup,
			HashMap<Face, Face> newFaceLookup){
			
			this.polyhedron = polyhedron;
			this.newVertexLookup = newVertexLookup;
			this.newEdgeLookup = newEdgeLookup;
			this.newFaceLookup = newFaceLookup;
		}
	}
	
	/**
	 * Duplicate the specified collection of faces, maintaining connectivity.
	 */
	public static 
		<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
		 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
		 Face extends PolyhedronFace<Vertex, Edge, Face>>
	
	CopyFacesData<Vertex,Edge,Face> copyFaces(
		List<Face> copyFaces,
		Edge3d.Edge3dConstructor<Vertex, Edge> edgeConstructor,
		EmbeddedPolygon.Face3dConstructor<Vertex, Edge, Face> faceConstructor){
		
		//new components
		LinkedHashSet<Vertex> newVertices = new LinkedHashSet<Vertex>();
		LinkedHashSet<Edge> newEdges = new LinkedHashSet<Edge>();
		LinkedHashSet<Face> newFaces = new LinkedHashSet<Face>();
		
		//lookups for new components from old components
		HashMap<Vertex, Vertex> newVertexLookup = new HashMap<Vertex, Vertex>();
		HashMap<Edge, Edge> newEdgeLookup = new HashMap<Edge, Edge>();
		HashMap<Face, Face> newFaceLookup = new HashMap<Face, Face>();

		//duplicate faces
		for(Face face : copyFaces){
			
			//new face components
			LinkedHashSet<EmbeddedVertex<Vertex,Edge>> newEmbVertices = new LinkedHashSet<EmbeddedVertex<Vertex,Edge>>();
			LinkedHashSet<EmbeddedEdge<Vertex,Edge>> newEmbEdges = new LinkedHashSet<EmbeddedEdge<Vertex,Edge>>();
			
			//lookup for new face vertices from old
			HashMap<EmbeddedVertex<Vertex,Edge>, EmbeddedVertex<Vertex,Edge>> newEmbVertexLookup = new HashMap<EmbeddedVertex<Vertex,Edge>, EmbeddedVertex<Vertex,Edge>>(); 
			
			//duplicate vertices
			for(EmbeddedVertex<Vertex,Edge> embVertex : face.getVertices()){
				
				//get new "global" vertex from original, or duplicate
				Vertex oldVertex = embVertex.getGlobalVertex();
				Vertex newVertex = newVertexLookup.get(oldVertex);
				if(newVertex == null){
					newVertex = oldVertex.copy();
					newVertices.add(newVertex);
					newVertexLookup.put(oldVertex, newVertex);
				}
				
				//duplicate embedded vertex
				EmbeddedVertex<Vertex,Edge> newEmbVertex = new EmbeddedVertex<Vertex,Edge>(
					embVertex.getPosition().copy(),
					newVertex
				);
				newEmbVertices.add(newEmbVertex);
				newEmbVertexLookup.put(embVertex, newEmbVertex);
			}
			
			//duplicate edges
			for(EmbeddedEdge<Vertex,Edge> embEdge : face.getEdges()){
				
				//get new "global" edge from original, or duplicate
				Edge oldEdge = embEdge.getGlobalEdge();
				Edge newEdge = newEdgeLookup.get(oldEdge);
				if(newEdge == null){
					Vertex p = newVertexLookup.get(oldEdge.getInitialVertex());
					Vertex q = newVertexLookup.get(oldEdge.getFinalVertex());
					newEdge = edgeConstructor.construct(p,q);
					newEdges.add(newEdge);
					newEdgeLookup.put(oldEdge, newEdge);
				}
				
				//duplicate embedded edge
				EmbeddedEdge<Vertex,Edge> newEmbEdge = new EmbeddedEdge<Vertex,Edge>(
					newEmbVertexLookup.get(embEdge.getInitialVertex()),
					newEmbVertexLookup.get(embEdge.getFinalVertex()),
					newEdge,
					embEdge.getGlobalEdgeRelativeOrientation()
				);
				newEmbEdges.add(newEmbEdge);
			}
			
			Face newFace = faceConstructor.construct(face, face.getPlane().copy(), newEmbVertices, newEmbEdges);
			newFaceLookup.put(face, newFace);
			
			//add new face to set
			newFaces.add(newFace);
		}
		
		//return CopyFacesData
		return new CopyFacesData<Vertex,Edge,Face>(
			new Polyhedron<Vertex,Edge,Face>(newVertices, newEdges, newFaces),
			newVertexLookup, newEdgeLookup, newFaceLookup
		);
	}
}
