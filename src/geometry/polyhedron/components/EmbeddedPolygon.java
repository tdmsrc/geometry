package geometry.polyhedron.components;

import java.util.HashMap;
import java.util.LinkedHashSet;

import geometry.common.OutputTree;
import geometry.common.components.Edge3d;
import geometry.common.components.Plane;
import geometry.common.components.Vertex3d;
import geometry.common.components.Edge2d.Edge2dConstructor;
import geometry.common.components.Vertex2d.Vertex2dConstructor;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.polygon.components.Polygon;
import geometry.polygon.components.PolygonEdge;
import geometry.polygon.components.PolygonVertex;

//A Polygon with a Plane.
//The 3d face is made up of 3d vertices and edges which lie on the plane.
//The polygon is stored using the 2d plane coordinates for the vertices and edges.
//Each 2d vertex and edge has a reference back to the 3d vertex or edge.

/**
 * A {@link Polygon} with sub-components
 * <ul>
 * <li>{@link EmbeddedVertex}: A 2D vertex containing {@link Polygon} combinatorics 
 * with a reference to a 3D "global vertex" containing {@link Polyhedron} combinatorics.
 * <li>{@link EmbeddedEdge}: A 2D edge containing {@link Polygon} combinatorics
 * with a reference to a 3D "global edge" containing {@link Polyhedron} combinatorics.
 * </ul>
 * and a reference to a {@link Plane} which describes how the 2D polygon is embedded into 3D.
 */
public class EmbeddedPolygon
	//the type of 3d vertex and edge the embedded vertices and edges link to
	<Vertex extends Vertex3d, 
	 Edge extends Edge3d<Vertex>>

	extends Polygon<EmbeddedVertex<Vertex, Edge>, EmbeddedEdge<Vertex, Edge>>
{
	
	/**
	 * Generic interface to provide a method to construct instances
	 * of a class derived from {@link EmbeddedPolygon}.
	 */
	public static interface Face3dConstructor<Vertex extends Vertex3d, Edge extends Edge3d<Vertex>, Face extends EmbeddedPolygon<Vertex, Edge>>{
		public Face construct(Plane plane, LinkedHashSet<EmbeddedVertex<Vertex,Edge>> vertices, LinkedHashSet<EmbeddedEdge<Vertex,Edge>> edges);
		public Face construct(Face oldFace, Plane plane, LinkedHashSet<EmbeddedVertex<Vertex,Edge>> vertices, LinkedHashSet<EmbeddedEdge<Vertex,Edge>> edges);
	}
	
	private Plane plane;
	
	/**
	 * Creates a new {@link EmbeddedPolygon}. 
	 * Sets embedding plane and sub-components as specified.
	 * 
	 *  @see {@link Polygon#Polygon(LinkedHashSet, LinkedHashSet)}
	 */
	public EmbeddedPolygon(Plane plane, LinkedHashSet<EmbeddedVertex<Vertex,Edge>> vertices, LinkedHashSet<EmbeddedEdge<Vertex,Edge>> edges){		
		super(vertices, edges);
		
		this.plane = plane;
	}
	
	/**
	 * Get the plane in which this polygon is embedded.
	 * 
	 * @return The plane in which this polygon is embedded.
	 */
	public Plane getPlane(){ return plane; }
	
	/**
	 * Flip the orientation of this polygon.
	 */
	@Override
	public void flip(){

		//flip edge orientations
		super.flip();
		
		//flip plane, and flip vertex x-values to account for flipping plane's e1
		plane.flip();
		super.flipXValues();
	}
	
	/**
	 * Creates and returns a new collection containing all "global edges"
	 * (i.e., edges in 3D) referenced by the edges of this embedded polygon.
	 * 
	 * @return A collection of global edges.
	 */
	public LinkedHashSet<Edge> getGlobalEdges(){
		
		LinkedHashSet<Edge> globalEdges = new LinkedHashSet<Edge>();
		for(EmbeddedEdge<Vertex, Edge> edge : edges){
			globalEdges.add(edge.getGlobalEdge());
		}
		return globalEdges;
	}
	
	/**
	 * Creates and returns a new collection containing all "global vertices" 
	 * (i.e., vertices in 3D) referenced by the vertices of this embedded polygon.
	 * 
	 * @return A collection of global vertices.
	 */
	public LinkedHashSet<Vertex> getGlobalVertices(){
		
		LinkedHashSet<Vertex> globalVertices = new LinkedHashSet<Vertex>();
		for(EmbeddedVertex<Vertex, Edge> vertex : vertices){
			globalVertices.add(vertex.getGlobalVertex());
		}
		return globalVertices;
	}
	
	
	//===============================================
	// CAMERA PROJECTION
	//===============================================
	
	/**
	 * Project this EmbeddedPolygon using the specified Camera onto a 2D Polygon
	 * using {@link Camera#getScreenCoordsFromObjectCoords(int, int, Vector3d)}.
	 * <br><br>
	 * Set flipY true if the screen's coordinate system starts from the upper-left;
	 * if it starts from the lower-left, then set flipY to false.
	 * 
	 * @param camera The camera from which to perform the projection.
	 * @param screenWidth Passed to {@link Camera#getScreenCoordsFromObjectCoords(int, int, Vector3d)} 
	 * to determine the projection.
	 * @param screenHeight Passed to {@link Camera#getScreenCoordsFromObjectCoords(int, int, Vector3d)} 
	 * to determine the projection.
	 * @return Returns a Polygon representing the projection of this EmbeddedPolygon into 2D.
	 */
	public 
		<ProjVertex extends PolygonVertex<ProjVertex,ProjEdge>, 
		 ProjEdge extends PolygonEdge<ProjVertex,ProjEdge>, 
		 ProjPoly extends Polygon<ProjVertex,ProjEdge>> 
	
	ProjPoly getCameraProjection(Camera camera, int screenWidth, int screenHeight, boolean flipY,
		Vertex2dConstructor<ProjVertex> vertexConstructor,
		Edge2dConstructor<ProjVertex, ProjEdge> edgeConstructor,
		PolygonConstructor<ProjVertex, ProjEdge, ProjPoly> polyConstructor){
		
		//first create vertices, along with a lookup to find a newly-created vertex from an original
		LinkedHashSet<ProjVertex> projVerts = new LinkedHashSet<ProjVertex>();
		HashMap<EmbeddedVertex<Vertex,Edge>, ProjVertex> newVertexLookup = new HashMap<EmbeddedVertex<Vertex,Edge>, ProjVertex>(); 
		
		for(EmbeddedVertex<Vertex,Edge> vertex : vertices){
			
			Vector3d vPos = vertex.getGlobalVertex().getPosition();
			Vector2d pvPos = camera.getScreenCoordsFromObjectCoords(screenWidth, screenHeight, flipY, vPos);
			ProjVertex pv = vertexConstructor.construct(pvPos); 
			
			projVerts.add(pv);
			newVertexLookup.put(vertex, pv);
		}
		
		//now create edges
		LinkedHashSet<ProjEdge> projEdges = new LinkedHashSet<ProjEdge>();
		
		for(EmbeddedEdge<Vertex,Edge> edge : edges){
			
			ProjVertex p = newVertexLookup.get(edge.getInitialVertex());
			ProjVertex q = newVertexLookup.get(edge.getFinalVertex());
			ProjEdge pe = edgeConstructor.construct(p, q);
			
			projEdges.add(pe);
		}
		
		return polyConstructor.construct(projVerts, projEdges);
	}
	
	
	//===============================================
	// SERIALIZATION
	//===============================================
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("Face3d (index: " + getIndex() + ", Plane: n=" + plane.getNormal() + " o=" + plane.getOrigin() + " e1=" + plane.getXAxisVector() + " e2=" + plane.getYAxisVector() + ")");
		
		OutputTree childLocal = outputTree.spawnChild("Face2d");
		super.populateOutputTree(childLocal);
	}
}
