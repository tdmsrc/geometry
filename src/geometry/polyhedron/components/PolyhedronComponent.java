package geometry.polyhedron.components;

import geometry.math.Vector3d;

/**
 * A component of a {@link Polyhedron}.  Can be a vertex, edge, or face; this can be determined
 * using the method {@link #getPolyhedronComponentType()}.
 */
public interface PolyhedronComponent
	<Vertex extends PolyhedronVertex<Vertex, Edge, Face>,
	 Edge extends PolyhedronEdge<Vertex, Edge, Face>,
	 Face extends PolyhedronFace<Vertex, Edge, Face>> 
{
	
	public Polyhedron<Vertex,Edge,Face> getOwner();
	public void disown();
	public void setOwner(Polyhedron<Vertex,Edge,Face> owner);
	
	/**
	 * @return If this component is not a vertex, returns null.
	 * Otherwise, returns this object as a Vertex (which extends {@link PolyhedronVertex}).
	 */
	public Vertex getAsVertex();
	
	/**
	 * @return If this component is not an edge, returns null.
	 * Otherwise, returns this object as an Edge (which extends {@link PolyhedronEdge}).
	 */
	public Edge getAsEdge();
	
	/**
	 * @return If this component is not a face, returns null.
	 * Otherwise, returns this object as a Face (which extends {@link PolyhedronFace}).
	 */
	public Face getAsFace();
	
	/**
	 * Indicates the type of polyhedron component.  
	 * Can be one of:
	 * <ul>
	 * <li> {@link PolyhedronComponentType#VERTEX}
	 * <li> {@link PolyhedronComponentType#EDGE}
	 * <li> {@link PolyhedronComponentType#FACE}
	 * </ul> 
	 */
	public static enum PolyhedronComponentType{
		/** Polyhedron component of vertex type. **/ 
		VERTEX, 
		/** Polyhedron component of edge type. **/ 
		EDGE, 
		/** Polyhedron component of face type. **/ 
		FACE
	}
	
	/**
	 * @return Returns a {@link PolyhedronComponentType} indicating the type of this component.
	 */
	public PolyhedronComponentType getPolyhedronComponentType();
	
	/**
	 * Checks whether, if the vector "v" were placed with its head on an
	 * interior point of this component, points infinitesimally near the
	 * head of "v" would be inside the polyhedron of which this is a component.
	 * 
	 * @param v The vector whose head would be placed on the interior of this component.
	 * @return True if points infinitesimally near the head of "v" 
	 * are inside the polyhedron, false otherwise.
	 */
	public boolean checkInfinitesimalInterior(Vector3d v);
}
