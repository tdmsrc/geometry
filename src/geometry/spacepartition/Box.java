package geometry.spacepartition;

import geometry.math.FixedDimensionalVector;

/**
 * An axis-aligned box.  Implements {@link Culler}.
 * 
 * @param <Vector> A class extending {@link FixedDimensionalVector} which has the desired dimension.
 */

public class Box<Vector extends FixedDimensionalVector<Vector>>
	implements Culler<Vector>{
	
	protected Vector min, max;
	
	/**
	 * Create an axis-aligned box with the range of coordinate i
	 * being [min_i, max_i].
	 * 
	 * @param min The minimum value of each coordinate.
	 * @param max The maximum value of each coordinate.
	 */
	public Box(Vector min, Vector max){
		this.min = min;
		this.max = max;
	}
	
	/**
	 * Return the minimum values for each coordinate.
	 * 
	 * @return The minimum values for each coordinate.
	 */
	public Vector getMin(){
		return min;
	}
	
	/**
	 * Return the maximum values for each coordinate.
	 * 
	 * @return The maximum values for each coordinate.
	 */
	public Vector getMax(){
		return max;
	}
	
	/**
	 * Linearly interpolate along each coordinate of the box.
	 * In particular, returns the vector with ith coordinate 
	 * equal to min_i + t_i*(max_i-min_i).
	 * 
	 * @param t The vector of lerp values.
	 * @return The interpolated point.
	 */
	public Vector lerp(Vector t){
		Vector x = max.copy();
		x.subtract(min);
		x.multiplyComponentWise(t);
		x.add(min);
		return x;
	}
	
	/**
	 * Get the vector t such that lerp(t) = x.
	 * In particular, returns the vector with ith coordinate 
	 * equal to (x_i-min_i)/(max_i-min_i).
	 * 
	 * @see {@link #lerp(FixedDimensionalVector)}
	 */
	public Vector getLerp(Vector x){
		
		Vector t = x.copy();
		t.subtract(min);
		t.divideComponentWise(getDimensions());
		return t;
	}
	
	/**
	 * Clamps the specified vector to this box.
	 * In particular, for each i, sets v_i = clamp(v_i, [min_i, max_i]).
	 * 
	 * @param v The vector to be clamped.
	 */
	public void clamp(Vector v){
	
		int n = v.getDimension();
		for(int i=0; i<n; i++){
			if(v.get(i) < min.get(i)){ v.set(i, min.get(i)); }
			if(v.get(i) > max.get(i)){ v.set(i, max.get(i)); }
		}
	}
	
	/**
	 * Clamps the specified box to this box.
	 * In particular, for each i, sets:
	 * rhs.min_i = clamp(rhs.min_i, [min_i, max_i]),
	 * rhs.max_i = clamp(rhs.max_i, [min_i, max_i]).
	 * 
	 * @param range The box to be clamped.
	 */
	public void clamp(Box<Vector> rhs){
		
		clamp(rhs.min);
		clamp(rhs.max);
	}
	
	/**
	 * Return the center point of the box.
	 * 
	 * @return The center point of the box.
	 */
	public Vector getCenter(){
		Vector temp = min.copy();
		temp.add(max);
		temp.scale(0.5f);
		return temp;
	}
	
	/**
	 * Return the dimensions of the box.
	 * 
	 * @return The dimensions of the box.
	 */
	public Vector getDimensions(){
		Vector temp = max.copy();
		temp.subtract(min);
		return temp;
	}
	
	/**
	 * Return the n-volume of the box.
	 * 
	 * @return The n-volume of the box.
	 */
	public float computeVolume(){
		return getDimensions().prod();
	}
	
	/**
	 * Check if the specified point is inside this box (fattened by "eps").
	 * 
	 * @return True if the point is in the box, false otherwise.
	 */
	public boolean containsPoint(Vector p, float eps){
		
		int n = min.getDimension();
		for(int i=0; i<n; i++){
			if(p.get(i) < min.get(i)-eps){ return false; }
			if(p.get(i) > max.get(i)+eps){ return false; }
		}
		return true;
	}
	
	/**
	 * Determine whether "box" is contained in this box.
	 * 
	 * @return BoxContainment indicating how "box" is contained in this box.
	 */
	@Override
	public BoxContainment getBoxContainment(Box<Vector> box) {

		//"box" contained iff \forall i [box.min_i, box.max_i] \subset [min_i, max_i]
		//empty intersect w/ "box" iff \exists i [box.min_i, box.max_i] \cap [min_i, max_i] = empty
		
		boolean completeContainment = true;
		
		int n = min.getDimension();
		for(int i=0; i<n; i++){
			
			//if [min_i, max_i] \cap [box.min_i, box.max_i] = empty, NO intersection
			if( (box.max.get(i) < min.get(i)) || 
				(max.get(i) < box.min.get(i)) ){ 
				return BoxContainment.CONTAINMENT_NONE; }
			
			//if NOT [box.min_i, box.max_i] \subset [min_i, max_i], NO complete containment
			if( (box.min.get(i) < min.get(i)) || 
				(box.max.get(i) > max.get(i)) ){ 
				completeContainment = false; }
		}
		
		return (completeContainment ? BoxContainment.CONTAINMENT_COMPLETE : BoxContainment.CONTAINMENT_PARTIAL);
	}
	
	/**
	 * Find the square of the smallest distance from this box to the point p.
	 * 
	 * @param p The point from which the distance is to be computed.
	 * @return The square of the smallest distance from this box to p.
	 */
	public float getMinimumDistanceSquared(Vector p){
		
		//For each dimension, dist contribution is the distance in R of
		//p_i to the interval [min_i,max_i]; this is piecewise defined as:
		//|p_i-min_i| if p_i < min_i; 0 if p_i \in [min_i,max_i]; |p_i-max_i| if p_i > max_i
		
		int n = min.getDimension();
		
		float ret = 0.0f;
		for(int i=0; i<n; i++){
			float d = 0.0f;
			if(p.get(i) < min.get(i)){ d = min.get(i) - p.get(i); }
			else if(p.get(i) > max.get(i)){ d = p.get(i) - max.get(i); }
			ret += d*d;
		}
		return ret;
	}
	
	/**
	 * Find the smallest distance from this box to the point p.
	 * 
	 * @param p The point from which the distance is to be computed.
	 * @return The smallest distance from this box to p.
	 */
	public float getMinimumDistance(Vector p){
		
		return (float)Math.sqrt(getMinimumDistanceSquared(p));
	}
	
	/**
	 * Find the square of the largest distance from this box to the point p.
	 * 
	 * @param p The point from which the distance is to be computed.
	 * @return The square of the largest distance from this box to p.
	 */
	public float getMaximumDistanceSquared(Vector p){
		
		int n = min.getDimension();
		
		float ret = 0.0f;
		for(int i=0; i<n; i++){
			float dmin = p.get(i) - min.get(i); if(dmin < 0){ dmin = -dmin; }
			float dmax = p.get(i) - max.get(i); if(dmax < 0){ dmax = -dmax; }
			ret += (dmin > dmax) ? dmin*dmin : dmax*dmax;
		}
		return ret;
	}
	
	/**
	 * Find the largest distance from this box to the point p.
	 * 
	 * @param p The point from which the distance is to be computed.
	 * @return The largest distance from this box to p.
	 */
	public float getMaximumDistance(Vector p){
		
		return (float)Math.sqrt(getMaximumDistanceSquared(p));
	}
	
	/**
	 * Intersect the edge from p to q with this box.
	 * 
	 * @see {@link #clipEdge(FixedDimensionalVector, FixedDimensionalVector)}
	 * @see {@link #clipEdge(FixedDimensionalVector, FixedDimensionalVector, FixedDimensionalVector, FixedDimensionalVector)}
	 */
	private boolean clipEdge(Vector p, Vector q, boolean outputClippedEdge, Vector pOut, Vector qOut){
		
		Vector pClip = p, qClip = q;
		
		//for each coordinate i, intersect with each region bounded between 
		//hyperplanes [ith coord = min] and [ith coord = max]
		int n = p.getDimension();
		for(int i=0; i<n; i++){
			
			boolean pLMin = pClip.get(i) < min.get(i);
			boolean qLMin = qClip.get(i) < min.get(i);
			
			if(pLMin && qLMin){ return false; }
			//if exactly one endpoint has ith coord < min, set it to intersection point
			else if(pLMin ^ qLMin){
				float t = (min.get(i) - pClip.get(i)) / (qClip.get(i) - pClip.get(i));
				Vector x = pClip.lerp(qClip, t);
				if(pLMin && !qLMin){ pClip = x; }else{ qClip = x; }
			}
			
			boolean pGMax = pClip.get(i) > max.get(i);
			boolean qGMax = qClip.get(i) > max.get(i);
			
			if(pGMax && qGMax){ return false; }
			//if exactly one endpoint has ith coord > max, set it to intersection point
			else if(pGMax ^ qGMax){
				float t = (max.get(i) - pClip.get(i)) / (qClip.get(i) - pClip.get(i));
				Vector x = pClip.lerp(qClip, t);
				if(pGMax && !qGMax){ pClip = x; }else{ qClip = x; }
			}
		}
		
		if(outputClippedEdge){
			pOut.setEqualTo(pClip);
			qOut.setEqualTo(qClip);
		}
		return true;
	}
	
	/**
	 * Intersect the edge from p to q with this box.
	 * 
	 * @param p The initial point of the edge.
	 * @param q The final point of the edge.
	 * @return Returns false if the edge from p to q does not intersect this box,
	 * true otherwise.
	 * 
	 * @see {@link #clipEdge(FixedDimensionalVector, FixedDimensionalVector, FixedDimensionalVector, FixedDimensionalVector)}
	 */
	public boolean clipEdge(Vector p, Vector q){
		
		return clipEdge(p, q, false, null, null);
	}
	
	/**
	 * Intersect the edge from p to q with this box.
	 * 
	 * @param p The initial point of the edge.
	 * @param q The final point of the edge.
	 * @param pOut Will be set equal to the initial point of the clipped edge.
	 * @param qOut Will be set equal to the final point of the clipped edge.
	 * @return Returns false if the edge from p to q does not intersect this box,
	 * true otherwise.
	 * 
	 * @see {@link #clipEdge(FixedDimensionalVector, FixedDimensionalVector)}
	 */
	public boolean clipEdge(Vector p, Vector q, Vector pOut, Vector qOut){
		
		return clipEdge(p, q, true, pOut, qOut);
	}
}
