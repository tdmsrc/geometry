package geometry.spacepartition;

import geometry.math.FixedDimensionalVector;

/**
 * A {@link Culler} used to traverse nodes or leaves of a {@link QuadtreeNodeCullable}
 * or {@link OctreeNodeCullable} which intersect the specified edge.
 */
public class CullerEdge<Vector extends FixedDimensionalVector<Vector>>
	implements Culler<Vector>{
	
	private Vector p, q;
	
	
	public CullerEdge(Vector p, Vector q){
		this.p = p;
		this.q = q;
	}
	
	/**
	 * Get the initial point of this edge.
	 * 
	 * @return The initial point of this edge.
	 */
	public Vector getInitialPoint(){ return p; }
	
	/**
	 * Change the initial point of this edge.<br><br>
	 * This can be used during a tree traversal (e.g. when picking).
	 * 
	 * @param p The new initial point of the edge.
	 */
	public void setInitialPoint(Vector p){ this.p = p; }
	
	/**
	 * Get the final point of this edge.
	 * 
	 * @return The final point of this edge.
	 */
	public Vector getFinalPoint(){ return q; }
	
	/**
	 * Change the final point of this edge.<br><br>
	 * This can be used during a tree traversal (e.g. when picking).
	 * 
	 * @param q The new final point of the edge.
	 */
	public void setFinalPoint(Vector q){ this.q = q; }
	
	
	@Override
	public BoxContainment getBoxContainment(Box<Vector> box){
		
		return box.clipEdge(p, q) ? 
			BoxContainment.CONTAINMENT_PARTIAL : BoxContainment.CONTAINMENT_NONE;
	}
}
