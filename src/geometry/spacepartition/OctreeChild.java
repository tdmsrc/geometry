package geometry.spacepartition;

import geometry.math.Vector3d;

/**
 * An enum which represents one of the eight possible children of an {@link OctreeNode}.
 * 
 * The eight children of an {@link OctreeNode} are specified by choosing a boolean
 * for each of XMAX, YMAX, and ZMAX.
 * 
 * @see {@link #get(boolean, boolean, boolean)}
 */
public enum OctreeChild{

	CHILD_XMIN_YMIN_ZMIN{
		@Override public boolean isXMAX(){ return false; }
		@Override public boolean isYMAX(){ return false; }
		@Override public boolean isZMAX(){ return false; }
	},
	
	CHILD_XMAX_YMIN_ZMIN{
		@Override public boolean isXMAX(){ return true; }
		@Override public boolean isYMAX(){ return false; }
		@Override public boolean isZMAX(){ return false; }
	}, 
	
	CHILD_XMIN_YMAX_ZMIN{
		@Override public boolean isXMAX(){ return false; }
		@Override public boolean isYMAX(){ return true; }
		@Override public boolean isZMAX(){ return false; }
	}, 
	
	CHILD_XMAX_YMAX_ZMIN{
		@Override public boolean isXMAX(){ return true; }
		@Override public boolean isYMAX(){ return true; }
		@Override public boolean isZMAX(){ return false; }
	},
	
	CHILD_XMIN_YMIN_ZMAX{
		@Override public boolean isXMAX(){ return false; }
		@Override public boolean isYMAX(){ return false; }
		@Override public boolean isZMAX(){ return true; }
	},
	
	CHILD_XMAX_YMIN_ZMAX{
		@Override public boolean isXMAX(){ return true; }
		@Override public boolean isYMAX(){ return false; }
		@Override public boolean isZMAX(){ return true; }
	}, 
	
	CHILD_XMIN_YMAX_ZMAX{
		@Override public boolean isXMAX(){ return false; }
		@Override public boolean isYMAX(){ return true; }
		@Override public boolean isZMAX(){ return true; }
	}, 
	
	CHILD_XMAX_YMAX_ZMAX{
		@Override public boolean isXMAX(){ return true; }
		@Override public boolean isYMAX(){ return true; }
		@Override public boolean isZMAX(){ return true; }
	};

	/**
	 * Determine if this {@link OctreeChild} is of XMAX type.
	 */
	public abstract boolean isXMAX();
	/**
	 * Determine if this {@link OctreeChild} is of YMAX type.
	 */
	public abstract boolean isYMAX();
	/**
	 * Determine if this {@link OctreeChild} is of ZMAX type.
	 */
	public abstract boolean isZMAX();
	
	/**
	 * Determines if this {@link OctreeChild} and "rhs" have the same XMAX type.
	 */
	public boolean isXAligned(OctreeChild rhs){ return (isXMAX() == rhs.isXMAX()); }
	/**
	 * Determines if this {@link OctreeChild} and "rhs" have the same YMAX type.
	 */
	public boolean isYAligned(OctreeChild rhs){ return (isYMAX() == rhs.isYMAX()); }
	/**
	 * Determines if this {@link OctreeChild} and "rhs" have the same ZMAX type.
	 */
	public boolean isZAligned(OctreeChild rhs){ return (isZMAX() == rhs.isZMAX()); }
	
	/**
	 * Returns the {@link OctreeChild} of specified XMAX, YMAX, and ZMAX type.
	 */
	public static OctreeChild get(boolean XMAX, boolean YMAX, boolean ZMAX){	
		return XMAX ?
			(YMAX ? 
				(ZMAX ? OctreeChild.CHILD_XMAX_YMAX_ZMAX : OctreeChild.CHILD_XMAX_YMAX_ZMIN ) : 
				(ZMAX ? OctreeChild.CHILD_XMAX_YMIN_ZMAX : OctreeChild.CHILD_XMAX_YMIN_ZMIN )): 
			(YMAX ? 
				(ZMAX ? OctreeChild.CHILD_XMIN_YMAX_ZMAX : OctreeChild.CHILD_XMIN_YMAX_ZMIN ) : 
				(ZMAX ? OctreeChild.CHILD_XMIN_YMIN_ZMAX : OctreeChild.CHILD_XMIN_YMIN_ZMIN ));
	}
	
	/**
	 * Returns the {@link OctreeChild} with opposite XMAX type.
	 */
	public OctreeChild flipX(){ return OctreeChild.get(!isXMAX(),  isYMAX(),  isZMAX()); }
	/**
	 * Returns the {@link OctreeChild} with opposite YMAX type.
	 */
	public OctreeChild flipY(){ return OctreeChild.get( isXMAX(), !isYMAX(),  isZMAX()); }
	/**
	 * Returns the {@link OctreeChild} with opposite ZMAX type.
	 */
	public OctreeChild flipZ(){ return OctreeChild.get( isXMAX(),  isYMAX(), !isZMAX()); }
	
	/**
	 * The {@link Box} "box" can be split into eight sub-boxes, by halving it
	 * by its X-midpoint plane, Y-midpoint plane, and Z-midpoint plane.
	 * <br>
	 * This method returns the sub-box indicated by the XMAX, YMAX, and ZMAX
	 * type of this {@link OctreeChild}.
	 * 
	 * @param box The {@link Box} to be split.
	 * @return The sub-box of "box" indicated by this {@link OctreeChild}.
	 */
	public Box<Vector3d> getSubBox(Box<Vector3d> box){
		Vector3d lerpMin = new Vector3d(isXMAX() ? 0.5f : 0.0f, isYMAX() ? 0.5f : 0.0f, isZMAX() ? 0.5f : 0.0f);
		Vector3d lerpMax = new Vector3d(isXMAX() ? 1.0f : 0.5f, isYMAX() ? 1.0f : 0.5f, isZMAX() ? 1.0f : 0.5f);
		return new Box<Vector3d>(box.lerp(lerpMin), box.lerp(lerpMax));
	}
	
	/**
	 * Fills the array childOrder with all eight {@link OctreeChild}s in such a way 
	 * that if j > i, then childOrder[j] is farther along "ray" than childOrder[i],
	 * assuming that the children represent Boxes produced using {@link #getSubBox(Box)}
	 * applied to a box with the specified dimensions.
	 * 
	 * @param ray The direction along which to order the child boxes.
	 * @param dimensions The size of the box which was split into sub-boxes.
	 * @param childOrder An array already allocated to hold the children.
	 */
	public static void sort(Vector3d ray, Vector3d dimensions, OctreeChild[] childOrder){
		
		//assign a point to each child with coords e.g. x = isXMAX() ? 0 : dimensions.getX(), etc; 
		//order children by value of ray.dot(child's point).

		//smallest dot value has XMAX = (ray.getX() < 0), etc.
		boolean[] max = new boolean[]{ ray.getX() < 0, ray.getY() < 0, ray.getZ() < 0 };
		
		//flipping XMAX increases the dot value by xcmp = |ray.getX()*dimensions.getX()|, etc.
		Vector3d cmp = ray.copy(); cmp.multiplyComponentWise(dimensions);
		if(cmp.getX() < 0){ cmp.setX(-cmp.getX()); }
		if(cmp.getY() < 0){ cmp.setY(-cmp.getY()); }
		if(cmp.getZ() < 0){ cmp.setZ(-cmp.getZ()); }
		
		//order the cmp values, so that cmp.get(c0) is the smallest, etc.
		int c0, c1, c2;	
		if(cmp.getX() < cmp.getY()){ 			//XYZ XZY or ZXY
			if(cmp.getY() < cmp.getZ()){		c0 = 0; c1 = 1; c2 = 2; } //XYZ
			else if(cmp.getZ() < cmp.getX()){	c0 = 2; c1 = 0; c2 = 1; } //ZXY
			else{								c0 = 0; c1 = 2; c2 = 1; } //XZY
		}else{ 									//YXZ YZX or ZYX 
			if(cmp.getX() < cmp.getZ()){		c0 = 1; c1 = 0; c2 = 2; } //YXZ
			else if(cmp.getZ() < cmp.getY()){	c0 = 2; c1 = 1; c2 = 0; } //ZYX
			else{								c0 = 1; c1 = 2; c2 = 0; } //YZX
		}
		
		//the values of ray.dot(child's point) are offset from the minimum value in the order:
		//0, c0, c1, {c0+c1, c2}, c0+c2, c1+c2, c0+c1+c2,
		//where the bracketed values could be in either order, and must be checked.
		int ci = 0;
		for(int i=0; i<2; i++, max[c2] ^= true){
		for(int j=0; j<2; j++, max[c1] ^= true){
		for(int k=0; k<2; k++, max[c0] ^= true){
			//child (max[0],max[1],max[2]) has dot value (i%2)*c2+(j%2)*c1+(k%2)*c0.
			childOrder[ci++] = OctreeChild.get(max[0], max[1], max[2]);
		}}}
		
		//check if cmp0+cmp1 and cmp2 need to be switched
		if(cmp.get(c2) < (cmp.get(c0) + cmp.get(c1))){
			OctreeChild temp = childOrder[3];
			childOrder[3] = childOrder[4];
			childOrder[4] = temp;
		}
	}
}
