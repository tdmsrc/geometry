package geometry.spacepartition;

/**
 * A generic octree.
 */
public abstract class OctreeNode<T extends OctreeNode<T>> {

	//octree methods
	/**
	 * Returns "this".
	 */
	protected abstract T getThisNode();
	
	/**
	 * Returns the parent of this OctreeNode.
	 * 
	 * @return The parent of this OctreeNode; null if this node is the root.
	 */
	protected abstract T getParent();
	
	/**
	 * The children of an OctreeNode are specified by a {@link OctreeChild};
	 * This OctreeNode is the child of its parent of type indicated by this method.
	 *  
	 * @return The {@link OctreeChild} type by which this node's parent refers to it.
	 */
	//protected abstract OctreeChild getChildType();
	
	/**
	 * Determine whether this node is a leaf of the octree to which it belongs.
	 */
	protected abstract boolean isLeaf();
	
	/**
	 * Get the child of this OctreeNode of type indicated by "child".
	 * 
	 * @param child The {@link OctreeChild} indicating which child of this node to return.
	 * @return The child indicated by "child"; null if there is no such child.
	 */
	protected abstract T getChild(OctreeChild child);
	
	
	//==========================================
	// TREE TRAVERSAL
	//==========================================
	
	/**
	 * Traverse all nodes of the octree beneath (and including) this node.
	 * 
	 * @param octreeTraversal Provides a method to call for each node encountered.
	 * @param leavesOnly If true, only uses the octreeTraversal method on leaves; if false, 
	 * uses the octreeTraversal method on all nodes encountered in the traversal.
	 * @param childOrder The order in which to traverse the children of a node.
	 * Use e.g. {@link OctreeChild#values()} or {@link OctreeChild#sort(Vector3d, Vector3d, OctreeChild[])}.
	 * @return The number of nodes encountered during the traversal.
	 * 
	 * @see {@link OctreeNodeCullable#traverse(OctreeNodeTraversal, boolean, OctreeChild[], Culler)
	 * @see {@link OctreeNodeCullable#traverse(OctreeNodeTraversal, boolean, OctreeChild[], Culler, Culler)
	 */
	public int traverse(OctreeNodeTraversal<T> octreeTraversal, boolean leavesOnly,
		OctreeChild[] childOrder){
		
		//handle this node
		int n = 0;
		
		if(!leavesOnly || isLeaf()){ octreeTraversal.handleOctreeNode(getThisNode()); n++; }
		if(isLeaf()){ return n; }
		
		//recurse without culling
		for(OctreeChild child : childOrder){
			OctreeNode<T> childNode = getChild(child);
			if(childNode == null){ continue; }
			n += childNode.traverse(octreeTraversal, leavesOnly, childOrder);
		}
		return n;
	}
}
