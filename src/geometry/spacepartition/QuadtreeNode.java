package geometry.spacepartition;

import java.util.EnumMap;
import java.util.Stack;

/**
 * A generic quadtree.
 */
public abstract class QuadtreeNode<T extends QuadtreeNode<T>>{

	//quadtree methods
	/**
	 * Returns "this".
	 */
	protected abstract T getThisNode();
	
	/**
	 * Returns the parent of this QuadtreeNode.
	 * 
	 * @return The parent of this QuadtreeNode; null if this node is the root.
	 */
	protected abstract T getParent();
	
	/**
	 * The children of a QuadtreeNode are specified by a {@link QuadtreeChild};
	 * This QuadtreeNode is the child of its parent of type indicated by this method.
	 *  
	 * @return The {@link QuadtreeChild} type by which this node's parent refers to it.
	 */
	protected abstract QuadtreeChild getChildType();
	
	/**
	 * Determine whether this node is a leaf of the quadtree to which it belongs.
	 */
	protected abstract boolean isLeaf();
	
	/**
	 * Get the child of this QuadtreeNode of type indicated by "child".
	 * 
	 * @param child The {@link QuadtreeChild} indicating which child of this node to return.
	 * @return The child indicated by "child"; null if there is no such child.
	 */
	protected abstract T getChild(QuadtreeChild child);
	
	
	//==========================================
	// NEIGHBOR LINKS (ROPES)
	//==========================================
	
	/**
	 * Indicates a neighbor of this QuadtreeNode.
	 */
	public static enum QuadtreeNeighbor{ NEIGHBOR_XMAX, NEIGHBOR_XMIN, NEIGHBOR_YMAX, NEIGHBOR_YMIN }
	
	/**
	 * Determine the four neighbors of this QuadtreeNode.
	 */
	protected EnumMap<QuadtreeNeighbor,T> getNeighbors(){
		
		//find neighbors of this node
		EnumMap<QuadtreeNeighbor,T> neighbors = new EnumMap<QuadtreeNeighbor,T>(QuadtreeNeighbor.class);
		neighbors.put(QuadtreeNeighbor.NEIGHBOR_XMAX, getNeighborX(true));
		neighbors.put(QuadtreeNeighbor.NEIGHBOR_XMIN, getNeighborX(false));
		neighbors.put(QuadtreeNeighbor.NEIGHBOR_YMAX, getNeighborY(true));
		neighbors.put(QuadtreeNeighbor.NEIGHBOR_YMIN, getNeighborY(false));
		
		return neighbors;
	}

	private T getNeighborX(boolean isXMAX){

		if(getParent() == null){ return null; }
		
		T relative = getThisNode();
		Stack<QuadtreeChild> descent = new Stack<QuadtreeChild>();
		
		//keep finding parents until a common ancestor is found, at the same time pushing
		//the childTypes necessary to descend to the neighbor from that common ancestor
		while(relative.getChildType().isXMAX() == isXMAX){
			descent.push(relative.getChildType().flipX());
			relative = relative.getParent();
			if(relative.getParent() == null){ return null; }
		}
		descent.push(relative.getChildType().flipX());
		relative = relative.getParent();
		
		//descent from the common ancestor to the neighbor
		while(!descent.isEmpty()){
			relative = relative.getChild(descent.pop());
			if(relative == null){ return null; }
		}
		return relative;
	}
	
	private T getNeighborY(boolean isYMAX){

		if(getParent() == null){ return null; }
		
		T relative = getThisNode();
		Stack<QuadtreeChild> descent = new Stack<QuadtreeChild>();
		
		//keep finding parents until a common ancestor is found, at the same time pushing
		//the childTypes necessary to descend to the neighbor from that common ancestor
		while(relative.getChildType().isYMAX() == isYMAX){
			descent.push(relative.getChildType().flipY());
			relative = relative.getParent();
			if(relative.getParent() == null){ return null; }
		}
		descent.push(relative.getChildType().flipY());
		relative = relative.getParent();
		
		//descent from the common ancestor to the neighbor
		while(!descent.isEmpty()){
			relative = relative.getChild(descent.pop());
			if(relative == null){ return null; }
		}
		return relative;
	}
	
	//==========================================
	// TREE TRAVERSAL
	//==========================================
	
	/**
	 * Traverse all nodes of the quadtree beneath (and including) this node.
	 * 
	 * @param quadtreeTraversal Provides a method to call for each node encountered.
	 * @param leavesOnly If true, only uses the quadtreeTraversal method on leaves; if false, 
	 * uses the quadtreeTraversal method on all nodes encountered in the traversal.
	 * @param childOrder The order in which to traverse the children of a node.
	 * Use e.g. {@link QuadtreeChild#values()} or {@link QuadtreeChild#sort(Vector2d, Vector2d, QuadtreeChild[])}.
	 * @return The number of nodes encountered during the traversal.
	 * 
	 * @see {@link QuadtreeNodeCullable#traverse(QuadtreeNodeTraversal, boolean, QuadtreeChild[], Culler)}
	 * @see {@link QuadtreeNodeCullable#traverse(QuadtreeNodeTraversal, boolean, QuadtreeChild[], Culler, Culler)}
	 */
	public int traverse(QuadtreeNodeTraversal<T> quadtreeTraversal, boolean leavesOnly, 
		QuadtreeChild[] childOrder){
		
		//handle this node
		int n = 0;
		
		if(!leavesOnly || isLeaf()){ quadtreeTraversal.handleQuadtreeNode(getThisNode()); n++; }
		if(isLeaf()){ return n; }
		
		//recurse without culling
		for(QuadtreeChild child : childOrder){
			QuadtreeNode<T> childNode = getChild(child);
			if(childNode == null){ continue; }
			n += childNode.traverse(quadtreeTraversal, leavesOnly, childOrder);
		}
		return n;
	}
}
