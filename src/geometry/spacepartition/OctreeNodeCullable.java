package geometry.spacepartition;

import geometry.math.FixedDimensionalVector;
import geometry.spacepartition.Culler.BoxContainment;

/**
 * A generic node of an octree which occupies a region of space indicated by a {@link Box}.
 * This {@link Box} can be used to traverse the octree with culling, by specifying a {@link Culler}.
 * <br><br>
 * It is possible when traversing with culling that extra nodes will be encountered during
 * the traversal, because {@link Culler#getBoxContainment(Box)} is allowed to
 * return a greater degree of containment than is actually true.
 * 
 * @param <Vector> A class extending {@link FixedDimensionalVector} which indicates the
 * dimension of the {@link Box} that this octree node occupies.
 */
public abstract class OctreeNodeCullable
	<T extends OctreeNodeCullable<T,Vector>, 
	 Vector extends FixedDimensionalVector<Vector>>

	extends OctreeNode<T>
{
	
	/**
	 * Specifies a {@link Box} that this OctreeNode occupies.
	 */
	protected abstract Box<Vector> getCullBounds();
	
	
	//==========================================
	// TREE TRAVERSAL WITH CULLING
	//==========================================

	/**
	 * Traverse all nodes of the octree beneath (and possibly including) this node
	 * satisfying the property that "culler" completely or partially contains the 
	 * {@link Box} occupied by the OctreeNode 
	 * (i.e., as specified by {@link #getCullBounds}).
	 * 
	 * @param octreeTraversal Provides a method to call for each node encountered.
	 * @param leavesOnly If true, only uses the octreeTraversal method on leaves; if false, 
	 * uses the octreeTraversal method on all nodes encountered in the traversal.
	 * @param childOrder The order in which to traverse the children of a node.
	 * Use e.g. {@link OctreeChild#values()} or {@link OctreeChild#sort(Vector3d, Vector3d, OctreeChild[])}.
	 * @return The number of leaves encountered during the traversal.
	 * 
	 * @see {@link OctreeNode#traverse(OctreeNodeTraversal, boolean, OctreeChild[])}
	 * @see {@link OctreeNodeCullable#traverse(OctreeNodeTraversal, boolean, OctreeChild[], Culler, Culler)
	 */
	public int traverse(OctreeNodeTraversal<T> octreeTraversal, boolean leavesOnly, 
		OctreeChild[] childOrder, Culler<Vector> culler){
		
		//check culler box containment, quitting if NONE
		BoxContainment cullResult = culler.getBoxContainment(getCullBounds());
		if(cullResult == BoxContainment.CONTAINMENT_NONE){ return 0; }
		
		//handle this node
		int n = 0;
		
		if(!leavesOnly || isLeaf()){ octreeTraversal.handleOctreeNode(getThisNode()); n++; }
		if(isLeaf()){ return n; }
		
		//continue with children
		switch(cullResult){
		case CONTAINMENT_COMPLETE:
			
			//no further culling necessary
			for(OctreeChild child : childOrder){
				OctreeNodeCullable<T,Vector> childNode = getChild(child);
				if(childNode == null){ continue; }
				n += childNode.traverse(octreeTraversal, leavesOnly, childOrder);
			}
			return n;
		
		default: //CONTAINMENT_PARTIAL (the only other possibility at this point)
			
			//need to continue culling
			for(OctreeChild child : childOrder){
				OctreeNodeCullable<T,Vector> childNode = getChild(child);
				if(childNode == null){ continue; }
				n += childNode.traverse(octreeTraversal, leavesOnly, childOrder, culler);
			}
			return n;
		}
	}

	/**
	 * Traverse all nodes of the octree beneath (and possibly including) this node
	 * satisfying the property that both "culler1" and "culler2" either completely 
	 * or partially contain the {@link Box} occupied by the OctreeNode 
	 * (i.e., as specified by {@link #getCullBounds}).
	 * 
	 * @param octreeTraversal Provides a method to call for each node encountered.
	 * @param leavesOnly If true, only uses the octreeTraversal method on leaves; if false, 
	 * uses the octreeTraversal method on all nodes encountered in the traversal.
	 * @param childOrder The order in which to traverse the children of a node.
	 * Use e.g. {@link OctreeChild#values()} or {@link OctreeChild#sort(Vector3d, Vector3d, OctreeChild[])}.
	 * @return The number of leaves encountered during the traversal.
	 * 
	 * @see {@link OctreeNode#traverse(OctreeNodeTraversal, boolean, OctreeChild[])}
	 * @see {@link OctreeNodeCullable#traverse(OctreeNodeTraversal, boolean, OctreeChild[], Culler)
	 */
	public int traverse(OctreeNodeTraversal<T> octreeTraversal, boolean leavesOnly, 
		OctreeChild[] childOrder, Culler<Vector> culler1, Culler<Vector> culler2){
		
		//check culler box containment, quitting if either is NONE
		BoxContainment cullResult1 = culler1.getBoxContainment(getCullBounds());
		if(cullResult1 == BoxContainment.CONTAINMENT_NONE){ return 0; }
		
		BoxContainment cullResult2 = culler2.getBoxContainment(getCullBounds());
		if(cullResult2 == BoxContainment.CONTAINMENT_NONE){ return 0; }
		
		//handle this node
		int n = 0;
		
		if(!leavesOnly || isLeaf()){ octreeTraversal.handleOctreeNode(getThisNode()); n++; }
		if(isLeaf()){ return n; }
		
		//continue with children
		switch(cullResult1){
		case CONTAINMENT_COMPLETE:
			switch(cullResult2){
			case CONTAINMENT_COMPLETE:
				
				//no further culling necessary
				for(OctreeChild child : childOrder){
					OctreeNodeCullable<T,Vector> childNode = getChild(child);
					if(childNode == null){ continue; }
					n += childNode.traverse(octreeTraversal, leavesOnly, childOrder);
				}
				return n;
				
			default: //CONTAINMENT_PARTIAL (the only other possibility at this point)
				
				//only need to cull by culler2 now
				for(OctreeChild child : childOrder){
					OctreeNodeCullable<T,Vector> childNode = getChild(child);
					if(childNode == null){ continue; }
					n += childNode.traverse(octreeTraversal, leavesOnly, childOrder, culler2);
				}
				return n;
				
			}
		default: //CONTAINMENT_PARTIAL (the only other possibility at this point)
			switch(cullResult2){
			case CONTAINMENT_COMPLETE:
				
				//only need to cull by culler1 now
				for(OctreeChild child : childOrder){
					OctreeNodeCullable<T,Vector> childNode = getChild(child);
					if(childNode == null){ continue; }
					n += childNode.traverse(octreeTraversal, leavesOnly, childOrder, culler1);
				}
				return n;
				
			default: //CONTAINMENT_PARTIAL (the only other possibility at this point)
				
				//need to continue culling by both
				for(OctreeChild child : childOrder){
					OctreeNodeCullable<T,Vector> childNode = getChild(child);
					if(childNode == null){ continue; }
					n += childNode.traverse(octreeTraversal, leavesOnly, childOrder, culler1, culler2);
				}
				return n;
				
			}
		}
	}
}
