package geometry.spacepartition;

/**
 * An interface for traversing a quadtree.
 *
 * @param <T> The type of {@link QuadtreeNode} to be traversed.
 * @see {@link QuadtreeNode#traverseNodesNoCulling}
 * @see {@link QuadtreeNodeCullable#traverseNodes}
 */
public interface QuadtreeNodeTraversal<T extends QuadtreeNode<T>>{
	
	/**
	 * This method is called for each Quadtree node encountered in a traversal.
	 * 
	 * @param node The node encountered in a traversal.
	 */
	public void handleQuadtreeNode(T node);
}
