package geometry.spacepartition;

/**
 * An interface for traversing an octree.
 *
 * @param <T> The type of {@link OctreeNode} to be traversed.
 * @see {@link OctreeNode#traverseNodesNoCulling}
 * @see {@link OctreeNodeCullable#traverseNodes}
 */
public interface OctreeNodeTraversal<T extends OctreeNode<T>>{

	/**
	 * This method is called for each Octree node encountered in a traversal.
	 * 
	 * @param node The node encountered in a traversal.
	 */
	public void handleOctreeNode(T node);
}
