package geometry.spacepartition;

import geometry.math.FixedDimensionalVector;

/**
 * A sphere.  Implements {@link Culler}.
 * 
 * @param <Vector> A class extending {@link FixedDimensionalVector} which has the desired dimension.
 */
public class Sphere<Vector extends FixedDimensionalVector<Vector>> 
	implements Culler<Vector>{

	private Vector position;
	private float radius;
	
	/**
	 * Create a sphere with center "position" and radius "radius".
	 * 
	 * @param position The center of the sphere.
	 * @param radius The radius of the sphere.
	 */
	public Sphere(Vector position, float radius){
		this.position = position;
		this.radius = radius;
	}
	
	/**
	 * Returns the center of this sphere.
	 * 
	 * @return The center of this sphere.
	 */
	public Vector getPosition(){ return position; }
	
	/**
	 * Returns the radius of this sphere.
	 * 
	 * @return The radius of this sphere.
	 */
	public float getRadius(){ return radius; }
	
	/**
	 * Sets the center of this sphere to "position".
	 * 
	 * @param position The new center of the sphere.
	 */
	public void setPosition(Vector position){ 
		this.position = position;
	}
	
	/**
	 * Sets the radius of this sphere to "radius".
	 * 
	 * @param radius The new radius of the sphere.
	 */
	public void setRadius(float radius){ 
		this.radius = radius;
	}

	@Override
	public BoxContainment getBoxContainment(Box<Vector> box) {
		return 
		(box.getMinimumDistance(position) > radius) ? BoxContainment.CONTAINMENT_NONE :
		((box.getMaximumDistance(position) < radius) ? BoxContainment.CONTAINMENT_COMPLETE : 
			BoxContainment.CONTAINMENT_PARTIAL);
	}
}
