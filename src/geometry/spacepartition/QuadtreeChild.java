package geometry.spacepartition;

import geometry.math.Vector2d;

/**
 * An enum which represents one of the four possible children of a {@link QuadtreeNode}.
 * 
 * The four children of a {@link QuadtreeNode} are specified by choosing a boolean
 * for XMAX and YMAX.
 * 
 * @see {@link #get(boolean, boolean)}
 */
public enum QuadtreeChild{

	CHILD_XMIN_YMIN{
		@Override public boolean isXMAX(){ return false; }
		@Override public boolean isYMAX(){ return false; }
	},
	
	CHILD_XMAX_YMIN{
		@Override public boolean isXMAX(){ return true; }
		@Override public boolean isYMAX(){ return false; }
	}, 
	
	CHILD_XMIN_YMAX{
		@Override public boolean isXMAX(){ return false; }
		@Override public boolean isYMAX(){ return true; }
	}, 
	
	CHILD_XMAX_YMAX{
		@Override public boolean isXMAX(){ return true; }
		@Override public boolean isYMAX(){ return true; }
	};

	/**
	 * Determine if this {@link QuadtreeChild} is of XMAX type.
	 */
	public abstract boolean isXMAX();
	/**
	 * Determine if this {@link QuadtreeChild} is of YMAX type.
	 */
	public abstract boolean isYMAX();
	
	/**
	 * Determines if this {@link QuadtreeChild} and "rhs" have the same XMAX type.
	 */
	public boolean isXAligned(QuadtreeChild rhs){ return (isXMAX() == rhs.isXMAX()); }
	/**
	 * Determines if this {@link QuadtreeChild} and "rhs" have the same YMAX type.
	 */
	public boolean isYAligned(QuadtreeChild rhs){ return (isYMAX() == rhs.isYMAX()); }
	
	/**
	 * Returns the {@link QuadtreeChild} of specified XMAX and YMAX type.
	 */
	public static QuadtreeChild get(boolean XMAX, boolean YMAX){	
		return XMAX ?
			(YMAX ? QuadtreeChild.CHILD_XMAX_YMAX : QuadtreeChild.CHILD_XMAX_YMIN) : 
			(YMAX ? QuadtreeChild.CHILD_XMIN_YMAX : QuadtreeChild.CHILD_XMIN_YMIN);
	}
	
	/**
	 * Returns the {@link QuadtreeChild} with opposite XMAX type.
	 */
	public QuadtreeChild flipX(){ return QuadtreeChild.get(!isXMAX(),  isYMAX()); }
	/**
	 * Returns the {@link QuadtreeChild} with opposite YMAX type.
	 */
	public QuadtreeChild flipY(){ return QuadtreeChild.get( isXMAX(), !isYMAX()); } 
	
	/**
	 * The Box "box" can be split into four sub-boxes, by halving it
	 * by its X-midpoint plane and Y-midpoint plane.
	 * <br>
	 * This method returns the sub-box indicated by the XMAX and YMAX
	 * type of this {@link QuadtreeChild}.
	 * 
	 * @param box The {@link Box} to be split.
	 * @return The sub-box of "box" indicated by this {@link QuadtreeChild}.
	 */
	public Box<Vector2d> getSubBox(Box<Vector2d> box){
		Vector2d lerpMin = new Vector2d(isXMAX() ? 0.5f : 0.0f, isYMAX() ? 0.5f : 0.0f);
		Vector2d lerpMax = new Vector2d(isXMAX() ? 1.0f : 0.5f, isYMAX() ? 1.0f : 0.5f);
		return new Box<Vector2d>(box.lerp(lerpMin), box.lerp(lerpMax));
	}

	/**
	 * Fills the array childOrder with all four {@link QuadtreeChild}s in such a way 
	 * that if j > i, then childOrder[j] is farther along "ray" than childOrder[i],
	 * assuming that the children represent Boxes produced using {@link #getSubBox(Box)}
	 * applied to a box with the specified dimensions.
	 * 
	 * @param ray The direction along which to order the child boxes.
	 * @param dimensions The size of the box which was split into sub-boxes.
	 * @param childOrder An array already allocated to hold the children.
	 */
	public static void sort(Vector2d ray, Vector2d dimensions, QuadtreeChild[] childOrder){
	
		//assign a point to each child with coords e.g. x = isXMAX() ? 0 : dimensions.getX(), etc; 
		//order children by value of ray.dot(child's point).
		
		//smallest dot value has XMAX = (ray.getX() < 0), etc.
		boolean[] max = new boolean[]{ ray.getX() < 0, ray.getY() < 0 };
		
		//flipping XMAX increases the dot value by xcmp = |ray.getX()*dimensions.getX()|, etc.
		Vector2d cmp = ray.copy(); cmp.multiplyComponentWise(dimensions);
		if(cmp.getX() < 0){ cmp.setX(-cmp.getX()); }
		if(cmp.getY() < 0){ cmp.setY(-cmp.getY()); }
		
		//the values of ray.dot(child's point) are offset from the minimum value in the order:
		//0, c0, c1, c0+c1, 
		//where c0 is the index of the smaller cmp value.
		childOrder[0] = QuadtreeChild.get( max[0],  max[1]);
		if(cmp.getX() < cmp.getY()){
			childOrder[1] = QuadtreeChild.get(!max[0],  max[1]);
			childOrder[2] = QuadtreeChild.get( max[0], !max[1]);
		}else{
			childOrder[1] = QuadtreeChild.get( max[0], !max[1]);
			childOrder[2] = QuadtreeChild.get(!max[0],  max[1]);
		}
		childOrder[3] = QuadtreeChild.get(!max[0], !max[1]);
	}
}
