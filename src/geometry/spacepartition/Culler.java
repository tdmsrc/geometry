package geometry.spacepartition;

import geometry.math.FixedDimensionalVector;

/**
 * A class that implements this interface represents a region of R^n that
 * may completely, partially, or not at all contain a {@link Box}.
 * The class specifies this containment by the {@link #getBoxContainment(Box)}
 * method which returns a {@link BoxContainment}.
 * <br>
 * This is used by {@link QuadtreeNodeCullable} and {@link OctreeNodeCullable}, 
 * for traversal with culling.
 * @param <Vector> A class extending {@link FixedDimensionalVector} which indicates the
 * dimension of the {@link Box} that this Culler can test.
 * 
 * @see {@link CullerNoCull}
 * @see {@link CullerEdge}
 * @see {@link Box}
 * @see {@link Sphere}
 * @see {@link Camera}
 */
public interface Culler<Vector extends FixedDimensionalVector<Vector>>{

	/**
	 * Indicates whether the region of space occupied by a {@link Culler}
	 * contains a {@link Box} completely, partially, or not at all.
	 */
	public static enum BoxContainment{ CONTAINMENT_COMPLETE, CONTAINMENT_PARTIAL, CONTAINMENT_NONE }
	
	/**
	 * Determine whether the region represented by this class 
	 * completely, partially, or does not at all contain the
	 * specified box.
	 * <br>
	 * This method may return a greater degree of containment than is
	 * actually true, but it may not return a lesser degree of containment.
	 * 
	 * @param box The box whose containment in this region is to be checked.
	 * @return A {@link BoxContainment} indicating the degree of containment.
	 */
	public BoxContainment getBoxContainment(Box<Vector> box);
}
