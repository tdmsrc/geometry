package geometry.spacepartition;

import geometry.math.FixedDimensionalVector;

/**
 * A {@link Culler} implementation which represents all of R^n, and hence 
 * does not cause any culling while traversing a tree.
 * That is, the result of {@link #getBoxContainment} is
 * always {@link BoxContainment#CONTAINMENT_COMPLETE}.
 */
public class CullerNoCull<Vector extends FixedDimensionalVector<Vector>>
	implements Culler<Vector>{

	public CullerNoCull(){ }
	
	@Override
	public BoxContainment getBoxContainment(Box<Vector> box){
		return BoxContainment.CONTAINMENT_COMPLETE;
	}
}
