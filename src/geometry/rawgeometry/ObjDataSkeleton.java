package geometry.rawgeometry;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import geometry.common.components.Edge3d;
import geometry.common.components.Vertex3d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;

public class ObjDataSkeleton{
	
	private static ObjData constructTube(Vector3d p, Vector3d q, float tubeRadius, int tubeVerts){
		
		ObjData objData = new ObjData();
		objData.hasNormal = true;
		objData.hasTex = true;
		
		//prepare tex data
		objData.tex = new Vector2d[1];
		objData.tex[0] = new Vector2d(0,0);
		
		//prepare position and face data
		objData.position = new Vector3d[2*tubeVerts];
		objData.normal = new Vector3d[2*tubeVerts];
		objData.faces = new LinkedList<VertexData[]>();
		
		//get orthonormal frame
		Vector3d t = q.copy(); t.subtract(p);
		Vector3d n = t.getOrthogonalVector();
		Vector3d b = n.cross(t); b.normalize();
		
		//construct vertices
		int k=0;
		for(int a=0; a<tubeVerts; a++){
			float theta = (float)(2*Math.PI) * (float)a / (float)tubeVerts;
			Vector3d offset = Vector3d.linearCombination((float)Math.cos(theta), n, (float)Math.sin(theta), b);
			offset.scale(tubeRadius);
			
			Vector3d vp = p.copy(); vp.add(offset);
			objData.position[k] = vp;
			objData.normal[k] = offset;
			k++;
			
			Vector3d vq = q.copy(); vq.add(offset);
			objData.position[k] = vq;
			objData.normal[k] = offset;
			k++;
		}
		
		//construct faces
		for(int i=0; i<tubeVerts; i++){
			int j = (i+1)%tubeVerts;
			objData.faces.add(ObjData.attributeList(
				i*2  , 0, i*2  ,
				i*2+1, 0, i*2+1,
				j*2+1, 0, j*2+1, 
				j*2  , 0, j*2  ));
		}
		
		//finished
		return objData;
	}

	//creates "tube and joint" type skeleton; "edges" and "verts" can be empty or null
	public static ObjData construct(
		Collection<? extends Edge3d<? extends Vertex3d>> edges, float tubeRadius, int tubeVerts,
		Collection<? extends Vertex3d> verts, float ballRadius){
		
		//prepare list of ObjData components
		List<ObjData> skeletonComponents = new LinkedList<ObjData>();
		
		//make tubes
		if(edges != null){
		for(Edge3d<? extends Vertex3d> e : edges){
			Vector3d p = e.getInitialVertex().getPosition();
			Vector3d q = e.getFinalVertex().getPosition();
			skeletonComponents.add(ObjDataSkeleton.constructTube(p, q, tubeRadius, tubeVerts));
		}}
		
		//make joints
		Vector3d jointEllipsoidRadii = new Vector3d(ballRadius, ballRadius, ballRadius);
		if(verts != null){
		for(Vertex3d v : verts){
			skeletonComponents.add(ObjFactory.ellipsoid(v.getPosition(), jointEllipsoidRadii, 8, 6, 1, 1));
		}}
		
		//disjoint union of joints and tubes
		return new ObjData(skeletonComponents);
	}
}
