package geometry.rawgeometry;

import java.util.LinkedList;

import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;

class ObjDataGenTorus{
	
	public static ObjData construct(Vector3d center, float outerRadius, int outerVerts, float innerRadius, int innerVerts){
		
		ObjData objData = new ObjData();
		objData.hasTex = true;
		objData.hasNormal = true;
		
		int kp, kt, kn;
		objData.position = new Vector3d[innerVerts * outerVerts];
		objData.tex = new Vector2d[(innerVerts+1) * (outerVerts+1)];
		objData.normal = new Vector3d[innerVerts * outerVerts];
		
		float innerRadStep = 2*(float)Math.PI/(float)innerVerts;
		float outerRadStep = 2*(float)Math.PI/(float)outerVerts;
		
		//make lists for position and normal
		kp=0; kn=0;
		for(int i=0; i<outerVerts; i++){
			//vector pointing to point on outer circle
			Vector3d outerRadiusUnitVector = new Vector3d((float)Math.cos(i*outerRadStep), (float)Math.sin(i*outerRadStep), 0);
			Vector3d v = new Vector3d(outerRadiusUnitVector);
			v.scale(outerRadius);
			
			for(int j=0; j<innerVerts; j++){
				//vertex normal
				Vector3d n = new Vector3d(outerRadiusUnitVector);
				n.scale((float)Math.cos(j*innerRadStep));
				n.add(new Vector3d(0, 0, (float)Math.sin(j*innerRadStep)));
				
				//vertex position
				Vector3d p = new Vector3d(n);
				p.scale(innerRadius);
				p.add(v);
				p.add(center);
				
				//add to arrays
				objData.position[kp] = p; kp++;
				objData.normal[kn] = n; kn++;
			}
		}
		
		//make list for tex
		int innerTextureWraps = 1;
		int outerTextureWraps = (int)((float)innerTextureWraps*outerRadius/innerRadius);
		
		kt=0;
		for(int i=0; i<=outerVerts; i++){
		for(int j=0; j<=innerVerts; j++){
			
			Vector2d t = new Vector2d(
				(float)(outerTextureWraps*i)/(float)outerVerts, 
				(float)(innerTextureWraps*j)/(float)innerVerts
			);
			
			//add to array
			objData.tex[kt] = t; kt++;
		}}
		
		//make the faces
		objData.faces = new LinkedList<VertexData[]>();
		
		for(int i=0; i<outerVerts; i++){
		for(int j=0; j<innerVerts; j++){
			
			objData.faces.add(ObjData.attributeList(
				i*innerVerts + j, 
				i*(innerVerts+1) + j, 
				i*innerVerts + j,
				
				((i+1)%outerVerts)*innerVerts + j, 
				((i+1)%(outerVerts+1))*(innerVerts+1) + j, 
				((i+1)%outerVerts)*innerVerts + j,
				
				((i+1)%outerVerts)*innerVerts + ((j+1)%innerVerts), 
				((i+1)%(outerVerts+1))*(innerVerts+1) + ((j+1)%(innerVerts+1)), 
				((i+1)%outerVerts)*innerVerts + ((j+1)%innerVerts),
				
				i*innerVerts + ((j+1)%innerVerts), 
				i*(innerVerts+1) + ((j+1)%(innerVerts+1)), 
				i*innerVerts + ((j+1)%innerVerts)
			));
		}}
		
		return objData;
	}
}
