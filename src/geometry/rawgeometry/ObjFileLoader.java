package geometry.rawgeometry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

import geometry.common.MessageOutput;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;

public class ObjFileLoader{
	
	protected static Vector2d readObjVector2d(String[] explodedLine){
		
		if(explodedLine.length < 3){
			MessageOutput.printError("(ObjLoader) Reading Vector2d from " + explodedLine[0] + ": Not enough components (" + (explodedLine.length-1) + ")");
			return null; 
		}else if(explodedLine.length > 3){
			MessageOutput.printDebug("(ObjLoader) Reading Vector2d from " + explodedLine[0] + ": Ignoring " +  (explodedLine.length-3) + " extra component(s)");
		}
		
		return new Vector2d(
			Float.parseFloat(explodedLine[1]), 
			Float.parseFloat(explodedLine[2]));
	}
	
	protected static Vector3d readObjVector3d(String[] explodedLine, float scale){
		
		if(explodedLine.length < 4){
			MessageOutput.printError("(ObjLoader) Reading Vector3d from " + explodedLine[0] + ": Not enough components (" + (explodedLine.length-1) + ")");
			return null; 
		}else if(explodedLine.length > 4){
			MessageOutput.printDebug("(ObjLoader) Reading Vector3d from " + explodedLine[0] + ": Ignoring " +  (explodedLine.length-4) + " extra component(s)");
		}
		
		Vector3d vRead = new Vector3d(
			Float.parseFloat(explodedLine[1]), 
			Float.parseFloat(explodedLine[2]), 
			Float.parseFloat(explodedLine[3]));
		vRead.scale(scale);
		return vRead; 
	}
	
	protected static VertexData[] readObjFace(String[] explodedLine){
		
		if(explodedLine.length < 3){
			MessageOutput.printError("(ObjLoader) Reading face from " + explodedLine[0] + ": Not enough vertices (" + (explodedLine.length-1) + ")");
			return null; 
		}
		
		int nv = explodedLine.length;
		VertexData[] vd = new VertexData[nv-1];
		
		for(int i=1; i<nv; i++){
			String data[] = explodedLine[i].split("/");
			
			int vi = 1, vt = 1, vn = 1; 
			
			vi = Integer.parseInt(data[0]);
			
			if(data.length > 1){
				//this entry is optional, ignore if not an integer
				try{ vt = Integer.parseInt(data[1]); }
				catch(NumberFormatException e){ }
			}
			
			if(data.length > 2){
				//this entry is optional, ignore if not an integer
				try{ vn = Integer.parseInt(data[2]); }
				catch(NumberFormatException e){ }
			}
			
			//obj file indexes start at 1, but arrays start at 0, so subtract 1 from indices
			vd[i-1] = new VertexData(vi-1, vt-1, vn-1);
		}
		
		return vd;
	}
	
	/**
	 * Produces an ObjData from an InputStream pointing to obj file content.
	 * <br>
	 * Reads "v", "vt", "vn", and "f" fields only (no smoothing groups, material, etc).
	 * 
	 * @param is InputStream pointing to obj file content.
	 * @return ObjData holding the obj file data.
	 */
	public static ObjData readObjFileVertices(InputStream is, float scale) throws IOException{
		
		ObjData objData = new ObjData();
		objData.hasTex = false;
		objData.hasNormal = false;
		
		objData.faces = new LinkedList<VertexData[]>();
		
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		ArrayList<Vector3d> v = new ArrayList<Vector3d>();
		ArrayList<Vector2d> vt = new ArrayList<Vector2d>();
		ArrayList<Vector3d> vn = new ArrayList<Vector3d>();
		
		String line;
		while((line = br.readLine()) != null){
			
			String[] explodedLine = line.split("\\s+");
			
			if(explodedLine[0].equals("v")){
				//"v x y z"
				v.add(readObjVector3d(explodedLine, scale));
				
			}else if(explodedLine[0].equals("vt")){
				//"vt s t"
				vt.add(readObjVector2d(explodedLine));
				objData.hasTex = true;
				
			}else if(explodedLine[0].equals("vn")){
				//"vn x y z"
				vn.add(readObjVector3d(explodedLine, 1.0f));
				objData.hasNormal = true;
				
			}else if(explodedLine[0].equals("f")){
				//"f v ...", OR "f v/vt ...", OR "f v/vt/vn ...", OR "f v//vn ..."
				objData.faces.add(readObjFace(explodedLine));
			}
		}
		
		objData.position = (Vector3d[])v.toArray(new Vector3d[v.size()]);
		objData.tex = (Vector2d[])vt.toArray(new Vector2d[vt.size()]);
		objData.normal = (Vector3d[])vn.toArray(new Vector3d[vn.size()]);
		
		return objData;
	}
}
