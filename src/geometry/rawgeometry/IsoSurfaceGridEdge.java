package geometry.rawgeometry;

import java.util.ArrayList;

import geometry.math.Vector3d;

public class IsoSurfaceGridEdge{
	
	protected IsoSurfaceGridVertex p,q;
	
	protected boolean hasIsoVertex;
	protected float isoVertexLerp;
	protected int isoVertexIndex;
	
	
	public IsoSurfaceGridEdge(IsoSurfaceGridVertex p, IsoSurfaceGridVertex q){
		this.p = p;
		this.q = q;
		
		removeIsoVertex();
	}
	
	public IsoSurfaceGridVertex getInitialVertex(){ return p; }
	public IsoSurfaceGridVertex getFinalVertex(){ return q; }
	
	public boolean hasIsoVertex(){ return hasIsoVertex; }
	public float getIsoVertexLerp(){ return isoVertexLerp; }
	public int getIsoVertexIndex(){ return isoVertexIndex; }
	
	public void updateIsoVertex(ArrayList<Vector3d> objVertices){
		removeIsoVertex();
		
		if((p.getFunctionValue() > 0) != (q.getFunctionValue() > 0)){
			hasIsoVertex = true;
			isoVertexLerp = p.getFunctionValue() / (p.getFunctionValue() - q.getFunctionValue());
			isoVertexIndex = objVertices.size();
			
			objVertices.add(p.getPos().lerp(q.getPos(), isoVertexLerp));
		}
	}
	
	public void removeIsoVertex(){
		this.hasIsoVertex = false;
		this.isoVertexLerp = 0;
		this.isoVertexIndex = 0;
	}
}
