package geometry.rawgeometry;

import java.util.List;

import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;


public class IsoSurfaceGridTetrahedron{
	
	protected IsoSurfaceGridEdge e01, e02, e03, e12, e13, e23;
	protected IsoSurfaceGridVertex v[]; //IsoSurfaceGridVertex at endpoints of edges
	
	
	public IsoSurfaceGridTetrahedron(
		IsoSurfaceGridEdge e01, IsoSurfaceGridEdge e02, IsoSurfaceGridEdge e03, 
		IsoSurfaceGridEdge e12, IsoSurfaceGridEdge e13, IsoSurfaceGridEdge e23){
		
		this.e01 = e01; this.e02 = e02; this.e03 = e03;
		this.e12 = e12; this.e13 = e13; this.e23 = e23;
		
		this.v = new IsoSurfaceGridVertex[4];
		this.v[0] = e01.getInitialVertex();
		this.v[1] = e01.getFinalVertex();
		this.v[2] = e02.getFinalVertex();
		this.v[3] = e03.getFinalVertex();
	}
	
	//return the edge joining the specified tetrahedron vertices
	public IsoSurfaceGridEdge getEdge(int i, int j){
		
		if(i == 0){ 
			if(j == 1){ return e01; }
			else if(j == 2){ return e02; }
			else if(j == 3){ return e03; }
		}else if(i == 1){
			if(j == 0){ return e01; }
			else if(j == 2){ return e12; }
			else if(j == 3){ return e13; }
		}else if(i == 2){
			if(j == 0){ return e02; }
			else if(j == 1){ return e12; }
			else if(j == 3){ return e23; }
		}else if(i == 3){
			if(j == 0){ return e03; }
			else if(j == 1){ return e13; }
			else if(j == 2){ return e23; }
		}
		
		//if indices are out of bounds or are equal, return null
		return null;
	}
	
	//return an edge adjacent to a tetrahedron vertex (no specific ordering as i varies)
	public IsoSurfaceGridEdge getAdjacentEdge(int tetraIndex, int i){
		
		if(tetraIndex == 0){ 
			if(i == 0){ return e01; }
			else if(i == 1){ return e02; }
			else if(i == 2){ return e03; }
		}else if(tetraIndex == 1){
			if(i == 0){ return e01; }
			else if(i == 1){ return e12; }
			else if(i == 2){ return e13; }
		}else if(tetraIndex == 2){
			if(i == 0){ return e02; }
			else if(i == 1){ return e12; }
			else if(i == 2){ return e23; }
		}else if(tetraIndex == 3){
			if(i == 0){ return e03; }
			else if(i == 1){ return e13; }
			else if(i == 2){ return e23; }
		}
		
		//if either index is out of bounds, return null
		return null;
	}
	
	//check if p-q-r is CCW when viewed from x
	public boolean orientedCCW(Vector3d x, Vector3d p, Vector3d q, Vector3d r){
		
		Vector3d vPX = x.copy(); vPX.subtract(p);
		
		Vector3d vPQ = q.copy(); vPQ.subtract(p);
		Vector3d vPR = r.copy(); vPR.subtract(p);
		Vector3d n = vPQ.cross(vPR);
		
		return (vPX.dot(n) > 0);
	}
	
	/**
	 * For the tetrahedron vertex tetraIndex, get the isosurface vertices along the 
	 * three adjacent edges and arrange them in CCW order when viewed from the 
	 * indicated vertex (or CW order if the flag is set to false)
	 */
	public VertexData[] getIsoTriangleAtVertex(int tetraIndex, boolean ccw){
		
		int isvIndex[] = new int[3]; //isosurface vertex indices
		Vector3d isvPos[] = new Vector3d[3]; //corresponding positions
		
		//get indices and positions
		for(int i=0; i<3; i++){
			IsoSurfaceGridEdge e = getAdjacentEdge(tetraIndex, i);
			isvIndex[i] = e.getIsoVertexIndex();
			isvPos[i] = e.getInitialVertex().getPos().lerp(e.getFinalVertex().getPos(), e.getIsoVertexLerp());
		}
		
		//determine orientation from point of view of tetraIndex's position; flip if necessary
		boolean orientedCCW = orientedCCW(v[tetraIndex].getPos(), isvPos[0], isvPos[1], isvPos[2]);
		if(orientedCCW ^ ccw){ 
			int tempi = isvIndex[1];
			isvIndex[1] = isvIndex[2]; 
			isvIndex[2] = tempi;
			
			Vector3d tempv = isvPos[1];
			isvPos[1] = isvPos[2]; 
			isvPos[2] = tempv;
		}
		
		//return triangle
		return ObjData.attributeList(
			isvIndex[0],isvIndex[0],isvIndex[0], 
			isvIndex[1],isvIndex[1],isvIndex[1],
			isvIndex[2],isvIndex[2],isvIndex[2]
		);
		//return ObjData.attributeListPosOnly(isvIndex[0], isvIndex[1], isvIndex[2]);
	}
	
	/**
	 * Get the isosurface vertices along the four edges adjacent to the edge ij
	 * and arrange them into a quad in CCW order when viewed from a point on
	 * the edge ij (or CW order if the flag is set to false).
	 **/
	public VertexData[] getIsoQuadAtEdge(int i, int j, boolean ccw){
	
		//determine opposite vertices
		int k,l;
		for(k=0; k<=3; k++){ if((k != i) && (k != j)){ break; }}
		for(l=3; l>=0; l--){ if((l != i) && (l != j)){ break; }}
		
		//get edges in correct order (wrong order will produce figure-8 type quad from ij's POV)
		IsoSurfaceGridEdge e[] = new IsoSurfaceGridEdge[]{
			getEdge(i,k), getEdge(k,j), getEdge(j,l), getEdge(l,i)
		};
		
		//determine the quad's isosurface vertices
		int isvIndex[] = new int[4]; //isosurface vertex indices
		Vector3d isvPos[] = new Vector3d[4]; //corresponding positions
		
		for(int t=0; t<4; t++){
			isvIndex[t] = e[t].getIsoVertexIndex();
			isvPos[t] = e[t].getInitialVertex().getPos().lerp(e[t].getFinalVertex().getPos(), e[t].getIsoVertexLerp());
		}
		
		//ensure orientation is correct; flip if necessary
		boolean orientedCCW = orientedCCW(v[i].getPos(), isvPos[0], isvPos[1], isvPos[2]);
		if(orientedCCW ^ ccw){
			int tempi = isvIndex[1];
			isvIndex[1] = isvIndex[3]; 
			isvIndex[3] = tempi;
			
			Vector3d tempv = isvPos[1];
			isvPos[1] = isvPos[3]; 
			isvPos[3] = tempv;
		}
		
		//return quad
		return ObjData.attributeList(
			isvIndex[0],isvIndex[0],isvIndex[0], 
			isvIndex[1],isvIndex[1],isvIndex[1],
			isvIndex[2],isvIndex[2],isvIndex[2],
			isvIndex[3],isvIndex[3],isvIndex[3]
		);
		//return ObjData.attributeListPosOnly(isvIndex[0], isvIndex[1], isvIndex[2], isvIndex[3]);
	}
	
	/**
	 * Get the isosurface faces formed by the isosurface vertices along this tetrahedron's edges.
	 */
	public void getFaces(List<VertexData[]> addTo){
		
		//prepare list of VertexData[] (ObjData faces) to return
		//List<VertexData[]> ret = new LinkedList<VertexData[]>();
		
		//determine, for each vertex, whether the function value is > 0
		boolean[] b = new boolean[4];
		for(int i=0; i<4; i++){ b[i] = (v[i].getFunctionValue() > 0); }
		
		//number of vertices with function value > 0
		int n = 0;
		for(int i=0; i<4; i++){ if(b[i]){ n++; }}
		
		//nothing if all positive or all negative
		if((n == 0) || (n == 4)){ return; }
		
		//single triangle if only one positive or only one negative
		else if((n == 1) || (n == 3)){
			//find the odd vertex out
			int oddIndex=0;
			if(n == 1){ //odd one is positive
				for(int i=0; i<4; i++){ if(b[i]){ oddIndex = i; }}
			}else{ //odd one is negative
				for(int i=0; i<4; i++){ if(!b[i]){ oddIndex = i; }}
			}
			
			//add triangle
			addTo.add(getIsoTriangleAtVertex(oddIndex, b[oddIndex]));
		}
		
		//quad if two positive and two negative
		else if(n == 2){
			//find which vertices are positive
			int posIndex[] = new int[2];
			int k=0;
			for(int i=0; i<4; i++){ if(b[i]){ posIndex[k] = i; k++; }}
			
			//add quad
			addTo.add(getIsoQuadAtEdge(posIndex[0], posIndex[1], true));
		}
	}
}
