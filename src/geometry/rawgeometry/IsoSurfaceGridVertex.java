package geometry.rawgeometry;

import geometry.math.Vector3d;

public class IsoSurfaceGridVertex{

	protected float f; //function value
	protected Vector3d pos; //absolute position
	
	public IsoSurfaceGridVertex(Vector3d pos){
		this.pos = pos;
	}
	
	public void setFunctionValue(float f){ this.f = f; }
	public float getFunctionValue(){ return f; }
	
	public Vector3d getPos(){ return pos; }
}
