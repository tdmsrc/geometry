package geometry.rawgeometry;

import geometry.math.Vector2d;
import geometry.math.Vector3d;

public abstract class FunctionR2toR3{
	
	public FunctionR2toR3(){ }
	
	/**
	 * Return the function evaluated at the specified point in R^2.
	 */
	public abstract Vector3d eval(Vector2d p);
	
	/**
	 * Return the vector normalize(F_s X F_t) evaluated at the specified point in R^2.
	 */
	public Vector3d normal(Vector2d p){ return new Vector3d(); }
	
	/**
	 * Return the texture coordinate at the specified point in R^2.
	 */
	public Vector2d tex(Vector2d p){ return new Vector2d(); }
}
