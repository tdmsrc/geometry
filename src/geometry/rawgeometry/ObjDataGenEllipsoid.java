package geometry.rawgeometry;

import java.util.LinkedList;

import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;

public class ObjDataGenEllipsoid{
	
	/**
	 * Construct {@link ObjData} for an ellipsoid.
	 * 
	 * @param center The center of the ellipsoid
	 * @param radii The radii in the x, y, and z directions
	 * @param nLong The number of lines of longitude
	 * @param nLat The number of lines of latitude (excluding poles)
	 * @param thetaTextureWraps Number of times the texture repeats horizontally
	 * @param phiTextureWraps Number of times the texture repeats vertically
	 * @return The {@link ObjData}
	 */
	public static ObjData construct(Vector3d center, Vector3d radii, 
		int nLong, int nLat, int thetaTextureWraps, int phiTextureWraps){
		
		//vertex indices (total: kn+2)
		//let n = nLong, k = nLat
		//index nk: N pole, index nk+1: S pole
		//i=0...k-1: [(i+1)th "band" (line of latitude) CCW from N] j=0...n-1: index n*i+j
		//at (i,j): phi = (i+1)*phiStep, theta = j*thetaStep
		
		//faces (total: n(k-1) quads, 2n tris)
		//N flower: j=0...n-1: {nk, j, (j+1)%n}
		//S flower: j=0...n-1: {nk+1, (nk-1)-j, (nk-1)-((j+1)%n)}
		//for i = 1...k-1: j=0...n-1:  {n*i+j, n*i+(j+1)%n, n*(i-1)+(j+1)%n, n*(i-1)+j} 
		
		//texture positions sampled from n+1 x k+1 grid
		
		ObjData objData = new ObjData();
		objData.hasTex = true;
		objData.hasNormal = true;
		
		int kp, kt, kn;
		objData.position = new Vector3d[nLat*nLong+2];
		objData.tex = new Vector2d[(nLat+2)*(nLong+1)];
		objData.normal = new Vector3d[nLat*nLong+2];
		
		float thetaStep = 2*(float)Math.PI/(float)nLong;
		float phiStep = (float)Math.PI/(float)(1+nLat);
		
		//make lists for position and normal
		kp=0; kn=0;
		for(int i=0; i<nLat; i++){
		for(int j=0; j<nLong; j++){
			
			float phi = (i+1)*phiStep, theta = j*thetaStep;
			Vector3d unitSphPos = new Vector3d(
				(float)Math.cos(theta)*(float)Math.sin(phi),
				(float)Math.cos(phi),
				-(float)Math.sin(theta)*(float)Math.sin(phi));
			
			//vertex position:
			Vector3d p = unitSphPos.copy();
			p.multiplyComponentWise(radii);
			p.add(center);
			
			//normal
			Vector3d n = unitSphPos.copy();
			n.divideComponentWise(radii);
			n.normalize();
			
			//add to arrays
			objData.position[kp] = p; kp++;
			objData.normal[kn] = n; kn++;
		}}
		
		//north pole then south pole
		Vector3d pnp = new Vector3d(0,1,0); 
		pnp.multiplyComponentWise(radii); pnp.add(center);
		objData.position[kp] = pnp; kp++;
		objData.normal[kn] = new Vector3d(0,1,0); kn++;
		
		Vector3d psp = new Vector3d(0,-1,0); 
		psp.multiplyComponentWise(radii); psp.add(center);
		objData.position[kp] = psp; kp++;
		objData.normal[kn] = new Vector3d(0,-1,0); kn++;
		
		//make list for tex		
		kt=0;
		for(int i=0; i<=nLat+1; i++){
		for(int j=0; j<=nLong; j++){
			
			Vector2d t = new Vector2d(
				(float)(thetaTextureWraps*j)/(float)nLong,
				(float)(phiTextureWraps*i)/(float)(1+nLat)
			);
			
			//add to array
			objData.tex[kt] = t; kt++;
		}}
		
		//make the faces
		objData.faces = new LinkedList<VertexData[]>();
		
		//N flower
		for(int j=0; j<nLong; j++){
			objData.faces.add(ObjData.attributeList(
				nLong*nLat,
				j,
				nLong*nLat,
				
				j,
				(nLong+1)+j,
				j,
				
				(j+1)%nLong,
				(nLong+1)+(j+1),
				(j+1)%nLong
			));
		}
		
		//S flower
		for(int j=0; j<nLong; j++){
			objData.faces.add(ObjData.attributeList(
				nLong*nLat+1,
				((nLong+1)*(nLat+2)-1)-((j+1)%nLong),
				nLong*nLat+1,
				
				(nLong*nLat-1)-j,
				((nLong+1)*(nLat+1)-1)-((j+1)%nLong),
				(nLong*nLat-1)-j,
				
				(nLong*nLat-1)-((j+1)%nLong),
				((nLong+1)*(nLat+1)-2)-((j+1)%nLong),
				(nLong*nLat-1)-((j+1)%nLong)
			));
		}
		
		//horizontal bands
		for(int i=1; i<nLat; i++){
		for(int j=0; j<nLong; j++){
			objData.faces.add(ObjData.attributeList(
				nLong*i+j,
				(nLong+1)*(i+1)+j,
				nLong*i+j,
				
				nLong*i+(j+1)%nLong,
				(nLong+1)*(i+1)+(j+1),
				nLong*i+(j+1)%nLong,
				
				nLong*(i-1)+(j+1)%nLong,
				(nLong+1)*i+(j+1),
				nLong*(i-1)+(j+1)%nLong,
				
				nLong*(i-1)+j,
				(nLong+1)*i+j,
				nLong*(i-1)+j
			));
		}}
		
		return objData;
	}
}
