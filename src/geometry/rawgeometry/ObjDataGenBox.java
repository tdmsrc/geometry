package geometry.rawgeometry;

import java.util.LinkedList;

import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;

class ObjDataGenBox {

	public static ObjData construct(Vector3d center, Vector3d size){
		
		ObjData objData = new ObjData();
		objData.hasTex = true;
		objData.hasNormal = true;
		
		//vertex positions
		objData.position = new Vector3d[]{
			new Vector3d(0,0,1), new Vector3d(1,0,1), new Vector3d(1,1,1), new Vector3d(0,1,1),
			new Vector3d(0,0,0), new Vector3d(1,0,0), new Vector3d(1,1,0), new Vector3d(0,1,0)
		};
		//scale and translate
		Vector3d offsetFix = new Vector3d(0.5f, 0.5f, 0.5f);
		for(int i=0; i<8; i++){
			objData.position[i].subtract(offsetFix);
			objData.position[i].multiplyComponentWise(size);
			objData.position[i].add(center);
		}
		
		//vertex normals
		objData.normal = new Vector3d[]{
			new Vector3d(1,0,0), new Vector3d(-1,0,0),
			new Vector3d(0,1,0), new Vector3d(0,-1,0),
			new Vector3d(0,0,1), new Vector3d(0,0,-1)
		};
		
		//vertex tex coords
		objData.tex = new Vector2d[]{
			new Vector2d(0,0), new Vector2d(1,0), new Vector2d(1,1), new Vector2d(0,1)
		};
		
		//make faces
		objData.faces = new LinkedList<VertexData[]>();
		objData.faces.add(ObjData.attributeList(
			0,0,4,  1,1,4,  2,2,4,  3,3,4  //z+
		));
		objData.faces.add(ObjData.attributeList( 
			5,0,5,  4,1,5,  7,2,5,  6,3,5  //z-
		));
		objData.faces.add(ObjData.attributeList(
			1,0,0,  5,1,0,  6,2,0,  2,3,0  //x+
		));
		objData.faces.add(ObjData.attributeList(
			4,0,1,  0,1,1,  3,2,1,  7,3,1  //x-
		));
		objData.faces.add(ObjData.attributeList(
			3,0,2,  2,1,2,  6,2,2,  7,3,2  //y+
		));
		objData.faces.add(ObjData.attributeList(
			4,0,3,  5,1,3,  1,2,3,  0,3,3  //y-
		));
		
		return objData;
	}
}
