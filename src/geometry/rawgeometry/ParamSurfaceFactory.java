package geometry.rawgeometry;

import java.util.LinkedList;

import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;
import geometry.spacepartition.Box;

//TODO: make a version which can produce normal and/or texture data

public class ParamSurfaceFactory{
	
	protected FunctionR2toR3 f;
	protected Box<Vector2d> bounds;
	protected boolean closedS, closedT;
	
	protected boolean flipDiagonal; //split square into triangles in one way or the other
	
	
	public ParamSurfaceFactory(FunctionR2toR3 f, Box<Vector2d> bounds, 
		boolean closedS, boolean closedT){
		
		this.f = f;
		this.bounds = bounds;
		
		this.closedS = closedS;
		this.closedT = closedT;
		
		this.flipDiagonal = false;
	}
	
	//flip the way squares are split into triangles
	public void flipDiagonal(){ flipDiagonal = !flipDiagonal; }
	
	public ObjData make(int samplesS, int samplesT, boolean writeNormalData, boolean writeTextureData){
		
		//prepare ObjData
		ObjData ret = new ObjData();
		ret.hasNormal = writeNormalData;
		ret.hasTex = writeTextureData;
		
		//compute positions
		int nv = samplesS * samplesT;
		ret.position = new Vector3d[nv];
		if(writeNormalData){ ret.normal = new Vector3d[nv]; }
		if(writeTextureData){ ret.tex = new Vector2d[nv]; }
		
		float iMax = (float)(samplesS + (closedS ? 0 : -1));
		float jMax = (float)(samplesT + (closedT ? 0 : -1));
		
		int k=0;
		Vector2d p = new Vector2d();
		for(int i=0; i<samplesS; i++){	p.setX((float)i / iMax);
		for(int j=0; j<samplesT; j++){	p.setY((float)j / jMax);
			Vector2d x = bounds.lerp(p);
			ret.position[k] = f.eval(x);
			if(writeNormalData){ ret.normal[k] = f.normal(x); }
			if(writeTextureData){ ret.tex[k] = f.tex(x); }
			k++;
		}}
		
		//compute faces
		ret.faces = new LinkedList<VertexData[]>();
		
		int facesS = samplesS + (closedS ? 0 : -1);
		int facesT = samplesT + (closedT ? 0 : -1);
		
		for(int i=0; i<facesS; i++){
		for(int j=0; j<facesT; j++){
			int ni = (i+1) % samplesS;
			int nj = (j+1) % samplesT;
			
			int k00 = j + i*samplesT; 
			int k10 = j + ni*samplesT;
			int k11 = nj + ni*samplesT;
			int k01 = nj + i*samplesT;
			
			if(flipDiagonal){
				ret.faces.add(ObjData.attributeList(k01,k01,k01, k00,k00,k00, k10,k10,k10));
				ret.faces.add(ObjData.attributeList(k10,k10,k10, k11,k11,k11, k01,k01,k01));
			}else{
				ret.faces.add(ObjData.attributeList(k00,k00,k00, k10,k10,k10, k11,k11,k11));
				ret.faces.add(ObjData.attributeList(k11,k11,k11, k01,k01,k01, k00,k00,k00));
			}
		}}
		
		//return ObjData
		return ret;
	}
}
