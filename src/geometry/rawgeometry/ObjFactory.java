package geometry.rawgeometry;

import java.util.Collection;

import geometry.common.components.Edge3d;
import geometry.common.components.Vertex3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronFactory;

/**
 * ObjData constructed using ObjFactory should meet all conditions necessary to
 * perform CSG operations on a Polyhedron constructed from the ObjData; also,
 * all faces will be convex (although this <i>need not</i> necessarily be true
 * for the ObjData to be loaded into a Polyhedron and used for CSG; see 
 * {@link PolyhedronFactory#makeFromObjData(ObjData)}).
 */
public class ObjFactory{

	//create a box
	public static ObjData box(Vector3d center, Vector3d size){
		return ObjDataGenBox.construct(center, size);
	}
	
	/**
	 * @see {@link ObjDataGenEllipsoid#construct(Vector3d, Vector3d, int, int, int, int)}
	 */
	public static ObjData ellipsoid(Vector3d center, Vector3d radii, 
		int nLong, int nLat, int thetaTextureWraps, int phiTextureWraps){
		
		return ObjDataGenEllipsoid.construct(center, radii, nLong, nLat, thetaTextureWraps, phiTextureWraps);
	}
	
	//create a torus
	public static ObjData torus(Vector3d center, float outerRadius, int outerVerts, float innerRadius, int innerVerts){
		return ObjDataGenTorus.construct(center, outerRadius, outerVerts, innerRadius, innerVerts);
	}
	
	//create a tubular skeleton from a list of edges
	public static ObjData skeleton(
		Collection<? extends Edge3d<? extends Vertex3d>> edges, float tubeRadius, int tubeVerts,
		Collection<? extends Vertex3d> verts, float ballRadius){
		
		return ObjDataSkeleton.construct(edges, tubeRadius, tubeVerts, verts, ballRadius);
	}
}
