package geometry.rawgeometry;

import geometry.math.Vector2d;
import geometry.math.Vector3d;

public abstract class FunctionR3toR{
	
	public FunctionR3toR(){ }
	
	/**
	 * Return the function evaluated at the specified point in R^3.
	 */
	public abstract float eval(Vector3d p);
	
	/**
	 * Return the normalized gradient at the specified point in R^3.
	 */
	public Vector3d normal(Vector3d p){ return new Vector3d(); }
	
	/**
	 * Return the texture coordinate at the specified point in R^3.
	 */
	public Vector2d tex(Vector3d p){ return new Vector2d(); }
}
