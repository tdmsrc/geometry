package geometry.rawgeometry;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import geometry.math.Vector2d;
import geometry.math.Vector3d;

//similar to obj file format data; used in ObjFactory, PolyhedronFactory, etc...

/**
 * Mimics the obj file format.  Consists of arrays of vertex data:
 * <ul>
 * <li>A {@link Vector3d} array {@link #position} of vertex positions,
 * <li>(optional) A {@link Vector2d} array {@link #tex} of texture coordinates,
 * <li>(optional) A {@link Vector3d} array {@link #normal} of normal vectors.
 * </ul>
 * The geometry is a {@link List} of faces, specified as arrays of {@link VertexData} objects.
 * Faces can be created using the convenience methods
 * {@link ObjData#attributeListPosOnly(int...)} or {@link ObjData#attributeList(int...)}. 
 */
public class ObjData{
	
	public List<VertexData[]> faces;
	public Vector3d[] position;
	
	//optional
	public boolean hasTex, hasNormal;
	public Vector2d[] tex;
	public Vector3d[] normal;
	
	/**
	 * Create an empty geometry.  Does not initialize {@link ObjData#faces}.
	 */
	public ObjData(){ }
	
	/**
	 * Create a disjoint union of ObjDatas.  
	 * Does not actually copy any Vector data, only copies the reference.
	 */
	public ObjData(Collection<ObjData> components){
		
		//get total size of arrays
		int np=0, nt=0, nn=0;
		for(ObjData c : components){
			np += ((c.position != null) ? c.position.length : 0);
			nt += ((c.tex != null) ? c.tex.length : 0);
			nn += ((c.normal != null) ? c.normal.length : 0);
		}
		
		//prepare arrays
		faces = new LinkedList<VertexData[]>();
		
		hasTex = true;
		hasNormal = true;
		
		position = new Vector3d[np];
		tex = new Vector2d[nt];
		normal = new Vector3d[nn];
		
		//copy data for each component
		int kp=0, kt=0, kn=0;
		for(ObjData c : components){
			//add face with shifted indices
			for(VertexData[] face : c.faces){
				int nf = face.length;
				VertexData[] faceShifted = new VertexData[nf];
				for(int i=0; i<nf; i++){
					faceShifted[i] = new VertexData(kp+face[i].posIndex, kt+face[i].texIndex, kn+face[i].normalIndex);
				}
				faces.add(faceShifted);
			}
			
			//add refs to pos, tex, normal
			if(c.position != null){
				for(int i=0; i<c.position.length; i++){ position[kp+i] = c.position[i]; }
				kp += c.position.length;
			}
			
			if(c.tex != null){
				for(int i=0; i<c.tex.length; i++){ tex[kt+i] = c.tex[i]; }
				kt += c.tex.length;
			}
			
			if(c.normal != null){
				for(int i=0; i<c.normal.length; i++){ normal[kn+i] = c.normal[i]; }
				kn += c.normal.length;
			}
		}
	}
	
	/**
	 * Indicates the position, texture coordinate, and normal vector of a vertex by 
	 * referring to the index of the attribute in the corresponding {@link ObjData}
	 * attribute array.
	 */
	public static class VertexData{
		public int posIndex, texIndex, normalIndex;
		
		/**
		 * Create a {@link VertexData} with position, texture coordinate, and normal vector attributes.
		 * @param posIndex The index of the position in {@link ObjData#position}
		 * @param texIndex The index of the texture coordinate in {@link ObjData#tex}
		 * @param normalIndex The index of the normal vector in {@link ObjData#normal}
		 */
		public VertexData(int posIndex, int texIndex, int normalIndex){
			this.posIndex = posIndex;
			this.texIndex = texIndex;
			this.normalIndex = normalIndex;
		}
		
		/**
		 * Create a {@link VertexData} with only a position attribute.
		 * @param posIndex The index of the position in {@link ObjData#position}
		 */
		public VertexData(int posIndex){ this.posIndex = posIndex; }
		
		public void setTexIndex(int texIndex){ this.texIndex = texIndex; }
		public void setNormalIndex(int normalIndex){ this.normalIndex = normalIndex; }
	}
	
	/**
	 * Return an array of {@link VertexData} objects (i.e., a face) given a list 
	 * of attribute indices (i.e., the index of the attribute in {@link ObjData#position}, 
	 * {@link ObjData#tex}, or {@link ObjData#normal}).
	 * 
	 * @param args Attribute indices in the order: pos, tex, normal, pos, tex, normal, ... 
	 */
	public static VertexData[] attributeList(int...args){
		
		int n = args.length / 3;
		VertexData[] ret = new VertexData[n];
		
		int k=0;
		for(int i=0; i<n; i++){
			ret[i] = new VertexData(args[k], args[k+1], args[k+2]);
			k+=3;
		}
		
		return ret;
	}
	
	/**
	 * Return an array of {@link VertexData} objects (i.e., a face) given a list 
	 * of position attribute indices (i.e., the index in {@link ObjData#position}).
	 * 
	 * @param args List of position attribute indices. 
	 */
	public static VertexData[] attributeListPosOnly(int...args){
		
		int n = args.length;
		VertexData[] ret = new VertexData[n];
		
		for(int i=0; i<n; i++){
			ret[i] = new VertexData(args[i]);
		}
		
		return ret;
	}
}
