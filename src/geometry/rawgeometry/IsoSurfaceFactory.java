package geometry.rawgeometry;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import geometry.common.ProgressListener;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData.VertexData;
import geometry.spacepartition.Box;

//TODO: make a version which can produce normal/texture data

public class IsoSurfaceFactory{
	
	protected static final int PROGRESS_UPDATE_PERCENT_STEP = 5;
	
	protected FunctionR3toR f;
	protected Box<Vector3d> bounds;

	public IsoSurfaceFactory(FunctionR3toR f, Box<Vector3d> bounds){
		
		this.f = f;
		this.bounds = bounds;
	}
	
	public ObjData make(float isoValue, int samplesX, int samplesY, int samplesZ, 
		boolean writeNormalData, boolean writeTextureData, ProgressListener progressListener){
		
		//clamp sample counts to [2,inf)
		if(samplesX < 2){ samplesX = 2; }
		if(samplesY < 2){ samplesY = 2; }
		if(samplesZ < 2){ samplesZ = 2; }
		
		//prepare ObjData
		ObjData ret = new ObjData();
		ret.hasNormal = writeNormalData;
		ret.hasTex = writeTextureData;
		
		//create grid vertices with function values attached
		IsoSurfaceGridVertex[][][] gv = new IsoSurfaceGridVertex[samplesX][samplesY][samplesZ];
		Vector3d lerp = new Vector3d(0,0,0);
		
		float lerpStepX = 1.0f / (float)(samplesX-1);
		float lerpStepY = 1.0f / (float)(samplesY-1);
		float lerpStepZ = 1.0f / (float)(samplesZ-1);
		
		//used to keep track of progress
		progressListener.startEvent("Computing grid values", samplesX*samplesY*samplesZ);
		
		for(int ix=0; ix<samplesX; ix++){ lerp.setX(ix*lerpStepX);
		for(int iy=0; iy<samplesY; iy++){ lerp.setY(iy*lerpStepY);
		for(int iz=0; iz<samplesZ; iz++){ lerp.setZ(iz*lerpStepZ);
			Vector3d pos = bounds.lerp(lerp);
			
			IsoSurfaceGridVertex v = new IsoSurfaceGridVertex(pos);
			v.setFunctionValue(f.eval(pos) - isoValue);
			gv[ix][iy][iz] = v; 
			
			//update progress
			progressListener.tickEvent();
		}}}
		
		progressListener.endEvent();
		
		//prepare isosurface vertex arraylist
		ArrayList<Vector3d> objVertices = new ArrayList<Vector3d>();
		
		//If the grid cubes' vertices are indexed in the following way:
		//
		//   3-----7    
		//  /|    /|                     
		// / 2---/-6    z+               these tetrahedra      these are obtained from
		// 1-----5 /     | / y+          split the prism with  the first 3 by replacing
		// |/    |/      |/              edges 02, 46, 57      each with its "antipodal"
		// 0-----4       o--- x+                  |                   |
		//                                        v                   v
		//then we want these tetrahedra: [2670, 6704, 7045], [5107, 1073, 0732]
		
		//create x-aligned grid edges and find isosurface vertices
		IsoSurfaceGridEdge[][][] geX = new IsoSurfaceGridEdge[samplesX-1][samplesY][samplesZ]; 
		for(int ix=0; ix<samplesX-1; ix++){
		for(int iy=0; iy<samplesY; iy++){
		for(int iz=0; iz<samplesZ; iz++){
			IsoSurfaceGridEdge e = new IsoSurfaceGridEdge(gv[ix][iy][iz], gv[ix+1][iy][iz]);
			e.updateIsoVertex(objVertices);
			geX[ix][iy][iz] = e;
		}}}
		
		//create y-aligned grid edges and find isosurface vertices
		IsoSurfaceGridEdge[][][] geY = new IsoSurfaceGridEdge[samplesX][samplesY-1][samplesZ];
		for(int ix=0; ix<samplesX; ix++){
		for(int iy=0; iy<samplesY-1; iy++){
		for(int iz=0; iz<samplesZ; iz++){
			IsoSurfaceGridEdge e = new IsoSurfaceGridEdge(gv[ix][iy][iz], gv[ix][iy+1][iz]);
			e.updateIsoVertex(objVertices);
			geY[ix][iy][iz] = e;
		}}}
		
		//create z-aligned grid edges and find isosurface vertices
		IsoSurfaceGridEdge[][][] geZ = new IsoSurfaceGridEdge[samplesX][samplesY][samplesZ-1];
		for(int ix=0; ix<samplesX; ix++){
		for(int iy=0; iy<samplesY; iy++){
		for(int iz=0; iz<samplesZ-1; iz++){
			IsoSurfaceGridEdge e = new IsoSurfaceGridEdge(gv[ix][iy][iz], gv[ix][iy][iz+1]);
			e.updateIsoVertex(objVertices);
			geZ[ix][iy][iz] = e;
		}}}
		
		//create yz-aligned edges
		IsoSurfaceGridEdge[][][] geYZ = new IsoSurfaceGridEdge[samplesX][samplesY-1][samplesZ-1];
		for(int ix=0; ix<samplesX; ix++){
		for(int iy=0; iy<samplesY-1; iy++){
		for(int iz=0; iz<samplesZ-1; iz++){
			IsoSurfaceGridEdge e = new IsoSurfaceGridEdge(gv[ix][iy][iz], gv[ix][iy+1][iz+1]);
			e.updateIsoVertex(objVertices);
			geYZ[ix][iy][iz] = e;
		}}}
		
		//create xz-aligned edges
		IsoSurfaceGridEdge[][][] geXZ = new IsoSurfaceGridEdge[samplesX-1][samplesY][samplesZ-1];
		for(int ix=0; ix<samplesX-1; ix++){
		for(int iy=0; iy<samplesY; iy++){
		for(int iz=0; iz<samplesZ-1; iz++){
			IsoSurfaceGridEdge e = new IsoSurfaceGridEdge(gv[ix][iy][iz], gv[ix+1][iy][iz+1]);
			e.updateIsoVertex(objVertices);
			geXZ[ix][iy][iz] = e;
		}}}
		
		//create xy-aligned edges
		IsoSurfaceGridEdge[][][] geXY = new IsoSurfaceGridEdge[samplesX-1][samplesY-1][samplesZ];
		for(int ix=0; ix<samplesX-1; ix++){
		for(int iy=0; iy<samplesY-1; iy++){
		for(int iz=0; iz<samplesZ; iz++){
			IsoSurfaceGridEdge e = new IsoSurfaceGridEdge(gv[ix][iy][iz], gv[ix+1][iy+1][iz]);
			e.updateIsoVertex(objVertices);
			geXY[ix][iy][iz] = e;
		}}}
		
		//create cube-diagonal edges
		IsoSurfaceGridEdge[][][] geXYZ = new IsoSurfaceGridEdge[samplesX-1][samplesY-1][samplesZ-1];
		for(int ix=0; ix<samplesX-1; ix++){
		for(int iy=0; iy<samplesY-1; iy++){
		for(int iz=0; iz<samplesZ-1; iz++){
			IsoSurfaceGridEdge e = new IsoSurfaceGridEdge(gv[ix][iy][iz], gv[ix+1][iy+1][iz+1]);
			e.updateIsoVertex(objVertices);
			geXYZ[ix][iy][iz] = e;
		}}}
		
		//create 6 grid tetrahedra for each subcube
		List<IsoSurfaceGridTetrahedron> gc = new LinkedList<IsoSurfaceGridTetrahedron>();
		for(int ix=0; ix<samplesX-1; ix++){
		for(int iy=0; iy<samplesY-1; iy++){
		for(int iz=0; iz<samplesZ-1; iz++){
			//the comments refer to the cube vertex indices above
			//tetrahedron vertex indices are chosen so that the edge orientations are always correct
			
			//vertices 2670 > edges 02 06 07 26 27 67
			gc.add(new IsoSurfaceGridTetrahedron(
				geY[ix][iy][iz], geXY[ix][iy][iz], geXYZ[ix][iy][iz],
				geX[ix][iy+1][iz], geXZ[ix][iy+1][iz], geZ[ix+1][iy+1][iz]));
			
			//vertices 6704 > edges 04 06 07 46 47 67
			gc.add(new IsoSurfaceGridTetrahedron(
				geX[ix][iy][iz], geXY[ix][iy][iz], geXYZ[ix][iy][iz],
				geY[ix+1][iy][iz], geYZ[ix+1][iy][iz], geZ[ix+1][iy+1][iz]));
			
			//vertices 7045 > edges 04 05 07 45 47 57
			gc.add(new IsoSurfaceGridTetrahedron(
				geX[ix][iy][iz], geXZ[ix][iy][iz], geXYZ[ix][iy][iz],
				geZ[ix+1][iy][iz], geYZ[ix+1][iy][iz], geY[ix+1][iy][iz+1]));
			
			//vertices 5107 > edges 01 05 07 15 17 57
			gc.add(new IsoSurfaceGridTetrahedron(
				geZ[ix][iy][iz], geXZ[ix][iy][iz], geXYZ[ix][iy][iz],
				geX[ix][iy][iz+1], geXY[ix][iy][iz+1], geY[ix+1][iy][iz+1]));
			
			//vertices 1073 > edges 01 03 07 13 17 37
			gc.add(new IsoSurfaceGridTetrahedron(
				geZ[ix][iy][iz], geYZ[ix][iy][iz], geXYZ[ix][iy][iz],
				geY[ix][iy][iz+1], geXY[ix][iy][iz+1], geX[ix][iy+1][iz+1]));
			
			//vertices 0732 > edges 02 03 07 23 27 37
			gc.add(new IsoSurfaceGridTetrahedron(
				geY[ix][iy][iz], geYZ[ix][iy][iz], geXYZ[ix][iy][iz],
				geZ[ix][iy+1][iz], geXZ[ix][iy+1][iz], geX[ix][iy+1][iz+1]));
		}}}
		
		//update ObjData vertex list
		int nv = objVertices.size();
		
		ret.position = new Vector3d[nv];
		ret.position = objVertices.toArray(ret.position);
		
		if(writeNormalData){
			ret.normal = new Vector3d[nv];
			
			progressListener.startEvent("Computing normals", nv);
			for(int i=0; i<nv; i++){ 
				ret.normal[i] = f.normal(ret.position[i]);
				progressListener.tickEvent();
			}
			progressListener.endEvent();
		}
		if(writeTextureData){
			ret.tex = new Vector2d[nv];
			
			progressListener.startEvent("Computing texture coordinates", nv);
			for(int i=0; i<nv; i++){ 
				ret.tex[i] = f.tex(ret.position[i]); 
				progressListener.tickEvent();
			}
			progressListener.endEvent();
		}
		
		//loop through cubes to get faces
		ret.faces = new LinkedList<VertexData[]>();
		for(IsoSurfaceGridTetrahedron c : gc){ c.getFaces(ret.faces); }
		
		//return the ObjData
		return ret;
	}
}
