package geometry.math;

import java.util.EnumMap;

import geometry.math.CameraRotation.CameraRotationListener;
import geometry.spacepartition.Box;
import geometry.spacepartition.Culler;

/*
 * The 4 rays at the corner of the screen (based at [cam pos]) are:
 *   Lower-left:	f - t*u - at*s
 *   Lower-right:	f - t*u + at*s
 *   Upper-left:	f + t*u - at*s
 *   Upper-right:	f + t*u + at*s
 * Where
 *   a = aspect, t = tan(fovy/2), f = [cam fwd]; u,s are as in Matrix4d.gluLookAt:
 *   (assume [cam fwd] and [cam up] are normalized)
 *   s = normalize([cam fwd] x [cam up])
 *   u = s x f.
 *  It follows that the 4 planes making up the sides of the camera frustum
 *  pass through [cam pos] and have normals:
 *    top: 		tf-u = sin*f - cos*u
 *    bottom: 	tf+u = sin*f + cos*u
 *    left:		atf+s = a*sin*f + cos*s 
 *    right:	atf-s = a*sin*f - cos*s 
 *  Where
 *    sin = sin(fovy/2), cos = cos(fovy/2)
 */

/**
 * Represents a camera frustum.  Data consists of:
 * <ul>
 * <li>A {@link CameraRotation}.
 * <li>A {@link Vector3d} specifying the Camera position.
 * <li>Values for fovy, screen aspect ratio, and z-clip planes, for the perspective transformation.
 * </ul>
 * Implements {@link Culler}<{@link Vector3d}> by checking if a 
 * {@link Box}<{@link Vector3d}> is contained in the camera frustum.
 */
public class Camera implements Culler<Vector3d>, CameraRotationListener{
	
	private static final float DEG_TO_RAD = (float)(Math.PI / 180.0);

	//independent variables, which can be set by user
	private Vector3d position;
	private CameraRotation rotation;
	private float fovy, aspect, zNear, zFar;
	
	//variables automatically derived from free variables
	private Matrix4d projectionMatrix;

	/**
	 * Describes one of the "side planes" forming the Camera frustum.
	 * 
	 * @see {@link Camera#getFrustumPlaneNormal(FrustumPlaneNormal)}
	 */
	public static enum FrustumPlaneNormal{ TOP, BOTTOM, LEFT, RIGHT }
	private EnumMap<FrustumPlaneNormal,Vector3d> frustumPlaneNormals;
	
	
	/**
	 * Create a Camera with frustum given by the specified parameters.
	 * <br><br>
	 * Sets the default values:<br>
	 * position = (0,0,0), forward = (0,0,-1), up = (0,1,0).
	 * 
	 * @param fovy The angle (in degrees) formed between the planes passing through the
	 * camera position and the top and bottom edges of the screen.
	 * @param aspect The ratio (screen width)/(screen height).
	 * @param zNear The distance of the near clipping plane from the camera position.
	 * @param zFar The distance of the far clipping plane from the camera position.
	 */
	public Camera(float fovy, float aspect, float zNear, float zFar){
		
		this.fovy = fovy;
		this.zNear = zNear;
		this.zFar = zFar;
		this.aspect = aspect;
		
		this.position = new Vector3d(0, 0, 0);
		
		this.rotation = new CameraRotation();
		rotation.addListener(this);
		
		//initialize frustum plane normal storage
		frustumPlaneNormals = new EnumMap<FrustumPlaneNormal,Vector3d>(FrustumPlaneNormal.class);
		
		//initialize matrix
		projectionMatrix = new Matrix4d(); 
		updateProjectionMatrix();
		
		//compute frustum plane normals
		updateFrustumData();
	}
	
	/**
	 * Copy constructor.
	 */
	public Camera(Camera rhs){
		
		this.fovy = rhs.fovy;
		this.aspect = rhs.aspect;
		this.zNear = rhs.zNear;
		this.zFar = rhs.zFar;
		
		this.position = rhs.position.copy();
		
		this.rotation = rhs.rotation.copy();
		rotation.addListener(this);
		
		//initialize frustum plane normal storage
		frustumPlaneNormals = new EnumMap<FrustumPlaneNormal,Vector3d>(FrustumPlaneNormal.class);
		
		//initialize matrix
		projectionMatrix = rhs.projectionMatrix.copy();
		
		//compute frustum plane normals
		updateFrustumData();
	}
	
	/**
	 * Create a copy of this Camera.
	 */
	public Camera copy(){
		return new Camera(this);
	}
	
	//============================================
	// ACCESSORS AND SETTERS
	//============================================
	
	//accessors for "free" variables
	/**
	 * Get the FOV.  This is the angle (in degrees) formed between the planes passing 
	 * through the camera position and the top and bottom edges of the screen.
	 */
	public float getFOVy(){ return fovy; }
	
	/**
	 * Set the FOV.  This is the angle (in degrees) formed between the planes passing 
	 * through the camera position and the top and bottom edges of the screen.
	 */
	public void setFOVy(float fovy){
		this.fovy = fovy; 
		
		updateProjectionMatrix();
		updateFrustumData();
	}
	
	/**
	 * Get the aspect ratio: (screen width)/(screen height).
	 */
	public float getAspect(){ return aspect; }
	
	/**
	 * Set the aspect ratio: (screen width)/(screen height).
	 */
	public void setAspect(float aspect){
		this.aspect = aspect;
		
		updateProjectionMatrix();
		updateFrustumData();
	}
	
	/**
	 * Get the distance of the near clipping plane from the camera position.
	 */
	public float getZNear(){ return zNear; }
	
	/**
	 * Get the distance of the far clipping plane from the camera position.
	 */
	public float getZFar(){ return zFar; }
	
	/**
	 * Set the distances of the near and far clipping planes from the camera position.
	 */
	public void setZClippingPlanes(float zNear, float zFar){
		this.zNear = zNear;
		this.zFar = zFar;
		
		updateProjectionMatrix();
		updateFrustumData();
	}
	
	/**
	 * Get the camera position.<br>
	 * Default value is (0,0,0).
	 */
	public Vector3d getPosition(){ return position; }

	/**
	 * Get the camera rotation.
	 */
	public CameraRotation getRotation(){ return rotation; }
	
	@Override
	public void cameraRotationChanged(){ updateFrustumData(); }
	
	
	//accessors for variables derived from free variables
	/**
	 * Returns Matrix4d.gluPerspective(fovy, aspect, zNear, zFar).
	 * 
	 * @see {@link Matrix4d#gluPerspective(float, float, float, float)}
	 */
	public Matrix4d getProjectionMatrix(){
		return projectionMatrix; 
	}
	
	/**
	 * Return the normal to one of the planes making up the Camera frustum.
	 * 
	 * @param type The plane making up the Camera frustum whose normal is to be returned.
	 * @return The normal to the specified plane.
	 */
	public Vector3d getFrustumPlaneNormal(FrustumPlaneNormal type){
		return frustumPlaneNormals.get(type);
	}
	
	/**
	 * Applies a projective transformation mapping the Camera frustum to the cube [-1,1]^3
	 * (i.e., from object coordinates to OpenGL-style clip coordinates).
	 * <br>
	 * The screen position of the point is given by dropping the z coordinate and applying
	 * an affine linear transformation from [-1,1]^2 to the screen rectangle. 
	 * 
	 * @see {@link #getScreenCoordsFromObjectCoords(int, int, Vector3d)}
	 * @see {@link #getRayFromScreenCoords(int, int, Vector2d)} 
	 */
	public Vector3d getClipCoordsFromObjectCoords(Vector3d objCoords){
		
		//apply modelview, then projection, then scale to viewport
		Vector3d trans = objCoords.copy(); trans.subtract(position);
		trans = rotation.getMatrix().apply(trans);
		trans = projectionMatrix.applyProjective(trans, 1.0f);
		return trans;
	}
	
	/**
	 * Applies the projective transformation from getClipCoordsFromObjectCoords, then
	 * maps the (x,y) position from [-1,1]^2 to the specified screen rectangle.
	 * <br><br>
	 * If the returned point is passed to getRayFromScreenCoords, the resulting ray
	 * should contain the point objectCoords. 
	 * <br><br>
	 * Set flipY true if the screen's coordinate system starts from the upper-left;
	 * if it starts from the lower-left, then set flipY to false.
	 * 
	 * @see {@link #getClipCoordsFromObjectCoords(Vector3d)}
	 * @see {@link #getRayFromScreenCoords(int, int, Vector2d)}
	 */
	public Vector2d getScreenCoordsFromObjectCoords(float screenWidth, float screenHeight, boolean flipY, Vector3d objectCoords){
		
		Vector3d clipCoords = getClipCoordsFromObjectCoords(objectCoords);
		
		float sx = screenWidth  * ((1.0f + clipCoords.getX()) * 0.5f);
		float sy = flipY ? 
			screenHeight * (1.0f - clipCoords.getY()) * 0.5f:
			screenHeight * (1.0f + clipCoords.getY()) * 0.5f;
		
		return new Vector2d(sx, sy);
	}
	
	/**
	 * Returns a ray, in ambient (object) coordinates, given a point on the screen
	 * (i.e., for the purpose of picking).
	 * <br><br>
	 * Any point on the returned ray will return the specified screen point when
	 * passed to getScreenCoordsFromObjectCoords.
	 * <br><br>
	 * Set flipY true if the screen's coordinate system starts from the upper-left;
	 * if it starts from the lower-left, then set flipY to false.
	 * 
	 * @return The returned ray starts at the camera position and is in the direction
	 * of the returned vector.  The returned vector is always normalized.
	 * 
	 * @see {@link #getClipCoordsFromObjectCoords(Vector3d)}
	 * @see {@link #getScreenCoordsFromObjectCoords(int, int, Vector3d)}
	 */
	public Vector3d getRayFromScreenCoords(int screenWidth, int screenHeight, boolean flipY, int screenX, int screenY){
		
		//clip x and y for screen position
		float clipX = -1.0f + (2.0f * screenX) / (float)screenWidth;
		float clipY = flipY ? 
			 1.0f - (2.0f * screenY) / (float)screenHeight:
			-1.0f + (2.0f * screenY) / (float)screenHeight;
		
		//tan(fovy/2); side and screen-up vectors
		float tan = (float)Math.tan(DEG_TO_RAD * fovy * 0.5f);
		
		Vector3d s = rotation.getSide(); //right-pointing vector
		Vector3d u = rotation.getTiltedUp(); //upward-pointing vector (orth to fwd, s)
		
		//compute ray
		Vector3d ret = Vector3d.linearCombination(aspect*tan*clipX, s, tan*clipY, u);
		ret.add(rotation.getForward());
		ret.normalize();
		return ret;
	}
	
	//============================================
	// UPDATE METHODS
	//============================================

	/**
	 * Recompute the projection matrix.
	 */
	private void updateProjectionMatrix(){
		
		projectionMatrix.setAsGluPerspective(fovy, aspect, zNear, zFar);
	}
	
	/**
	 * Recompute the frustum plane normals when they are requested and invalid.
	 */
	private void updateFrustumData(){
		
		float tan = (float)Math.tan(DEG_TO_RAD * fovy * 0.5f);
		
		Vector3d f = rotation.getForward(); //forward-pointing vector
		Vector3d s = rotation.getSide(); //right-pointing vector
		Vector3d u = rotation.getTiltedUp(); //upward-pointing vector (orth to fwd, s)
		
		Vector3d temp;

		temp = Vector3d.linearCombination(tan, f, -1.0f, u); 
		frustumPlaneNormals.put(FrustumPlaneNormal.TOP, temp);
		
		temp = Vector3d.linearCombination(tan, f,  1.0f, u);
		frustumPlaneNormals.put(FrustumPlaneNormal.BOTTOM, temp);
		
		temp = Vector3d.linearCombination(aspect*tan, f,  1.0f, s);
		frustumPlaneNormals.put(FrustumPlaneNormal.LEFT, temp);
		
		temp = Vector3d.linearCombination(aspect*tan, f, -1.0f, s);
		frustumPlaneNormals.put(FrustumPlaneNormal.RIGHT, temp);
	}
	
	//============================================
	// MOVEMENT
	//============================================
	
	/**
	 * Set the position of the viewer.
	 * 
	 * @param position The new position of the viewer.
	 */
	public void setPosition(Vector3d position){
		
		this.position = position;
	}
	
	/**
	 * Add the vector "v" to the position of the viewer.
	 *  
	 * @param v The vector to add to the position of the viewer.
	 */
	public void addToPosition(Vector3d v){
		
		position.add(v);
	}
	
	/**
	 * Set Camera's forward vector to be from "position" to "lookTarget"
	 */
	public void lookAt(Vector3d lookTarget){
		
		Vector3d lookDir = lookTarget.copy();
		lookDir.subtract(position);
		rotation.setForward(lookDir);
	}
	
	
	//============================================
	// FRUSTUM CULLING
	//============================================
	
	@Override
	public BoxContainment getBoxContainment(Box<Vector3d> box){
		
		//Fast method:
		//-----------------------------------------------------------
		//For each camera frustum plane (6 total), check* if
		// (i)  all box vertices are on wrong side of plane (=> return CONTAINMENT_NONE),
		// (ii) all box vertices are on correct side of plane.
		//If (ii) occurs for all planes, then CONTAINMENT_COMPLETE
		//Else, if it hasn't returned yet, there was a plane with vertices on both sides.
		//In this case, return CONTAINMENT_PARTIAL (although the box *may* be NOT_VISIBLE)
		//
		//*: To check if all box verts are on one side of P = plane(normal n, point x),
		//it is enough to check 2 vertices--the ones with min and max value of dot(v-x,n).
		//Points v-x have ith coord (min_i - x_i + s_i w_i), where w_i is max_i - min_i, s_i = 0 or 1.
		//Dotting with n, we have ([const] + \sum_i s_i w_i n_i), which is max when 
		//s_i = 1 if n_i > 0, and s_i = 0 if n_i < 0; min when s_i's are opposite that.
		//-----------------------------------------------------------
		
		boolean completelyContained = true; 
			
		//check frustum sides
		for(FrustumPlaneNormal type : FrustumPlaneNormal.values()){
			//get frustum plane normal
			Vector3d n = getFrustumPlaneNormal(type);
			
			//compute max and min values, over all box verts, of dot(n, vert - cam.pos)
			float maxDot = 0, minDot = 0;
			for(int i=0; i<3; i++){
				if(n.get(i) > 0){
					maxDot += n.get(i) * (box.getMax().get(i) - position.get(i));
					minDot += n.get(i) * (box.getMin().get(i) - position.get(i));
				}else{
					maxDot += n.get(i) * (box.getMin().get(i) - position.get(i));
					minDot += n.get(i) * (box.getMax().get(i) - position.get(i));
				}
			}
			
			//reject if possible, and update completelyContained
			if(maxDot < 0){ return BoxContainment.CONTAINMENT_NONE; }
			completelyContained = completelyContained && (minDot > 0);
		}
		
		//check frustum front and back
		Vector3d n = rotation.getForward(); //(assumes this is normalized, to compare dot with zbounds)
		
		//compute max and min values, over all box verts, of dot(n, vert - cam.pos)
		float maxDot = 0, minDot = 0;
		for(int i=0; i<3; i++){
			if(n.get(i) > 0){
				maxDot += n.get(i) * (box.getMax().get(i) - position.get(i));
				minDot += n.get(i) * (box.getMin().get(i) - position.get(i));
			}else{
				maxDot += n.get(i) * (box.getMin().get(i) - position.get(i));
				minDot += n.get(i) * (box.getMax().get(i) - position.get(i));
			}
		}
		
		//reject if possible, and update completelyContained
		if(maxDot < zNear){ return BoxContainment.CONTAINMENT_NONE; }
		if(minDot > zFar ){ return BoxContainment.CONTAINMENT_NONE; }
		completelyContained = completelyContained && (minDot > zNear) && (maxDot < zFar);
		
		//if not completely contained and not yet rejected as invisible, regard as partially visible
		return completelyContained ? BoxContainment.CONTAINMENT_COMPLETE : BoxContainment.CONTAINMENT_PARTIAL;
	}
}
