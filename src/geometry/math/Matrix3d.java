package geometry.math;

/**
 * A 3x3 {@link Matrix}.
 */
public class Matrix3d extends Matrix{

	/**
	 * Creates a new Matrix3d with all entries equal to 0.
	 */
	public Matrix3d(){
		super(3,3);
	}
	
	/**
	 * Creates a new Matrix3d with the same entries as "rhs".
	 * 
	 * @param rhs The Matrix3d to copy.
	 */
	public Matrix3d(Matrix3d rhs){
		super(3,3);
		Matrix.copyData(rhs, this);
	}
	
	/**
	 * Creates a new Matrix3d with the same entries as this matrix.
	 */
	public Matrix3d copy(){
		return new Matrix3d(this);
	}
	
	/**
	 * Creates a new Matrix3d equal to the 3x3 identity matrix.
	 */
	public static Matrix3d createIdentityMatrix(){
		Matrix3d id = new Matrix3d();
		id.entries[0][0] = 1;
		id.entries[1][1] = 1;
		id.entries[2][2] = 1;
		return id;
	}
	
	/**
	 * Sets this matrix equal to the 3x3 identity matrix. 
	 */
	public void setAsIdentity(){
		
		entries[0][0] = 1; entries[0][1] = 0; entries[0][2] = 0;
		entries[1][0] = 0; entries[1][1] = 1; entries[1][2] = 0;
		entries[2][0] = 0; entries[2][1] = 0; entries[2][2] = 1;
	}
	
	//public methods with correct dimensions enforced
	/**
	 * Set the columns of this matrix equal to the specified vectors, in order.
	 */
	public void setColumns(Vector3d v1, Vector3d v2, Vector3d v3){ 
		super.setColumns(v1, v2, v3); 
	}
	
	/**
	 * Set the rows of this matrix equal to the specified vectors, in order.
	 */
	public void setRows(Vector3d v1, Vector3d v2, Vector3d v3){ 
		super.setRows(v1, v2, v3);
	}
	
	/**
	 * Sets this matrix equal to the transpose of "rhs".
	 */
	public void setAsTranspose(Matrix3d rhs){ super.setAsTranspose(rhs); }
	
	/**
	 * Sets this matrix equal to this + "rhs".
	 * 
	 * @param rhs The matrix to add to this matrix.
	 */
	public void add(Matrix3d rhs){ super.add(rhs); }
	
	/**
	 * Sets this matrix equal to this - "rhs".
	 * 
	 * @param rhs The matrix to subtract from this matrix.
	 */
	public void subtract(Matrix3d rhs){ super.subtract(rhs); }
	
	/**
	 * Sets this matrix equal to "B" * this.
	 * 
	 * @param B The Matrix3d by which to multiply this on the left. 
	 */
	public void multiplyLeft(Matrix3d B){ super.multiplyLeft(B); }
	
	/**
	 * Return new Vector3d equal to "this" * v.
	 */
	public Vector3d apply(Vector3d v){
		Vector3d ret = new Vector3d();
		Matrix.applyMatrix(this, v, ret);
		return ret;
	}
	
	/**
	 * Return result equal to unAugment("this" * augment(v,w)).
	 * 
	 * @see {@link Vector2d#augment(float)}
	 * @see {@link Vector3d#unAugment()}
	 */
	public Vector2d applyProjective(Vector2d v, float w){
		Vector2d ret = new Vector2d();
		Matrix.applyMatrixProjective(this, v, w, ret);
		return ret;
	}
	
	//computation of determinant and inverse
	private float cofactor(int i, int j){
		
		int i0,i1, j0,j1;
		
		switch(i){
			case 0: i0=1; i1=2; break;
			case 1: i0=0; i1=2; break;
			default: i0=0; i1=1; break;
		}
		
		switch(j){
			case 0: j0=1; j1=2; break;
			case 1: j0=0; j1=2; break;
			default: j0=0; j1=1; break;
		}

		float minor = 
			 entries[i0][j0]*entries[i1][j1]
			-entries[i0][j1]*entries[i1][j0];
		
		if(((i+j)%2)==0){ return minor; }else{ return -minor; }
	}
	
	/**
	 * Returns a new Matrix3d equal to the inverse of this matrix. <br>
	 * Does not check that the determinant of this matrix is nonzero.
	 * 
	 * @return The inverse of this matrix.
	 */
	public Matrix3d inverse(){
		
		//adjugate (transpose of matrix of cofactors)
		Matrix3d adj = new Matrix3d();
		for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			adj.entries[i][j] = cofactor(j,i);
		}}
		
		//divide by det (assumed to be nonzero)
		float det = 
			 entries[0][0]*adj.entries[0][0]
			+entries[0][1]*adj.entries[1][0]
			+entries[0][2]*adj.entries[2][0];
		
		adj.scaleEntries(1/det);
		return adj;
	}
	
	//===============================================
	// GL/GLU MODELVIEW MATRICES
	//===============================================
	
	/**
	 *  Rotation matrix; rotates CCW (with "axis" pointing at you) by "angle" radians. 
	 *  <br><br>
	 *  Imitates the corresponding OpenGL method.
	 */
	public void setAsGlRotate(Vector3d axis, float angle){
		
		setUpper3x3AsRotation(axis, angle);
	}
}
