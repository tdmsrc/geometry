package geometry.math;

import java.util.Collection;

import geometry.polyhedron.components.Polyhedron;


/**
 * Represents the physical state of an object:
 * <ul>
 * <li>Position ({@link #getTranslation()})
 * <li>Orientation ({@link #getRotation()})
 * <li>Linear velocity at centroid ({@link #getV()})
 * <li>Angular velocity ({@link #getW()}) 
 * </ul>
 * Update the state of the object after time has elapsed using {@link #integrate(float, Vector3d)}.
 * <br><br>
 * Computes impulse-based collision response with:
 * <ul>
 * <li> An immovable object: {@link #collide(Vector3d, Vector3d, Vector3d, float, float)}
 * <li> Another PhysicalState: {@link PhysicalState3d#collide(PhysicalState3d, PhysicalState3d, Vector3d, Vector3d, float, float)}
 * </ul>
 */
public class PhysicalState3d extends ObjectTransformation3d{
	
	private static final Vector3d ZERO = new Vector3d(0,0,0);
	
	//mass properties (centroid = offset)
	private float massInv;
	private Matrix3d bodyI, bodyIinv; //inertia matrix
	
	//state (in addition to ObjectTransformation3d rotation and translation (i.e. position))
	private Vector3d v; //velocity
	private Vector3d L, w; //angular momentum and angular velocity (must maintain L=Iw)
	
	
	/**
	 * Initialize the physical state of an object by specifying its 
	 * volume, density, centroid, and inertia matrix.
	 * 
	 * @param signedVolume Signed volume, as in {@link Polyhedron#computeSignedVolume()}
	 * @param density Density of the body
	 * @param volumeTimesCentroid Volume times centroid, as in {@link Polyhedron#computeVolumeTimesCentroid()}
	 * @param inertiaMatrix Inertia matrix of the body, as in {@link Polyhedron#computeInertiaTensor()} 
	 */
	public PhysicalState3d(float signedVolume, float density, Vector3d volumeTimesCentroid, Matrix3d inertiaMatrix){
		
		//volume and mass
		float volume = signedVolume;
		float mass = density*volume;
		massInv = 1/mass;
		
		//centroid
		offset.setEqualTo(volumeTimesCentroid); offset.scale(1/volume);
		
		//inertia matrix
		//(note this is in a fixed frame--conjugate by rotation)
		bodyI = inertiaMatrix.copy(); bodyI.scaleEntries(density);
		bodyIinv = bodyI.inverse();
		
		//dynamical state (position)
		v = new Vector3d();
		
		//dynamical state (rotation)
		w = new Vector3d();
		L = new Vector3d();
	}
	
	/**
	 * Set "target" equal to the inertia matrix times "target".
	 */
	private void timesI(Vector3d target){
		//bodyI is in a fixed frame, so conjugate by rotation
		target.setEqualTo(rotation.getMatrixInverse().apply(target));
		target.setEqualTo(bodyI.apply(target));
		target.setEqualTo(rotation.getMatrix().apply(target));
	}
	
	/**
	 * Set "target" equal to the inverse of the inertia matrix times "target".
	 */
	private void timesIinv(Vector3d target){
		//bodyI is in a fixed frame, so conjugate by rotation
		target.setEqualTo(rotation.getMatrixInverse().apply(target));
		target.setEqualTo(bodyIinv.apply(target));
		target.setEqualTo(rotation.getMatrix().apply(target));
	}
	
	
	//===============================================
	// BASIC METHODS
	//===============================================
	
	/**
	 * Get the linear velocity of the object (i.e., at the centroid).
	 */
	public Vector3d getV(){ return v; }
	
	/**
	 * Set the linear velocity of the object.
	 */
	public void setV(Vector3d v){ this.v.setEqualTo(v); }
	
	/**
	 * Get the angular velocity of the object.
	 */
	public Vector3d getW(){ return w; }
	
	/**
	 * Set the angular velocity of the object.
	 */
	public void setW(Vector3d w){
		//set L = Iw
		this.w.setEqualTo(w);
		L.setEqualTo(w); timesI(L);
	}
	
	/**
	 * Get the angular momentum of the object.
	 */
	public Vector3d getL(){ return L; }
	
	/**
	 * Set the angular momentum of the object.
	 */
	public void setL(Vector3d L){
		//set L = Iw
		this.L.setEqualTo(L);
		w.setEqualTo(L); timesIinv(w);
	}
	
	/**
	 * Get the velocity of a point on the object, taking into account
	 * both angular and linear velocity. The point should be specified 
	 * relative to the centroid of the object (for example, if r = (0,0,0), 
	 * the point is the centroid and the result equals the linear velocity).
	 * 
	 * @param r The point, specified relative to the centroid (i.e., (0,0,0) = centroid).
	 * @return A new {@link Vector3d} giving the velocity of the point.
	 */
	public Vector3d getVelocity(Vector3d r){
		Vector3d xv = w.cross(r); xv.add(v);
		return xv;
	}
		
	/**
	 * Return the sum of linear and angular kinetic energy.
	 */
	public float getKineticEnergy(){
		return 0.5f*(w.dot(L) + v.dot(v)/massInv);
	}
	
	//===============================================
	// INTEGRATION
	//===============================================
	
	/**
	 * Scale linear and angular velocity by exp(-factor*dt). 
	 * 
	 * @param dt Elapsed time
	 * @param factor Amount of drag
	 */
	public void drag(float dt, float factor){
		float exp = (float)Math.exp(-factor*dt);
		v.scale(exp);
		w.scale(exp);
		L.setEqualTo(w); timesI(L);
	}
	
	/**
	 * Update the physical state according to the current linear and 
	 * angular velocity, assuming no acceleration.
	 * 
	 * @param dt Elapsed time
	 */
	public void integrate(float dt){
		integrate(dt, ZERO);
	}
	
	/**
	 * Update the physical state according to the current linear and 
	 * angular velocity, as well as the specified linear acceleration.
	 * 
	 * @param dt Elapsed time
	 * @param acc Linear acceleration vector (i.e., due to gravity)
	 */
	public void integrate(float dt, Vector3d acc){
		
		//position
		//-----------------------------------------
		Vector3d dv = acc.copy(); dv.scale(dt);
		v.add(dv);
		
		Vector3d dp = v.copy(); dp.scale(dt); 
		Vector3d dp2 = acc.copy(); dp2.scale(0.5f*dt*dt); dp.add(dp2);
		translation.add(dp);
		
		//rotation (\tau = I\alpha + wxL)
		//-----------------------------------------
		//approximate new w (half, for convenience)
		Vector3d dw2 = L.cross(w); timesIinv(dw2);
		dw2.scale(dt*0.5f);
		
		//magnus expansion
		//-----------------------------------
		//m1 = (1/2) (w+w2) dt = (w + [dw/2]) * dt
		//m2 = (1/12) (w2 x w) dt^2 = (1/6) ([dw/2] x w) * dt^2
		Vector3d m = w.copy(); m.add(dw2); m.scale(dt);
		Vector3d m2 = dw2.cross(w); m2.scale(dt*dt/6.0f); m.add(m2);
		//-----------------------------------
		if(m.length() != 0){ //avoid div by zero
			rotation.leftMultiply(m, m.length());
			
			//recalculate w from L
			w.setEqualTo(L); timesIinv(w);
		}
	}
	
	
	public static interface IntegrationConstraint{
		public Vector3d getConstraintP(); //collision point
		public Vector3d getConstraintN(); //collision normal (assume normalized)
	}
	
	public static Vector3d applyConstraints(Vector3d v, Collection<IntegrationConstraint> constraints){
		Vector3d vRet = v.copy();
		
		//project onto positive side of each contact plane when necessary 
		for(IntegrationConstraint c : constraints){
			Vector3d n = c.getConstraintN();
			
			float vRetDotN = vRet.dot(n);
			if(vRetDotN > 0){ continue; }
			
			Vector3d vN = n.copy(); vN.scale(vRetDotN);
			vRet.subtract(vN);
		}
		
		return vRet;
	}
	
	
	private static final float CONTACT_PLANE_ACCELERATION_FACTOR = 0.2f;
	
	/**
	 * Update the physical state according to the current linear and 
	 * angular velocity, as well as the specified linear acceleration.
	 * 
	 * Also takes into account the given constraints, which are in the form of contact planes.
	 * 
	 * @param dt Elapsed time
	 * @param acc Linear acceleration vector (i.e., due to gravity)
	 */
	public void integrateWithConstraints(float dt, Vector3d acc, Collection<IntegrationConstraint> constraints){
		
		//to help stop objects from vibrating, reduce acceleration in the direction
		//of the contact planes--some is necessary so objects don't become floaty
		
		//position
		//-----------------------------------------
		Vector3d dv = acc.copy(); dv.scale(dt);
		Vector3d dvc = applyConstraints(dv, constraints);
		
		//now set v += dvc + t*(dv-dvc): if t=0, no accel against contact planes; if t=1, no constraint 
		Vector3d dvdiff = dv.copy(); dvdiff.subtract(dvc); 
		dvdiff.scale(CONTACT_PLANE_ACCELERATION_FACTOR);
		v.add(dvc); v.add(dvdiff);
		
		Vector3d dp = v.copy(); dp.scale(dt); 
		Vector3d dp2 = acc.copy(); dp2.scale(0.5f*dt*dt); dp.add(dp2);
		//Vector3d dpc = applyConstraints(dp, constraints);
		translation.add(dp);
		
		//rotation (\tau = I\alpha + wxL)
		//-----------------------------------------
		//approximate new w (half, for convenience)
		Vector3d dw2 = L.cross(w); timesIinv(dw2);
		dw2.scale(dt*0.5f);
		
		//magnus expansion
		//-----------------------------------
		//m1 = (1/2) (w+w2) dt = (w + [dw/2]) * dt
		//m2 = (1/12) (w2 x w) dt^2 = (1/6) ([dw/2] x w) * dt^2
		Vector3d m = w.copy(); m.add(dw2); m.scale(dt);
		Vector3d m2 = dw2.cross(w); m2.scale(dt*dt/6.0f); m.add(m2);
		//-----------------------------------
		if(m.length() != 0){ //avoid div by zero
			
			//TODO constrain rotation and induce rotation from linear+constraints?
			//i.e.: if rotation would cause any collision point to move into collision plane, quit
			/*Rotation3d dr = new Rotation3d(m, m.length());
			
			for(IntegrationConstraint c : constraints){
				Vector3d cp = c.getConstraintP();
				
				//dr applied to constraint
				Vector3d drcp = cp.copy(); drcp.subtract(offset); 
				drcp = dr.getMatrix().apply(drcp); drcp.add(offset);
				
				//if drcp is MORE on neg side of collision plane, totally quit
				drcp.subtract(cp);
				if(drcp.dot(c.getConstraintN()) < 0){ return; }
			}
			
			rotation.leftMultiply(dr);*/
			
			rotation.leftMultiply(m, m.length());
			
			//recalculate w from L
			w.setEqualTo(L); timesIinv(w);
		}
	}
	
	//===============================================
	// IMPULSE
	//===============================================
	
	/**
	 * Apply an impulse acting on the object.
	 * 
	 * @param jDir Normalized impulse vector
	 * @param jMag Length of impulse vector
	 * @param r The vector from the centroid to a point on the object, in world coordinates.
	 */
	private void applyImpulse(Vector3d jDir, float jMag, Vector3d r){
		
		//v += j/mass
		Vector3d dv = jDir.copy(); dv.scale(massInv*jMag); v.add(dv);
		//L += r x j
		Vector3d dL = r.cross(jDir); dL.scale(jMag); L.add(dL);
		
		//recompute w
		w.setEqualTo(L); timesIinv(w);
	}
	
	/**
	 * Utility function for computing impulse.
	 * 
	 * @return The quantity m^{-1} + dot( [I^{-1}(rxv)]xr, v )
	 */
	private float computeImpulseDenominator(Vector3d r, Vector3d v){
		
		Vector3d u = r.cross(v); timesIinv(u);
		return (massInv + u.cross(r).dot(v));
	}
	
	
	//if -vr.dot(n), i.e. relative speed of contact points in the normal direction,
	//is less than this, then the coefficient of restitution is scaled by MIN_VELOCITY_E_FACTOR
	private static final float MIN_VELOCITY = 0.5f;
	private static final float MIN_VELOCITY_E_FACTOR = 0.0f;
	
	/**
	 * Change this {@link PhysicalState3d} to reflect the result of a collision with 
	 * an immovable object at point p (world coordinates).
	 * 
	 * @param p Point of collision (world coordinates)
	 * @param n Normal to collision plane (pointing toward this object)
	 * @param vp2 Velocity of collision point on the immovable object (typically zero)
	 * @param e Coefficient of restitution
	 * @param mu Coefficient of friction
	 */
	public void collide(Vector3d p, Vector3d n, Vector3d vp2, float e, float mu){
		
		//vector from centroid to contact point
		Vector3d r = p.copy(); r.subtract(translation);
		//vr = relative velocity of points of contact
		Vector3d vp1, vr;
		
		//normal impulse
		//-----------------------------------------
		vp1 = getVelocity(r);
		vr = vp1.copy(); vr.subtract(vp2);
		
		float eFix = e, vrDotN = vr.dot(n);
		if(-vrDotN < MIN_VELOCITY){ eFix *= MIN_VELOCITY_E_FACTOR; }
		
		float nd = computeImpulseDenominator(r, n);
		float jn = -(1+eFix)*vrDotN / nd;
		
		//clamp and apply
		if(jn < 0){ return; } //setting jn = 0 results in 0 impulses from now on
		applyImpulse(n, jn, r);
		
		//friction impulse
		//-----------------------------------------
		vp1 = getVelocity(r);
		vr = vp1.copy(); vr.subtract(vp2);
		
		Vector3d t = Vector3d.linearCombination(1.0f, vr, -vr.dot(n), n);
		float tlen = t.length();
		if(tlen == 0){ return; }else{ t.scale(1.0f / tlen); }
		
		float td = computeImpulseDenominator(r, t);
		float jt = -vr.dot(t) / td;
		
		//clamp and apply
		if(jt < -mu*jn){ jt = -mu*jn; }else if(jt > mu*jn){ jt = mu*jn; } 
		applyImpulse(t, jt, r);
	}
	
	/**
	 * Change {@link PhysicalState3d}s "obj1" and "obj2" to reflect a collision between
	 * the objects at a point p (world coordinates).
	 * 
	 * @param obj1 The first object in the collision
	 * @param obj2 The other object in the collision
	 * @param p Point of collision (world coordinates)
	 * @param n Normal to collision plane (pointing toward obj1)
	 * @param e Coefficient of restitution
	 * @param mu Coefficient of friction
	 */
	public static void collide(PhysicalState3d obj1, PhysicalState3d obj2, Vector3d p, Vector3d n, float e, float mu){
		
		//vectors from centroids to p
		Vector3d r1 = p.copy(); r1.subtract(obj1.translation);
		Vector3d r2 = p.copy(); r2.subtract(obj2.translation);
		//vr = relative velocity of points of contact
		Vector3d vp1, vp2, vr;
		
		//normal impulse
		//-----------------------------------------
		vp1 = obj1.getVelocity(r1);
		vp2 = obj2.getVelocity(r2);
		vr = vp1.copy(); vr.subtract(vp2);
		
		float eFix = e, vrDotN = vr.dot(n);
		if(-vrDotN < MIN_VELOCITY){ eFix *= MIN_VELOCITY_E_FACTOR; }
		
		float nd1 = obj1.computeImpulseDenominator(r1, n);
		float nd2 = obj2.computeImpulseDenominator(r2, n);
		float jn = -(1+eFix)*vrDotN / (nd1 + nd2);
		
		//clamp and apply
		if(jn < 0){ return; } //setting jn = 0 results in 0 impulses from now on
		obj1.applyImpulse(n,  jn, r1);
		obj2.applyImpulse(n, -jn, r2);
		
		//friction impulse
		//-----------------------------------------
		vp1 = obj1.getVelocity(r1);
		vp2 = obj2.getVelocity(r2);
		vr = vp1.copy(); vr.subtract(vp2);
		
		//projection of vr onto n perp
		Vector3d t = Vector3d.linearCombination(1.0f, vr, -vr.dot(n), n);
		float tlen = t.length();
		if(tlen == 0){ return; }else{ t.scale(1.0f / tlen); }
		
		float td1 = obj1.computeImpulseDenominator(r1, t);
		float td2 = obj2.computeImpulseDenominator(r2, t);
		float jt = -vr.dot(t) / (td1 + td2);
		
		//clamp and apply
		if(jt < -mu*jn){ jt = -mu*jn; }else if(jt > mu*jn){ jt = mu*jn; }
		obj1.applyImpulse(t,  jt, r1);
		obj2.applyImpulse(t, -jt, r2);
	}
}
