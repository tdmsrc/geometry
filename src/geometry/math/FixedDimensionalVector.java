package geometry.math;

/**
 * A vector of some fixed dimension.
 * 
 * @see {@link Vector2d}
 * @see {@link Vector3d}
 * @see {@link Vector4d}
 */
public abstract class FixedDimensionalVector<V extends FixedDimensionalVector<V>> extends Vector {

	/**
	 * Creates a FixedDimensionalVector of the specified dimension with all entries 0.
	 * 
	 * @param dimension The dimension of the vector.
	 */
	public FixedDimensionalVector(int dimension) {
		super(dimension);
	}
	
	public abstract V copy();
	
	//make dimension-sensitive methods public; force argument to be same type
	/**
	 * Sets this vector equal to "rhs".
	 */
	public void setEqualTo(V rhs){ Vector.copyData(rhs, this); }
	
	/**
	 * Sets this vector equal to this + "rhs".
	 * 
	 * @param rhs The vector to be added to this vector.
	 */
	public void add(V rhs){ super.add(rhs); }
	
	/**
	 * Sets this vector equal to this - "rhs".
	 * 
	 * @param rhs The vector to be subtracted from this vector.
	 */
	public void subtract(V rhs){ super.subtract(rhs); }
	
	/**
	 * Sets the ith component of this vector equal to this_i * rhs_i.
	 * 
	 * @param rhs The vector whose components are to multiply this vector's components.
	 */
	public void multiplyComponentWise(V rhs){ super.multiplyComponentWise(rhs); }
	
	/**
	 * Sets the ith component of this vector equal to this_i / rhs_i.
	 * 
	 * @param rhs The vector whose components are to divide this vector's components.
	 */
	public void divideComponentWise(V rhs){ super.divideComponentWise(rhs); }
	
	/**
	 * Returns the dot product of this vector with "b".
	 *
	 * @return The dot product of this and "b".
	 */
	public float dot(V b){ return super.dot(b); }
	
	/**
	 * Returns the sum of squares of the components of the vector (this - "b").
	 * 
	 * @return The sum of squares of the components of the vector (this - "b").
	 */
	public float distanceSquared(V b){ return super.distanceSquared(b); }
	
	/**
	 * Returns the distance between this vector and "b".  In particular, returns
	 * the square root of the result returned by {@link #distanceSquared} with argument "b".
	 * 
	 * @return The distance between this vector and "b". 
	 */
	public float distance(V b){ return super.distance(b); }
	
	/**
	 * Linearly interpolate from this vector to q.
	 * In particular, returns this + t(q-this). 
	 * 
	 * @param q The point to interpolate with this point.
	 * @param t The linear interpolation parameter.
	 * @return The vector this + t(q-this).
	 */
	public V lerp(V q, float t){

		V ret = this.copy();
		for(int i=0; i<dimension; i++){
			ret.entries[i] += t*(q.entries[i] - ret.entries[i]);
		}
		return ret;
	}
	
	/**
	 * Find the value t which minimizes the distance of lerp(q,t) to x.
	 * 
	 * @param q The final point of the edge.
	 * @param x The point to project onto the edge from this to q.
	 * @return The lerp value of the projection of x onto the edge from this to q.
	 */
	public float unLerp(V q, V x){
		
		Vector xMinusP = x.copy(); xMinusP.subtract(this);
		Vector qMinusP = q.copy(); qMinusP.subtract(this);
		return xMinusP.dot(qMinusP) / qMinusP.lengthSquared();
	}
}
