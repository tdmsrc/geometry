package geometry.math;


/**
 * 2d-specific version of {@link FixedDimensionalVector}.
 */
public class Vector2d extends FixedDimensionalVector<Vector2d>{
	
	/**
	 * Creates a Vector2d with all entries 0.
	 */
	public Vector2d(){
		super(2);
	}
	
	/**
	 * Creates a Vector2d with the specified components.
	 */
	public Vector2d(float x, float y){
		super(2);
		entries[0] = x;
		entries[1] = y;
	}
	
	/**
	 * Creates a new Vector2d with the same components as "rhs".
	 * 
	 * @param rhs The Vector2d to copy.
	 */
	public Vector2d(Vector2d rhs){
		super(2);
		Vector.copyData(rhs, this);
	}
	
	/**
	 * Returns a new Vector2d with the same components as this.
	 */
	public Vector2d copy(){
		return new Vector2d(this);
	}
	
	//accessors
	public float getX(){ return entries[0]; }
	public float getY(){ return entries[1]; }
	
	public void setX(float x){ entries[0] = x; }
	public void setY(float y){ entries[1] = y; }
	
	//augment to 3d
	/**
	 * Returns a {@link Vector3d} equal to (this_x, this_y, "w").
	 * 
	 * @see {@link Vector3d#unAugment()}
	 */
	public Vector3d augment(float w){ 
		Vector3d ret = new Vector3d();
		for(int i=0; i<2; i++){ ret.entries[i] = entries[i]; }
		ret.entries[2] = w;
		return ret;
	}

	//special methods
	/**
	 * Returns a new Vector2d representing this*rhs using complex multiplication.
	 * <br>
	 * (A Vector2d (a,b) represents the complex number a+bi).
	 * 
	 * @param rhs The complex number to multiply on the right.
	 */
	public Vector2d cpxMultiply(Vector2d rhs){
		
		return new Vector2d(			
			entries[0]*rhs.entries[0] + -entries[1]*rhs.entries[1],
			entries[0]*rhs.entries[1] +  entries[1]*rhs.entries[0]
		);
	}
	
	/**
	 * Return the complex conjugate of this.
	 * <br>
	 * (A Vector2d (a,b) represents the complex number a+bi).
	 */
	public void cpxConjugate(){
		
		entries[1] = -entries[1];
	}
	
	/**
	 * Return the complex inverse of this.
	 * <br>
	 * (A Vector2d (a,b) represents the complex number a+bi).
	 */
	public Vector2d cpxInverse(){
		
		Vector2d ret = this.copy();
		ret.cpxConjugate();
		ret.scale(1/lengthSquared());
		return ret;
	}
	
	/**
	 * Return a new complex number (cos t, sin t)
	 * <br>
	 * (A Vector2d (a,b) represents the complex number a+bi).
	 */
	public Vector2d cpxFromAngle(float angle){
		
		float c = (float)Math.cos(angle);
		float s = (float)Math.sin(angle);
		
		return new Vector2d(c,s);
	}
	
	/**
	 * Returns the z component of (this.x,this.y,0) X (rhs.x,rhs.y,0).<br>
	 * That is, returns this.x*rhs.y-this.y*rhs.x.<br><br>
	 * 
	 * Note that Note that u.{@link #rotateCCW}() dot v = u.cross(v).
	 */
	public float cross(Vector2d rhs){
		
		return entries[0]*rhs.entries[1] - entries[1]*rhs.entries[0];
	}
	
	/**
	 * Creates and returns a new Vector2d equal to coeff1*basis1 + coeff2*basis2.
	 */
	public static Vector2d linearCombination(float coeff1, Vector2d basis1, float coeff2, Vector2d basis2){
		
		return new Vector2d(
			basis1.entries[0] * coeff1 + basis2.entries[0] * coeff2,
			basis1.entries[1] * coeff1 + basis2.entries[1] * coeff2
		);
	}
	
	
	/**
	 * Rotates this vector 90 degrees CCW.<br>
	 * That is, sets this = (-this.y, this.x).<br><br>
	 * 
	 * Note that u.rotateCCW() dot v = u.{@link #cross}(v).
	 */
	public void rotateCCW(){
		
		float temp = entries[0];
		entries[0] = -entries[1];
		entries[1] = temp;
	}
}
