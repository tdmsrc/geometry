package geometry.math;

/**
 * 4d-specific version of {@link FixedDimensionalVector}.
 */
public class Vector4d extends FixedDimensionalVector<Vector4d>{

	/**
	 * Creates a Vector4d with all entries 0.
	 */
	public Vector4d(){
		super(4);
	}
	
	/**
	 * Creates a Vector4d with the specified components.
	 */
	public Vector4d(float x, float y, float z, float w){
		super(4);
		entries[0] = x;
		entries[1] = y;
		entries[2] = z;
		entries[3] = w;
	}
	
	/**
	 * Creates a new Vector4d with the same components as "rhs".
	 * 
	 * @param rhs The Vector4d to copy.
	 */
	public Vector4d(Vector4d rhs){
		super(4);
		Vector.copyData(rhs, this);
	}
	
	/**
	 * Returns a new Vector4d with the same components as this.
	 */
	public Vector4d copy(){
		return new Vector4d(this);
	}
	
	//accessors
	public float getX(){ return entries[0]; }
	public float getY(){ return entries[1]; }
	public float getZ(){ return entries[2]; }
	public float getW(){ return entries[3]; }
	
	public void setX(float x){ entries[0] = x; }
	public void setY(float y){ entries[1] = y; }
	public void setZ(float z){ entries[2] = z; }
	public void setW(float w){ entries[3] = w; }
	
	//un-augment
	/**
	 * Returns a new {@link Vector3d} with ith component equal to this_i / this_w.
	 * 
	 * @see {@link Vector3d#augment(float)}
	 */
	public Vector3d unAugment(){
		Vector3d ret = new Vector3d();
		for(int i=0; i<3; i++){ ret.entries[i] = entries[i] / entries[3]; }
		return ret;
	}
	
	//special methods
	/**
	 * Returns a new Vector4d representing this*rhs using quaternion multiplication.
	 * <br>
	 * (A Vector4d (b,c,d,a) represents the quaternion a+bi+cj+dk).
	 * 
	 * @param rhs The quaternion to multiply on the right.
	 */
	public Vector4d quatMultiply(Vector4d rhs){
		
		//[16 mults]
		return new Vector4d(			
			entries[3]*rhs.entries[0] +  entries[0]*rhs.entries[3] +  entries[1]*rhs.entries[2] + -entries[2]*rhs.entries[1],
			entries[3]*rhs.entries[1] + -entries[0]*rhs.entries[2] +  entries[1]*rhs.entries[3] +  entries[2]*rhs.entries[0],
			entries[3]*rhs.entries[2] +  entries[0]*rhs.entries[1] + -entries[1]*rhs.entries[0] +  entries[2]*rhs.entries[3],
			entries[3]*rhs.entries[3] + -entries[0]*rhs.entries[0] + -entries[1]*rhs.entries[1] + -entries[2]*rhs.entries[2]
		);
	}
	
	/**
	 * Return the quaternionic conjugate of this.
	 * <br>
	 * (A Vector4d (b,c,d,a) represents the quaternion a+bi+cj+dk).
	 */
	public void quatConjugate(){
		
		entries[0] = -entries[0];
		entries[1] = -entries[1];
		entries[2] = -entries[2];
	}
	
	/**
	 * Return the quaternionic inverse of this.
	 * <br>
	 * (A Vector4d (b,c,d,a) represents the quaternion a+bi+cj+dk).
	 */
	public Vector4d quatInverse(){
		
		Vector4d ret = this.copy();
		ret.quatConjugate();
		ret.scale(1/lengthSquared());
		return ret;
	}
}
