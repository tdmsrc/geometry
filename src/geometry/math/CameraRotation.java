package geometry.math;

import java.util.ArrayList;
import java.util.List;

//This is different from a general Rotation3d because it assumes
//there is no roll, only pitch and yaw; i.e., there is a fixed "up" vector.  
//(default (0,1,0)).  This makes leftMultiply more efficient; 
//also, can be specified with/serialized as (up, pitch and yaw) or (up, forward)

/**
 * An instance of this class represents the viewing direction and orientation for a Camera. <br>
 * It maintains:
 * <ul>
 * <li> A forward vector and a fixed "up" vector <br>(i.e., to prevent roll),
 * <li> A side vector and tilted "up" vector <br>(i.e., so [f, s, tu] form a positive orthonormal basis)
 * <li> A 3x3 matrix and its inverse (transpose) <br>(i.e., the matrix to apply to objects viewed from this POV).
 * </ul>
 * It can be efficiently rotated by specifying a change in pitch and yaw. <br>
 * It also provides a slerp method (interpolate between two CameraRotations).
 */
public class CameraRotation{
	
	/**
	 * Interface for being notified when a CameraRotation changes.
	 */
	protected static interface CameraRotationListener{
		public void cameraRotationChanged();
	}
	private List<CameraRotationListener> listeners;

	//parameters defining the camera rotation
	private Vector3d forward, fixedUp; //<-ensure length is 1
	
	//derived from forward and up
	private Vector3d side, tiltedUp; //side = normalize(forward X up), tiltedUp = side X forward
	private Matrix3d matrix, matrixInverse;
	
	private Matrix3d tempRotate; //used to apply to forward vector when rotating camera
	
	
	/**
	 * Construct a default CameraRotation.<br>
	 * (i.e., forward = (0,0,-1) and fixedUp = (0,1,0))
	 */
	public CameraRotation(){
		
		//default up and forward
		fixedUp = new Vector3d(0, 1, 0);
		forward = new Vector3d(0, 0, -1);
		
		initializeData();
		invalidateDerived();
	}
	
	/**
	 * Copy constructor.<br>
	 * The new CameraRotation will have no CameraRotationListeners.
	 */
	public CameraRotation(CameraRotation rhs){
		
		//copy up and forward
		fixedUp = rhs.fixedUp.copy();
		forward = rhs.forward.copy();
		
		initializeData();
		invalidateDerived();
	}
	
	/**
	 * Create a copy of this CameraRotation.<br>
	 * The new CameraRotation will have no CameraRotationListeners.
	 */
	public CameraRotation copy(){
		return new CameraRotation(this);
	}
	
	private void initializeData(){
		
		listeners = new ArrayList<CameraRotationListener>();
		
		//create new objects
		side = new Vector3d();
		tiltedUp = new Vector3d();
		
		matrix = new Matrix3d();
		matrixInverse = new Matrix3d();
		
		tempRotate = new Matrix3d();
	}
	
	private void invalidateDerived(){
		
		//should be called whenever forward or fixedUp changes, 
		//and other data is to be recomputed
		computeOrthoBasis();
		computeMatrix();
		
		//notify listeners
		for(CameraRotationListener listener : listeners){ listener.cameraRotationChanged(); }
	}
	
	/**
	 * Add a listener which will be notified when this CameraRotation changes.
	 */
	protected void addListener(CameraRotationListener listener){
		listeners.add(listener);
	}
	
	
	//-----------------------------------------
	// ACCESSORS
	//-----------------------------------------

	/**
	 * Get the direction in which the camera is looking.<br>
	 * Default value is (0,0,-1).
	 */
	public Vector3d getForward(){ return forward; }
	
	/**
	 * Get the camera's fixed up vector, which determines the side vector.<br>
	 * Default value is (0,1,0).
	 */
	public Vector3d getFixedUp(){ return fixedUp; }
	
	/**
	 * Get the camera's side vector, which determines the tilted up vector.<br>
	 * This is equal to normalize(forward X up).<br>
	 * Default value is (1,0,0).
	 */
	public Vector3d getSide(){ return side; }
	
	/**
	 * Get the camera's tilted up vector.<br>
	 * This is equal to side X forward.<br>
	 * Default value is (1,0,0).
	 */
	public Vector3d getTiltedUp(){ return tiltedUp; }
	
	/**
	 * Get movement "d" units forward.
	 * 
	 * @param d Distance to move.
	 * @return Displacement vector for this movement.
	 */
	public Vector3d moveForward(float d){
		
		Vector3d v = forward.copy(); 
		v.scale(d);
		return v;
	}
	
	/**
	 * Get movement "d" units forward in the plane perpendicular to the fixed "up" vector.
	 * (i.e., in the direction of the projection of "forward" onto the plane perpendicular to up)
	 * 
	 * @param d Distance to move.
	 * @return Displacement vector for this movement.
	 */
	public Vector3d moveForwardUPerp(float d){
		
		//project forward onto uperp, then set length to df
		Vector3d proj = fixedUp.copy(); 
		proj.scale(forward.dot(fixedUp));
		
		Vector3d vfp = forward.copy();
		vfp.subtract(proj);
		vfp.normalize();
		vfp.scale(d);
		return vfp;
	}
	
	/**
	 * Get movement "d" units along the side vector.
	 * 
	 * @param d Distance to move.
	 * @return Displacement vector for this movement.
	 */
	public Vector3d moveSideways(float d){
		
		Vector3d v = side.copy();
		v.scale(d);
		return v;
	}
	
	/**
	 * Get movement "d" units in the fixed "up" direction.
	 * 
	 * @param d Distance to move.
	 * @return Displacement vector for this movement.
	 */
	public Vector3d moveUp(float d){
		
		Vector3d v = fixedUp.copy();
		v.scale(d);
		return v;
	}
		
	/**
	 * Get the matrix which is to be applied to objects that are to be
	 * viewed from the perspective of a Camera with this CameraRotation.
	 */
	public Matrix3d getMatrix(){ return matrix; }
	
	/**
	 * Get the inverse of the matrix returned by {@link #getMatrix()}.<br>
	 * Since the matrix is orthogonal, the inverse is just the transpose.
	 */
	public Matrix3d getMatrixInverse(){ return matrixInverse; }
	
	
	//-----------------------------------------
	// CONVERSION/UPDATING
	//-----------------------------------------
	
	/**
	 * Recompute side and tiltedUp.
	 */
	private void computeOrthoBasis(){
		
		side = forward.cross(fixedUp);
		side.normalize();
		tiltedUp = side.cross(forward);
	}
	
	/**
	 * Recompute matrix and matrixInverse.
	 */
	private void computeMatrix(){

		//change of basis [s, u, -f] -> [e1 e2 e3] (u = tiltedUp)
		//this is the matrix with rows s, u, -f
		//it is orthogonal so inverse is matrix with rows s, u, -f.
		
		Vector3d minusf = forward.copy(); minusf.flip();
		matrix.setRows(side, tiltedUp, minusf);
		matrixInverse.setColumns(side, tiltedUp, minusf);
	}
	
	
	//-----------------------------------------
	// SETTERS/ROTATION
	//-----------------------------------------
	
	/**
	 * Set the direction in which the camera is looking.
	 * Normalizes the vector without copying it.<br>
	 * Default value is (0,0,-1).
	 */
	public void setForward(Vector3d forward){ 
		this.forward = forward;
		forward.normalize();
		
		invalidateDerived();
	}
	
	/**
	 * Set the camera's fixed up vector, which determines the side vector.
	 * Normalizes the vector without copying it.<br>
	 * Default value is (0,1,0).
	 */
	public void setFixedUp(Vector3d fixedUp){
		this.fixedUp = fixedUp;
		fixedUp.normalize();
		
		invalidateDerived();
	}

	/**
	 * Rotate the camera CCW "yaw" radians about "fixedUp" and CCW "pitch" radians about "side".
	 * This does not affect the fixed "up" vector.
	 * <br>
	 * (i.e., positive yaw will rotate left, positive pitch will tilt upward)
	 */
	public void rotate(float yaw, float pitch){
		
		float cb = (float)Math.cos(pitch), sb = (float)Math.sin(pitch);
		
		tempRotate.setAsGlRotate(fixedUp, yaw);
		forward = tempRotate.apply(Vector3d.linearCombination(cb, forward, sb, tiltedUp));
		forward.normalize();
		
		invalidateDerived();
	}
	
	
	//-----------------------------------------
	// SLERP
	//-----------------------------------------
	
	/**
	 * Return the vector normalize(p.forward.lerp(q.forward, t)).
	 * 
	 * @see Vector3d#lerp(Vector3d, float)
	 * @see #setAsSlerp(CameraRotation, CameraRotation, float)
	 */
	public static Vector3d getSlerpForward(CameraRotation p, CameraRotation q, float t){
		
		Vector3d ret = p.forward.lerp(q.forward, t);
		ret.normalize();
		return ret;
	}
	
	/**
	 * Return the vector normalize(p.fixedUp.lerp(q.fixedUp, t)).
	 * 
	 * @see Vector3d#lerp(Vector3d, float)
	 * @see #setAsSlerp(CameraRotation, CameraRotation, float)
	 */
	public static Vector3d getSlerpFixedUp(CameraRotation p, CameraRotation q, float t){
		
		Vector3d ret = p.fixedUp.lerp(q.fixedUp, t);
		ret.normalize();
		return ret;
	}
	
	/**
	 * Interpolate between CameraRotations p and q.  <br>
	 * In particular, sets this CameraRotation to have:
	 * <ul>
	 * <li>this.forward = normalize(p.forward.lerp(q.forward, t)),
	 * <li>this.fixedUp = normalize(p.fixedUp.lerp(q.fixedUp, t)).
	 * </ul>
	 * Note that at t=0, the result will be p, and at t=1, the result will be q.
	 * 
	 * @param p The CameraRotation at t=0.
	 * @param q The CameraRotation at t=1.
	 * @param t The interpolation parameter.
	 * @see Vector3d#lerp(Vector3d, float)
	 */
	public void setAsSlerp(CameraRotation p, CameraRotation q, float t){
		
		//[TODO] this isn't good, do it a better way
		
		this.forward = getSlerpForward(p,q,t);
		this.fixedUp = getSlerpFixedUp(p,q,t);
		
		invalidateDerived();
	}
}
