package geometry.math;

/**
 * A 2x2 {@link Matrix}.
 */
public class Matrix2d extends Matrix{

	/**
	 * Creates a new Matrix2d with all entries equal to 0.
	 */
	public Matrix2d(){
		super(2,2);
	}

	/**
	 * Creates a new Matrix2d with the same entries as "rhs".
	 * 
	 * @param rhs The Matrix2d to copy.
	 */
	public Matrix2d(Matrix2d rhs){
		super(2,2);
		Matrix.copyData(rhs, this);
	}
	
	/**
	 * Creates a new Matrix2d with the same entries as this matrix.
	 */
	public Matrix2d copy(){
		return new Matrix2d(this);
	}
	
	/**
	 * Creates a new Matrix2d equal to the 2x2 identity matrix.
	 */
	public static Matrix2d createIdentityMatrix(){
		Matrix2d id = new Matrix2d();
		id.entries[0][0] = 1;
		id.entries[1][1] = 1;
		return id;
	}
	
	/**
	 * Sets this matrix equal to the 2x2 identity matrix. 
	 */
	public void setAsIdentity(){
		
		entries[0][0] = 1; entries[0][1] = 0;
		entries[1][0] = 0; entries[1][1] = 1;
	}
	
	//public methods with correct dimensions enforced
	/**
	 * Set the columns of this matrix equal to the specified vectors, in order.
	 */
	public void setColumns(Vector2d v1, Vector2d v2){ 
		super.setColumns(v1, v2); 
	}
	
	/**
	 * Set the rows of this matrix equal to the specified vectors, in order.
	 */
	public void setRows(Vector2d v1, Vector2d v2){ 
		super.setRows(v1, v2);
	}
	
	/**
	 * Sets this matrix equal to the transpose of "rhs".
	 */
	public void setAsTranspose(Matrix2d rhs){ super.setAsTranspose(rhs); }
	
	/**
	 * Sets this matrix equal to this + "rhs".
	 * 
	 * @param rhs The matrix to add to this matrix.
	 */
	public void add(Matrix2d rhs){ super.add(rhs); }
	
	/**
	 * Sets this matrix equal to this - "rhs".
	 * 
	 * @param rhs The matrix to subtract from this matrix.
	 */
	public void subtract(Matrix2d rhs){ super.subtract(rhs); }
	
	/**
	 * Sets this matrix equal to "B" * this.
	 * 
	 * @param B The Matrix2d by which to multiply this on the left. 
	 */
	public void multiplyLeft(Matrix2d B){ super.multiplyLeft(B); }
	
	/**
	 * Return new Vector2d equal to "this" * v.
	 */
	public Vector2d apply(Vector2d v){
		Vector2d ret = new Vector2d();
		Matrix.applyMatrix(this, v, ret);
		return ret;
	}
	
	//inverse
	/**
	 * Returns a new Matrix2d equal to the inverse of this matrix. <br>
	 * Does not check that the determinant of this matrix is nonzero.
	 * 
	 * @return The inverse of this matrix.
	 */
	public Matrix2d inverse(){

		//adjugate (transpose of matrix of cofactors)
		Matrix2d adj = new Matrix2d();
		adj.entries[0][0] = entries[1][1];
		adj.entries[0][1] = -entries[0][1];
		adj.entries[1][0] = -entries[1][0];
		adj.entries[1][1] = entries[0][0];
		
		//divide by det (assumed to be nonzero)
		float det = 
			 entries[0][0]*adj.entries[0][0]
			+entries[0][1]*adj.entries[1][0];
		
		adj.scaleEntries(1/det);
		return adj;
	}
	
	//===============================================
	// GL/GLU MODELVIEW MATRICES
	//===============================================
	
	/**
	 * Sets this matrix equal to a rotation matrix; rotates CCW "angle" radians.
	 */
	public void setAsGlRotate(float angle){
		
		float c = (float)Math.cos(angle);
		float s = (float)Math.sin(angle);

		//row 0
		entries[0][0] = c;
		entries[0][1] = -s;
		//row 1
		entries[1][0] = s;
		entries[1][1] = c;
	}
}
