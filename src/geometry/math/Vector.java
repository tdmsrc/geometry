package geometry.math;

/**
 * A vector.  Some basic methods are only visible by using a {@link FixedDimensionalVector};
 * in particular, those methods where a matching dimension is required to avoid an out-of-bounds
 * exception.
 */
public class Vector{

	protected int dimension;
	protected float[] entries;
	
	//===============================================
	// CONSTRUCTORS
	//===============================================
	
	/**
	 * Creates a vector of the specified dimension with all entires 0.
	 * 
	 * @param dimension The dimension of the vector.
	 */
	public Vector(int dimension){ 
		this.dimension = dimension;
		entries = new float[dimension];
	}
	
	//copy constructor
	/**
	 * Creates a vector of the same dimension and with the same components as "rhs".
	 * 
	 * @param rhs The vector to copy.
	 */
	public Vector(Vector rhs){ 
		dimension = rhs.dimension;
		entries = new float[dimension];
		Vector.copyData(rhs, this);
	}
	
	//does NOT check dimensions; make public versions in vector3d/4d to enforce
	/**
	 * Copy the components of "from" to the components of "to". <br>
	 * Does not check that the dimensions of "from" and "to" are equal.
	 */
	protected static void copyData(Vector from, Vector to){
		
		for(int i=0; i<from.dimension; i++){
			to.entries[i] = from.entries[i];
		}
	}

	//===============================================
	// BASIC METHODS
	//===============================================
	
	/**
	 * Return the dimension of this vector.
	 * 
	 * @return The dimension of this vector.
	 */
	public int getDimension(){ return dimension; }
	
	/**
	 * Return the ith component of this vector.<br>
	 * Does not check if "i" is out of bounds.
	 * 
	 * @param i The component of this vector to return.
	 * @return The ith component of this vector.
	 */
	public float get(int i){
		return entries[i];
	}
	
	/**
	 * Set the ith component of this vector to "val".<br>
	 * Does not check if "i" is out of bounds.
	 * 
	 * @param i The component of this vector to set.
	 * @param val The value with which the ith component of this vector is to be replaced.
	 */
	public void set(int i, float val){
		entries[i] = val;
	}

	@Override
	public String toString() {
		String result = new String("");
		result += "(" + entries[0];
		for (int i=1; i<entries.length; i++){ result += ", " + entries[i]; }
		result += ")";
		return result;
	}
	
	//===============================================
	// COMPUTATIONS
	//===============================================
	
	//Methods which don't check dimension, and should have public version made in
	//vector3d/4d:  add, subtract, multiplyComponentWise, dot, applyMatrix

	/**
	 * Sets this vector equal to this + "rhs". <br>
	 * Does not check that the dimensions of this and "rhs" are equal.
	 * 
	 * @param rhs The vector to be added to this vector.
	 */
	protected void add(Vector rhs) {
		for(int i=0; i<dimension; i++){
			entries[i] += rhs.entries[i];
		}
	}

	/**
	 * Sets this vector equal to this - "rhs". <br>
	 * Does not check that the dimensions of this and "rhs" are equal.
	 * 
	 * @param rhs The vector to be subtracted from this vector.
	 */
	protected void subtract(Vector rhs) {
		for(int i=0; i<dimension; i++){
			entries[i] -= rhs.entries[i];
		}
	}

	/**
	 * Scales this vector by "factor".
	 * 
	 * @param factor The factor by which to scale this vector.
	 */
	public void scale(float factor) {
		for(int i=0; i<dimension; i++){
			entries[i] *= factor;
		}
	}

	/**
	 * Sets the ith component of this vector equal to this_i * rhs_i. <br>
	 * Does not check that the dimensions of this and "rhs" are equal.
	 * 
	 * @param rhs The vector whose components are to multiply this vector's components.
	 */
	protected void multiplyComponentWise(Vector rhs){
		for(int i=0; i<dimension; i++){
			entries[i] *= rhs.entries[i];
		}
	}

	/**
	 * Sets the ith component of this vector equal to this_i / rhs_i. <br>
	 * Does not check that the dimensions of this and "rhs" are equal.
	 * 
	 * @param rhs The vector whose components are to divide this vector's components.
	 */
	protected void divideComponentWise(Vector rhs){
		for(int i=0; i<dimension; i++){
			entries[i] /= rhs.entries[i];
		}
	}

	/**
	 * Returns the product of all the components of this vector.
	 * 
	 * @return The product of all components of this vector.
	 */
	public float prod(){
		float ret = 1;
		for(int i=0; i<dimension; i++){
			ret *= entries[i];
		}
		return ret;
	}

	/**
	 * Returns the sum of all the components of this vector.
	 * 
	 * @return The sum of all the components of this vector.
	 */
	public float sum(){
		float ret = 0;
		for(int i=0; i<dimension; i++){
			ret += entries[i];
		}
		return ret;
	}

	/**
	 * Returns the dot product of this vector with "b". <br>
	 * Does not check that the dimensions of this and "b" are equal.
	 *
	 * @return The dot product of this and "b".
	 */
	protected float dot(Vector b) {
		float result = 0;
		for (int i = 0; i < dimension; i++) {
			result += entries[i] * b.entries[i];
		}
		return result;
	}

	/**
	 * Returns the sum of squares of the components of this vector.
	 * 
	 * @return The sum of squares of the components of this vector.
	 */
	public float lengthSquared(){ return dot(this); }

	//return |this|
	/**
	 * Returns the length of this vector.  In particular, returns the square
	 * root of {@link #lengthSquared}.
	 * 
	 * @return The length of this vector.
	 */
	public float length(){ return (float)Math.sqrt(lengthSquared()); }
	

	/**
	 * Returns the sum of squares of the components of the vector (this - "b").<br>
	 * Does not check that the dimensions of this and "b" are equal.
	 * 
	 * @return The sum of squares of the components of the vector (this - "b").
	 */
	protected float distanceSquared(Vector b){
		
		float result = 0;
		for(int i=0; i<dimension; i++){
			float di = entries[i]-b.entries[i];
			result += di*di;
		}
		return result;
	}

	/**
	 * Returns the distance between this vector and "b".  In particular, returns
	 * the square root of the result returned by {@link #distanceSquared} with argument "b".<br>
	 * Does not check that the dimensions of this and "b" are equal.
	 * 
	 * @return The distance between this vector and "b".
	 */
	protected float distance(Vector b){
		return (float)Math.sqrt(distanceSquared(b));
	}

	/**
	 * Normalizes this vector.  In particular, scales this vector by the reciprocal of
	 * the value returned by {@link #length}.
	 */
	public void normalize(){ scale(1 / length()); }
	
	/**
	 * Flips the direction of this vector.  In particular, sets this_i = -this_i.
	 */
	public void flip(){ scale(-1); }
}
