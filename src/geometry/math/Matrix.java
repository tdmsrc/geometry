package geometry.math;

/**
 * A matrix of floats.  Some basic methods are only visible by using a dimension-specific
 * Matrix class; in particular, those methods where a matching dimension is required to be 
 * well-defined or to avoid an out-of-bounds exception.
 */
public class Matrix{
	
	//entires[i][j] means ith row, jth column
	protected float[][] entries;
	protected int width, height;
	
	//===============================================
	// CONSTRUCTORS
	//===============================================
	
	/**
	 * Creates a new Matrix with "width" columns and "height" rows, with 
	 * all entries equal to 0.
	 * 
	 * @param width The number of columns.
	 * @param height The number of rows.
	 */
	public Matrix(int width, int height){
		this.width = width;
		this.height = height;
		entries = new float[height][width];
	}
	
	/**
	 * Returns a new Matrix with the same dimensions and entries as "rhs".
	 * 
	 * @param rhs The Matrix to copy.
	 */
	public Matrix(Matrix rhs){
		width = rhs.width;
		height = rhs.height;
		entries = new float[height][width];
		Matrix.copyData(rhs, this);
	}
	
	//copy data from one matrix to another
	//does NOT check dimensions; make public versions in matrix3d/4d to enforce
	/**
	 * Copy the components of "from" to the components of "to". <br>
	 * Does not check that the dimensions of "from" and "to" are equal.
	 */
	protected static void copyData(Matrix from, Matrix to){
		
		for(int i=0; i<from.height; i++){
		for(int j=0; j<from.width; j++){
			to.entries[i][j] = from.entries[i][j]; 
		}}
	}
	
	//===============================================
	// BASIC METHODS
	//===============================================
	
	@Override
	public String toString() {
		String result = new String("");
		for(int i=0; i<height; i++){
			result += "[" + entries[i][0];
			for (int j=1; j<width; j++){ result += ", " + entries[i][j]; }
			result += "]";
		}
		return result;
	}
	
	/**
	 * Get the number of columns in this matrix.
	 * 
	 * @return The number of columns in this matrix.
	 */
	public int getWidth(){ return width; }
	
	/**
	 * Get the number of rows in this matrix.
	 * 
	 * @return The number of rows in this matrix.
	 */
	public int getHeight(){ return height; }
	
	/**
	 * Return the entry of this matrix in row i and column j.<br>
	 * Does not check if indices are out of bounds.
	 * 
	 * @return The entry of this matrix in row i and column j.
	 */
	public float get(int i, int j){
		return entries[i][j];
	}
	
	/**
	 * Set the entry of this matrix in row i and column j to "val".<br>
	 * Does not check if indices are out of bounds.
	 */
	public void set(int i, int j, float val){
		entries[i][j] = val;
	}
	
	/**
	 * Multiply all entries of this matrix by "s".
	 * 
	 * @param s The value by which to multiply all entries of this matrix.
	 */
	public void scaleEntries(float s){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] *= s;
		}}
	}
	
	/**
	 * Sets this matrix equal to the transpose of "rhs". <br>
	 * Assumes that this matrix has width equal to the height of "rhs"
	 * and vice versa, but does not check.
	 */
	protected void setAsTranspose(Matrix rhs){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] = rhs.entries[j][i];
		}}
	}
	
	/**
	 * Sets the columns of this matrix equal to the specified vectors, in order.
	 * <br><br>
	 * Assumes, but does not check, that the number of arguments is equal to the
	 * width of this matrix, and the dimension of the arguments is equal to the
	 * height of this matrix.
	 */
	protected <V extends FixedDimensionalVector<V>> void setColumns(V...v){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] = v[j].get(i);
		}}
	}
	
	/**
	 * Sets the rows of this matrix equal to the specified vectors, in order.
	 * <br><br>
	 * Assumes, but does not check, that the number of arguments is equal to the
	 * height of this matrix, and the dimension of the arguments is equal to the
	 * width of this matrix.
	 */
	protected <V extends FixedDimensionalVector<V>> void setRows(V...v){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] = v[i].get(j);
		}}
	}
	

	/**
	 * Set this matrix to the tensor product of u and v (M_ij = u_i v_j).
	 * <br><br>
	 * Assumes, but does not check, that the dimension of u is the height of this matrix
	 * and that the dimension of v is the width of this matrix.
	 */
	public void setAsTensorProduct(Vector u, Vector v){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] = u.get(i)*v.get(j);
		}}
	}
	
	/**
	 * Set this matrix to the symmetric product of u and v (M_ij = u_i v_j + v_i u_j).
	 * <br>
	 * Note that there is no division by 2.
	 * <br><br>
	 * Assumes, but does not check, that this matrix is square of the same dimension
	 * as both u and v.
	 */
	public void setAsSymmetricProduct(Vector u, Vector v){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] = u.get(i)*v.get(j) + v.get(i)*u.get(j);
		}}
	}
	
	//===============================================
	// ADDITION/MULTIPLICATION
	// in general these don't check dimension; make public versions in Matrix3d/4d
	//===============================================

	/**
	 * Sets this matrix equal to this + "rhs".<br>
	 * Does not check that the dimensions are compatible.
	 * 
	 * @param rhs The matrix to be added to this matrix.
	 */
	protected void add(Matrix rhs){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] += rhs.entries[i][j];
		}}
	}
	
	/**
	 * Sets this matrix equal to this - "rhs".<br>
	 * Does not check that the dimensions are compatible.
	 * 
	 * @param rhs The matrix to be subtracted from this matrix.
	 */
	protected void subtract(Matrix rhs){
		
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			entries[i][j] -= rhs.entries[i][j];
		}}
	}
	
	/**
	 * Returns a new Matrix equal to the product lhs * rhs.<br>
	 * Does not check that the dimensions are compatible.
	 * 
	 * @return The product lhs * rhs.
	 */
	protected static Matrix multiply(Matrix lhs, Matrix rhs){

		Matrix ret = new Matrix(rhs.width, lhs.height);
		
		for(int i=0; i<lhs.height; i++){
		for(int j=0; j<rhs.width; j++){
			
			float val = 0;
			for(int k=0; k<lhs.width; k++){
				val += lhs.entries[i][k] * rhs.entries[k][j];
			}
			ret.entries[i][j] = val;
		}}
		
		return ret;
	}
	
	/**
	 * Sets this matrix equal to "B" * this. <br>
	 * Does not check that the dimensions are compatible.
	 * 
	 * @param B The matrix by which to multiply this on the left.
	 */
	protected void multiplyLeft(Matrix B){
		
		//compute B*this
		Matrix temp = Matrix.multiply(B, this);
		this.width = temp.width;
		this.height = temp.height;
		this.entries = temp.entries;
	}
	
	/**
	 * Sets "result" equal to A*v. <br>
	 * Does not check that the dimensions are compatible.
	 */
	protected static void applyMatrix(Matrix A, Vector v, Vector result){

		for(int i=0; i<A.height; i++){
			float val = 0;
			for(int j=0; j<A.width; j++){
				val += A.entries[i][j] * v.entries[j];
			}
			result.entries[i] = val;
		}
	}
	
	/**
	 * Sets "result" equal to unAugment(A * augment(v,w)). <br>
	 * Does not check that the dimensions are compatible.
	 */
	protected static void applyMatrixProjective(Matrix A, Vector v, float w, Vector result){

		for(int i=0; i<A.height-1; i++){
			float val = A.entries[i][A.width-1] * w;
			for(int j=0; j<A.width-1; j++){
				val += A.entries[i][j] * v.entries[j];
			}
			result.entries[i] = val;
		}
		
		float outW = A.entries[A.height-1][A.width-1] * w;
		for(int j=0; j<A.width-1; j++){
			outW += A.entries[A.height-1][j] * v.entries[j];
		}
		
		for(int i=0; i<A.height-1; i++){
			result.entries[i] /= outW;
		}
	}

	//===============================================
	// SERIALIZATION
	//===============================================
	
	/**
	 * Fills fArray with the entries of this matrix. Assumes fArray is already
	 * allocated and can hold the required number of entries.
	 * <br>
	 * The entries are indexed so that, e.g., the first this.width entries of 
	 * the returned array constitute the first row of this matrix, the
	 * next this.width entries of the returned array constitute the second
	 * row of this matrix, etc.
	 * 
	 * @param fArray The array into which the matrix entries are written. 
	 */
	public void serializeRowMajor(float[] fArray){

		int k=0;
		for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			fArray[k] = entries[i][j];
			k++;
		}}
	}
	
	/**
	 * Fills fArray with the entries of this matrix. Assumes fArray is already
	 * allocated and can hold the required number of entries.
	 * <br>
	 * The entries are indexed so that, e.g., the first this.height entries of 
	 * the returned array constitute the first column of this matrix, the
	 * next this.height entries of the returned array constitute the second
	 * column of this matrix, etc.
	 * 
	 * @param fArray The array into which the matrix entries are written.
	 */
	public void serializeColumnMajor(float[] fArray){

		int k=0;
		for(int j=0; j<width; j++){
		for(int i=0; i<height; i++){
			fArray[k] = entries[i][j];
			k++;
		}}
	}
	
	//===============================================
	// ROTATION
	//===============================================
	
	/**
	 * Sets the upper 3x3 block of this matrix equal to the rotation
	 * matrix rotating CCW "angle" radians about "axis" (i.e., with
	 * the vector "axis" pointing at you).
	 * 
	 * @param axis The axis around which to rotate.  Gets normalized.
	 * @param angle The angle, in radians, to rotate CCW around "axis".
	 */
	protected void setUpper3x3AsRotation(Vector3d axis, float angle){
		
		//[24 mults]
		//general formula ("Rodrigues formula"): v -> (v.n)n + (v-(v.n)n)c - (vxn)s
		//in matrix form: (1-c)[n*n^T] + cI - s[[0,n3,-n2][-n3,0,n1][n2,-n1,0]]
		//could also do Matrix3d r = (new Rotation3d(axis, angle)).getMatrix()
		//and read off entries, but this is (slightly) more efficient. 
		
		float c = (float)Math.cos(angle);
		float s = (float)Math.sin(angle);
		Vector3d n = new Vector3d(axis);
		n.normalize();
		
		//row 0
		entries[0][0] = (1-c)*n.getX()*n.getX() + c;
		entries[0][1] = (1-c)*n.getX()*n.getY() - s*n.getZ();
		entries[0][2] = (1-c)*n.getX()*n.getZ() + s*n.getY();
		//row 1
		entries[1][0] = (1-c)*n.getY()*n.getX() + s*n.getZ();
		entries[1][1] = (1-c)*n.getY()*n.getY() + c;
		entries[1][2] = (1-c)*n.getY()*n.getZ() - s*n.getX();
		//row 2
		entries[2][0] = (1-c)*n.getZ()*n.getX() - s*n.getY();
		entries[2][1] = (1-c)*n.getZ()*n.getY() + s*n.getX();
		entries[2][2] = (1-c)*n.getZ()*n.getZ() + c;
	}
}
