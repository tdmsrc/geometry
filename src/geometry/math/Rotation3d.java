package geometry.math;


//The quaternion is the principal means of defining the rotation;
//the other ways of representing rotation (angle/axis, matrix, basis) are derived from that.
//(quaternions better than matrix for serialization, slerping, and maintaining orthogonality)
//The matrices and basis are computed automatically; axis/angle lazily computed.

/**
 * An instance of this class represents an element of SO(3). <br>
 * It maintains:
 * <ul>
 * <li> The corresponding quaternion,
 * <li> The axis and angle of rotation,
 * <li> A 3x3 matrix and its inverse (transpose),
 * <li> A positively-oriented orthonormal basis <br>(i.e., the matrix applied to the standard basis).
 * </ul>
 * It can be efficiently left-multiplied by another Rotation3d (or equivalent data). <br>
 * It also provides a slerp method (interpolate between rotations R and S via a path contained in SO(3)).
 */
public class Rotation3d{

	//quaternion representing this transformation (always valid)
	private Vector4d quaternion; //<-ensure length is 1 (i.e., is a "versor")
	
	//rotation vector
	private boolean axisAngleValid;
	private Vector3d axis;
	private float angle;
	
	//matrix representing this transformation, and its inverse (transpose)
	private Matrix3d matrix, matrixInverse;
	
	//rotation applied to the standard basis (bi = rotate(ei))
	private Vector3d[] basis = new Vector3d[3];
	
	
	/**
	 * Construct an identity rotation.
	 */
	public Rotation3d(){
		
		//initialize axis and angle to represent identity rotation
		quaternion = new Vector4d(0, 0, 0, 1);
		
		initializeData();
		invalidateDerived();
	}
	
	/**
	 * Construct the rotation given by the specified quaternion.
	 */
	public Rotation3d(Vector4d quaternion){
		
		//initialize to specified quaternion
		this.quaternion = quaternion;
		
		initializeData();
		invalidateDerived();
	}
	
	/**
	 * Construct the rotation CCW "angle" radians about "axis" (i.e., with
	 * the vector "axis" pointing at you).
	 */
	public Rotation3d(Vector3d axis, float angle){
		
		//initialize to rotation using specified axis and angle
		this.quaternion = quatFromAxisAngle(axis, angle);
		
		initializeData();
		invalidateDerived();
	}
	
	/**
	 * Copy constructor.
	 */
	public Rotation3d(Rotation3d rhs){
		
		quaternion = rhs.quaternion.copy();
		
		initializeData();
		invalidateDerived();
	}
	
	/**
	 * Return a copy of this Rotation3d.
	 */
	public Rotation3d copy(){
		return new Rotation3d(this);
	}

	/**
	 * Set this Rotation3d to have the same data as "rhs".
	 */
	public void setEqualTo(Rotation3d rhs){
		
		this.quaternion = rhs.quaternion;
		
		invalidateDerived();
	}
	
	
	private void initializeData(){

		//create new objects
		matrix = new Matrix3d();
		matrixInverse = new Matrix3d();
		
		axis = new Vector3d();
		
		basis[0] = new Vector3d();
		basis[1] = new Vector3d();
		basis[2] = new Vector3d();
	}
	
	private void invalidateDerived(){
		
		//should be called whenever quaternion changes, and other data is to be recomputed
		axisAngleValid = false;
		
		computeRotatedBasis();
		computeMatrix();
	}
	
	//-----------------------------------------
	// ACCESSORS
	//-----------------------------------------
	
	/**
	 * Get the quaternion representing this rotation.
	 * <br>
	 * (A Vector4d (b,c,d,a) represents the quaternion a+bi+cj+dk).
	 */
	public Vector4d getQuaternion(){ return quaternion; }
	
	//axis-angle representation
	/**
	 * Get the axis of rotation (i.e., the vector which is fixed by this
	 * rotation, and such that if the vector is pointing at you, the rotation
	 * is CCW about the axis).
	 * 
	 * @see #getRotationAngle()
	 */
	public Vector3d getRotationAxis(){
		if(!axisAngleValid){ computeAxisAngle(); }
		
		return axis;
	}
	
	/**
	 * Get the angle of rotation about the rotation axis.
	 * 
	 * @see #getRotationAxis()
	 */
	public float getRotationAngle(){
		if(!axisAngleValid){ computeAxisAngle(); }
		
		return angle;
	}
	
	//matrix representation
	/**
	 * Get the Matrix3d representing this rotation.
	 * 
	 * @see #getMatrixInverse()
	 */
	public Matrix3d getMatrix(){ return matrix; }
	
	/**
	 * Get the Matrix3d representing the inverse of this rotation
	 * (i.e., the transpose of the matrix representing this rotation).
	 * 
	 * @see #getMatrix()
	 */
	public Matrix3d getMatrixInverse(){ return matrixInverse; }
	
	//rotation applied to standard basis
	/**
	 * Get the result of this rotation applied to the 
	 * standard basis vector E1 = (1,0,0).
	 */
	public Vector3d getRotatedE1(){ return basis[0]; }
	
	/**
	 * Get the result of this rotation applied to the 
	 * standard basis vector E2 = (0,1,0).
	 */
	public Vector3d getRotatedE2(){ return basis[1]; }
	
	/**
	 * Get the result of this rotation applied to the 
	 * standard basis vector E3 = (0,0,1).
	 */
	public Vector3d getRotatedE3(){ return basis[2]; }
	
	
	//-----------------------------------------
	// CONVERSION/UPDATING
	//-----------------------------------------
	
	/**
	 * Compute the quaternion representing the rotation CCW "angle" radians 
	 * about "axis" (i.e., with the vector "axis" pointing at you).
	 * <br>
	 * (A Vector4d (b,c,d,a) represents the quaternion a+bi+cj+dk).
	 * 
	 * @param axis The axis around which to rotate.  Gets normalized.
	 * @param angle The angle, in radians, to rotate CCW around "axis".
	 */
	public static Vector4d quatFromAxisAngle(Vector3d axis, float angle){
		
		//general formula: q = cos(t/2) + (nxi + nyj + nzk)sin(t/2) (n = normalized axis)
		
		float c = (float)Math.cos(angle*0.5f);
		float s = (float)Math.sin(angle*0.5f);
		Vector3d n = new Vector3d(axis);
		n.normalize();
		
		return new Vector4d(n.getX()*s, n.getY()*s, n.getZ()*s, c);
	}
	
	/**
	 * Recompute axis and angle. 
	 * <br>
	 * (This is only computed if the data is requested and axisAngleValid is false)
	 */
	private void computeAxisAngle(){
		
		//see "general formula" in quatFromAxisAngle
		//(note this requires an inverse cos and a normalization)
		
		angle = 2*(float)Math.acos(quaternion.getW());
		
		axis.setX(quaternion.getX());
		axis.setY(quaternion.getY());
		axis.setZ(quaternion.getZ());
		axis.normalize();
		
		axisAngleValid = true;
	}
	
	/**
	 * Recompute matrix and matrixInverse.
	 */
	private void computeMatrix(){
		
		matrix.setColumns(getRotatedE1(), getRotatedE2(), getRotatedE3());
		matrixInverse.setAsTranspose(matrix);
	}
	
	/**
	 * Recompute basis[0], basis[1], basis[2].
	 */
	private void computeRotatedBasis(){
		
		//[27 mults]
		float a = quaternion.entries[3], b = quaternion.entries[0], c = quaternion.entries[1], d = quaternion.entries[2];
		
		basis[0].setX(1.0f-2*(c*c+d*d)); //= a*a+b*b-c*c-d*d for unit q
		basis[0].setY(2*(b*c+a*d));
		basis[0].setZ(2*(b*d-c*a));
		
		basis[1].setX(2*(b*c-a*d));
		basis[1].setY(1.0f-2*(b*b+d*d)); //= a*a-b*b+c*c-d*d for unit q
		basis[1].setZ(2*(c*d+a*b));
		
		basis[2].setX(2*(b*d+a*c));
		basis[2].setY(2*(c*d-a*b));
		basis[2].setZ(1.0f-2*(b*b+c*c)); //= a*a-b*b-c*c+d*d for unit q
	}
	
	
	//-----------------------------------------
	// ROTATION (LEFT-MULTIPLICATION)
	//-----------------------------------------
	
	/**
	 * Set this rotation equal to this rotation followed by
	 * the rotation specified by the quaternion q.
	 * <br>
	 * (A Vector4d (b,c,d,a) represents the quaternion a+bi+cj+dk).
	 */
	public void leftMultiply(Vector4d q){
		
		this.quaternion = q.quatMultiply(this.quaternion);
		this.quaternion.normalize();
		
		invalidateDerived();
	}
	
	/**
	 * Set this rotation equal to this rotation followed by "r".
	 */
	public void leftMultiply(Rotation3d r){
		
		leftMultiply(r.quaternion);
	}
	
	/**
	 * Set this rotation equal to this rotation followed by the inverse of "r".
	 */
	public void leftMultiplyByInverse(Rotation3d r){
		
		leftMultiply(r.quaternion.quatInverse());
	}
	
	/**
	 * Set this rotation equal to this rotation followed by
	 * the rotation CCW "angle" radians about "axis" 
	 * (i.e., with the vector "axis" pointing at you).
	 */
	public void leftMultiply(Vector3d axis, float angle){
		
		leftMultiply(quatFromAxisAngle(axis,angle));
	}
	
	
	//-----------------------------------------
	// SLERP
	//-----------------------------------------

	/**
	 * Return the vector normalize(p.quaternion.lerp(q.quaternion, t)).
	 * 
	 * @see Vector4d#lerp(Vector4d, float)
	 * @see #setAsSlerp(Rotation3d, Rotation3d, float)
	 */
	public static Vector4d getSlerpQuaternion(Rotation3d p, Rotation3d q, float t){
		
		Vector4d ret = p.quaternion.lerp(q.quaternion, t);
		ret.normalize();
		return ret;
	}
	
	/**
	 * Interpolate between Rotation3ds p and q.  <br>
	 * In particular, sets this Rotation3d to have:
	 * <ul>
	 * <li>this.quaternion = normalize(p.quaternion.lerp(q.quaternion, t)).
	 * </ul>
	 * Note that at t=0, the result will be p, and at t=1, the result will be q.
	 * 
	 * @param p The Rotation3d at t=0.
	 * @param q The Rotation3d at t=1.
	 * @param t The interpolation parameter.
	 * @see Vector4d#lerp(Vector4d, float)
	 */
	public void setAsSlerp(Rotation3d p, Rotation3d q, float t){
		
		//[TODO] this isn't good, do it a better way
		
		this.quaternion = getSlerpQuaternion(p,q,t);
		
		invalidateDerived();
	}
}
