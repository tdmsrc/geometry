package geometry.math;

/**
 * 3d-specific version of {@link FixedDimensionalVector}.
 */
public class Vector3d extends FixedDimensionalVector<Vector3d>{

	/**
	 * Creates a Vector3d with all entries 0.
	 */
	public Vector3d(){
		super(3);
	}
	
	/**
	 * Creates a Vector3d with the specified components.
	 */
	public Vector3d(float x, float y, float z){
		super(3);
		entries[0] = x;
		entries[1] = y;
		entries[2] = z;
	}
	
	/**
	 * Creates a new Vector3d with the same components as "rhs".
	 * 
	 * @param rhs The Vector3d to copy.
	 */
	public Vector3d(Vector3d rhs){
		super(3);
		Vector.copyData(rhs, this);
	}
	
	/**
	 * Returns a new Vector3d with the same components as this.
	 */
	public Vector3d copy(){
		return new Vector3d(this);
	}

	//accessors
	public float getX(){ return entries[0]; }
	public float getY(){ return entries[1]; }
	public float getZ(){ return entries[2]; }
	
	public void setX(float x){ entries[0] = x; }
	public void setY(float y){ entries[1] = y; }
	public void setZ(float z){ entries[2] = z; }
	
	//augment to 4d and un-augment to 2d
	/**
	 * Returns a {@link Vector4d} equal to (this_x, this_y, this_z, "w").
	 * 
	 * @see {@link Vector4d#unAugment()}
	 */
	public Vector4d augment(float w){ 
		Vector4d ret = new Vector4d();
		for(int i=0; i<3; i++){ ret.entries[i] = entries[i]; }
		ret.entries[3] = w;
		return ret;
	}
	
	/**
	 * Returns a new {@link Vector2d} with ith component equal to this_i / this_w.
	 * 
	 * @see {@link Vector2d#augment(float)}
	 */
	public Vector2d unAugment(){
		Vector2d ret = new Vector2d();
		for(int i=0; i<2; i++){ ret.entries[i] = entries[i] / entries[2]; }
		return ret;
	}

	//special methods
	/**
	 * Creates and returns a new Vector3d equal to this X rhs.
	 */
	public Vector3d cross(Vector3d rhs){
		
		return new Vector3d(
			 entries[1] * rhs.entries[2] - entries[2] * rhs.entries[1],
			-entries[0] * rhs.entries[2] + entries[2] * rhs.entries[0],
			 entries[0] * rhs.entries[1] - entries[1] * rhs.entries[0]
		);
	}
	
	/**
	 * Creates and returns a new Vector3d equal to coeff1*basis1 + coeff2*basis2.
	 */
	public static Vector3d linearCombination(float coeff1, Vector3d basis1, float coeff2, Vector3d basis2){
	
		return new Vector3d(
			basis1.entries[0] * coeff1 + basis2.entries[0] * coeff2,
			basis1.entries[1] * coeff1 + basis2.entries[1] * coeff2,
			basis1.entries[2] * coeff1 + basis2.entries[2] * coeff2
		);
	}
	
	//some number a little bigger than 1/sqrt(3)
	private static final float M = 0.6f;
	
	/**
	 * Returns a new Vector3d of unit length which is orthogonal to this one.
	 */
	public Vector3d getOrthogonalVector(){
		//one component of v must have abs value <= |v|/sqrt(3)
		//for better conditioning, if e.g. abs(x) <= |v|/sqrt(3), cross with (1,0,0), etc
		
		Vector3d ret;
		float cmp = M * length();
		
		if((entries[0] > -cmp) && (entries[0] < cmp)){  
			ret = new Vector3d(0, entries[2], -entries[1]); 
		}else if((entries[1] > -cmp) && (entries[1] < cmp)){ 
			ret = new Vector3d(-entries[2], 0, entries[0]); 
		}else{ 
			ret = new Vector3d(entries[1], -entries[0], 0); 
		}
		
		ret.normalize();
		return ret;
	}
}
