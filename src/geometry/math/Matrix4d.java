package geometry.math;

/**
 * A 4x4 {@link Matrix}.
 */
public class Matrix4d extends Matrix{

	private static final float DEG_TO_RAD = (float)(Math.PI / 180.0);
	
	/**
	 * Creates a new Matrix4d with all entries equal to 0.
	 */
	public Matrix4d(){
		super(4,4);
	}
	
	/**
	 * Creates a new Matrix4d with the same entries as "rhs".
	 * 
	 * @param rhs The Matrix4d to copy.
	 */
	public Matrix4d(Matrix4d rhs){
		super(4,4);
		Matrix.copyData(rhs, this);
	}
	
	/**
	 * Creates a new Matrix4d with the same entries as this matrix.
	 */
	public Matrix4d copy(){
		return new Matrix4d(this);
	}
	
	/**
	 * Creates a new Matrix4d equal to the 4x4 identity matrix.
	 */
	public static Matrix4d createIdentityMatrix(){
		Matrix4d id = new Matrix4d();
		id.entries[0][0] = 1;
		id.entries[1][1] = 1;
		id.entries[2][2] = 1;
		id.entries[3][3] = 1;
		return id;
	}
	
	/**
	 * Sets this matrix equal to the 4x4 identity matrix. 
	 */
	public void setAsIdentity(){
		
		entries[0][0] = 1; entries[0][1] = 0; entries[0][2] = 0; entries[0][3] = 0;
		entries[1][0] = 0; entries[1][1] = 1; entries[1][2] = 0; entries[1][3] = 0;
		entries[2][0] = 0; entries[2][1] = 0; entries[2][2] = 1; entries[2][3] = 0;
		entries[3][0] = 0; entries[3][1] = 0; entries[3][2] = 0; entries[3][3] = 1;
	}

	//public methods with correct dimensions enforced
	/**
	 * Set the columns of this matrix equal to the specified vectors, in order.
	 */
	public void setColumns(Vector4d v1, Vector4d v2, Vector4d v3, Vector4d v4){ 
		super.setColumns(v1, v2, v3, v4); 
	}
	
	/**
	 * Set the rows of this matrix equal to the specified vectors, in order.
	 */
	public void setRows(Vector4d v1, Vector4d v2, Vector4d v3, Vector4d v4){ 
		super.setRows(v1, v2, v3, v4);
	}
	
	/**
	 * Sets this matrix equal to the transpose of "rhs".
	 */
	public void setAsTranspose(Matrix4d rhs){ super.setAsTranspose(rhs); }
	
	/**
	 * Sets this matrix equal to this + "rhs".
	 * 
	 * @param rhs The matrix to add to this matrix.
	 */
	public void add(Matrix4d rhs){ super.add(rhs); }
	
	/**
	 * Sets this matrix equal to this - "rhs".
	 * 
	 * @param rhs The matrix to subtract from this matrix.
	 */
	public void subtract(Matrix4d rhs){ super.subtract(rhs); }
	
	/**
	 * Sets this matrix equal to "B" * this.
	 * 
	 * @param B The Matrix4d by which to multiply this on the left. 
	 */
	public void multiplyLeft(Matrix4d B){ super.multiplyLeft(B); }
	
	/**
	 * Return new Vector4d equal to "this" * v.
	 */
	public Vector4d apply(Vector4d v){
		Vector4d ret = new Vector4d();
		Matrix.applyMatrix(this, v, ret);
		return ret;
	}
	
	/**
	 * Return result equal to unAugment("this" * augment(v,w)).
	 * 
	 * @see {@link Vector3d#augment(float)}
	 * @see {@link Vector4d#unAugment()}
	 */
	public Vector3d applyProjective(Vector3d v, float w){
		Vector3d ret = new Vector3d();
		Matrix.applyMatrixProjective(this, v, w, ret);
		return ret;
	}
	
	/**
	 * Returns (-1)^(i+j) times the determinant of the 3x3 matrix that remains
	 * after removing the ith row and jth column of this matrix.
	 */
	private float cofactor(int i, int j){
		
		int i0,i1,i2, j0,j1,j2;
		
		switch(i){
			case 0: i0=1; i1=2; i2=3; break;
			case 1: i0=0; i1=2; i2=3; break;
			case 2: i0=0; i1=1; i2=3; break;
			default: i0=0; i1=1; i2=2; break;
		}
		
		switch(j){
			case 0: j0=1; j1=2; j2=3; break;
			case 1: j0=0; j1=2; j2=3; break;
			case 2: j0=0; j1=1; j2=3; break;
			default: j0=0; j1=1; j2=2; break;
		}

		float minor = 
			 entries[i0][j0]*(entries[i1][j1]*entries[i2][j2]-entries[i1][j2]*entries[i2][j1])
			-entries[i0][j1]*(entries[i1][j0]*entries[i2][j2]-entries[i1][j2]*entries[i2][j0])
			+entries[i0][j2]*(entries[i1][j0]*entries[i2][j1]-entries[i1][j1]*entries[i2][j0]);
		
		if(((i+j)%2)==0){ return minor; }else{ return -minor; }
	}
	
	/**
	 * Returns a new Matrix4d equal to the inverse of this matrix. <br>
	 * Does not check that the determinant of this matrix is nonzero.
	 * 
	 * @return The inverse of this matrix.
	 */
	public Matrix4d inverse(){
		
		//adjugate (transpose of matrix of cofactors)
		Matrix4d adj = new Matrix4d();
		for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			adj.entries[i][j] = cofactor(j,i);
		}}
		
		//divide by det (assumed to be nonzero)
		float det = 
			 entries[0][0]*adj.entries[0][0]
			+entries[0][1]*adj.entries[1][0]
			+entries[0][2]*adj.entries[2][0]
			+entries[0][3]*adj.entries[3][0];
		
		adj.scaleEntries(1/det);
		return adj;
	}
	
	/**
	 * For use with a matrix which has bottom row (0,0,0,1).<br>
	 * Has the same result as setting this = this * translate(v).
	 */
	public void preTranslate(Vector3d v){
		
		float mvx = entries[0][0]*v.getX() + entries[0][1]*v.getY() + entries[0][2]*v.getZ();
		float mvy = entries[1][0]*v.getX() + entries[1][1]*v.getY() + entries[1][2]*v.getZ();
		float mvz = entries[2][0]*v.getX() + entries[2][1]*v.getY() + entries[2][2]*v.getZ();
		
		entries[0][3] += mvx;
		entries[1][3] += mvy;
		entries[2][3] += mvz;
	}
	
	//===============================================
	// GL/GLU PERSPECTIVE MATRICES
	// see e.g. http://www.songho.ca/opengl/gl_transform.html
	//===============================================
	
	/**
	 * Imitates the corresponding OpenGL method mapping eye coordinates to clip coordinates
	 * (i.e., a Projection matrix).
	 * <br><br>
	 * This matrix provides a perspective projection which maps the frustum described
	 * below into the box [-1,1]^3; anything outside of this box is clipped by OpenGL.
	 * <br><br>
	 * First consider the XY-aligned rectangle in the z=-n plane with minimum and maximum 
	 * (x,y) given by (l,b) and (r,t), respectively.
	 * The frustum is the set of all points on some line through the origin and a point in 
	 * that rectangle, with z value in the range [-f,-n].
	 * 
	 * @return The Matrix4d mapping the frustum described above to [-1,1]^3.
	 * @see {@link #setAsGluPerspective(float, float, float, float)}
	 */
	public void setAsGlFrustum(float l, float r, float b, float t, float n, float f){

		//row 0
		entries[0][0] = 2*n/(r-l);
		entries[0][1] = 0;
		entries[0][2] = (r+l)/(r-l);
		entries[0][3] = 0;
		//row 1
		entries[1][0] = 0;
		entries[1][1] = 2*n/(t-b);
		entries[1][2] = (t+b)/(t-b);
		entries[1][3] = 0;
		//row 2
		entries[2][0] = 0;
		entries[2][1] = 0;
		entries[2][2] = -(f+n)/(f-n);
		entries[2][3] = -2*f*n/(f-n);
		//row 3
		entries[3][0] = 0;
		entries[3][1] = 0;
		entries[3][2] = -1;
		entries[3][3] = 0;
	}
	
	/**
	 * Imitates the corresponding OpenGL method mapping eye coordinates to clip coordinates
	 * (i.e., a Projection matrix).
	 * <br><br>
	 * This matrix provides a perspective projection which maps the frustum described
	 * below into the box [-1,1]^3; anything outside of this box is clipped by OpenGL.
	 * <br><br>
	 * First consider the XY-aligned rectangle in the z=-zFront plane with width = height*aspect,
	 * and such that the angle formed between the planes passing through the origin
	 * and the top and bottom edges of the rectangle is fovy.
	 * The frustum is the set of all points on some line through the origin and a point in 
	 * that rectangle, with z value in the range [-f,-n].

	 * @return The Matrix4d mapping the frustum described above to [-1,1]^3.<br>
	 * In particular, returns glFrustum(-h*aspect, h*aspect, -h, h, zFront, zBack), where
	 * h = zFront*tan(fovy/2).
	 * @see {@link #setAsGlFrustum(float, float, float, float, float, float)}
	 * @see {@link Camera}
	 */
	public void setAsGluPerspective(float fovy, float aspect, float zFront, float zBack){
		
		float tan = (float)Math.tan(DEG_TO_RAD * fovy * 0.5f);
		float h = zFront * tan;
		float w = h * aspect;
		setAsGlFrustum(-w, w, -h, h, zFront, zBack);
	}
	
	/**
	 * Imitates the corresponding OpenGL method mapping eye coordinates to clip coordinates
	 * (i.e., a Projection matrix).
	 * <br><br>
	 * This matrix provides an orthogonal projection which maps the box described
	 * below into the box [-1,1]^3; anything outside of this box is clipped by OpenGL.
	 * <br><br>
	 * First consider the XY-aligned rectangle in the z=-n plane with minimum and maximum 
	 * (x,y) given by (l,b) and (r,t), respectively.  
	 * The frustum is the set of all points on some line parallel to the z-axis passing 
	 * through that rectangle, with z value in the range [-f,-n].
	 * 
	 * @return The Matrix4d mapping the frustum described above to [-1,1]^3.
	 * @see {@link #setAsGluOrtho2D(float, float, float, float)}
	 */
	public void setAsGlOrtho(float l, float r, float b, float t, float n, float f){

		//row 0
		entries[0][0] = 2/(r-l);
		entries[0][1] = 0;
		entries[0][2] = 0;
		entries[0][3] = -(r+l)/(r-l);
		//row 1
		entries[1][0] = 0;
		entries[1][1] = 2/(t-b);
		entries[1][2] = 0;
		entries[1][3] = -(t+b)/(t-b);
		//row 2
		entries[2][0] = 0;
		entries[2][1] = 0;
		entries[2][2] = -2/(f-n);
		entries[2][3] = -(f+n)/(f-n);
		//row 3
		entries[3][0] = 0;
		entries[3][1] = 0;
		entries[3][2] = 0;
		entries[3][3] = 1;
	}
	
	/**
	 * Imitates the corresponding OpenGL method mapping eye coordinates to clip coordinates
	 * (i.e., a Projection matrix).
	 * <br><br>
	 * This matrix provides an orthogonal projection which maps the box described
	 * below into the box [-1,1]^3; anything outside of this box is clipped by OpenGL.
	 * <br><br>
	 * First consider the XY-aligned rectangle in the z=-n plane with minimum and maximum 
	 * (x,y) given by (l,b) and (r,t), respectively. 
	 * The frustum is the set of all points on some line parallel to the z-axis passing 
	 * through that rectangle, with z value in the range [-1,1].
	 * 
	 * @return The Matrix4d mapping the frustum described above to [-1,1]^3.<br>
	 * In particular, returns glOrtho(left, right, bottom, top, -1, 1).
	 * @see {@link #setAsGlOrtho(float, float, float, float, float, float)}
	 */
	public void setAsGluOrtho2D(float left, float right, float bottom, float top){
		
		setAsGlOrtho(left, right, bottom, top, -1, 1);
	}
	
	//===============================================
	// GL/GLU MODELVIEW MATRICES
	//===============================================
	
	/**
	 * Imitates the corresponding OpenGL method mapping object coordinates to eye coordinates
	 * (i.e., a ModelView matrix).
	 * <br><br>
	 * This matrix is a rotation CCW (with "axis" pointing at you) by "angle" radians.
	 * 
	 * @see {@link Matrix3d#setAsGlRotate(Vector3d, float)}
	 */
	public void setAsGlRotate(float angle, Vector3d axis){
		
		setAsIdentity();
		setUpper3x3AsRotation(axis, angle);
	}
	
	/**
	 * Imitates the corresponding OpenGL method mapping object coordinates to eye coordinates
	 * (i.e., a ModelView matrix).
	 * <br><br>
	 * This matrix translates a vector by adding "v".
	 */
	public void setAsGlTranslate(Vector3d v){
		
		//initialize matrix
		setAsIdentity();
		
		//set last column to v, augmented with 1
		entries[0][3] = v.entries[0];
		entries[1][3] = v.entries[1];
		entries[2][3] = v.entries[2];
		entries[3][3] = 1;
	}
	
	/**
	 * Imitates the corresponding OpenGL method mapping object coordinates to eye coordinates
	 * (i.e., a ModelView matrix).
	 * <br><br>
	 * This matrix moves "eye" to the origin, then rotates about the origin so that
	 * "center" will get mapped onto the negative z-axis (i.e., into the viewport)
	 * and the "side vector", equal to normalize(forward X up), where "forward" is 
	 * the vector from "eye" to "center", will get mapped onto the positive x-axis 
	 * (i.e., to the right in the viewport).
	 * 
	 * @see {@link Camera}
	 */
	public void setAsGluLookAt(Vector3d eye, Vector3d center, Vector3d up){
		
		//eye = eye point; gets moved to origin
		//center = reference point; maps onto negative z axis (into the viewport) 
		//up = up direction; gets mapped to positive y axis (upward in viewport)
		
		//set f to be center-eye, normalized
		Vector3d f = new Vector3d(center);
		f.subtract(eye);
		f.normalize();
		
		//set s (right-pointing vector) to be f X up, normalized; "screen right"
		Vector3d s = f.cross(up);
		s.normalize();
		
		//set u = s x f to complete the orthonormal frame; "screen up"
		Vector3d su = s.cross(f);

		//row 0
		entries[0][0] = s.entries[0];
		entries[0][1] = s.entries[1];
		entries[0][2] = s.entries[2];
		entries[0][3] = 0;
		//row 1
		entries[1][0] = su.entries[0];
		entries[1][1] = su.entries[1];
		entries[1][2] = su.entries[2];
		entries[1][3] = 0;
		//row 2
		entries[2][0] = -f.entries[0];
		entries[2][1] = -f.entries[1];
		entries[2][2] = -f.entries[2];
		entries[2][3] = 0;
		//row 3
		entries[3][0] = 0;
		entries[3][1] = 0;
		entries[3][2] = 0;
		entries[3][3] = 1;
		
		//return rot * (translate by -eye)
		Vector3d eyeNeg = new Vector3d(eye);
		eyeNeg.flip();
		preTranslate(eyeNeg);
	}
	
}
