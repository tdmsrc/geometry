package geometry.math;


/**
 * Represents a translation (offset), then a rotation, then another translation.
 * The translations are specified via {@link Vector3d} and the rotation is specified 
 * by a {@link Rotation3d}.
 */
public class ObjectTransformation3d{

	//transformation applied to this object is x -> rotation(x-offset) + translation	
	protected Vector3d offset;
	protected Rotation3d rotation;
	protected Vector3d translation;
	
	
	//-----------------------------------------
	// CONSTRUCTORS
	//-----------------------------------------

	/**
	 * Construct an identity transformation.
	 */
	public ObjectTransformation3d(){
		offset = new Vector3d();
		rotation = new Rotation3d();
		translation = new Vector3d();
	}
	
	/**
	 * Copy constructor.
	 */
	public ObjectTransformation3d(ObjectTransformation3d rhs){
		offset = rhs.offset.copy();
		rotation = rhs.rotation.copy();
		translation = rhs.translation.copy();
	}
	
	/**
	 * Return a copy of this ObjectTransformation3d.
	 */
	public ObjectTransformation3d copy(){
		return new ObjectTransformation3d(this);
	}

	/**
	 * Set this ObjectTransformation3d to have the same data as "rhs".
	 */
	public void setEqualTo(ObjectTransformation3d rhs){
		offset.setEqualTo(rhs.offset);
		rotation.setEqualTo(rhs.rotation);
		translation.setEqualTo(rhs.translation);
	}

	
	//-----------------------------------------
	// BASIC METHODS
	//-----------------------------------------
	
	public Vector3d getOffset(){ return offset; }
	
	public Rotation3d getRotation(){ return rotation; }
	
	public Vector3d getTranslation(){ return translation; }
	
	
	//-----------------------------------------
	// APPLYING TRANSFORMATION
	//-----------------------------------------
	
	/**
	 * Apply the transformation (i.e., any rotation and/or translation 
	 * represented by this transformation) to the point x.
	 * 
	 * @return A new {@link Vector3d} with the transformed point.
	 */
	public Vector3d applyToPoint(Vector3d x){
		Vector3d ret = x.copy(); 
		
		ret.subtract(offset);
		ret.setEqualTo(rotation.getMatrix().apply(ret));
		ret.add(translation);
		
		return ret;
	}
	
	/**
	 * Apply the transformation (i.e., any rotation represented by this
	 * transformation, ignoring translation) to the vector v.
	 * 
	 * @return A new {@link Vector3d} with the transformed vector.
	 */
	public Vector3d applyToVector(Vector3d v){

		return rotation.getMatrix().apply(v);
	}
	
	/**
	 * Return new Vector3d y such that applyToPoint(y) = x.
	 * 
	 * @see {@link #applyToPoint(Vector3d)}
	 */
	public Vector3d unApplyToPoint(Vector3d x){
		Vector3d ret = x.copy();
		
		ret.subtract(translation);
		ret.setEqualTo(rotation.getMatrixInverse().apply(ret));
		ret.add(offset);

		return ret;
	}
	
	/**
	 * Return new Vector3d w such that applyToVector(w) = v.
	 * 
	 * @see {@link #applyToVector(Vector3d)}
	 */
	public Vector3d unApplyToVector(Vector3d v){

		return rotation.getMatrixInverse().apply(v);
	}
	
	//-----------------------------------------
	// COMPOSITION
	//-----------------------------------------
	
	/**
	 * Return a new ObjectTransformation3d equal to this
	 * transformation followed by the transformation "lhs".
	 */
	public ObjectTransformation3d leftMultiply(ObjectTransformation3d lhs){
		
		ObjectTransformation3d ret = new ObjectTransformation3d();
		
		//new offset = this.offset
		ret.offset.setEqualTo(this.offset);
		
		//new rotation = lhs.rot*this.rot
		ret.rotation.setEqualTo(this.rotation);
		ret.rotation.leftMultiply(lhs.rotation);
		
		//new trans = lhs.rot(this.trans-lhs.offset) + lhs.trans = lhs.apply(this.trans) 
		ret.translation.setEqualTo(lhs.applyToPoint(this.translation));
		
		return ret;
	}
	
	/**
	 * Return a new ObjectTransformation3d equal to this
	 * transformation followed by the inverse of the transformation "lhs".
	 */
	public ObjectTransformation3d leftMultiplyByInverse(ObjectTransformation3d lhs){
		
		ObjectTransformation3d ret = new ObjectTransformation3d();
		
		//new offset = this.offset
		ret.offset.setEqualTo(this.offset);
		
		//new rotation is lhs.rotInverse*this.rot
		ret.rotation.setEqualTo(this.rotation);
		ret.rotation.leftMultiplyByInverse(lhs.rotation);
		
		//new trans = lhs.rotInv(this.trans - lhs.trans) + lhs.off = lhs.unApply(this.trans)
		ret.translation.setEqualTo(lhs.unApplyToPoint(this.translation));
		
		return ret;
	}
}
