package geometry.common;



public class TimingManager {

	private static final int MAX_SIMUL_EVENTS = 10;
	
	//static data
	private int n;
	private String[] eventTypeNames;
	
	//cumulative data
	private long[] eventTimeTotals;
	
	//current events
	private int curSlot = 0;
	private int currentEventType[] = new int[MAX_SIMUL_EVENTS];
	private long currentEventStart[] = new long[MAX_SIMUL_EVENTS];
	
	
	public TimingManager(String[] eventTypeNames){
		n = eventTypeNames.length;
		this.eventTypeNames = eventTypeNames;
		eventTimeTotals = new long[n];
		
		resetTimings();
	}
	
	public void resetTimings(){
		//'cancel' any current events
		curSlot = 0;
		
		//reset cumulative timing totals
		for(int i=0; i<n; i++){
			eventTimeTotals[i] = 0;
		}
	}
	

	public void pushEvent(int eventType){
		if(curSlot >= MAX_SIMUL_EVENTS){
			MessageOutput.printError("(TimingManager) Error: Too many simultaneous events");
			return;
		}
		
		currentEventType[curSlot] = eventType;
		currentEventStart[curSlot] = System.currentTimeMillis();
		curSlot++;
	}
	
	public void popEvent(){
		if(curSlot == 0){ 
			MessageOutput.printError("(TimingManager) Error: No event to pop");
			return;
		}
		curSlot--;
		
		int eventType = currentEventType[curSlot];
		long eventStart = currentEventStart[curSlot];
		eventTimeTotals[eventType] += System.currentTimeMillis() - eventStart;
	}
	
	
	public long totalTime(){
		long total = 0;
		for(int i=0; i<n; i++){
			total += eventTimeTotals[i];
		}
		return total;
	}

	public void reportTimings(){
		MessageOutput.printDebug("Timing statistics:");
		for(int i=0; i<n; i++){
			MessageOutput.printDebug(eventTypeNames[i] + ": " + eventTimeTotals[i] + "ms");
		}
	}
	
	public void reportTimings(long elapsed, int frames){
		MessageOutput.printDebug("Timing statistics:");
		for(int i=0; i<n; i++){
			//figure out nanoseconds/frame
			float nsf = (100.0f*(float)eventTimeTotals[i])/(float)frames;
			int pct = (int)((100.0f*(float)eventTimeTotals[i])/(float)elapsed);
			MessageOutput.printDebug(eventTypeNames[i] + " ns/f: " + nsf + " (" + pct + "%)");
		}
		long untimed = elapsed - totalTime();
		float nsf = (100.0f*(float)untimed)/(float)frames;
		int pct = (int)((100.0f*(float)untimed)/(float)elapsed);
		MessageOutput.printDebug("Untimed ns/f: " + nsf + " (" + pct + "%)");
	}
}
