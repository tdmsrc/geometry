package geometry.common;

public class MessageOutput {

	public static void printDebug(String msg){
		System.out.println("(Debug) " + msg);
	}
	
	public static void printWarning(String msg){
		System.out.println("(Warning) " + msg);
	}
	
	public static void printError(String msg){
		System.err.println("(Error) " + msg);
	}
}
