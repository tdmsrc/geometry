package geometry.common;

public abstract class GeometricComponent {

	int index;
	
	public GeometricComponent(){
		index = 0;
	}
	
	/**
	 * Returns the index for this object.
	 * 
	 * @return The index for this object.
	 */
	public int getIndex(){
		return index;
	}
	
	/**
	 * Sets the index for this object.
	 * 
	 * @param index The index for this object.
	 */
	public void setIndex(int index){
		this.index = index;
	}
	
	/**
	 * Output some details about this geometric component.
	 * 
	 * @param outputTree The OutputTree that will take the output.
	 * @see OutputTree 
	 */
	public abstract void populateOutputTree(OutputTree outputTree);
}
