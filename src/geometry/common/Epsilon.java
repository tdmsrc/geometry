package geometry.common;

//basically a global float, storing "epsilon"

public class Epsilon {

	protected static float value = 0.01f;
	
	public static void set(float value){
		Epsilon.value = value;
	}
	
	public static float get(){
		return Epsilon.value;
	}
}
