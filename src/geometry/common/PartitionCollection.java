package geometry.common;

import java.util.HashSet;
import java.util.Set;

//Standard disjoint-set forest partition data structure 

/**
 * This represents a collection of T's which is split up into disjoint sets (i.e., the
 * collection is partitioned).
 */
public class PartitionCollection<T> {
	
	//set of roots
	private HashSet<PartitionNode<T>> roots;

	
	public PartitionCollection(){
		roots = new HashSet<PartitionNode<T>>();
	}
	
	public PartitionCollection(PartitionCollection<T> rhs){
		roots = new HashSet<PartitionNode<T>>(rhs.roots);
	}
	
	
	/**
	 * Returns a set containing the root node for each partition
	 * in the collection.
	 * 
	 * @return The set of root nodes.
	 */
	public Set<PartitionNode<T>> getRoots(){
		
		return roots;
	}
	
	/**
	 * Add a new partition containing only the object "object".
	 * 
	 * @param object The object to be added to the collection.
	 * @return The PartitionNode containing the object.
	 */
	public PartitionNode<T> add(T object){
		//make new root node
		PartitionNode<T> newRoot = new PartitionNode<T>(object);
		
		//add to lookups
		roots.add(newRoot);
		return newRoot;
	}
	
	/**
	 * Add to this collection the partition to which the specified
	 * PartitionNode belongs.
	 * 
	 * @param node A PartitionNode.
	 * @return Returns false if the partition was contained in the collection,
	 * true otherwise (i.e., returns true iff a change was made).
	 */
	public boolean add(PartitionNode<T> node){
		
		return roots.add(node.getRoot());
	}
	
	/**
	 * Remove from this collection the partition to which the specified
	 * PartitionNode belongs.
	 * 
	 * @param node A PartitionNode.
	 * @return Returns true if the partition was contained in the collection, 
	 * false otherwise (i.e., returns true iff a change was made).  
	 */
	public boolean remove(PartitionNode<T> node){
		
		return roots.remove(node.getRoot());
	}
	
	/**
	 * Check if the collection contains the partition to which the specified
	 * PartitionNode belongs.
	 * 
	 * @param node A PartitionNode.
	 * @return True if the partition is contained in the collection, false otherwise.
	 */
	public boolean contains(PartitionNode<T> node){
		
		return roots.contains(node.getRoot());
	}

	/**
	 * Merge the partitions to which the specified PartitionNodes belong.
	 * If either partition is not contained in the collection, does nothing.
	 * 
	 * Note that if the specified nodes belong to multiple collections,
	 * this will cause a problem.
	 * 
	 * @param node1 A PartitionNode.
	 * @param node2 Another PartitionNode.
	 * @return Returns false if the objects were already equal or if one of
	 * the nodes' partitions is not contained in the collection; true otherwise
	 * (i.e., returns true iff a change was made).
	 */
	public boolean mergePartitions(PartitionNode<T> node1, PartitionNode<T> node2){
		
		PartitionNode<T> root1 = node1.getRoot();
		PartitionNode<T> root2 = node2.getRoot();
		
		if(!roots.contains(root1) || !roots.contains(root2)){ return false; }

		//merge, and remove un-rooted node if there was one
		PartitionNode<T> unRooted = PartitionNode.<T>mergePartitionsByRoot(root1, root2);
		
		if(unRooted == null){ return false; }
		roots.remove(unRooted); 
		return true;
	}
}
