package geometry.common;

public abstract class ProgressListener{
	
	private static final int PROGRESS_UPDATE_PERCENT_STEP = 2;
	
	//current event properties
	protected String eventName;
	protected long startTime;
	protected int totalTicks;
	protected int elapsedTicks;
	protected float percentComplete;
	
	public ProgressListener(){
		
	}
	
	/**
	 * Get the name of the event that is currently happening.
	 */
	public String getCurrentEventName(){ return eventName; }
	
	
	/**
	 * Indicate that a new progress event is starting.
	 * 
	 * @param eventName The name of the new progress event.
	 */
	public void startEvent(String eventName, int totalTicks){ 
		this.eventName = eventName;
		this.startTime = System.currentTimeMillis();
		this.totalTicks = totalTicks;
		this.elapsedTicks = 0;
		this.percentComplete = 0.0f;
		
		this.handleProgressEventStarted(eventName);
		this.updateProgress(0.0f);
	}
	
	public void tickEvent(){
		
		float oldPercentComplete = percentComplete;
		int nOld = (int)(oldPercentComplete*100.0f/(float)PROGRESS_UPDATE_PERCENT_STEP);
		
		percentComplete = (float)elapsedTicks / (float)totalTicks;
		int nNew = (int)(percentComplete*100.0f/(float)PROGRESS_UPDATE_PERCENT_STEP);
		
		if(nNew > nOld){ this.updateProgress(percentComplete); }
		elapsedTicks++;
	}
	
	/**
	 * Indicate that the current progress event has completed.
	 */
	public void endEvent(){ 
		long totalTime = System.currentTimeMillis() - startTime;
	
		this.updateProgress(1.0f);
		this.handleProgressEventEnded(totalTime);
	}
	
	
	/**
	 * Handles notification that the progress is the given percent complete.
	 * @param percentComplete The current progress.
	 * @return Return false to cancel the operation.
	 */
	public abstract boolean updateProgress(float percentComplete);
	
	/**
	 * Handles notification that a new progress event has started.
	 */
	public abstract void handleProgressEventStarted(String eventName);
	
	/**
	 * Handles notification that the current progress event has ended.
	 */
	public abstract void handleProgressEventEnded(long totalTime);
}
