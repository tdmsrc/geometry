package geometry.common;

import java.util.Iterator;
import java.util.LinkedList;

//same as script.common.ViewableTreeText

public class OutputTreeText implements OutputTree{
	
	protected String title, childTitle;
	protected LinkedList<OutputTreeText> children;
	
	public OutputTreeText(){
		title = null;
		childTitle = null;
		children = new LinkedList<OutputTreeText>();
	}

	@Override
	public OutputTree spawnChild(String childTitle) {
		OutputTreeText child = new OutputTreeText();
		child.childTitle = childTitle;
		children.add(child);
		return child;
	}
	
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString(){ return toString("  "); }
	
	public String toString(String padding){
		
		String temp = padding + "[" + title + "]\n";
		
		Iterator<OutputTreeText> it = children.iterator();
		while(it.hasNext()){
		
			OutputTreeText child = it.next();
			String nextpad = "    ";
			if(it.hasNext()){ nextpad = " |  "; }
			
			temp += padding + " |  \n";
			temp += padding + " +--(" + child.childTitle + ")\n";
			temp += child.toString(padding + nextpad);
		}
		
		return temp;
	}
}
