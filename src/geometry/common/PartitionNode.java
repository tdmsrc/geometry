package geometry.common;

import java.util.Collection;
import java.util.HashSet;


public class PartitionNode<T>{
	
	private int depth;
	private T object;
	
	private HashSet<PartitionNode<T>> children; //null iff leaf
	private PartitionNode<T> parent; //null iff root
	
	
	public PartitionNode(T object){
		//initial configuration--leaf and root
		this.object = object;
		parent = null;
		children = null;
		depth = 0;
	}
	
	/**
	 * Get the object stored at this node.
	 * 
	 * @return The object stored at this node.
	 */
	public T getObject(){
		return object;
	}
	
	/**
	 * Check if this is a root node.
	 * 
	 * @return True if this is a root node, false otherwise.
	 */
	public boolean checkIsRootNode(){
		return (parent == null);
	}
	
	/**
	 * Set the depth of this node to the max depth among its children plus 1.
	 */
	/*private void updateDepth(){
		
		if(children == null){ depth = 0; return; }
		
		for(PartitionNode<T> child : children){
			if(child.depth >= depth){ depth = child.depth + 1; }
		}
	}*/
	
	//the reason for insisting root1 and root2 are roots, is that 
	//in PartitionCollection.mergePartitions, it makes sure that both
	//partitions are in the collection; to do so, it finds the roots.
	//so, it would find the roots twice if this method also found the roots.
	//[although, if path compression is used, then actually it hardly matters...]
	/**
	 * Merge the partitions to which the PartitionNodes "root1" and "root2" belong.
	 * 
	 * @param root1 A root PartitionNode.
	 * @param root2 Another root PartitionNode.
	 * @return If the partitions to be merged were already equal or if either
	 * of the nodes is not a root node, returns null; otherwise, returns the 
	 * node which, after merging, is no longer a root node.
	 */
	protected static <T> PartitionNode<T> mergePartitionsByRoot(PartitionNode<T> root1, PartitionNode<T> root2){
		
		if(!root1.checkIsRootNode() || !root2.checkIsRootNode()){ return null; }
		if(root1 == root2){ return null; }
		
		//organize by depth
		PartitionNode<T> root = root1;
		PartitionNode<T> child = root2;
		if(root1.depth < root2.depth){
			root = root2;
			child = root1;
		}
		
		//update depth, children, and parent
		if(root.depth == child.depth){ root.depth++; }
		
		//update children and parent
		if(root.children == null){
			root.children = new HashSet<PartitionNode<T>>();
		}
		root.children.add(child);
		child.parent = root;
		
		//return the root that became a child
		return child;
	}

	/**
	 * Every partition has a root node.  This method returns the 
	 * root node for the partition to which this PartitionNode belongs.
	 * 
	 * Two PartitionNodes belong to the same partition if and only if
	 * they have the same root node.
	 * 
	 * @return The root node for the partition to which this belongs.
	 */
	public PartitionNode<T> getRoot(){
		if(parent == null){ return this; }
		
		PartitionNode<T> root = parent.getRoot();
		
		//[TODO] use path compression to flatten the tree: set parent directly to root
		/*if(parent != root){
			//remove from current parent
			parent.children.remove(this);
			//update depth of (now previous) parent, if necessary:
			if(parent.depth == depth + 1){
				parent.updateDepth();
			}
			//ugh, this changes the depth of everything *above* parent too!
			
			//add as child of root
			parent = root;
			parent.children.add(this); //can *this* change parent depth?
		}*/
		return root;
	}
	
	/**
	 * Check whether the partition nodes "this" and "rhs" are elements
	 * of the same partition.
	 *  
	 * @param rhs The other partition node.
	 * @return True if the partitions are the same, false otherwise.
	 */
	public boolean checkPartitionEquivalence(PartitionNode<T> rhs){
		return (getRoot() == rhs.getRoot());
	}

	/**
	 * Get list of all elements in the partition to which this
	 * node belongs. 
	 * 
	 * @return List of elements in the partition.
	 */
	public void addPartitionElementsTo(Collection<T> elements){
		getRoot().addPartitionElementsToRecurse(elements);
	}
	
	private void addPartitionElementsToRecurse(Collection<T> elements){
		
		if(object != null){ elements.add(object); }
		if(children == null){ return; }
		
		for(PartitionNode<T> child : children){
			child.addPartitionElementsToRecurse(elements);
		}
	}
	
	/**
	 * Get the number of objects in the partition to which this
	 * node belongs.
	 */
	public int getPartitionSize(){
		return getRoot().getPartitionSizeRecurse();
	}
	
	private int getPartitionSizeRecurse(){
		if(children == null){ return 1; }
		
		int size = 1;
		for(PartitionNode<T> child : children){
			size += child.getPartitionSizeRecurse();
		}
		return size;
	}
}
