package geometry.common;

//same as script.common.ViewableTree

public interface OutputTree {

	public OutputTree spawnChild(String childTitle);
	public void setTitle(String title);
}
