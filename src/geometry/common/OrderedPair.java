package geometry.common;

public class OrderedPair<T> {

	private T a;
	private T b;
	
	public OrderedPair(T a, T b){
		this.a = a;
		this.b = b;
	}
	
	public T getA(){ return a; }
	public T getB(){ return b; }
	
	public void flip(){
		T temp = a;
		a = b; b = temp;
	}
	
	public boolean contains(T cmp){
		return ((a.equals(cmp)) || (b.equals(cmp)));
	}
	
	@Override
	public int hashCode() { return a.hashCode() ^ b.hashCode(); }
	
	@Override
	public boolean equals(Object obj){
		if(this == obj){ return true; }
		if(obj == null){ return false; }
		OrderedPair<?> rhs = (OrderedPair<?>)obj;
		if(rhs.a == null){ 
			if(rhs.b == null){ return ((a == null) && (b == null)); }
			else{ return ((a == null) && (rhs.b.equals(b))); }
		}else{
			if(rhs.b == null){ return ((rhs.a.equals(a)) && (b == null)); }
			else{ return ((rhs.a.equals(a)) && (rhs.b.equals(b))); }
		}
	}
}
