package geometry.common;

//for 0d, + or -, respectively
//for 1d, -> or <-, respectively
//for 2d, CCW or CW, respectively

public enum Orientation {
	
	POSITIVE{ 
		public Orientation flip() { return NEGATIVE; }
		public String toString(){ return "Positive"; }
	},
	NEGATIVE{ 
		public Orientation flip() { return POSITIVE; }
		public String toString(){ return "Negative"; }
	};
	
	public abstract Orientation flip();
}
