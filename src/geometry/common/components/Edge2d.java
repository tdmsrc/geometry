package geometry.common.components;

import geometry.common.OutputTree;
import geometry.math.Vector2d;

//Line segment in 2d

public class Edge2d
	<Vertex extends Vertex2d> 
	extends FixedDimensionalEdge<Vector2d, Vertex>
{

	/**
	 * Generic interface to provide a method to construct instances
	 * of a class derived from {@link Edge2d}.
	 */
	public static interface Edge2dConstructor<Vertex extends Vertex2d, Edge extends Edge2d<Vertex>>{
		public Edge construct(Vertex p, Vertex q);
		public Edge construct(Edge oldEdge, Vertex p, Vertex q);
	}
	
	
	public Edge2d(Vertex p, Vertex q){
		super(p, q);
	}
		
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("Edge2d (index: " + getIndex() +  ")");
		
		OutputTree child0 = outputTree.spawnChild("Initial Point");
		getInitialVertex().populateOutputTree(child0);
		
		OutputTree child1 = outputTree.spawnChild("Final Point");
		getFinalVertex().populateOutputTree(child1);
	}
}
