package geometry.common.components;

import geometry.common.GeometricComponent;
import geometry.common.OutputTree;
import geometry.math.FixedDimensionalVector;

//fixed-dimensional vector which extends GeometricComponent

public class FixedDimensionalVertex
	<Vector extends FixedDimensionalVector<Vector>> 
	extends GeometricComponent 
{
	
	/**
	 * Generic interface to provide a method to construct instances
	 * of a class derived from {@link FixedDimensionalVertex}.
	 */
	public static interface VertexConstructor
		<Vector extends FixedDimensionalVector<Vector>, 
		 Vertex extends FixedDimensionalVertex<Vector>>{
		
		public Vertex construct(Vector v);
	}
	
	private Vector v;
	
	/**
	 * Creates new {@link FixedDimensionalVertex} with position "v".
	 * The vector "v" is not duplicated.
	 * 
	 * @param v The position of the new vertex.
	 */
	public FixedDimensionalVertex(Vector v){
		this.v = v;
	}
	
	/**
	 * Returns the position of this vertex.
	 * 
	 * @return The position of this vertex.
	 */
	public Vector getPosition(){ return v; }
	
	/**
	 * Sets the position of this vertex to "v".
	 * The vector "v" is not duplicated.
	 * 
	 * @param v The new position of this vertex.
	 */
	public void setPosition(Vector v){ this.v = v; }
	
	
	@Override
	public void populateOutputTree(OutputTree outputTree) {
		
		outputTree.setTitle("Generic Vertex (index: " + getIndex() +  ", position: " + getPosition() + ")");
	}

}
