package geometry.common.components;

import geometry.common.OutputTree;
import geometry.math.FixedDimensionalVector;
import geometry.math.Vector2d;

//Point in 2d

public class Vertex2d extends FixedDimensionalVertex<Vector2d>{

	/**
	 * Generic interface to provide a method to construct instances
	 * of a class derived from {@link Vertex2d}.
	 */ 
	public static interface Vertex2dConstructor
		<Vertex extends Vertex2d> 
		extends VertexConstructor<Vector2d, Vertex>{}
	
	/**
	 * @see {@link FixedDimensionalVertex#FixedDimensionalVertex(FixedDimensionalVector)}
	 */
	public Vertex2d(Vector2d v){
		super(v);
	}
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("Vertex2d (index: " + getIndex() +  ", position: " + getPosition() + ")");
	}
}
