package geometry.common.components;

import geometry.common.OutputTree;
import geometry.math.FixedDimensionalVector;
import geometry.math.Vector3d;

//Point in 3d

public class Vertex3d extends FixedDimensionalVertex<Vector3d>{
	
	/**
	 * Generic interface to provide a method to construct instances
	 * of a class derived from {@link Vertex3d}.
	 */
	public static interface Vertex3dConstructor
		<Vertex extends Vertex3d>
		extends VertexConstructor<Vector3d, Vertex>{}
	
	/**
	 * @see {@link FixedDimensionalVertex#FixedDimensionalVertex(FixedDimensionalVector)}
	 */
	public Vertex3d(Vector3d v){
		super(v);
	}
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("Vertex3d (index: " + getIndex() +  ", position: " + getPosition() + ")");
	}
}
