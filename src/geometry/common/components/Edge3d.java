package geometry.common.components;

import geometry.common.OutputTree;
import geometry.math.Vector3d;

//Line segment in 3d

public class Edge3d
	<Vertex extends Vertex3d> 
	extends FixedDimensionalEdge<Vector3d, Vertex>
{
	
	/**
	 * Generic interface to provide a method to construct instances
	 * of a class derived from {@link Edge3d}.
	 */
	public static interface Edge3dConstructor<Vertex extends Vertex3d, Edge extends Edge3d<Vertex>>{
		public Edge construct(Vertex p, Vertex q);
	}
	
	
	public Edge3d(Vertex p, Vertex q){
		super(p, q);
	}
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("Edge3d (index: " + getIndex() +  ")");
		
		OutputTree child0 = outputTree.spawnChild("Initial Point");
		getInitialVertex().populateOutputTree(child0);
		
		OutputTree child1 = outputTree.spawnChild("Final Point");
		getFinalVertex().populateOutputTree(child1);
	}
}
