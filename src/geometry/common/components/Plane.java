package geometry.common.components;

import geometry.math.ObjectTransformation3d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;

//plane embedded in R^3
//consists of origin and two orthonormal basis vectors

public class Plane {

	private Vector3d origin;
	private Vector3d normal, e1, e2;
	
	private Plane(Vector3d origin, Vector3d normal, Vector3d e1, Vector3d e2){
		this.origin = origin;
		this.normal = normal;
		this.e1 = e1;
		this.e2 = e2;
	}
	
	//p is any point on the plane, n is the normal to the plane
	public Plane(Vector3d p, Vector3d n){
		this.origin = p;
		this.normal = new Vector3d(n); normal.normalize();
		
		//set coordinate axes so they are positively oriented when
		//normal is pointing at you (i.e. e2 = e1 rotated about n CCW by pi/2)
		this.e1 = n.getOrthogonalVector();
		this.e2 = normal.cross(e1);
	}
	
	/**
	 * Return a duplicate of the same plane. 
	 * 
	 * @return A duplicate of the plane.
	 */
	public Plane copy(){
		return new Plane(origin.copy(), normal.copy(), e1.copy(), e2.copy());
	}
	
	/**
	 * Return a new Plane equal to this Plane but with the specified transformation applied.
	 * 
	 * @return A transformed version of this Plane.
	 */
	public Plane copy(ObjectTransformation3d trans){
		
		return new Plane(trans.applyToPoint(origin), 
			trans.applyToVector(normal), trans.applyToVector(e1), trans.applyToVector(e2));
	}
	
	/**
	 * Determine a plane that contains all of the 3d vertices in a list.
	 * 
	 * The vertices are assumed to lie on a common plane.
	 * 
	 * @param verts The list of vertices.
	 * @return A plane containing the vertices.
	 */
	public static Plane getFromVertices(Vector3d[] verts){
		
		//any point on the face
		Vector3d planeOrigin = new Vector3d(verts[0]);
		
		//get tangent vector from 0th to 1st vertex of face
		Vector3d plane_e1 = new Vector3d(verts[1]);
		plane_e1.subtract(planeOrigin); plane_e1.normalize();
		
		//find the most-perpendicular tangent vector to plane_e1 going from origin to another face vertex
		float smallestAbsDot = 1; 
		Vector3d plane_e2 = plane_e1;
		
		for(int i=2; i<verts.length; i++){ 
			//get tangent vector from 0th to ith vertex of face
			Vector3d planeTangent = new Vector3d(verts[i]);
			planeTangent.subtract(planeOrigin); planeTangent.normalize();
			
			//compare to previous smallest dot product
			float absDot = plane_e1.dot(planeTangent); if(absDot < 0){ absDot = -absDot; }
			if(absDot < smallestAbsDot){
				smallestAbsDot = absDot;
				plane_e2 = planeTangent;
			}
		}
		
		//cross to get a vector normal to the face (its direction is corrected later)
		Vector3d planeNormal = plane_e1.cross(plane_e2);
		
		//return plane
		return new Plane(planeOrigin, planeNormal);
	}
	
	/**
	 * Return the unit normal vector to the plane.
	 * 
	 * @return The unit normal vector.
	 */
	public Vector3d getNormal(){
		return normal;
	}
	
	/**
	 * Return the origin (0,0) of the plane in ambient 3d coordinates.
	 * 
	 * @return The point (0,0) on the plane.
	 */
	public Vector3d getOrigin(){
		return origin;
	}
	
	/**
	 * Return the unit vector parallel to the x-axis of the plane.
	 * 
	 * @return The unit x-axis Vector3d.
	 */
	public Vector3d getXAxisVector(){
		return e1;
	}
	
	/**
	 * Return the unit vector parallel to the y-axis of the plane.
	 * 
	 * @return The unit y-axis Vector3d.
	 */
	public Vector3d getYAxisVector(){
		return e2;
	}
	
	/**
	 * Flips the normal and x-axis vector of the plane.
	 */
	public void flip(){
		normal.flip();
		e1.flip();
	}
	
	/**
	 * Returns the distance of the point "p" from the plane.
	 * 
	 * @param p The point whose distance from the plane is to be determined.
	 * @return The distance between the point and the plane.
	 */
	public float getDistance(Vector3d p){
		
		float d = getOrientedDistance(p);
		if(d < 0){ return -d; }
		return d;
	}
	
	/**
	 * Returns the oriented distance of the point "p" from the plane,
	 * i.e., if the point is on the side of the plane that the normal is 
	 * not pointed toward, the distance will be negative.
	 * 
	 * @param p The point whose oriented distance from the plane is to be determined.
	 * @return The oriented distance between the point and the plane.
	 */
	public float getOrientedDistance(Vector3d p){
		
		Vector3d v = new Vector3d(p); v.subtract(origin);
		return normal.dot(v);
	}
	
	/**
	 * Projects the specified 3d point onto the plane, and returns the result
	 * in the 2d coordinates of the plane.
	 * 
	 * In particular, returns (v dot e1, v dot e2), where v = p - o, where o is
	 * the 3d position of the origin of the plane, and where e1 and e2 are the
	 * 3d vectors in the direction of the x-axis and y-axis, respectively.
	 *  
	 * @param p The point to be projected onto the plane.
	 * @return The projection of the point, in the plane's coordinates.
	 */
	public Vector2d getProjection(Vector3d p){
		
		Vector3d v = new Vector3d(p); v.subtract(origin);
		return new Vector2d(v.dot(e1), v.dot(e2));
	}
	
	/**
	 * Projects the vector specified in ambient 3d coordinates onto the plane,
	 * and returns the result in the 2d coordinates of the plane.
	 * 
	 * In particular, returns (v dot e1, v dot e2), where e1 and e2 are the
	 * 3d vectors in the direction of the x-axis and y-axis, respectively.
	 * 
	 * @param v The vector to be projected onto the plane.
	 * @return The projection of the vector, in the plane's coordinates.
	 */
	public Vector2d getVectorProjection(Vector3d v){
		
		return new Vector2d(v.dot(e1), v.dot(e2));		
	}

	/**
	 * Returns the 3d position of the point "p", which is specified in the 2d
	 * coordinates of the plane.
	 * 
	 * In particular, returns e1*p.x + e2*p.y + o, where o is the 3d position
	 * of the origin of the plane, and where e1 and e2 are the 3d vectors in
	 * the direction of the x-axis and y-axis, respectively.
	 * 
	 * @param p The point on the plane, specified in plane's coordinates.
	 * @return The point "p" in the ambient 3d coordinates.
	 */
	public Vector3d getEmbedding(Vector2d p){
		
		Vector3d ret = Vector3d.linearCombination(p.getX(), e1, p.getY(), e2);
		ret.add(origin);
		return ret;
	}
	
	/**
	 * Returns the vector "v", which is specified in the 2d coordinates of the
	 * plane, in 3d ambient coordinates.
	 * 
	 * In particular, returns e1*v.x + e2*v.y, where e1 and e2 are the 3d vectors
	 * in the direction of the x-axis and y-axis, respectively.
	 * 
	 * @param v The vector on the plane, specified in the 2d coordinates of the plane.
	 * @return The vector "v" in the ambient 3d coordinates.
	 */
	public Vector3d getVectorEmbedding(Vector2d v){
		
		Vector3d ret = Vector3d.linearCombination(v.getX(), e1, v.getY(), e2);
		return ret;
	}
	
	/**
	 * Returns the intersection of a the line containing the edge
	 * with this plane.  The intersection is specified by the lerp
	 * value along the edge.
	 * 
	 * If the plane and edge are parallel, returns null.
	 *  
	 * @param edge The edge to intersect with the plane.
	 * @return The lerp value along the edge which yields the intersection.
	 */
	public Float computeEdgeIntersection(Edge3d<?> edge){
		//say E goes from P to Q.
		//Let O be the plane's origin, N its unit normal.
		//The lerp value of the intersection is (O-P).N / (Q-P).N
		
		Vector3d u = new Vector3d(origin); u.subtract(edge.getInitialVertex().getPosition());
		Vector3d v = edge.getEdgeVector();
		
		float denom = v.dot(normal);
		if(denom == 0){ return null; }
		
		return u.dot(normal) / denom; 
	}
	
}
