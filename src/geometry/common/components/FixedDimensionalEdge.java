package geometry.common.components;

import geometry.common.GeometricComponent;
import geometry.common.Orientation;
import geometry.common.OutputTree;
import geometry.math.FixedDimensionalVector;

//edge with some fixed-dimensional vertices as endpoints

public class FixedDimensionalEdge
	<Vector extends FixedDimensionalVector<Vector>, 
	 Vertex extends FixedDimensionalVertex<Vector>> 

	extends GeometricComponent 
{

	private Vertex p,q;
	
	public FixedDimensionalEdge(Vertex p, Vertex q){
		this.p = p;
		this.q = q;
	}
	
	/**
	 * Return the initial point of the edge.
	 * 
	 * @return The initial point of the edge.
	 */
	public Vertex getInitialVertex(){
		return p;
	}
	
	/**
	 * Return the final point of the edge.
	 * 
	 * @return The final point of the edge.
	 */
	public Vertex getFinalVertex(){
		return q;
	}
	

	/**
	 * Set the initial point of the edge.
	 * 
	 * @param p The new initial point of the edge.
	 */
	public void setInitialVertex(Vertex p){
		this.p = p;
	}
	
	/**
	 * Set the final point of the edge.
	 * 
	 * @param q The new final point of the edge.
	 */
	public void setFinalVertex(Vertex q){
		this.q = q;
	}
	
	/**
	 * Return the initial point of the edge with the specified
	 * orientation imposed.
	 * 
	 * @return The initial point of the edge with specified orientation.
	 */
	public Vertex getInitialVertex(Orientation o){
		switch(o){
		case POSITIVE: return p;
		case NEGATIVE: return q;
		}
		return null;
	}
	
	/**
	 * Return the final point of the edge with the specified
	 * orientation imposed.
	 * 
	 * @return The final point of the edge with specified orientation.
	 */
	public Vertex getFinalVertex(Orientation o){
		switch(o){
		case POSITIVE: return q;
		case NEGATIVE: return p;
		}
		return null;
	}
	
	/**
	 * Returns the Vector representing the edge.
	 * That is, (final point) - (initial point).
	 * 
	 * @return The Vector representing the edge.
	 */
	public Vector getEdgeVector(){
		Vector ret = q.getPosition().copy();
		ret.subtract(p.getPosition());
		return ret;
	}
	
	/**
	 * Returns the Vector representing the edge
	 * with the specified orientation imposed.
	 * For positive orientation, returns (final point) - (initial point).
	 * For negative orientation, returns (initial point) - (final point).
	 * 
	 * @param o The orientation to impose on the edge.
	 * @return The Vector representing the edge.
	 */
	public Vector getEdgeVector(Orientation o){
		Vector ret = getFinalVertex(o).getPosition().copy();
		ret.subtract(getInitialVertex(o).getPosition());
		return ret;
	}
	
	/**
	 * Exchanges the initial point and final point.
	 */
	public void flip(){
		Vertex temp = p;
		p = q;
		q = temp;
	}
	
	/**
	 * Determines the length of the edge.
	 * 
	 * @return The length of the edge.
	 */
	public float getLength(){
		return getEdgeVector().length();
	}
	
	/**
	 * Gives a point along the linear, unit-length parameterization 
	 * of the edge.  
	 * The parameterization runs from the initial to the final point.
	 * 
	 * @param t The value of the parameter.  
	 * Values between 0 and 1 will return a point on the edge.
	 * @return The point along the parameterization of the edge.  
	 */
	public Vector lerp(float t){
		
		return p.getPosition().lerp(q.getPosition(), t);
	}
	
	/**
	 * Gives a point along the linear, unit-length parameterization 
	 * of the edge with the specified orientation imposed.
	 * For positive orientation, the parameterization runs from initial to final point.
	 * For negative orientation, the parameterization runs from final to initial point.
	 * 
	 * @param t The value of the parameter.  
	 * Values between 0 and 1 will return a point on the edge.
	 * @return The point along the parameterization of the edge.  
	 */
	public Vector lerp(float t, Orientation o){
		
		return getInitialVertex(o).getPosition().lerp(getFinalVertex(o).getPosition(), t);
	}
	
	/**
	 * Compute the lerp value of the closest point on the line
	 * containing this edge to the point "x".
	 * 
	 * @param x The point.
	 * @return The lerp value of the closest point on the line to "x"
	 */
	public float computePointIntersection(Vector x){
		//Say E goes from P to Q, and X is the point.
		//Let u = Q-P, v = X-P.
		//Then the lerp value is (u.v)/(u.u).
		
		//Vector u = getEdgeVector();
		//Vector v = p.copy(); v.subtract(getInitialVertex().getPosition());
		//return (u.dot(v)/u.dot(u));
		return this.p.getPosition().unLerp(q.getPosition(), x);
	}

	/**
	 * Get the point along the line containing this edge which is closest
	 * to the line containing the edge "edge", and vice versa.
	 * These points are specified by their lerp values for the appropriate edge.
	 * 
	 * If the lines are exactly parallel, "null" will be returned.
	 * 
	 * @param edge The edge whose proximity to this edge is to be computed.
	 * @return A structure containing the lerp values.
	 */
	public EdgeEdgeIntersection computeEdgeIntersection(FixedDimensionalEdge<Vector, ?> edge){
		/*Say E goes from P to Q, E' goes from P' to Q'
		Let u1 = Q-P, u2 = Q'-P', v = P'-P

		Then the lerp value t for E and s for E' at the *closest point* is:
		[u1.u1 u1.u2][ t]   [v.u1]
		[u2.u1 u2.u2][-s] = [v.u2]
		
		Computing inverse gives the following method.*/
		
		Vector u1 = getEdgeVector();
		Vector u2 = edge.getEdgeVector();
		
		Vector v = edge.p.getPosition().copy();
		v.subtract(p.getPosition());
		
		float u1u1 = u1.dot(u1), u1u2 = u1.dot(u2), u2u2 = u2.dot(u2);
		
		float det = u1u1*u2u2 - u1u2*u1u2;
		if(det == 0){ return null; }
		
		float vu1 = v.dot(u1), vu2 = v.dot(u2);
		return new EdgeEdgeIntersection(
			(u2u2*vu1-u1u2*vu2)/det,
			(u1u2*vu1-u1u1*vu2)/det
		);
	}
	
	//data structure containing Edge-Edge intersection information
	public static class EdgeEdgeIntersection{
		public float lerpCallingEdge, lerpArgumentEdge;
		
		public EdgeEdgeIntersection(float lerpCallingEdge, float lerpArgumentEdge){
			this.lerpCallingEdge = lerpCallingEdge;
			this.lerpArgumentEdge = lerpArgumentEdge;
		}
	}

	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		outputTree.setTitle("Generic Edge (index: " + getIndex() +  ")");
		
		OutputTree child0 = outputTree.spawnChild("Initial Point");
		getInitialVertex().populateOutputTree(child0);
		
		OutputTree child1 = outputTree.spawnChild("Final Point");
		getFinalVertex().populateOutputTree(child1);
	}
}
