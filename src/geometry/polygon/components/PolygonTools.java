package geometry.polygon.components;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import geometry.common.PartitionCollection;
import geometry.common.PartitionNode;
import geometry.common.components.Edge2d;
import geometry.common.components.FixedDimensionalEdge;
import geometry.common.components.FixedDimensionalVertex;
import geometry.common.components.Vertex2d;
import geometry.math.FixedDimensionalVector;
import geometry.math.Vector2d;
import geometry.polygon.components.PolygonEdge.EdgeRecurseMethod;
import geometry.polygon.components.PolygonEdge.RecurseToken;
import geometry.polygon.triangulate.EdgeLoopPolygon;
import geometry.polygon.triangulate.EdgeLoopTools;
import geometry.polygon.triangulate.Triangle2d;

//This is an assortment of methods that can be used on various
//collections of Polygon components, but need not be used
//on an actual Polygon object.

public class PolygonTools{
	
	private PolygonTools(){ }
	
	
	//=================================================
	// AREA, CENTROID, INERTIA
	//=================================================
	
	/**
	 * Determines the signed area of the polygon with specified edges.
	 * 
	 * @return The signed area of the polygon.
	 */
	public static float computeSignedArea(Collection<? extends Edge2d<? extends Vertex2d>> edges){
		//quantity is additive; add for each triangle 0pq
		//area(0pq) = pxq/2
		
		float val = 0;
		for(Edge2d<?> edge : edges){
			Vector2d p = edge.getInitialVertex().getPosition();
			Vector2d q = edge.getFinalVertex().getPosition();
			val += p.cross(q);
		}
		return val/2.0f;
	}
	
	/**
	 * Determines the centroid (i.e., the center of mass assuming uniform
	 * density) scaled by the area for the polygon with specified edges.
	 * 
	 * @return The centroid scaled by the area.
	 */
	public static Vector2d computeAreaTimesCentroid(Collection<? extends Edge2d<? extends Vertex2d>> edges){
		//quantity is additive; add for each triangle 0pq
		//area(0pq) = pxq/2, centroid(0pq) = (p+q)/3 

		float valx = 0, valy = 0;
		for(Edge2d<?> edge : edges){
			Vector2d p = edge.getInitialVertex().getPosition();
			Vector2d q = edge.getFinalVertex().getPosition();
					
			float det = p.cross(q);
			valx += det * (p.getX() + q.getX());
			valy += det * (p.getY() + q.getY());
		}
		return new Vector2d(valx/6.0f, valy/6.0f);
	}
	
	/**
	 * For the polygon with specified edges, compute the moment of inertia about 
	 * the z-parallel axis passing through the centroid of the polygon.
	 * 
	 * @return The moment of inertia.
	 */
	public static float computeInertia(Collection<? extends Edge2d<? extends Vertex2d>> edges){
		//relative to (0,0), quantity is additive; add for triangles 0pq
		
		float val = 0;
		for(Edge2d<?> edge : edges){
			Vector2d p = edge.getInitialVertex().getPosition();
			Vector2d q = edge.getFinalVertex().getPosition();
			val += p.cross(q) * (p.dot(p) + p.dot(q) + q.dot(q));
		}
		
		//apply parallel axis theorem
		float sa = PolygonTools.computeSignedArea(edges);
		float cl2 = PolygonTools.computeAreaTimesCentroid(edges).lengthSquared();
		return val/12.0f - cl2/sa;
	}
		
	/**
	 * Integrate the function x^2 over the polygon with the specified edges.
	 * 
	 * @return The value of the integral.
	 */
	public static float computeIntegralX2(Collection<? extends Edge2d<? extends Vertex2d>> edges){
		//add the integrals over each triangle 0pq
		
		float val = 0;
		for(Edge2d<?> edge : edges){
			Vector2d p = edge.getInitialVertex().getPosition();
			Vector2d q = edge.getFinalVertex().getPosition();
			
			float pi = p.getX(), qi = q.getX();
			val += p.cross(q) * (pi*pi + pi*qi + qi*qi);
		}
		return val/12.0f;
	}
	
	/**
	 * Integrate the function xy over the polygon with the specified edges.
	 * 
	 * @return The value of the integral.
	 */
	public static float computeIntegralXY(Collection<? extends Edge2d<? extends Vertex2d>> edges){
		//add the integrals over each triangle 0pq
		
		float val = 0;
		for(Edge2d<?> edge : edges){
			Vector2d p = edge.getInitialVertex().getPosition();
			Vector2d q = edge.getFinalVertex().getPosition();
			
			float pi = p.getX(), qi = q.getX(), pj = p.getY(), qj = q.getY();
			val += p.cross(q) * ( 2*(pi*pj + qi*qj) + pi*qj + pj*qi );
		}
		return val/24.0f;
	}
	
	/**
	 * Integrate the function y^2 over the polygon with the specified edges.
	 * 
	 * @return The value of the integral.
	 */
	public static float computeIntegralY2(Collection<? extends Edge2d<? extends Vertex2d>> edges){
		//add the integrals over each triangle 0pq
		
		float val = 0;
		for(Edge2d<?> edge : edges){
			Vector2d p = edge.getInitialVertex().getPosition();
			Vector2d q = edge.getFinalVertex().getPosition();
			
			float pi = p.getY(), qi = q.getY();
			val += p.cross(q) * (pi*pi + pi*qi + qi*qi);
		}
		return val/12.0f;
	}
	
	
	//=================================================
	// CLOSEST OBJECT/CONTAINMENT QUERIES
	//=================================================
	
	//return value for getClosestObject
	/**
	 * Contains information about the closest Polygon component to a point.
	 * Exactly one of closestVertex and closestEdge should be non-null.
	 * The vector v is the vector from the query point to the closest point.
	 * 
	 * @see {@link PolygonTools#getClosestObject(Collection, Collection, FixedDimensionalVector)}
	 */
	public static class ClosestObjectData
		<Vector extends FixedDimensionalVector<Vector>,
		 Vertex extends FixedDimensionalVertex<Vector>,
		 Edge extends FixedDimensionalEdge<Vector, ? extends FixedDimensionalVertex<Vector>>>
	{
		
		public Vertex closestVertex;
		public Edge closestEdge;
		
		public Vector v; //vector from query point to closest point
		
		public ClosestObjectData(Vertex closestVertex, Edge closestEdge, Vector v){
			this.closestVertex = closestVertex;
			this.closestEdge = closestEdge;
			this.v = v;
		}
	}
	
	/**
	 * Given collections of fixed-dimensional vertices and edges and a point,
	 * determines which object is the closest to the point.  Returns a
	 * {@link ClosestObjectData} object, which contains the closest object from
	 * either collection, as well as the vector from the point to the closest 
	 * point on the closest object. 
	 * 
	 * @param verts Any collection of fixed-dimensional vertices.
	 * @param edges Any collection of fixed-dimensional edges.
	 * @param p A point.
	 * @return A {@link ClosestObjectData} object.
	 */
	public static 
		<Vector extends FixedDimensionalVector<Vector>,
		 Vertex extends FixedDimensionalVertex<Vector>,
		 Edge extends FixedDimensionalEdge<Vector, ? extends FixedDimensionalVertex<Vector>>>
	
	ClosestObjectData<Vector,Vertex,Edge> getClosestObject(Collection<? extends Vertex> verts, Collection<? extends Edge> edges, Vector p){ 
		
		boolean haveFirst = false;
		Vertex closestVertex = null;
		Edge closestEdge = null;
		Vector closestPoint = null;
		float d2min = 0;
		
		//check vertices
		for(Vertex vertex : verts){

			//find and compare distance
			float d2 = p.distanceSquared(vertex.getPosition());
			if((d2 < d2min) || !haveFirst){
				closestVertex = vertex; closestEdge = null;
				closestPoint = vertex.getPosition().copy();
				d2min = d2;
				haveFirst = true;
			}
		}
		
		//check edges
		for(Edge edge : edges){
			
			//find closest point on edge
			float t = edge.computePointIntersection(p);
			
			//if closest point is not on edge interior, endpoint is closer; defer to that
			if((t < 0) || (t > 1)){ continue; }
			
			//find distance from p to the edge and compare
			Vector x = edge.lerp(t);
			float d2 = p.distanceSquared(x);
			if(d2 < d2min){
				closestVertex = null; closestEdge = edge;
				closestPoint = x;
				d2min = d2;
			}
		}
		
		//return ClosestObjectData
		Vector v = closestPoint; v.subtract(p);
		return new ClosestObjectData<Vector,Vertex,Edge>(closestVertex, closestEdge, v);
	}
	
	/**
	 *  Determines if "p" is in the interior of the polygon with 
	 *  specified vertices and edges (plus edges incident to
	 *  the specified vertices).
	 *  <br><br>
	 *  Note that the boundary of a polygon is oriented, so if
	 *  e.g. the normals of the edges are pointing away from the region
	 *  that the boundary of the polygon bounds, that region is
	 *  considered the interior of the polygon.  If the normals are
	 *  pointing the opposite way, that region is the exterior.
	 *  
	 *  @param p The point whose containment is to be checked.
	 *  @return True or false.
	 */
	public static boolean containsPoint(
		Collection<? extends PolygonVertex<?,?>> vertices, 
		Collection<? extends PolygonEdge<?,?>> edges, 
		Vector2d p){
		
		ClosestObjectData<Vector2d,PolygonVertex<?,?>,PolygonEdge<?,?>> clobj =
			PolygonTools.<Vector2d,PolygonVertex<?,?>,PolygonEdge<?,?>>getClosestObject(vertices, edges, p);
		
		if(clobj.closestVertex != null){
			return clobj.closestVertex.checkInfinitesimalInterior(clobj.v);
		}else if(clobj.closestEdge != null){
			return clobj.closestEdge.checkInfinitesimalInterior(clobj.v);
		}
		
		return false;
	}
	
	/**
	 *  Same as {@link #containsPoint(Collection, Collection, Vector2d)}, but checks
	 *  if p is within epsilon of the boundary.
	 *  
	 *  @param epsilon Return true if p is this close to the boundary of the polygon.
	 */
	public static boolean containsPoint(
		Collection<? extends PolygonVertex<?,?>> vertices, 
		Collection<? extends PolygonEdge<?,?>> edges, 
		Vector2d p, float epsilon){
		
		ClosestObjectData<Vector2d,PolygonVertex<?,?>,PolygonEdge<?,?>> clobj =
			PolygonTools.<Vector2d,PolygonVertex<?,?>,PolygonEdge<?,?>>getClosestObject(vertices, edges, p);
		if(clobj.v.length() < epsilon){ return true; }
		
		if(clobj.closestVertex != null){
			return clobj.closestVertex.checkInfinitesimalInterior(clobj.v);
		}else if(clobj.closestEdge != null){
			return clobj.closestEdge.checkInfinitesimalInterior(clobj.v);
		}
		
		return false;
	}
	
	//=================================================
	// COMBINATORIAL/CONNECTEDNESS COMPUTATIONS
	//=================================================
	
	/**
	 * Create a PartitionCollection for the specified collection of edges.
	 * 
	 * This assigns to each edge a new PartitionNode, and completely ignores
	 * any existing PartitionNodes.
	 */
	protected static 
		<Vertex extends PolygonVertex<Vertex, Edge>, 
		 Edge extends PolygonEdge<Vertex, Edge>> 

		PartitionCollection<Edge> computeEdgePartitions(Collection<Edge> edges){
	
		//new edge partition collection
		final PartitionCollection<Edge> partitionCollection = new PartitionCollection<Edge>();
		
		//recursion method:
		EdgeRecurseMethod<Edge> edgeRecurseMethod = new EdgeRecurseMethod<Edge>(){
			@Override
			public boolean action(Edge previousEdge, Edge thisEdge){
				
				//set partition node to a new node, which is added to the collection
				PartitionNode<Edge> newNode = partitionCollection.add(thisEdge);
				thisEdge.setEdgePartitionNode(newNode);
				
				//merge to edge that called this edge via the recursion
				if(previousEdge != null){
					partitionCollection.mergePartitions(thisEdge.getEdgePartitionNode(), previousEdge.getEdgePartitionNode());
				}
				
				return true;
			}
		};
		
		//recurse with single token over all faces
		RecurseToken token = PolygonEdge.generateRecurseToken();
		for(Edge edge : edges){
			edge.recurse(null, edgeRecurseMethod, token);
		}
		
		return partitionCollection;
	}
	
	public static class PolygonComponentData
		<Vertex extends PolygonVertex<Vertex, Edge>, 
		 Edge extends PolygonEdge<Vertex, Edge>>
	{
		public LinkedHashSet<Vertex> vertices;
		public LinkedHashSet<Edge> edges;
		
		public PolygonComponentData(LinkedHashSet<Vertex> vertices, LinkedHashSet<Edge> edges){
			this.vertices = vertices;
			this.edges = edges;
		}
	}
	
	/**
	 * This method creates a list of Polygons from the given edges, the union
	 * of which is the same polygon that would be created by the usual constructor;
	 * each polygon in the list is connected (possibly only connected at a single
	 * vertex, but connected nonetheless).
	 * 
	 * @param vertices List of vertices to be used in the Polygons.
	 * @param edges List of edges to be used in the Polygons.
	 * @return List of PolygonComponentData objects, each of which has a subset of "vertices"
	 * and "edges", and represents a connected Polygon. 
	 */
	public static 
		<Vertex extends PolygonVertex<Vertex, Edge>, 
		 Edge extends PolygonEdge<Vertex, Edge>>
	
	List<PolygonComponentData<Vertex,Edge>> computeConnectedComponents(
		HashSet<Vertex> vertices, HashSet<Edge> edges){
		
		List<PolygonComponentData<Vertex,Edge>> componentDataList = new LinkedList<PolygonComponentData<Vertex,Edge>>();
		
		//split edges into partitions
		PartitionCollection<Edge> edgePartitions = PolygonTools.<Vertex,Edge>computeEdgePartitions(edges);

		//put partitions with positive signed area into a new face, and put the partitions with
		//negative signed area into another list, to be added to appropriate faces in next step
		LinkedList<PolygonComponentEdgeLoopData<Vertex,Edge>> newFaces = new LinkedList<PolygonComponentEdgeLoopData<Vertex,Edge>>();
		LinkedList<PolygonComponentData<Vertex,Edge>> negPartitions = new LinkedList<PolygonComponentData<Vertex,Edge>>();
		
		for(PartitionNode<Edge> root : edgePartitions.getRoots()){
			
			LinkedHashSet<Edge> partitionEdges = new LinkedHashSet<Edge>();
			root.addPartitionElementsTo(partitionEdges);
			
			LinkedHashSet<Vertex> partitionVertices = new LinkedHashSet<Vertex>();
			for(Edge edge : partitionEdges){
				partitionVertices.add(edge.getInitialVertex());
				partitionVertices.add(edge.getFinalVertex());
			}
			
			PolygonComponentData<Vertex,Edge> partitionData = new PolygonComponentData<Vertex,Edge>(partitionVertices, partitionEdges);
			
			if(PolygonTools.computeSignedArea(partitionData.edges) > 0){
				newFaces.add(new PolygonComponentEdgeLoopData<Vertex,Edge>(partitionData));
			}else{
				negPartitions.add(partitionData);
			}
		}
		
		//each negative component is assumed to be inside some positive component; sort in this way
		for(PolygonComponentData<Vertex,Edge> neg : negPartitions){
			
			//get some point on this collection of edges
			Vector2d p = neg.vertices.iterator().next().getPosition();
			
			//figure out to which positive partition this belongs
			for(PolygonComponentEdgeLoopData<Vertex,Edge> newFace : newFaces){
				if(!PolygonTools.containsPoint(newFace.pos.vertices, newFace.pos.edges, p)){ continue; }
				newFace.negList.add(neg);
				break;
			}
		}
		
		//merge each FaceComponentData and return
		for(PolygonComponentEdgeLoopData<Vertex,Edge> newFace : newFaces){
			componentDataList.add(newFace.merge());
		}
		return componentDataList;
	}
	
	private static class PolygonComponentEdgeLoopData
		<Vertex extends PolygonVertex<Vertex, Edge>, 
		 Edge extends PolygonEdge<Vertex, Edge>>
	{
		public PolygonComponentData<Vertex,Edge> pos;
		public LinkedList<PolygonComponentData<Vertex,Edge>> negList;
		
		public PolygonComponentEdgeLoopData(PolygonComponentData<Vertex,Edge> pos){
			this.pos = pos;
			negList = new LinkedList<PolygonComponentData<Vertex,Edge>>();
		}
		
		public PolygonComponentData<Vertex,Edge> merge(){
			for(PolygonComponentData<Vertex,Edge> neg : negList){
				pos.vertices.addAll(neg.vertices);
				pos.edges.addAll(neg.edges);
			}
			negList.clear();
			return pos;
		}
	}

	
	//=================================================
	// TRIANGULATION
	//=================================================
	
	/**
	 * Triangulate the polygon specified by the given collection of
	 * vertices and their incident edges.
	 * <br>
	 * See EdgeLoopPolygon.computeEdgeLoopDecomposition for the 
	 * conditions on the polygon components.
	 * 
	 * @return A list of {@link Triangle2d} objects with references to the original vertices.
	 * @see {@link EdgeLoopTools#computeEdgeLoopDecomposition(Collection)}
	 */
	public static 
		<Vertex extends PolygonVertex<Vertex,Edge>,
		 Edge extends PolygonEdge<Vertex,Edge>>
	List<Triangle2d<Vertex>> triangulate(Collection<Vertex> vertices){
		
		List<EdgeLoopPolygon<Vertex>> polygonList = 
			EdgeLoopTools.<Vertex,Edge>computeEdgeLoopDecomposition(vertices);
		
		LinkedList<Triangle2d<Vertex>> triList = new LinkedList<Triangle2d<Vertex>>();
		for(EdgeLoopPolygon<Vertex> polygon : polygonList){
			polygon.computeTriangulation(triList);
		}
		return triList;
	}
}
