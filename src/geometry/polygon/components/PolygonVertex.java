package geometry.polygon.components;

import java.util.HashSet;
import java.util.Set;

import geometry.common.Orientation;
import geometry.common.components.Vertex2d;
import geometry.math.Vector2d;

/**
 * A PolygonVertex extends {@link Vertex2d} and holds the following data:
 * <ul>
 * <li><i>Sub-components</i> (inherited from {@link Vertex2d}):
 * <ul>
 * <li>Reference to position as {@link Vector2d} 
 * </ul>
 * <li><i>Super-components</i>:
 * <ul>
 * <li>Reference to {@link Polygon} that owns this component
 * <li>Collection of incident Edge objects
 * </ul>
 * </ul>
 * 
 * Automatic cross-reference updating:
 * <ul>
 * <li>If a {@link Polygon} is created with this vertex specified as belonging to it, {@link #setOwner}
 * is called to automatically take ownership of this vertex.
 * <li>If a {@link PolygonEdge} is created with this vertex as an endpoint, {@link #addIncidentEdge} 
 * is called to automatically add it to this vertex's collection of incident edges.
 * </ul>
 */
public abstract class PolygonVertex
	<Vertex extends PolygonVertex<Vertex, Edge>,
	 Edge extends PolygonEdge<Vertex, Edge>>
	 
	extends Vertex2d
	implements PolygonComponent<Vertex, Edge>
{
	
	//polygon this component belongs to
	protected Polygon<Vertex, Edge> owner; 
	
	//incident edge data 
	private HashSet<Edge> outwardPointingEdges; //the edges for which this vertex is the initial point
	private HashSet<Edge> inwardPointingEdges; //the edges for which this vertex is the final point
	
	
	/**
	 * Creates a PolygonVertex with the specified position,
	 * "null" as owner, and empty collection of incident edges.
	 * 
	 * @see {@link Vertex2d#Vertex2d(Vector2d)}
	 */
	public PolygonVertex(Vector2d v){
		super(v);
		owner = null;
		
		//clear incidence data
		outwardPointingEdges = new HashSet<Edge>();
		inwardPointingEdges = new HashSet<Edge>();
	}
	
	protected abstract Vertex getThisVertex();
	
	@Override public Vertex getAsVertex(){ return getThisVertex(); }
	@Override public Edge getAsEdge(){ return null; }
	
	@Override public PolygonComponentType getPolygonComponentType(){ return PolygonComponentType.VERTEX; }
	
	/**
	 * Return {@link Polygon} that owns this vertex.
	 */
	@Override
	public Polygon<Vertex, Edge> getOwner(){ return owner; }
	
	/**
	 * Set to null the reference to the {@link Polygon} that owns this vertex. <br> 
	 * This has no effect on components incident to this vertex. <br>
	 * This has no effect on the component list of the current owner.
	 */
	@Override
	public void disown(){ owner = null; }
	
	/**
	 * Set {@link Polygon} that owns this vertex to "owner". <br>
	 * This has no effect on components incident to this vertex. <br>
	 * This has no effect on the component list of the current or new owner.
	 */
	@Override
	public void setOwner(Polygon<Vertex, Edge> owner){
		this.owner = owner;
	}
	
	/**
	 * Add reference to an incident edge.
	 * If the specified edge does not have this vertex as an endpoint, does nothing.
	 * 
	 * @param edge The edge incident to this vertex.
	 */
	public void addIncidentEdge(Edge edge){
		if(edge.getInitialVertex() == this){
			outwardPointingEdges.add(edge);
		}else if(edge.getFinalVertex() == this){
			inwardPointingEdges.add(edge);
		}
	}

	/**
	 * Remove reference to an incident edge. 
	 *  
	 * @param edge The edge whose reference is to be removed.
	 * @see Polygon.removeEdge
	 */
	protected void removeIncidentEdge(Edge edge){
		inwardPointingEdges.remove(edge);
		outwardPointingEdges.remove(edge);
	}
	
	/**
	 * Return the set of edges which have this vertex as the initial point.
	 * 
	 * @return The set of edges for which this vertex is the initial point.
	 */
	public Set<Edge> getOutwardPointingEdges(){
		return outwardPointingEdges;
	}
	
	/**
	 * Return the set of edges which have this vertex as the final point.
	 * 
	 * @return The set of edges for which this vertex is the final point.
	 */
	public Set<Edge> getInwardPointingEdges(){
		return inwardPointingEdges;
	}
	
	/**
	 * Returns true if this vertex has no incident edges, otherwise false.
	 * 
	 * @return True if this vertex has no incident edges, otherwise false.
	 */
	public boolean isIsolated(){
		return (outwardPointingEdges.isEmpty() && inwardPointingEdges.isEmpty());
	}
	
	/**
	 * Reverses inward- and outward-pointing edges.
	 * This should only be called if every incident edge has
	 * been flipped.
	 * 
	 * @see Polygon.flip()
	 */
	protected void flip(){
		HashSet<Edge> temp = inwardPointingEdges;
		inwardPointingEdges = outwardPointingEdges;
		outwardPointingEdges = temp;
	}
	
	/**
	 * Flip the sign of the x-coordinates of the vertex.
	 * 
	 * @see EmbeddedPolygon.flip()
	 */
	public void flipXValue(){
		Vector2d vp = getPosition();
		vp.setX(-vp.getX());
	}
	
	/**
	 * Checks whether, if the vector "v" were placed with its head on 
	 * this vertex, points infinitesimally near the head of "v" 
	 * would be inside the polygon of which this vertex is a component.
	 * 
	 * Assumes points infinitesimally near the head of "v" are not
	 * on either edge incident to this vertex.
	 * 
	 * If "v" is very near zero, this could be true or false, but will run successfully.
	 * 
	 * @param v The vector whose head would be placed on this vertex.
	 * @return True if points infinitesimally near the head of "v" 
	 * are inside the polygon, false otherwise.
	 */
	@Override
	public boolean checkInfinitesimalInterior(Vector2d v){
		
		//use same method as in PolygonEdge, on the edge such that v.e is smaller, where e
		//is the edge vector pointing away from this vertex [so, the one "angled toward v" the most]
		//this is only unstable when the interior angle at this vertex is near 0 or 2pi
		//[given the assumption that points inf near v's head are reasonably far from edges]
		
		boolean hadFirst = false;
		float dmin = 0;
		Edge goodEdge = null;
		
		//check inward-pointing edges
		for(Edge edge : inwardPointingEdges){
			
			//find v.length*cos for e = outward edge (need not bother dividing by v.length)
			Vector2d e = edge.getEdgeVector(Orientation.NEGATIVE);
			float d = e.dot(v)/e.length();
			
			//compare and set minimum
			if(!hadFirst || (d < dmin)){
				dmin = d;
				goodEdge = edge;
				hadFirst = true;
			}
		}
		
		//check outward-pointing edges
		for(Edge edge : outwardPointingEdges){
			
			//find v.length*cos for e = outward edge (need not bother dividing by v.length)
			Vector2d e = edge.getEdgeVector(Orientation.POSITIVE);
			float d = e.dot(v)/e.length();
			
			//compare and set minimum
			if(!hadFirst || (d < dmin)){
				dmin = d;
				goodEdge = edge;
				hadFirst = true;
			}
		}

		return (goodEdge.getInwardPointingNormal().dot(v) < 0);
	}
}
