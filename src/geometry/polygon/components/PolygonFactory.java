package geometry.polygon.components;

import geometry.common.components.Edge2d.Edge2dConstructor;
import geometry.common.components.Vertex2d.Vertex2dConstructor;
import geometry.polygon.components.Polygon.PolygonConstructor;


public class PolygonFactory
	<Vertex extends PolygonVertex<Vertex, Edge>,
	 Edge extends PolygonEdge<Vertex, Edge>,
	 Poly extends Polygon<Vertex, Edge>>  
{
	
	//constructors for the polygon and its components
	protected Vertex2dConstructor<Vertex> vertexConstructor;
	protected Edge2dConstructor<Vertex, Edge> edgeConstructor;
	protected PolygonConstructor<Vertex, Edge, Poly> polyConstructor;
	
	public PolygonFactory(
		Vertex2dConstructor<Vertex> vertexConstructor,
		Edge2dConstructor<Vertex, Edge> edgeConstructor,
		PolygonConstructor<Vertex, Edge, Poly> polyConstructor)
	{
			
		this.vertexConstructor = vertexConstructor;
		this.edgeConstructor = edgeConstructor;
		this.polyConstructor = polyConstructor;
	}
}
