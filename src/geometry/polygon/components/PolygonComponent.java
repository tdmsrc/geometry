package geometry.polygon.components;

import geometry.math.Vector2d;

/**
 * A component of a {@link Polygon}.  Can be a vertex or edge; this can be determined
 * using the method {@link #getPolygonComponentType()}.
 */
public interface PolygonComponent
	<Vertex extends PolygonVertex<Vertex, Edge>,
	 Edge extends PolygonEdge<Vertex, Edge>>
{
	
	public Polygon<Vertex, Edge> getOwner();
	public void disown();
	public void setOwner(Polygon<Vertex, Edge> owner);
	
	/**
	 * @return If this component is not a vertex, returns null.
	 * Otherwise, returns this object as a Vertex (which extends {@link PolygonVertex}).
	 */
	public Vertex getAsVertex();
	
	/**
	 * @return If this component is not an edge, returns null.
	 * Otherwise, returns this object as an Edge (which extends {@link PolygonEdge}).
	 */
	public Edge getAsEdge();
	
	/**
	 * Indicates the type of polygon component.  
	 * Can be one of:
	 * <ul>
	 * <li> {@link PolygonComponentType#VERTEX}
	 * <li> {@link PolygonComponentType#EDGE}
	 * </ul> 
	 */
	public static enum PolygonComponentType{
		/** Polygon component of vertex type. **/ 
		VERTEX, 
		/** Polygon component of edge type. **/ 
		EDGE
	}
	public PolygonComponentType getPolygonComponentType();
	
	/**
	 * Checks whether, if the vector "v" were placed with its head on an
	 * interior point of this component, points infinitesimally near the
	 * head of "v" would be inside the polygon of which this is a component.
	 * 
	 * @param v The vector whose head would be placed on the interior of this component.
	 * @return True if points infinitesimally near the head of "v" 
	 * are inside the polygon, false otherwise.
	 */
	public boolean checkInfinitesimalInterior(Vector2d v);
}
