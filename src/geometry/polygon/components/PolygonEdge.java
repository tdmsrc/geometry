package geometry.polygon.components;

import geometry.common.PartitionNode;
import geometry.common.components.Edge2d;
import geometry.common.components.Vertex2d;
import geometry.math.Vector2d;

/**
 * A PolygonEdge extends {@link Edge2d} and holds the following data:
 * <ul>
 * <li><i>Sub-components</i> (inherited from {@link Edge2d}):
 * <ul>
 * <li>Reference to {@link PolygonVertex} endpoints 
 * </ul>
 * <li><i>Super-components</i>:
 * <ul>
 * <li>Reference to {@link Polygon} that owns this component
 * </ul>
 * <li><i>Extra data</i>:
 * <ul>
 * <li>{@link PartitionNode} of edges to which this edge belongs
 * </ul>
 * </ul>
 * 
 * Automatic cross-reference updating:<br>
 * If a {@link Polygon} is created with this edge specified as belonging to it, that Polygon
 * <ul>
 * <li>will take ownership of this edge by calling {@link #setOwner(Polygon)}
 * <li>will partition its edges and set the PartitionNode to which this edge belongs by calling {@link #setEdgePartitionNode(PartitionNode)}
 * </ul>
 */
public abstract class PolygonEdge 
	<Vertex extends PolygonVertex<Vertex, Edge>,
	 Edge extends PolygonEdge<Vertex, Edge>>

	extends Edge2d<Vertex>
	implements PolygonComponent<Vertex, Edge>
{

	//polygon this component belongs to
	protected Polygon<Vertex, Edge> owner;
	//edges partitioned by equiv rel: e1 ~ e2 iff joined by a sequence of incident verts
	protected PartitionNode<Edge> partitionNode;
	
	/**
	 * Creates a PolygonEdge with the specified endpoints, 
	 * "null" as owner, and belonging to a {@link PartitionNode} containing only this edge
	 * (the PartitionNode will be updated automatically if a {@link Polygon} is created
	 * with this edge specified as belonging to it).
	 * <br><br>
	 * Automatically adds the new PolygonEdge to the collection of incident edges of the 
	 * specified endpoints.
	 * 
	 * @see {@link Edge2d#Edge2d(Vertex2d, Vertex2d)}
	 */
	public PolygonEdge(Vertex p, Vertex q){
		super(p, q);
		owner = null;
		partitionNode = new PartitionNode<Edge>(getThisEdge());
		
		//update endpoints' incidence data
		p.addIncidentEdge(getThisEdge());
		q.addIncidentEdge(getThisEdge());
	}
	
	protected abstract Edge getThisEdge(); 

	@Override public Vertex getAsVertex(){ return null; }
	@Override public Edge getAsEdge(){ return getThisEdge(); }

	@Override public PolygonComponentType getPolygonComponentType(){ return PolygonComponentType.EDGE; }
	
	/**
	 * Return {@link Polygon} that owns this edge.
	 */
	@Override
	public Polygon<Vertex, Edge> getOwner(){ return owner; }
	
	/**
	 * Set to null the reference to the {@link Polygon} that owns this edge. <br> 
	 * This has no effect on components incident to this edge. <br>
	 * This has no effect on the component lists of the current owner.
	 */
	@Override
	public void disown(){ owner = null; }
	
	/**
	 * Set {@link Polygon} that owns this edge to "owner".<br>
	 * Calls {@link PolygonVertex#setOwner} with argument "owner" on endpoints of this edge.<br>
	 * This has no effect on the component lists of the current or new owner.
	 */
	@Override
	public void setOwner(Polygon<Vertex, Edge> owner){
		this.owner = owner;
		
		//trickle down to vertices
		getInitialVertex().setOwner(owner);
		getFinalVertex().setOwner(owner);
	}
	
	public PartitionNode<Edge> getEdgePartitionNode(){
		return partitionNode;
	}
	
	public void setEdgePartitionNode(PartitionNode<Edge> partitionNode){
		this.partitionNode = partitionNode;
	}

	/**
	 * Returns the inward-pointing normal to this edge.
	 * 
	 * @return The inward-pointing normal to this edge.
	 */
	public Vector2d getInwardPointingNormal(){
		Vector2d n = getEdgeVector(); 
		n.rotateCCW();
		return n;
	}
	
	/**
	 * Checks whether, if the vector "v" were placed with its head on any point
	 * on the interior of this edge, points infinitesimally near the head of "v" 
	 * would be inside the polygon of which this edge is a component.
	 * 
	 * Assumes points infinitesimally near the head of "v" are not on this edge.
	 * 
	 * @param v The vector whose head would be placed on the interior of this edge.
	 * @return True if points infinitesimally near the head of "v" 
	 * are inside the polygon, false otherwise.
	 */
	@Override
	public boolean checkInfinitesimalInterior(Vector2d v){

		return (v.dot(getInwardPointingNormal()) < 0);
	}
	
	
	//==============================================
	// RECURSION OVER VERTEX-CONNECTED EDGES
	//==============================================
	
	/**
	 * An action to be performed on an edge.
	 * 
	 * @see {@link PolygonEdge#recurse(EdgeRecurseMethod)}
	 * @see {@link PolygonEdge#recurse(PolygonEdge, EdgeRecurseMethod, RecurseToken)}
	 */
	public static interface EdgeRecurseMethod<Edge extends PolygonEdge<?, Edge>>{
		/**
		 * An action to be performed on an edge.
		 * 
		 * @param previousEdge The edge immediately preceding thisEdge in the recursion.
		 * @param thisEdge The edge on which the action is to be performed.
		 * @return Return true to continue the recursion.  If false is returned, recursion will stop.
		 */
		public boolean action(Edge previousEdge, Edge thisEdge);
	}
	
	//see recurse method
	public static class RecurseToken{
		private static long lastid = 0;
		private long id;
		
		protected RecurseToken(){
			id = RecurseToken.lastid++;
			RecurseToken.lastid = id;
		}
		public long getID(){ return id; }
	}
	private RecurseToken lastRecurseToken = null;
	
	
	/**
	 * This method generates a token which represents an edge recursion.
	 * When an edge is encountered, the recursion stops if the last set
	 * token is equal to the current token.  Otherwise, the last set
	 * token is set to the current token.
	 * 
	 * @return A recursion token.
	 */
	public static RecurseToken generateRecurseToken(){
		return new RecurseToken();
	}
	
	/**
	 * This method generates a new token and calls
	 * {@link PolygonEdge#recurse(PolygonEdge, EdgeRecurseMethod, RecurseToken)}
	 * with parameters (null, edgeRecurseMethod, new token).
	 * 
	 * @param edgeRecurseMethod The action to perform for each edge.
	 */
	public void recurse(EdgeRecurseMethod<Edge> edgeRecurseMethod){
		recurse(null, edgeRecurseMethod, new RecurseToken());
	}
	
	/**
	 * This method does nothing if the last token set was "token".
	 * 
	 * Otherwise, this method calls edgeRecurseMethod for this edge,
	 * and sets the last token to "token".  
	 * 
	 * If edgeRecurseMethod returns true, then this method is called for
	 * every edge incident to this one.
	 * 
	 * @param previousEdge The edge immediately preceding this edge in the recursion.  May be set to null initially.
	 * @param edgeRecurseMethod The action to perform for each edge.
	 * @param token A token which indicates whether an edge has been visited during
	 * the current recursion.
	 */
	public void recurse(Edge previousEdge, EdgeRecurseMethod<Edge> edgeRecurseMethod, RecurseToken token){
		
		//if this edge was already visited, do nothing
		if(lastRecurseToken == token){ return; }
		
		//perform method, and if it returns false, stop
		lastRecurseToken = token;
		if(!edgeRecurseMethod.action(previousEdge, getThisEdge())){ return; }

		//recurse
		for(Edge edge : getInitialVertex().getInwardPointingEdges()){
			edge.recurse(getThisEdge(), edgeRecurseMethod, token);
		}
		for(Edge edge : getInitialVertex().getOutwardPointingEdges()){
			edge.recurse(getThisEdge(), edgeRecurseMethod, token);
		}
		
		for(Edge edge : getFinalVertex().getInwardPointingEdges()){
			edge.recurse(getThisEdge(), edgeRecurseMethod, token);
		}
		for(Edge edge : getFinalVertex().getOutwardPointingEdges()){
			edge.recurse(getThisEdge(), edgeRecurseMethod, token);
		}
	}
	
}
