package geometry.polygon.components;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

import geometry.common.GeometricComponent;
import geometry.common.OutputTree;
import geometry.common.PartitionCollection;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;

/**
 * A Polygon holds the following data:
 * <ul>
 * <li><i>Sub-components</i>:
 * <ul>
 * <li>A collection of Vertex objects (extending {@link PolygonVertex})
 * <li>A collection of Edge objects (extending {@link PolygonEdge})
 * </ul>
 * <li><i>Extra data</i>:
 * <ul>
 * <li>A {@link PartitionCollection} of Edge objects.  
 * Two edges are in the same partition iff they can be joined by a sequence of incident vertices.
 * </ul>
 * </ul>
 * 
 * In general:
 * <ul>
 * <li>Polygon components naturally contain references to sub-components 
 * (e.g. edges refer to vertex endpoints)
 * and hold lists of super-components which are updated automatically by those 
 * super-components' constructors.
 * <li>Polygon components refer to Polygon that owns them, and this reference is updated
 * either by Polygon directly or by a super-component's owner being explicitly set
 * (except for disown, which should not modify sub-components).
 * </ul>
 */
public class Polygon
	<Vertex extends PolygonVertex<Vertex, Edge>, 
	 Edge extends PolygonEdge<Vertex, Edge>> 

	extends GeometricComponent 
{
	
	/**
	 * Generic interface to provide a method to construct instances
	 * of a class derived from {@link Polygon}.
	 */
	public static interface PolygonConstructor
		<Vertex extends PolygonVertex<Vertex, Edge>,
		 Edge extends PolygonEdge<Vertex, Edge>,
		 Poly extends Polygon<Vertex, Edge>>{
		
		public Poly construct(LinkedHashSet<Vertex> vertices, LinkedHashSet<Edge> edges);
	}
	
	//components of the polygon
	protected LinkedHashSet<Vertex> vertices; //<-"no duplicates" used in Polygon(edges)
	protected LinkedHashSet<Edge> edges;
	
	//edges partitioned by equiv rel: e1 ~ e2 iff joined by a sequence of incident verts
	protected PartitionCollection<Edge> edgePartitionCollection;


	/**
	 * Constructs a Polygon with the specified collection of vertices and edges.
	 * <br><br>
	 * The new Polygon takes ownership of all of its components 
	 * via the {@link PolygonComponent#setOwner(Polygon)} method.
	 * <br><br> 
	 * The Edge partition data is initialized using 
	 * {@link PolygonTools#computeEdgePartitions(Collection)}.
	 *  
	 * @param vertices The collection of vertices of this Polygon.
	 * @param edges The collection of edges of this Polygon.
	 */
	public Polygon(LinkedHashSet<Vertex> vertices, LinkedHashSet<Edge> edges){
		
		//store components
		this.vertices = vertices;
		this.edges = edges;
		
		//trickle down owner from edges
		for(Edge edge : edges){
			edge.setOwner(this);
		}

		//set combinatorial components for faces
		edgePartitionCollection = PolygonTools.<Vertex,Edge>computeEdgePartitions(edges);
	}
	
	/**
	 * Constructs a Polygon with the specified collection of edges.
	 * <br>
	 * The collection of vertices of the new Polygon is determined from the endpoints of the edges in the specified collection.
	 * <br><br>
	 * The new Polygon takes ownership of all of its components 
	 * via the {@link PolygonComponent#setOwner(Polygon)} method.
	 * <br><br>
	 * The Edge partition data is initialized using 
	 * {@link PolygonTools#computeEdgePartitions(Collection)}.
	 *  
	 * @param edges The collection of edges of this Polygon.
	 */
	public Polygon(LinkedHashSet<Edge> edges){
		
		//store components
		this.edges = edges; //new HashSet<Edge>(edges);
		
		this.vertices = new LinkedHashSet<Vertex>();
		for(Edge edge : edges){
			vertices.add(edge.getInitialVertex());
			vertices.add(edge.getFinalVertex());
		}
		
		//trickle down owner from edges
		for(Edge edge : edges){
			edge.setOwner(this);
		}

		//set combinatorial components for faces
		edgePartitionCollection = PolygonTools.<Vertex,Edge>computeEdgePartitions(edges);
	}

	/**
	 * Returns the set of vertices of this polygon.
	 * 
	 * @return The set of vertices.
	 */
	public LinkedHashSet<Vertex> getVertices(){ 
		return vertices;	
	}
	
	/**
	 * Returns the set of edges of this polygon.
	 * 
	 * @return The set of edges.
	 */
	public LinkedHashSet<Edge> getEdges(){ 
		return edges;	
	}
	
	/**
	 * Return the smallest Box which contains all the vertices of this Polygon.
	 */
	public Box<Vector2d> getBoundingBox(){
		
		if(vertices.isEmpty()){ return null; }
		
		Iterator<Vertex> it = vertices.iterator();
		Vertex vFirst = it.next();
		Vector2d vFirstPos = vFirst.getPosition();
		Vector2d min = vFirstPos.copy(), max = vFirstPos.copy();
		
		while(it.hasNext()){
			Vertex v = it.next();
			Vector2d vPos = v.getPosition();
		
			if(vPos.getX() < min.getX()){ min.setX(vPos.getX()); }
			if(vPos.getY() < min.getY()){ min.setY(vPos.getY()); }
			if(vPos.getX() > max.getX()){ max.setX(vPos.getX()); }
			if(vPos.getY() > max.getY()){ max.setY(vPos.getY()); }
		}
		
		return new Box<Vector2d>(min, max);
	}
	
	/**
	 * Returns the set of combinatorial connected components
	 * for this polygon.
	 * 
	 * Two edges are in the same combinatorial component iff they are
	 * connected by a sequence of incident edges. 
	 */
	public PartitionCollection<Edge> getEdgePartitionCollection(){
		return edgePartitionCollection;
	}
	
	/**
	 *  Determines if "p" is in the interior of the polygon.
	 *  
	 *  @see {@link PolygonTools#containsPoint(Collection, Collection, Vector2d)}
	 */
	public boolean containsPoint(Vector2d p){
		return PolygonTools.containsPoint(vertices, edges, p);
	}
	
	/**
	 *  Determines if "p" is in the interior of the polygon.
	 *  
	 *  @param epsilon Return true if p is this close to the boundary of the polygon.
	 *  @see {@link PolygonTools#containsPoint(Collection, Collection, Vector2d, float)}
	 */
	public boolean containsPoint(Vector2d p, float epsilon){
		return PolygonTools.containsPoint(vertices, edges, p, epsilon);
	}
	
	/**
	 * Determines the signed area of the polygon.
	 * 
	 * @return The signed area of the polygon.
	 */
	
	public float computeSignedArea(){
		return PolygonTools.computeSignedArea(edges);
	}

	/**
	 * Determines the centroid (i.e., the center of mass assuming uniform
	 * density) scaled by the area for the polygon.
	 * 
	 * @return The centroid scaled by the area.
	 */
	public Vector2d computeAreaTimesCentroid(){
		return PolygonTools.computeAreaTimesCentroid(edges);
	}
	
	/**
	 * Integrate the function x^2 over this polygon.
	 * 
	 * @return The value of the integral.
	 */
	public float computeIntegralX2(){
		return PolygonTools.computeIntegralX2(edges);
	}
	
	/**
	 * Integrate the function xy over this polygon.
	 * 
	 * @return The value of the integral.
	 */
	public float computeIntegralXY(){
		return PolygonTools.computeIntegralXY(edges);
	}
	
	/**
	 * Integrate the function y^2 over this polygon.
	 * 
	 * @return The value of the integral.
	 */
	public float computeIntegralY2(){
		return PolygonTools.computeIntegralY2(edges);
	}
	
	/**
	 * Integrate a quadratic integrand over this polygon.
	 * 
	 * @see {@link PolygonTools#computeQuadraticIntegral(Collection, float, float, float)}
	 */
	/*public float computeQuadraticIntegral(float a, float b, float c){
		return PolygonTools.computeQuadraticIntegral(edges, a, b, c);
	}*/
	
	/**
	 * Flips the orientation of every edge in this polygon.
	 */
	public void flip(){
		//reverse initial and final points in every edge
		for(Edge edge : edges){ edge.flip(); }
		//reverse inward- and outward-pointing edge sets in every vertex
		for(Vertex vertex : vertices){ vertex.flip(); }
	}
	
	/**
	 * Flip the sign of the x-coordinates of every vertex.
	 * 
	 * @see EmbeddedPolygon.flip()
	 */
	public void flipXValues(){
		for(Vertex vertex : vertices){ vertex.flipXValue(); }
	}
	
	/**
	 * Clears everything, leaving an empty Polygon.
	 */
	public void clear(){
		vertices = new LinkedHashSet<Vertex>();
		edges = new LinkedHashSet<Edge>();
		
		edgePartitionCollection = new PartitionCollection<Edge>();
	}
	
	
	//===============================================
	// COPY [TODO]
	//===============================================
	

	
	//===============================================
	// SERIALIZATION
	//===============================================
	
	@Override
	public void populateOutputTree(OutputTree outputTree){
		
		/*outputTree.setTitle("Polygon (index: " + getIndex() + ")");
		
		for(ConnectedPolygon<Vertex, Edge> connectedFace : connectedFaces){
			OutputTree child = outputTree.spawnChild("Connected Component");
			connectedFace.populateOutputTree(child);
		}*/
	}
	
	
	//==========================================================
	// CUT AND GLUE
	//==========================================================
	
	/**
	 * Removes an edge from this polygon.
	 * 
	 * After removing the edge, removes from this polygon
	 * any vertices which have no incident edges.
	 * 
	 * Note that this breaks the edge in that its component
	 * vertices will no longer recognize it as being incident.
	 */
	/*public void removeEdge(Edge edge){
		
		//set edge's owner to null and remove from polygon edge set
		if(!edges.remove(edge)){ return; }
		edge.disown();
		
		//remove references to "edge" from its endpoints, and check for isolated vertices
		Vertex vertex;
		
		vertex = edge.getInitialVertex();
		vertex.removeIncidentEdge(edge);
		//if isolated, set owner to null and remove from polygon vertex set
		if(vertex.isIsolated()){
			vertex.disown();
			vertices.remove(vertex);
		}
		
		vertex = edge.getFinalVertex();
		vertex.removeIncidentEdge(edge);
		//if isolated, set owner to null and remove from polygon vertex set
		if(vertex.isIsolated()){
			vertex.disown();
			vertices.remove(vertex);
		}
	}*/
}
