package geometry.polygon.intersect;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

import geometry.common.components.Edge2d;
import geometry.common.components.Vertex2d;
import geometry.common.components.FixedDimensionalEdge.EdgeEdgeIntersection;
import geometry.math.Vector2d;
import geometry.polygon.components.Polygon;
import geometry.polygon.components.PolygonComponent;
import geometry.polygon.components.PolygonEdge;
import geometry.polygon.components.PolygonVertex;
import geometry.polygon.intersect.PolygonEdgeIntersection.Intersection.EdgePointType;


public class PolygonEdgeIntersection{
	
	/**
	 * Data structure that stores information about a single intersection between
	 * an edge and a PolygonComponent.
	 */
	protected static class Intersection	
		<Vertex extends PolygonVertex<Vertex,Edge>, 
		 Edge extends PolygonEdge<Vertex,Edge>>
	
		implements Comparable<Intersection<Vertex,Edge>>{
		
		//DATA
		public static enum EdgePointType{
			INITIAL_POINT, FINAL_POINT, INTERIOR_POINT
		}
		protected EdgePointType edgePointType;
		protected float lerp;
		protected PolygonComponent<Vertex,Edge> intersectedComponent;
		
		//CONSTRUCTORS
		protected Intersection(EdgePointType edgePointType, float lerp, PolygonComponent<Vertex,Edge> intersectedComponent){
			this.edgePointType = edgePointType;
			this.lerp = lerp;
			this.intersectedComponent = intersectedComponent;
		}
		
		public static <Vertex extends PolygonVertex<Vertex,Edge>, Edge extends PolygonEdge<Vertex,Edge>> 
		Intersection<Vertex,Edge> newIntersection(float lerp, PolygonComponent<Vertex,Edge> intersectedComponent){
			return new Intersection<Vertex,Edge>(EdgePointType.INTERIOR_POINT, lerp, intersectedComponent);
		}
		
		public static <Vertex extends PolygonVertex<Vertex,Edge>, Edge extends PolygonEdge<Vertex,Edge>> 
		Intersection<Vertex,Edge> newIntersectionInitial(PolygonComponent<Vertex,Edge> intersectedComponent){
			return new Intersection<Vertex,Edge>(EdgePointType.INITIAL_POINT, 0, intersectedComponent);
		}
		
		public static <Vertex extends PolygonVertex<Vertex,Edge>, Edge extends PolygonEdge<Vertex,Edge>> 
		Intersection<Vertex,Edge> newIntersectionFinal(PolygonComponent<Vertex,Edge> intersectedComponent){
			return new Intersection<Vertex,Edge>(EdgePointType.FINAL_POINT, 1, intersectedComponent);
		}
		
		//ACCESSORS
		public EdgePointType getEdgePointType(){ return edgePointType; }
		public float getLerp(){ return lerp; }
		public PolygonComponent<Vertex,Edge> getIntersectedComponent(){ return intersectedComponent; }
		
		//SORTING BY LERP
		@Override
		public int compareTo(Intersection<Vertex,Edge> rhs){
			if(lerp < rhs.lerp){ return -1; }
			if(lerp > rhs.lerp){ return 1; }
			return 0;
		}
	}
	
	/**
	 * Determine which subset of "edge" is overlapped by the Polygon "polygon".
	 */
	public static 
		<Vertex extends PolygonVertex<Vertex,Edge>, 
		 Edge extends PolygonEdge<Vertex,Edge>> 
	IntervalSubset intersectPolygonAndEdge(
		Polygon<Vertex,Edge> polygon, Edge2d<? extends Vertex2d> edge, float epsilon){
		
		//METHOD
		//-----------------------------------------------
		//find intersections of various edge sub-components and polygon sub-components in order,
		//ensuring that no duplicate intersections are found
		//split edge into sub-edges at the intersections with polygon components
		//for each sub-edge, determine whether it is inside or outside the polygon
		//-----------------------------------------------
		
		//list of all intersections
		LinkedList<Intersection<Vertex,Edge>> intersections = new LinkedList<Intersection<Vertex,Edge>>();
		
		//list of intersections with polygon vertices
		HashMap<Vertex, Intersection<Vertex,Edge>> vertexIntersections = new HashMap<Vertex, Intersection<Vertex,Edge>>();
		
		//intersection with initial point of "edge" (if any)
		boolean initialPointHasIntersection = false;
		Intersection<Vertex,Edge> initialPointIntersection = null;
		
		boolean finalPointHasIntersection = false;
		Intersection<Vertex,Edge> finalPointIntersection = null;
		
		//EDGE ENDPOINT - POLYGON VERTEX INTERSECTIONS
		//-----------------------------------------------
		for(Vertex v : polygon.getVertices()){
			//check if initial point is within epsilon of v's position
			Vector2d p = edge.getInitialVertex().getPosition();
			if(!(p.distance(v.getPosition()) < epsilon)){ continue; }
			
			//intersection occurred; store
			initialPointHasIntersection = true;
			initialPointIntersection = Intersection.<Vertex,Edge>newIntersectionInitial(v);
			intersections.add(initialPointIntersection);
			vertexIntersections.put(v, initialPointIntersection);
		}
		
		for(Vertex v : polygon.getVertices()){
			//check if final point is within epsilon of v's position
			Vector2d q = edge.getFinalVertex().getPosition();
			if(!(q.distance(v.getPosition()) < epsilon)){ continue; }
			
			//intersection occurred; store
			finalPointHasIntersection = true;
			finalPointIntersection = Intersection.<Vertex,Edge>newIntersectionFinal(v);
			intersections.add(finalPointIntersection);
			vertexIntersections.put(v, finalPointIntersection);
		}
		
		//EDGE ENDPOINT - POLYGON EDGE INTERSECTIONS
		//-----------------------------------------------
		for(Edge e : polygon.getEdges()){
			Vector2d p = edge.getInitialVertex().getPosition();
			
			//make sure initial point didn't already intersect either endpoint of e
			if(initialPointHasIntersection){
				PolygonComponent<Vertex,Edge> ipic = initialPointIntersection.getIntersectedComponent();
				if((ipic == e) || (ipic == e.getInitialVertex()) || (ipic == e.getFinalVertex())){ continue; }
			}
			
			//find lerp of closest point on e to initial point
			float t = e.computePointIntersection(p);
			if(!((t > 0) && (t < 1))){ continue; }
			
			//ensure the initial point is within a distance of epsilon from closest point on e
			Vector2d x = e.lerp(t);
			if(!(p.distance(x) < epsilon)){ continue; }
			
			//intersection occurred; store in data structures
			initialPointHasIntersection = true;
			initialPointIntersection = Intersection.<Vertex,Edge>newIntersectionInitial(e);
			intersections.add(initialPointIntersection);
		}
		
		for(Edge e : polygon.getEdges()){
			Vector2d q = edge.getInitialVertex().getPosition();
			
			//make sure final point didn't already intersect either endpoint of e
			if(finalPointHasIntersection){
				PolygonComponent<Vertex,Edge> fpic = finalPointIntersection.getIntersectedComponent();
				if((fpic == e) || (fpic == e.getInitialVertex()) || (fpic == e.getFinalVertex())){ continue; }
			}
			
			//find lerp of closest point on e to final point
			float t = e.computePointIntersection(q);
			if(!((t > 0) && (t < 1))){ continue; }
			
			//ensure the final point is within a distance of epsilon from closest point on e
			Vector2d y = e.lerp(t);
			if(!(q.distance(y) < epsilon)){ continue; }
			
			//intersection occurred; store in data structures
			finalPointHasIntersection = true;
			finalPointIntersection = Intersection.<Vertex,Edge>newIntersectionFinal(e);
			intersections.add(finalPointIntersection);
		}
		
		//EDGE INTERIOR - POLYGON VERTEX INTERSECTIONS
		//-----------------------------------------------
		for(Vertex v : polygon.getVertices()){
			//make sure "edge" endpoints didn't already intersect this vertex
			if(initialPointHasIntersection && (initialPointIntersection.getIntersectedComponent() == v)){ continue; }
			if(finalPointHasIntersection && (finalPointIntersection.getIntersectedComponent() == v)){ continue; }
			
			//find lerp of closest point on edge to v's position
			float t = edge.computePointIntersection(v.getPosition());
			if(!((t > 0) && (t < 1))){ continue; }
			
			//ensure v is within a distance of epsilon from the edge's closest point
			Vector2d p = edge.lerp(t);
			if(!(p.distance(v.getPosition()) < epsilon)){ continue; }
			
			//intersection occurred; store in data structures
			Intersection<Vertex,Edge> intersection = Intersection.<Vertex,Edge>newIntersection(t, v);
			vertexIntersections.put(v, intersection);
			intersections.add(intersection);
		}
		
		//EDGE INTERIOR - POLYGON EDGE INTERSECTIONS
		//-----------------------------------------------
		for(Edge e : polygon.getEdges()){
			//make sure "edge" endpoints didn't intersect this edge or its endpoints already
			if(initialPointHasIntersection){
				PolygonComponent<Vertex,Edge> ipic = initialPointIntersection.getIntersectedComponent();
				if((ipic == e) || (ipic == e.getInitialVertex()) || (ipic == e.getFinalVertex())){ continue; }
			}
			if(finalPointHasIntersection){
				PolygonComponent<Vertex,Edge> fpic = finalPointIntersection.getIntersectedComponent();
				if((fpic == e) || (fpic == e.getInitialVertex()) || (fpic == e.getFinalVertex())){ continue; }
			}
			
			//don't bother checking if "edge" intersected either endpoint of e
			if(vertexIntersections.containsKey(e.getInitialVertex())){ continue; }
			if(vertexIntersections.containsKey(e.getFinalVertex())){ continue; }
			
			//check if edge intersects e
			EdgeEdgeIntersection eei = edge.computeEdgeIntersection(e);
			if(eei == null){ continue; }
			if(!((eei.lerpCallingEdge > 0) && (eei.lerpCallingEdge < 1))){ continue; }
			if(!((eei.lerpArgumentEdge > 0) && (eei.lerpArgumentEdge < 1))){ continue; }
			
			//intersection occurred; store in data structures
			Intersection<Vertex,Edge> intersection = Intersection.<Vertex,Edge>newIntersection(eei.lerpCallingEdge, e);
			intersections.add(intersection);
		}
		
		//PREPARE INTERVAL SUBSET
		//-----------------------------------------------
		IntervalSubset ret = new IntervalSubset(false);
		
		//DETERMINE WHAT TO DO IF THERE ARE *NO* INTERSECTIONS, AND RETURN
		//-----------------------------------------------
		if(intersections.isEmpty()){
			//determine if the entire segment is inside or outside the polygon
			//by checking e.g. the midpoint of the segment
			Vector2d m = edge.lerp(0.5f);
			boolean containedInPolygon = polygon.containsPoint(m);
			if(containedInPolygon){ ret.addAll(); }else{ ret.removeAll(); }
			return ret;
		}
		
		//THERE ARE INTERSECTIONS: SUB-EDGE IN POLYGON TESTS
		//-----------------------------------------------
		//for convenience, add initial and final point to collection if they aren't already there
		if(!initialPointHasIntersection){ intersections.add(Intersection.<Vertex,Edge>newIntersectionInitial(null)); }
		if(!finalPointHasIntersection){ intersections.add(Intersection.<Vertex,Edge>newIntersectionFinal(null)); }
		
		//sort intersection list by lerp value and subdivide edge
		Collections.sort(intersections);
		
		//edge vectors, forward and backward
		Vector2d edgeForward = edge.getEdgeVector();
		Vector2d edgeBackward = edgeForward.copy(); edgeBackward.flip();
		
		//loop through consecutive pairs in "intersections"
		ListIterator<Intersection<Vertex,Edge>> it = intersections.listIterator();
		Intersection<Vertex,Edge> ia = null, ib = it.next();

		while(it.hasNext()){
			//update pair
			ia = ib; ib = it.next();

			//determine if subsegment is inside polygon (one or the other may have null component)
			boolean containedInPolygon = (ib.getIntersectedComponent() == null) ?
				ia.getIntersectedComponent().checkInfinitesimalInterior(edgeBackward) : 
				ib.getIntersectedComponent().checkInfinitesimalInterior(edgeForward);
			
			//add to list
			if(!containedInPolygon){ continue; }
			
			if(ia.getEdgePointType() == EdgePointType.INITIAL_POINT){
				if(ib.getEdgePointType() == EdgePointType.FINAL_POINT){ ret.addAll(); }
				else{ ret.addLeftAlignedInterval(ib.getLerp()); }
			}else{
				if(ib.getEdgePointType() == EdgePointType.FINAL_POINT){ ret.addRightAlignedInterval(ia.getLerp()); }
				else{ ret.addInterval(ia.getLerp(), ib.getLerp()); }
			}
		}
		
		return ret;
	}
}
