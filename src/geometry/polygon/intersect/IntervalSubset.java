package geometry.polygon.intersect;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * A closed subset of [0,1] comprised of a collection of disjoint closed sub-intervals.
 */
public class IntervalSubset{
	
	private static final int RESOLUTION = 100000; 
	
	//Store as a bunch of closed intervals of the form [a,b], where
	//a and b are integers, in [0,RESOLUTION]
	private static class SubInterval implements Comparable<SubInterval>{
		public int a, b;
		
		public SubInterval(int a, int b){
			this.a = a;
			this.b = b;
		}
		
		@Override
		public int compareTo(SubInterval rhs){
			return a - rhs.a; //(a < rhs.a) ? -1 : ((a > rhs.a) ? 1 : 0);
		}
	}
	
	//list of (disjoint) subintervals making up this subset
	private LinkedList<SubInterval> subIntervals;
	
	
	/**
	 * Creates a new IntervalSubset.  The subset will be full or empty,
	 * depending on the value of "initiallyFull".
	 * 
	 * @param initiallyFull If this is true, the subset is initially full;
	 * otherwise, the subset is initially empty.
	 */
	public IntervalSubset(boolean initiallyFull){
		
		subIntervals = new LinkedList<SubInterval>();
		if(initiallyFull){
			subIntervals.add(new SubInterval(getInitialTick(),getFinalTick()));
		}
	}
	
	//conversions
	private float convertTickToFloat(int a){ return (float)a/(float)RESOLUTION; }
	
	private int convertFloatToTick(float a){ return (int)(a * RESOLUTION); }
	private int getInitialTick(){ return 0; }
	private int getFinalTick(){ return RESOLUTION; }
	
	//complement
	public IntervalSubset getComplement(){
		IntervalSubset ret = new IntervalSubset(true);
		ret.remove(this);
		return ret;
	}
	
	
	//ACCESSORS
	//-----------------------------------------------
	public boolean isEmpty(){	
		return subIntervals.isEmpty();
	}
	
	public boolean isFull(){
		
		if(subIntervals.size() == 1){
			SubInterval s = subIntervals.getFirst();
			return ((s.a == getInitialTick()) && (s.b == getFinalTick()));
		}else{ 
			return false; 
		}
	}
	
	public interface IntervalTraversalMethod{
		public void processFullInterval();
		public void processLeftAlignedSubInterval(float b);
		public void processRightAlignedSubInterval(float a);
		public void processInteriorSubInterval(float a, float b);
	}
	
	public void traverseSubIntervals(IntervalTraversalMethod method){
		
		//empty and full cases
		if(isEmpty()){ return; }
		if(isFull()){ method.processFullInterval(); }
		
		//otherwise:
		for(SubInterval s : subIntervals){
			if(s.a == getInitialTick()){ 
				method.processLeftAlignedSubInterval(convertTickToFloat(s.b)); 
				continue; 
			}
			if(s.b == getFinalTick()){
				method.processRightAlignedSubInterval(convertTickToFloat(s.a));
				continue;
			}
			method.processInteriorSubInterval(convertTickToFloat(s.a), convertTickToFloat(s.b));
		}
	}
	
	
	//TOOLS
	//TODO: these methods can be done in O(log n) rather than O(n).
	//-----------------------------------------------
	/**
	 * Remove any subinterval which is property contained in [at,bt]
	 */
	private void removeSIContainedIn(int at, int bt){
		
		ListIterator<SubInterval> it = subIntervals.listIterator();
		while(it.hasNext()){
			SubInterval s = it.next();
			if((s.a >= at) && (s.b <= bt)){ it.remove(); }
		}
	}
	
	/**
	 * Remove any subinterval which is property contained in (-inf,bt]
	 * @param bt
	 */
	private void removeSIContainedInL(int bt){
		
		ListIterator<SubInterval> it = subIntervals.listIterator();
		while(it.hasNext()){
			SubInterval s = it.next();
			if(s.b <= bt){ it.remove(); }
		}
	}
	
	/**
	 * Remove any subinterval which is property contained in [at,inf)
	 */
	private void removeSIContainedInR(int at){
		
		ListIterator<SubInterval> it = subIntervals.listIterator();
		while(it.hasNext()){
			SubInterval s = it.next();
			if(s.a >= at){ it.remove(); }
		}
	}
	
	/**
	 * Removes the first subinterval which, when its endpoints are extended by "delta",
	 * contains the tick value t (i.e., so that t is contained in [s.a-delta, s.b+delta]).
	 * Note that delta can be negative, which has the effect of shrinking the interval.
	 * 
	 * @return Returns the removed SubInterval, if there is one; otherwise, returns null.
	 */
	private SubInterval removeSIContaining(int t, int delta){
		
		ListIterator<SubInterval> it = subIntervals.listIterator();
		while(it.hasNext()){
			SubInterval s = it.next();
			if(((s.a-delta) <= t) && ((s.b+delta) >= t)){ 
				it.remove();
				return s;
			}
		}
		
		return null;
	}
	
	
	//EDITING (ADDITION)
	//-----------------------------------------------
	/**
	 * Set the subset to be [0,1].
	 */
	public void addAll(){
		subIntervals.clear();
		subIntervals.add(new SubInterval(getInitialTick(),getFinalTick()));
	}
	
	/**
	 * Add the interval [a,b] to the subset.
	 */
	public void addInterval(float a, float b){
		//convert endpoint(s) to tick values
		int at = convertFloatToTick(a), bt = convertFloatToTick(b);
		
		//first step: remove all subintervals contained inside [a,b]
		removeSIContainedIn(at, bt);
		
		//next step: check for subinterval containing or abutting a; similarly for b
		SubInterval sa = removeSIContaining(at, 1);
		SubInterval sb = removeSIContaining(bt, 1);
		if((sb == null) && (sa != null)){
			if(((sa.a-1) <= bt) && ((sa.b+1) >= bt)){ sb = sa; }
		}
		
		//final step: see if [a,b] should be extended to include sa and sb; add extended interval
		if(sa != null){ at = sa.a; }
		if(sb != null){ bt = sb.b; }
		subIntervals.add(new SubInterval(at, bt));
	}
	
	/**
	 * Add the interval [0,b] to the subset.
	 */
	public void addLeftAlignedInterval(float b){
		//convert endpoint(s) to tick values
		int bt = convertFloatToTick(b);
		
		//first step: remove subintervals contained in (-inf,b]
		removeSIContainedInL(bt);
		
		//next step: check for subinterval containing or abutting b
		SubInterval sb = removeSIContaining(bt, 1);
		
		//final step: see if [0,b] should be extended to include sb; add extended interval
		if(sb != null){ bt = sb.b; }
		subIntervals.add(new SubInterval(getInitialTick(), bt));
	}
	
	/**
	 * Add the interval [a,1] to the subset.
	 */
	public void addRightAlignedInterval(float a){
		//convert endpoint(s) to tick values
		int at = convertFloatToTick(a);
		
		//first step: remove subintervals contained in [a,inf)
		removeSIContainedInR(at);
		
		//next step: check for subinterval containing or abutting a
		SubInterval sa = removeSIContaining(at, 1);
		
		//final step: see if [a,1] should be extended to include sa; add extended interval
		if(sa != null){ at = sa.a; }
		subIntervals.add(new SubInterval(at, getFinalTick()));
	}
	
	
	//EDITING (SUBTRACTION)
	//-----------------------------------------------
	/**
	 * Set the subset to be empty.
	 */
	public void removeAll(){
		subIntervals.clear();
	}
	
	/**
	 * Remove the interval (a,b) from the subset.
	 */
	public void removeInterval(float a, float b){
		
		int at = convertFloatToTick(a), bt = convertFloatToTick(b);
		
		//first step: remove all subintervals contained inside [a,b]
		removeSIContainedIn(at, bt);
		
		//next step: check for subinterval containing a in its interior; similarly for b
		SubInterval sa = removeSIContaining(at, -1);
		SubInterval sb = removeSIContaining(bt, -1);
		if((sb == null) && (sa != null)){
			if(((sa.a+1) <= bt) && ((sa.b-1) >= bt)){ sb = sa; }
		}
		
		//final step: see if sa, sb should be clipped and re-added (it's possible sa == sb)
		if(sa != null){ subIntervals.add(new SubInterval(sa.a, at)); }
		if(sb != null){ subIntervals.add(new SubInterval(bt, sb.b)); }
	}
	
	/**
	 * Remove the interval [0,b) from the subset.
	 */
	public void removeLeftAlignedInterval(float b){
		//convert endpoint(s) to tick values
		int bt = convertFloatToTick(b);
		
		//first step: remove subintervals contained in (-inf,b]
		removeSIContainedInL(bt);
		
		//next step: check for subinterval containing b in its interior
		SubInterval sb = removeSIContaining(bt, -1);
		
		//final step: see if sb should be clipped and re-added
		if(sb != null){ subIntervals.add(new SubInterval(bt, sb.b)); }
	}
	
	/**
	 * Remove the interval (a,1] from the subset.
	 */
	public void removeRightAlignedInterval(float a){
		//convert endpoint(s) to tick values
		int at = convertFloatToTick(a);
		
		//first step: remove subintervals contained in [a,inf)
		removeSIContainedInR(at);
		
		//next step: check for subinterval containing a in its interior
		SubInterval sa = removeSIContaining(at, -1);
		
		//final step: see if sa should be clipped and re-added
		if(sa != null){ subIntervals.add(new SubInterval(sa.a, at)); }
	}
	
	
	//EDITING (SET)
	//-----------------------------------------------
	/**
	 * Add the entire subset "rhs" to this subset.
	 */
	public void add(IntervalSubset rhs){
		rhs.traverseSubIntervals(new IntervalTraversalMethod(){
			@Override public void processFullInterval(){ addAll(); }
			@Override public void processLeftAlignedSubInterval(float b){ addLeftAlignedInterval(b); }
			@Override public void processRightAlignedSubInterval(float a){ addRightAlignedInterval(a); }
			@Override public void processInteriorSubInterval(float a, float b){ addInterval(a,b); }
		});
	}
	
	/**
	 * Remove the (interior of the) entire subset "rhs" from this subset.
	 */
	public void remove(IntervalSubset rhs){
		rhs.traverseSubIntervals(new IntervalTraversalMethod(){
			@Override public void processFullInterval(){ removeAll(); }
			@Override public void processLeftAlignedSubInterval(float b){ removeLeftAlignedInterval(b); }
			@Override public void processRightAlignedSubInterval(float a){ removeRightAlignedInterval(a); }
			@Override public void processInteriorSubInterval(float a, float b){ removeInterval(a,b); }
		});
	}
	
	
	//TO STRING
	//-----------------------------------------------
	@Override
	public String toString(){
		
		class ITMOut implements IntervalTraversalMethod{
			String outString;
			public ITMOut(){ outString = ""; }
			
			@Override public String toString(){ return outString; }
			
			@Override public void processFullInterval(){ outString += "[0*,1*]"; }
			@Override public void processInteriorSubInterval(float a, float b){ outString += "[" + a + "," + b + "]"; }
			@Override public void processLeftAlignedSubInterval(float b) { outString += "[0*," + b + "]"; }
			@Override public void processRightAlignedSubInterval(float a){ outString += "[" + a + ",1*]"; }
		}
		
		ITMOut temp = new ITMOut();
		traverseSubIntervals(temp);
		return temp.toString();
	}
}
