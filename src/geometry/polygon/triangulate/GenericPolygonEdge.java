package geometry.polygon.triangulate;

import geometry.polygon.components.PolygonEdge;
import geometry.polygon.components.PolygonVertex;

/**
 * Used as a {@link PolygonEdge} in {@link EdgeLoopNode#getGenericPolygonComponents()}
 */
class GenericPolygonEdge
	<Vertex extends PolygonVertex<Vertex,?>>

	extends PolygonEdge<GenericPolygonVertex<Vertex>, GenericPolygonEdge<Vertex>>
{

	public GenericPolygonEdge(GenericPolygonVertex<Vertex> p, GenericPolygonVertex<Vertex> q) {
		super(p, q);
	}

	@Override
	protected GenericPolygonEdge<Vertex> getThisEdge(){ return this; }
}
