package geometry.polygon.triangulate;

import geometry.common.components.FixedDimensionalVertex;
import geometry.math.FixedDimensionalVector;

/**
 * Represents an oriented triangle with vertices specified by 
 * instances of any class derived from {@link FixedDimensionalVertex}.
 */
public class FixedDimensionalTriangle 
	<Vector extends FixedDimensionalVector<Vector>,
	 Vertex extends FixedDimensionalVertex<Vector>>	
{
	
	private Vertex p,q,r;
	
	/**
	 * Create a triangle with vertices, in counterclockwise order, "p", "q", "r".
	 * None of "p", "q", or "r" is duplicated.
	 * 
	 * @param p Triangle vertex 0.
	 * @param q Triangle vertex 1.
	 * @param r Triangle vertex 2.
	 */
	public FixedDimensionalTriangle(Vertex p, Vertex q, Vertex r){
		this.p = p;
		this.q = q;
		this.r = r;
	}
	
	/**
	 * Return the first vertex.
	 */
	public Vertex getVertex0(){ return p; }
	
	/**
	 * Return the second vertex.
	 */
	public Vertex getVertex1(){ return q; }
	
	/**
	 * Return the third vertex.
	 */
	public Vertex getVertex2(){ return r; }
	
	/**
	 * Return the ith vertex of the triangle.
	 * 
	 * @param i Any non-negative integer.
	 * @return Returns vertex i mod 3.
	 */
	public Vertex getVertex(int i){
		switch(i%3){
		case 0: return p;
		case 1: return q;
		case 2: return r;
		}
		return null;
	}
	
	/**
	 * Set the first vertex.  Does not duplicate "p".
	 * 
	 * @param p The new first vertex.
	 */
	public void setVertex0(Vertex p){ this.p = p; }
	
	/**
	 * Set the second vertex.  Does not duplicate "q".
	 * 
	 * @param q The new second vertex.
	 */
	public void setVertex1(Vertex q){ this.q = q; }
	
	/**
	 * Set the third vertex.  Does not duplicate "r".
	 * 
	 * @param r The new third vertex.
	 */
	public void setVertex2(Vertex r){ this.r = r; }
	
	/**
	 * Get the edge vector from vertex i to vertex j.
	 * 
	 * @see FixedDimensionalTriangle.getVertex 
	 */
	public Vector getEdgeVector(int i, int j){
		Vector ret = getVertex(j).getPosition().copy();
		ret.subtract(getVertex(i).getPosition());
		return ret;
	}
	
	/**
	 * Flip the order of vertices, so that a counterclockwise
	 * triangle becomes clockwise, and vice versa.
	 */
	public void flip(){
		Vertex temp = p;
		p = r; r = temp; 
	}
	
	/**
	 * Get the signed area of this triangle.
	 */
	public float computeArea(){
		Vector v = q.getPosition().copy(); v.subtract(p.getPosition());
		Vector w = r.getPosition().copy(); w.subtract(p.getPosition());
		float vdotw = v.dot(w);
		return 0.5f * (float)Math.sqrt(v.dot(v)*w.dot(w) - vdotw*vdotw);
	}
	
	/**
	 * Get the centroid of this triangle.
	 */
	public Vector computeCentroid(){
		Vector ret = p.getPosition().copy();
		ret.add(q.getPosition()); 
		ret.add(r.getPosition());
		ret.scale(1.0f/3.0f);
		return ret;
	}
}
