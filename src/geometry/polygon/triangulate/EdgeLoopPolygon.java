package geometry.polygon.triangulate;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import geometry.common.Epsilon;
import geometry.common.MessageOutput;
import geometry.common.OrderedPair;
import geometry.common.components.FixedDimensionalEdge;
import geometry.common.components.Vertex2d;
import geometry.math.Vector2d;
import geometry.polygon.components.PolygonVertex;
import geometry.polygon.triangulate.EdgeLoopNode.EdgeLoopNodeEnumeration;
import geometry.polygon.triangulate.EdgeLoopTools.RayIntersectionData;

/**
 * A polygon specified by:
 * <ul>
 * <li>A "positive" edge loop specifying the outer boundary of the polygon,
 * <li>Multiple "negative" edge loops specifying holes in the polygon.
 * </ul>
 * Create from a Polygon via {@link EdgeLoopTools#computeEdgeLoopDecomposition(Collection)}.
 */
public class EdgeLoopPolygon
	<Vertex extends PolygonVertex<Vertex, ?>>
{
	
	private EdgeLoopNode<Vertex> positiveHead;
	private LinkedList<EdgeLoopNode<Vertex>> negativeHeads;
	
	
	/**
	 * To create EdgeLoopPolygons from a Polygon, use 
	 * {@link EdgeLoopTools#computeEdgeLoopDecomposition(Collection)}
	 */
	public EdgeLoopPolygon(EdgeLoopNode<Vertex> positiveHead){
		this.positiveHead = positiveHead;
		negativeHeads = new LinkedList<EdgeLoopNode<Vertex>>();
	}
	
	protected void addNegativeHead(EdgeLoopNode<Vertex> head){
		negativeHeads.add(head);
	}
	
	protected EdgeLoopNode<Vertex> getPositiveHead(){
		return positiveHead;
	}
	
	protected List<EdgeLoopNode<Vertex>> getNegativeHeads(){
		return negativeHeads;
	}
	
	
	//============================================
	// TRIANGULATION
	//============================================
	
	/**
	 * Successively join each negative edge loop to the positive edge loop, 
	 * then split the positive edge loop by diagonals until only triangles are left.
	 * The triangles are added to the specified list.
	 * 
	 * @return Returns false if there was an error.
	 */
	public boolean computeTriangulation(List<Triangle2d<Vertex>> triList){
		
		//find edges connecting negative edge loops to each other and positive loop
		joinEdgeLoops();
		
		//make a list of edge loops. while there are still loops in the list, pop the 
		//first one off; if it is a triangle, add to trilist, if not, find a diagonal
		//and append heads for both new loops to the list.
		LinkedList<EdgeLoopNode<Vertex>> headList = new LinkedList<EdgeLoopNode<Vertex>>();
		headList.add(positiveHead);
		
		while(!headList.isEmpty()){
			//get a pending loop head
			EdgeLoopNode<Vertex> head = headList.removeFirst();
			
			//check if this is a triangle
			Triangle2d<Vertex> tri = head.constructTriangle();
			if(tri != null){
				triList.add(tri);
				continue;
			}
			
			//find a diagonal
			OrderedPair<EdgeLoopNode<Vertex>> diagonal = findDiagonal(head);
			if(diagonal == null){
				MessageOutput.printError("Something went wrong in findDiagonal");
				return false;
			}
			
			//cut along the diagonal, pushing the new loops on headList
			EdgeLoopNode<Vertex> newHead = EdgeLoopTools.<Vertex>joinNodes(diagonal.getA(), diagonal.getB());
			headList.addLast(head);
			headList.addLast(newHead);
		}
		
		return true;
	}
	
	/**
	 * Return a pair of nodes from the edge loop containing the node "head"
	 * such that the line segment joining the nodes is contained in the interior
	 * of the region bounded by the edge loop.
	 * 
	 * @param head A node of an edge loop.
	 * @return A pair of nodes constituting a diagonal.
	 */
	private OrderedPair<EdgeLoopNode<Vertex>> findDiagonal(EdgeLoopNode<Vertex> head){
	
		//iterate through edge loop to find an ear:
		//i.e., find a convex node p, with adjacent nodes q and r.
		//an ear satisfies the property that no other vertex is in the interior of 
		//the triangle pqr, nor on the edge qr.
		
		earfindloop:
		for(EdgeLoopNodeEnumeration<Vertex> i = head.getEnumeration(); i.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = i.nextElement();
			
			//ensure this is a convex vertex
			Vector2d vIn = node.getIncomingEdgeVector(); vIn.normalize();
			Vector2d vOut = node.getOutgoingEdgeVector(); vOut.normalize();
			if(vIn.cross(vOut) < 0){ continue; }
			
			//make triangle and edge
			EdgeLoopNode<Vertex> r = node.getPreviousNode();
			EdgeLoopNode<Vertex> q = node.getNextNode();
			Triangle2d<Vertex> triEar = new Triangle2d<Vertex>(node.getVertex(), q.getVertex(), r.getVertex());
			FixedDimensionalEdge<Vector2d,Vertex> edgeOpp = new FixedDimensionalEdge<Vector2d,Vertex>(q.getVertex(), r.getVertex());
			
			//make sure no vertex is inside the triangle or on the edge
			for(EdgeLoopNodeEnumeration<Vertex> j = head.getEnumeration(); j.hasMoreElements(); ){
				EdgeLoopNode<Vertex> nodej = j.nextElement();
				
				//do not check vertices of triangle
				if((nodej == node) || (nodej == q) || (nodej == r)){ continue; }
				
				Vector2d x = nodej.getVertex().getPosition();
				float t = edgeOpp.computePointIntersection(x);
				if((t > 0) && (t < 1)){ 
					if(edgeOpp.lerp(t).distance(x) < Epsilon.get()){ continue earfindloop; }
				}
				if(triEar.containsPoint(x)){ continue earfindloop; }
			}
			
			//if not, found an ear
			return new OrderedPair<EdgeLoopNode<Vertex>>(q,r);
		}
		
		//could not find an ear
		return null;
	}
	
	/**
	 * Continually apply joinSingleNegativeEdgeLoop until no more negative edge loops are left.
	 */
	private void joinEdgeLoops(){
		
		while(!negativeHeads.isEmpty()){
			if(!joinSingleNegativeEdgeLoop()){
				MessageOutput.printError("Something went wrong in joinSingleNegativeEdgeLoop");
				break;
			}
		}
	}
	
	/**
	 * Find an edge contained on the interior of the polygon which connects 
	 * a node on a negative edge loop with a node on the positive edge loop.
	 * Joins the nodes and removes the negative edge loop from the list. 
	 * 
	 * @return Returns false if no such edge could be found.
	 */
	private boolean joinSingleNegativeEdgeLoop(){
		
		EdgeLoopNode<Vertex> maxNode = findNegativeNodeWithMaximumXValue();
		EdgeLoopNode<Vertex> maxHead = maxNode.getHead();
		Vector2d maxNodePos = maxNode.getVertex().getPosition().copy();
		
		EdgeLoopNode<Vertex> joinNode = null;
		
		//intersect the ray from maxNode.vertex in direction (1,0) with positive edge loop
		Vector2d ray = new Vector2d(1,0);
		RayIntersectionData<Vertex> rayIntersection = EdgeLoopTools.<Vertex>getRayIntersection(
			positiveHead, maxNodePos, ray);
		
		//if it intersected in a vertex, join there
		if(rayIntersection.closestNode != null){
			joinNode = rayIntersection.closestNode; 
		}
		
		//if it intersected an edge:
		else if(rayIntersection.closestEdgeInitialNode != null){
			
			//make a triangle (maxNode.vertex -> edge initial node -> edge intersection)
			Vector2d edgeIntersection = maxNodePos.copy(); edgeIntersection.add(rayIntersection.v);
			Triangle2d<Vertex2d> tri = new Triangle2d<Vertex2d>(
				maxNode.getVertex(), rayIntersection.closestEdgeInitialNode.getVertex(), new Vertex2d(edgeIntersection));
			
			//find the positive node which is inside the triangle, such that the vector
			//maxNodePos->node.pos forms the smallest angle with "ray", and among those with
			//the common smallest angle, the one that is closest to maxNodePos.
			EdgeLoopNode<Vertex> bestNode = null;
			Vector2d bestUnitVector = null; float dMin = 0;
			boolean hadFirst = false;
			
			for(EdgeLoopNodeEnumeration<Vertex> e = positiveHead.getEnumeration(); e.hasMoreElements(); ){
				EdgeLoopNode<Vertex> node = e.nextElement();
				
				//check if vertex is in the triangle
				if(!tri.containsPoint(node.getVertex().getPosition())){ continue; }
				
				//compare vector from maxNodePos to node.pos
				Vector2d u = node.getVertex().getPosition().copy(); u.subtract(maxNodePos);
				
				boolean isBetter = false;
				if(bestUnitVector != null){
					//check if node.pos is on the edge maxNodePos->bestNode.pos
					float dot = bestUnitVector.dot(u);
					Vector2d x = bestUnitVector.copy(); x.scale(dot);
					//if so, check if length is less than dMin
					if(x.distance(u) < Epsilon.get()){ isBetter = (dot < dMin); }
					//if not, check if u forms smaller angle with "ray"
					else{ isBetter = (bestUnitVector.cross(u) > 0); }
				}
				
				if(!hadFirst || isBetter){
					bestNode = node;
					dMin = u.length();
					bestUnitVector = u; bestUnitVector.scale(1/dMin); 
					hadFirst = true;
				}
			}
			
			//if there are no pos verts in the triangle, join with closestEdgeInitialNode
			joinNode = (bestNode == null) ? rayIntersection.closestEdgeInitialNode : bestNode;
		}
		
		//otherwise, something went wrong
		else{ return false; }
		
		//now join maxNode and joinNode
		EdgeLoopTools.<Vertex>joinNodes(joinNode, maxNode);
		negativeHeads.remove(maxHead);
		return true;
	}
	
	/**
	 * Find node with maximum x-coordinate among all nodes in negative edge loops.
	 * 
	 * @return Negative node with maximum x-coordinate.
	 */
	private EdgeLoopNode<Vertex> findNegativeNodeWithMaximumXValue(){
		
		//find vertex among negative loops with largest x-coordinate
		EdgeLoopNode<Vertex> maxNode = null;
		float maxX = 0;
		boolean hadFirst = false;
		
		for(EdgeLoopNode<Vertex> head : negativeHeads){
			
			//iterate through vertices in this edge loop
			for(EdgeLoopNodeEnumeration<Vertex> e = head.getEnumeration(); e.hasMoreElements(); ){
				EdgeLoopNode<Vertex> node = e.nextElement();
				//see if node.vertex has max x value
				float x = node.getVertex().getPosition().getX();
				if(!hadFirst || (x > maxX)){
					maxNode = node;
					maxX = x;
					hadFirst = true;
				}
			}
		}
		
		return maxNode;
	}	
}
