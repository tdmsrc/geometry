package geometry.polygon.triangulate;

import geometry.common.components.FixedDimensionalVertex;
import geometry.common.components.Vertex2d;
import geometry.math.Vector2d;
import geometry.math.Vector3d;

/**
 * 2d-specific version of {@link FixedDimensionalTriangle}
 */
public class Triangle2d 
	<Vertex extends Vertex2d>
	extends FixedDimensionalTriangle<Vector2d, Vertex>
{

	/**
	 * Create a {@link Triangle2d} with the specified vertices.
	 * 
	 * @see {@link FixedDimensionalTriangle#FixedDimensionalTriangle(FixedDimensionalVertex, FixedDimensionalVertex, FixedDimensionalVertex)}
	 */
	public Triangle2d(Vertex p, Vertex q, Vertex r) {
		super(p, q, r);
	}
	
	/**
	 * Check if the point p is contained in the interior of this triangle.
	 * 
	 * Assumes that p is not on any vertex or edge of the triangle.
	 * 
	 * @param p A point.
	 * @return True if the point is in the triangle, false otherwise.
	 */
	public boolean containsPoint(Vector2d p){
		
		Vector3d b = computeBarycentricFromPosition(p);
		return
			((b.getX() > 0) && (b.getX() < 1) &&
			 (b.getY() > 0) && (b.getY() < 1) &&
			 (b.getZ() > 0) && (b.getZ() < 1));
	}
	
	/**
	 * Returns the barycentric coordinates representing the specified 2d position.
	 * 
	 * @param p The 2d position of a point.
	 * @return The Vector3d of barycentric coordinates representing p.
	 */
	public Vector3d computeBarycentricFromPosition(Vector2d p){
		
		Vector2d v = getVertex1().getPosition().copy(); v.subtract(getVertex0().getPosition());
		Vector2d w = getVertex2().getPosition().copy(); w.subtract(getVertex0().getPosition());
		Vector2d x = p.copy(); x.subtract(getVertex0().getPosition());
		
		float d = v.cross(w);
		float b1 = (x.cross(w))/d;
		float b2 = (v.cross(x))/d;
		
		return new Vector3d(1-b1-b2, b1, b2);
	}
	
	/**
	 * Returns the 2d point represented by the specified barycentric coordinates.
	 * 
	 * @param b The Vector3d of barycentric coordinates.
	 * @return The 2d position represented by b.
	 */
	public Vector2d computePositionFromBarycentric(Vector3d b){
		
		Vector2d temp;
		Vector2d p = new Vector2d(0,0);
		
		temp = getVertex0().getPosition().copy(); temp.scale(b.getX()); p.add(temp);
		temp = getVertex1().getPosition().copy(); temp.scale(b.getY()); p.add(temp);
		temp = getVertex2().getPosition().copy(); temp.scale(b.getZ()); p.add(temp);
		
		return p;
	}
	
	@Override
	public float computeArea(){
		
		Vector2d v = getVertex1().getPosition().copy(); v.subtract(getVertex0().getPosition());
		Vector2d w = getVertex2().getPosition().copy(); w.subtract(getVertex0().getPosition());
		
		return 0.5f * v.cross(w);
	}
}
