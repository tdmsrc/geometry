package geometry.polygon.triangulate;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.NoSuchElementException;

import geometry.math.Vector2d;
import geometry.polygon.components.PolygonTools;
import geometry.polygon.components.PolygonVertex;


/**
 * A node in a linked loop of vertices.
 * Use {@link #getEnumeration()} to loop through each node in an edge loop.
 */
public class EdgeLoopNode
	<Vertex extends PolygonVertex<Vertex, ?>> 
{
	
	private Vertex vertex;
	
	private EdgeLoopNode<Vertex> head;
	private EdgeLoopNode<Vertex> nextNode;
	private EdgeLoopNode<Vertex> previousNode;
	
	//================================================================
	//Enumeration class for iterating over edge loop vertices; see EdgeLoopNode.getEnumeration
	//----------------------------------------------------------------
	//to loop through all the vertices in an edge loop, starting at "head", do:
	//for(EdgeLoopNodeEnumeration<Vertex> e = head.getEnumeration(); e.hasMoreElements(); ){
	//	EdgeLoopNode<Vertex> node = e.nextElement();
	//}
	//----------------------------------------------------------------
	public static class EdgeLoopNodeEnumeration
		<Vertex extends PolygonVertex<Vertex, ?>>
		implements Enumeration<EdgeLoopNode<Vertex>>
	{
		
		private boolean hasMoreElements;
		private EdgeLoopNode<Vertex> initialNode;
		private EdgeLoopNode<Vertex> currentNode;
			
		
		public EdgeLoopNodeEnumeration(EdgeLoopNode<Vertex> initialNode){
			this.initialNode = initialNode;
			this.currentNode = initialNode;
			hasMoreElements = true;
		}
		
		@Override
		public boolean hasMoreElements(){
			return hasMoreElements;
		}

		@Override
		public EdgeLoopNode<Vertex> nextElement(){
			if(!hasMoreElements){ throw new NoSuchElementException(); }
			
			EdgeLoopNode<Vertex> returnNode = currentNode;
			currentNode = currentNode.getNextNode();
			if(currentNode == initialNode){ hasMoreElements = false; }
			return returnNode;
		}
	}
	//================================================================
	
	public EdgeLoopNode(Vertex vertex){
		this.vertex = vertex;
		head = this;
		nextNode = null;
		previousNode = null;
	}
	
	public Vertex getVertex(){
		return vertex;
	}
	
	public EdgeLoopNode<Vertex> getHead(){
		return head;
	}
	
	public EdgeLoopNode<Vertex> getNextNode(){
		return nextNode;
	}
	
	public EdgeLoopNode<Vertex> getPreviousNode(){
		return previousNode;
	}
	
	protected void setHead(EdgeLoopNode<Vertex> head){
		this.head = head;
	}
	
	protected void setNextNode(EdgeLoopNode<Vertex> nextNode){
		this.nextNode = nextNode;
	}
	
	protected void setPreviousNode(EdgeLoopNode<Vertex> previousNode){
		this.previousNode = previousNode;
	}
	
	/**
	 * Get {@link Enumeration} which starts at this node.
	 */
	public EdgeLoopNodeEnumeration<Vertex> getEnumeration(){
		return new EdgeLoopNodeEnumeration<Vertex>(this);
	}
	
	/**
	 * Get the vector representing the edge (this vertex)->(next vertex).
	 * 
	 * @return The Vector2d representing the edge. 
	 */
	public Vector2d getOutgoingEdgeVector(){
		
		Vector2d ret = nextNode.vertex.getPosition().copy();
		ret.subtract(vertex.getPosition());
		return ret;
	}
	
	/**
	 * Get the vector representing the edge (previous vertex)->(this vertex).
	 * 
	 * @return The Vector2d representing the edge. 
	 */
	public Vector2d getIncomingEdgeVector(){
		
		return previousNode.getOutgoingEdgeVector();
	}
	
	/**
	 * Returns the midpoint of the edge (this vertex)->(next vertex).
	 * 
	 * @return A Vector2d with the midpoint.
	 */
	public Vector2d getNextEdgeMidpoint(){
		
		Vector2d mid = vertex.getPosition().copy();
		mid.add(nextNode.vertex.getPosition());
		mid.scale(0.5f);
		return mid;
	}
	
	/**
	 * Returns a Triangle2d object that represents the edge loop
	 * to which this node belongs.  If the edge loop does not form
	 * a triangle, this method returns null.
	 * 
	 * @return A Triangle2d object, or null if the loop is not a triangle.
	 */
	public Triangle2d<Vertex> constructTriangle(){
		
		EdgeLoopNode<Vertex> it = this;
		Vertex p = it.getVertex(); it = it.getNextNode();
		Vertex q = it.getVertex(); it = it.getNextNode();
		Vertex r = it.getVertex(); it = it.getNextNode();
		if(it != this){ return null; }
		
		return new Triangle2d<Vertex>(p, q, r);
	}
	
	/**
	 * Determines the signed area of the polygon formed by the edge loop
	 * to which this node belongs.
	 * 
	 * A counterclockwise edge loop will have positive signed area; a
	 * clockwise edge loop will have negative signed area.
	 * 
	 * @return The signed area of the polygon.
	 * @see PolygonTools.computeSignedArea
	 */
	public float computeSignedArea(){
		
		float val = 0;
		
		for(EdgeLoopNodeEnumeration<Vertex> e = getEnumeration(); e.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = e.nextElement();
			
			Vector2d p = node.getVertex().getPosition();
			Vector2d q = node.getNextNode().getVertex().getPosition();
			val += p.cross(q);
		}
		
		return val/2.0f;
	}

	
	//============================================
	// POLYGON COMPONENTS FROM EDGE LOOP
	//============================================
	
	//return value for getGenericPolygonComponents
	/**
	 * Generic Polygon edge and vertex components making up an edge loop.  
	 */
	protected static class GenericPolygonComponents
		<Vertex extends PolygonVertex<Vertex, ?>>
	{
		public HashSet<GenericPolygonVertex<Vertex>> vertices;
		public HashSet<GenericPolygonEdge<Vertex>> edges;
		
		public GenericPolygonComponents(){
			vertices = new HashSet<GenericPolygonVertex<Vertex>>();
			edges = new HashSet<GenericPolygonEdge<Vertex>>();
		}
	}
	
	/**
	 * Return, via a {@link GenericPolygonComponents} object, a collection
	 * of polygon vertices and edges that make up this edge loop.
	 * <br><br>
	 * Note that if the edge loop originated from Polygon components, new
	 * components must be created via this method, since the combinatorics 
	 * may differ between the edge loop and original Polygon.
	 * 
	 * @return A {@link GenericPolygonComponents} object.
	 */
	protected GenericPolygonComponents<Vertex> getGenericPolygonComponents(){
		
		GenericPolygonComponents<Vertex> gpc = new GenericPolygonComponents<Vertex>();
		
		//get components
		boolean hadFirst = false;
		GenericPolygonVertex<Vertex> previousVertex = null, firstVertex = null;
		
		for(EdgeLoopNodeEnumeration<Vertex> e = getEnumeration(); e.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = e.nextElement();
			
			GenericPolygonVertex<Vertex> currentVertex = new GenericPolygonVertex<Vertex>(node.getVertex());
			gpc.vertices.add(currentVertex);
			if(previousVertex != null){
				gpc.edges.add(new GenericPolygonEdge<Vertex>(previousVertex, currentVertex));
			}
			
			if(!hadFirst){
				firstVertex = currentVertex;
				hadFirst = true;
			}
			previousVertex = currentVertex;
		}
		gpc.edges.add(new GenericPolygonEdge<Vertex>(previousVertex, firstVertex));
		
		//return data
		return gpc;
	}
}
