package geometry.polygon.triangulate;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import geometry.common.Epsilon;
import geometry.common.MessageOutput;
import geometry.common.components.FixedDimensionalEdge;
import geometry.common.components.Vertex2d;
import geometry.common.components.FixedDimensionalEdge.EdgeEdgeIntersection;
import geometry.math.Vector2d;
import geometry.polygon.components.PolygonEdge;
import geometry.polygon.components.PolygonTools;
import geometry.polygon.components.PolygonVertex;
import geometry.polygon.triangulate.EdgeLoopNode.EdgeLoopNodeEnumeration;
import geometry.polygon.triangulate.EdgeLoopNode.GenericPolygonComponents;

public class EdgeLoopTools{


	//============================================
	// EDGE LOOP DECOMPOSITION OF POLYGON
	//============================================
	
	/**
	 * Sorts the specified polygon vertices into individual edge loops, and associates 
	 * each negatively-oriented edge loop to the positively-oriented edge loop in which
	 * it is contained.
	 * <br><br>
	 * Assumes that:
	 * <ul>
	 * <li>An edge loop decomposition exists,
	 * <li>Edge loops only self-intersect or intersect one another at a set of vertices,
	 * <li>Negative edge loops are always, and positive edge loops are never, contained 
	 * in the (closed) interior of a region bounded by a positive loop.
	 * </ul>
	 * Loops follow the "sharpest left turn", and will satisfy the following:
	 * <ul>
	 * <li>A negative edge loop will never touch its containing positive edge loop,
	 * <li>A positive edge loop may touch itself at a vertex, and so may a negative edge loop,
	 * <li>If self-touching vertices of a positive edge loop are removed, the interior of 
	 * that region is still connected.
	 * </ul>
	 */
	public static 
		<Vertex extends PolygonVertex<Vertex,Edge>,
		 Edge extends PolygonEdge<Vertex,Edge>>
	
	List<EdgeLoopPolygon<Vertex>> computeEdgeLoopDecomposition(Collection<Vertex> vertices){
		
		//copy set of vertices
		HashSet<Vertex> pendingVertices = new HashSet<Vertex>(vertices);
		
		//prepare list of edge loops
		LinkedList<EdgeLoopPolygon<Vertex>> edgeLoopPolygons = new LinkedList<EdgeLoopPolygon<Vertex>>();
		LinkedList<EdgeLoopNode<Vertex>> negativeHeads = new LinkedList<EdgeLoopNode<Vertex>>();
		
		//populate lists of edge loops
		while(!pendingVertices.isEmpty()){
			
			//get any pending vertex
			Vertex vertex = pendingVertices.iterator().next();
			pendingVertices.remove(vertex);
			
			//build an edge loop with head node at "vertex"
			int n = 0; //number of nodes in the edge loop
			
			EdgeLoopNode<Vertex> head = new EdgeLoopNode<Vertex>(vertex); n++;
			EdgeLoopNode<Vertex> previousNode = null;
			EdgeLoopNode<Vertex> currentNode = head;
			
			//traverse edges with polygon interior to left
			PolygonEdge<Vertex, ?> edge = vertex.getOutwardPointingEdges().iterator().next();
			for(Vertex nextVertex = edge.getFinalVertex(); 
				nextVertex != head.getVertex();
				nextVertex = EdgeLoopTools.<Vertex,Edge>computeNextEdgeLoopVertex(currentNode.getVertex(), previousNode.getVertex())){
				
				//remove next vertex from pending vertices, and check size of edge loop
				pendingVertices.remove(nextVertex);
				if(n > vertices.size()){ 
					MessageOutput.printError("Infinite loop in computeEdgeLoopDecomposition");
					return null;
				}
				
				//shift forward
				previousNode = currentNode;
				currentNode = new EdgeLoopNode<Vertex>(nextVertex); n++;
				
				//update neighboring node and head references
				currentNode.setHead(head);
				currentNode.setPreviousNode(previousNode);
				previousNode.setNextNode(currentNode);
			}
			
			//close the loop
			head.setPreviousNode(currentNode);
			currentNode.setNextNode(head);

			//ignore if size < 3
			if(n < 3){
				MessageOutput.printWarning("Found edge loop consisting of fewer than 3 edges");
				continue;
			}
			
			//add to appropriate list of edge loops
			if(head.computeSignedArea() > 0){
				edgeLoopPolygons.add(new EdgeLoopPolygon<Vertex>(head));
			}
			else{ negativeHeads.add(head); }
		}
		
		//organize edge loops into EdgeLoopComponents
		//[TODO] this can fail if: neg loop inside pos loop inside neg loop inside pos loop.
		//[that this isn't the case is specified in the assumptions for this method;
		// this is not a problem if e.g. this is applied to a connected polygon]
		for(EdgeLoopPolygon<Vertex> polygon : edgeLoopPolygons){
			
			//get generic polygon components making up the positive edge loop
			GenericPolygonComponents<Vertex> posLoopGPC = polygon.getPositiveHead().getGenericPolygonComponents();
			
			//figure out which negative edge loops belong to this
			Iterator<EdgeLoopNode<Vertex>> it = negativeHeads.iterator();
			while(it.hasNext()){
				EdgeLoopNode<Vertex> negativeHead = it.next();
				
				//find any edge-interior point on negative edge loop
				Vector2d p = negativeHead.getNextEdgeMidpoint();
				
				//if it is contained in the positive loop, add to that polygon and remove from list
				if(!PolygonTools.containsPoint(posLoopGPC.vertices, posLoopGPC.edges, p)){ continue; }
				polygon.addNegativeHead(negativeHead);
				it.remove();
			}
		}
		
		return edgeLoopPolygons;
	}
	
	/**
	 * After having moved from previousVertex to currentVertex, find the next vertex
	 * to move to in order to continue traveling along polygon edges with interior
	 * to the left, and making the sharpest possible left-turns along the way.
	 */
	private static 
		<Vertex extends PolygonVertex<Vertex,Edge>,
		 Edge extends PolygonEdge<Vertex,Edge>>
	
	Vertex computeNextEdgeLoopVertex(Vertex currentVertex, Vertex previousVertex){
		
		//get normalized incoming vector
		Vector2d vn = currentVertex.getPosition().copy();
		vn.subtract(previousVertex.getPosition());
		vn.normalize();
		
		//next edge is outgoing edge with sharpest left turn/minimum interior angle
		//this is computed without computing theta, but rather a value monotonic in theta
		Vertex nextVertex = null;
		float minMonoTheta = 0;
		boolean foundOption = false;
		
		for(PolygonEdge<Vertex,?> edge : currentVertex.getOutwardPointingEdges()){
			
			//compute monoTheta value
			Vector2d wn = edge.getEdgeVector(); wn.normalize();
			float monoTheta = EdgeLoopTools.computeMonoTheta(vn, wn);
			
			//update max
			if(!foundOption || (monoTheta < minMonoTheta)){
				nextVertex = edge.getFinalVertex();
				minMonoTheta = monoTheta;
				foundOption = true;
			}
		}
		
		return nextVertex;
	}

	/**
	 * Returns a value that is monotonic in theta, where theta is the interior angle 
	 * at the vertex v, where the head of "incomingVector" is placed at v, the tail
	 * of "outgoingVector" is placed at v, and the region to the left of the vectors
	 * is regarded as the interior.
	 * 
	 * The value varies from -2 to 2, with monoTheta = -1, 0, 1 corresponding to 
	 * theta = pi/2, pi, 3pi/2, respectively (i.e., negative values are convex).
	 * 
	 * @param incomingVector Normalized incoming vector.
	 * @param outgoingVector Normalized outgoing vector.
	 * @return The value monotonic in theta.
	 */
	private static float computeMonoTheta(Vector2d incomingVector, Vector2d outgoingVector){
		
		float monoTheta = incomingVector.dot(outgoingVector) - 1;
		if(incomingVector.cross(outgoingVector) < 0){ monoTheta = -monoTheta; }
		return monoTheta;
	}
	
	
	//============================================
	// CUT AND JOIN
	//============================================
	
	/**
	 * Join nodeA and nodeB.  
	 * 
	 * If they are in the same edge loop, then this edge loop will be split 
	 * into two.  The head of one loop will be the original common head; the 
	 * head of the other loop will be returned by this method.
	 * 
	 * If they are in different edge loops, then the head references of the 
	 * vertices in the original nodeB loop will be replaced by the head of 
	 * the original nodeA loop.  In this case, the method returns null.
	 * 
	 * @param nodeA An edge loop node.
	 * @param nodeB Another edge loop node.
	 * @return The new edge loop head if nodeA and nodeB are in the same
	 * edge loop; null if they are in different edge loops.
	 */
	public static <Vertex extends PolygonVertex<Vertex,?>>
	EdgeLoopNode<Vertex> joinNodes(EdgeLoopNode<Vertex> nodeA, EdgeLoopNode<Vertex> nodeB){
		
		EdgeLoopNode<Vertex> headA = nodeA.getHead();
		EdgeLoopNode<Vertex> headB = nodeB.getHead();
		
		if(headA == headB){ return EdgeLoopTools.<Vertex>joinNodesFromSameLoop(nodeA, nodeB); } 
		EdgeLoopTools.<Vertex>joinNodesFromDifferentLoops(nodeA, nodeB);
		return null;
	}
	
	/**
	 * Join nodeA and nodeB, assuming they are in different edge loops.
	 * 
	 * The head references of the vertices in the original nodeB loop will
	 * be replaced by the head of the original nodeA loop.
	 * 
	 * @param nodeA An edge loop node.
	 * @param nodeB Another edge loop node.
	 */
	private static <Vertex extends PolygonVertex<Vertex,?>>
	void joinNodesFromDifferentLoops(EdgeLoopNode<Vertex> nodeA, EdgeLoopNode<Vertex> nodeB){
		
		EdgeLoopNode<Vertex> headA = nodeA.getHead();
		EdgeLoopNode<Vertex> headB = nodeB.getHead();
		
		//replace all headB references with headA
		for(EdgeLoopNodeEnumeration<Vertex> e = headB.getEnumeration(); e.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = e.nextElement();
			node.setHead(headA);
		}
		
		//join nodes by previous and next references
		EdgeLoopTools.<Vertex>joinNodesByReference(nodeA, nodeB);
	}
	
	/**
	 * Join nodeA and nodeB, assuming that they are in the same edge loop.
	 * 
	 * This will split the edge loop into two different edge loops. 
	 * One of the new loops will have the original common head, and the
	 * other loop will have head returned by this method.
	 * 
	 * @param nodeA An edge loop node.
	 * @param nodeB Another edge loop node.
	 * @return The head node of the new loop; the other loop has the original common head.
	 */
	private static <Vertex extends PolygonVertex<Vertex,?>>
	EdgeLoopNode<Vertex> joinNodesFromSameLoop(EdgeLoopNode<Vertex> nodeA, EdgeLoopNode<Vertex> nodeB){
		
		EdgeLoopNode<Vertex> oldHead = nodeA.getHead();
		
		//join nodes by previous and next references
		EdgeLoopTools.<Vertex>joinNodesByReference(nodeA, nodeB);
		
		//first, assume oldHead is in nodeB loop, in which case newHead is nodeA
		EdgeLoopNode<Vertex> newHead = nodeA;
		
		//see if oldHead is in fact in nodeA loop; if it is, switch newHead to nodeB 
		for(EdgeLoopNodeEnumeration<Vertex> e = nodeA.getEnumeration(); e.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = e.nextElement();
			if(node == oldHead){ newHead = nodeB; break; }
		}
		
		//set all head references in newHead loop to newHead
		for(EdgeLoopNodeEnumeration<Vertex> e = newHead.getEnumeration(); e.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = e.nextElement();
			node.setHead(newHead);
		}
		
		//return new edge loop head
		return newHead;
	}

	/**
	 * Join nodeA and nodeB in such a way that, if this were to split an edge
	 * loop into two, then nodeA would be a vertex of one, and nodeB would be
	 * a vertex of the other.
	 * 
	 * In particular, makes duplicate nodes A' and B' which have the same underlying
	 * vertices as A and B, respectively, and then joins them as:
	 * (A.prev to A' to B to B.next) and (B.prev to B' to A to A.next),
	 * and sets the head of A' to the same as B, and the head of B' to the same as A.
	 */
	private static <Vertex extends PolygonVertex<Vertex,?>> 
	void joinNodesByReference(EdgeLoopNode<Vertex> nodeA, EdgeLoopNode<Vertex> nodeB){
		
		//            same head as B 
		//           /
		// A.prev-->A' A -->A.next
		//          v  ^ 
		// B.next<--B  B'<--B.prev
		//              \
		//               same head as A
		
		EdgeLoopNode<Vertex> nodeACopy = new EdgeLoopNode<Vertex>(nodeA.getVertex());
		EdgeLoopNode<Vertex> nodeBCopy = new EdgeLoopNode<Vertex>(nodeB.getVertex());
		
		nodeACopy.setHead(nodeB.getHead());
		nodeBCopy.setHead(nodeA.getHead());
		
		nodeA.getPreviousNode().setNextNode(nodeACopy);
		nodeACopy.setPreviousNode(nodeA.getPreviousNode());
		
		nodeB.getPreviousNode().setNextNode(nodeBCopy);
		nodeBCopy.setPreviousNode(nodeB.getPreviousNode());
		
		nodeACopy.setNextNode(nodeB);
		nodeB.setPreviousNode(nodeACopy);
		
		nodeBCopy.setNextNode(nodeA);
		nodeA.setPreviousNode(nodeBCopy);
	}
	
	
	//============================================
	// RAY INTERSECTION
	//============================================
	
	//return value for getRayIntersection
	/**
	 * Contains information about the intersection of a ray and an edge loop.
	 * Exactly one of closestNode and closestEdgeInitialNode should be non-null
	 * (closestEdgeInitialNode is the initial node on the edge intersected by the ray).
	 * The vector v is the vector from the query point to the closest point.
	 * 
	 * @see {@link EdgeLoopNode#getRayIntersection(EdgeLoopNode, Vector2d, Vector2d)}
	 */
	public static class RayIntersectionData<Vertex extends PolygonVertex<Vertex,?>>{
		public EdgeLoopNode<Vertex> closestNode;
		public EdgeLoopNode<Vertex> closestEdgeInitialNode;
		public Vector2d v;
		
		public RayIntersectionData(EdgeLoopNode<Vertex> closestNode, EdgeLoopNode<Vertex> closestEdgeInitialNode, Vector2d v){
			this.closestNode = closestNode;
			this.closestEdgeInitialNode = closestEdgeInitialNode;
			this.v = v;
		}
	}
	
	/**
	 * Returns an intersection of the edge loop containing "intLoopNode" 
	 * with the ray starting at "p" with direction "ray".  Among all such
	 * intersections, the one returned is the one closest to "p".
	 *  
	 * @return A {@link RayIntersectionData} object.
	 */
	public static <Vertex extends PolygonVertex<Vertex,?>>
	RayIntersectionData<Vertex> getRayIntersection(EdgeLoopNode<Vertex> intLoopNode, Vector2d p, Vector2d ray){
		
		//normalize ray vector, and make an edge object
		ray.normalize();
		Vector2d q = p.copy(); q.add(ray);
		FixedDimensionalEdge<Vector2d,?> edgeRay = new FixedDimensionalEdge<Vector2d,Vertex2d>(new Vertex2d(p), new Vertex2d(q));
		
		//prepare closest ray intersection data
		boolean haveFirst = false;
		EdgeLoopNode<Vertex> closestNode = null;
		EdgeLoopNode<Vertex> closestEdgeInitialNode = null;
		Vector2d closestPoint = null; 
		float dmin = 0;
		
		//check vertices
		HashSet<Vertex> intersectedVertices = new HashSet<Vertex>();
		
		for(EdgeLoopNodeEnumeration<Vertex> e = intLoopNode.getEnumeration(); e.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = e.nextElement();
			
			Vector2d nodePos = node.getVertex().getPosition();
			
			//lerp value of closest point on ray to node.vertex
			float t = edgeRay.computePointIntersection(nodePos);
			if(t < 0){ continue; }
			
			//point on ray, and distance from node.vertex
			Vector2d x = edgeRay.lerp(t);
			if(x.distance(nodePos) > Epsilon.get()){ continue; }

			//add to intersected vertex list and compare distance
			intersectedVertices.add(node.getVertex());
			if(!haveFirst || (t < dmin)){
				closestNode = node; closestEdgeInitialNode = null;
				closestPoint = node.getVertex().getPosition().copy();
				dmin = t;
				haveFirst = true;
			}
		}
		
		//check edges
		for(EdgeLoopNodeEnumeration<Vertex> e = intLoopNode.getEnumeration(); e.hasMoreElements(); ){
			EdgeLoopNode<Vertex> node = e.nextElement();
	
			Vertex e0 = node.getVertex();
			Vertex e1 = node.getNextNode().getVertex();
			
			//if either endpoint was intersected by ray, continue
			if(intersectedVertices.contains(e0) || intersectedVertices.contains(e1)){ continue; }
			
			//make edge object for current edge, and intersect with edgeRay
			FixedDimensionalEdge<Vector2d,?> edge = new FixedDimensionalEdge<Vector2d,Vertex2d>(e0, e1);
			EdgeEdgeIntersection eei = edgeRay.computeEdgeIntersection(edge);
			if(	(eei == null) || (eei.lerpCallingEdge < 0) || 
				(eei.lerpArgumentEdge < 0) || (eei.lerpArgumentEdge > 1)){ continue; }
			
			//compare distance
			if(!haveFirst || (eei.lerpCallingEdge < dmin)){
				closestNode = null; closestEdgeInitialNode = node;
				closestPoint = edgeRay.lerp(eei.lerpCallingEdge);
				dmin = eei.lerpCallingEdge;
				haveFirst = true;
			}
		}

		//return RayIntersectionData
		Vector2d v = closestPoint; v.subtract(p); 
		return new RayIntersectionData<Vertex>(closestNode, closestEdgeInitialNode, v);
	}
}
