package geometry.polygon.triangulate;

import geometry.polygon.components.PolygonVertex;

/**
 * Used as a {@link PolygonVertex} in {@link EdgeLoopNode#getGenericPolygonComponents()}
 */
class GenericPolygonVertex
	<Vertex extends PolygonVertex<Vertex,?>>

	extends PolygonVertex<GenericPolygonVertex<Vertex>,GenericPolygonEdge<Vertex>>
{

	private Vertex attachedVertex;
	
	
	public GenericPolygonVertex(Vertex attachedVertex){
		super(attachedVertex.getPosition());
		
		this.attachedVertex = attachedVertex;
	}
	
	public Vertex getAttachedVertex(){
		return attachedVertex;
	}
	
	@Override
	protected GenericPolygonVertex<Vertex> getThisVertex(){ return this; }
}
